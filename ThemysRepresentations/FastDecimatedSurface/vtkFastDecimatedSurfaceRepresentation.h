#ifndef vtkFastDecimatedSurfaceRepresentation_h
#define vtkFastDecimatedSurfaceRepresentation_h

#include "vtkGeometryRepresentationWithFaces.h"
#include "vtkNew.h"
#include "vtkRemotingViewsModule.h" // for export macro
#include "vtkScalarsToColors.h"
#include "vtkSetGet.h"

/**
 * @class  vtkFastDecimatedSurfaceRepresentation
 * @brief  This representation decimates a quad mesh based on the discretization
 * of the selected scalar data array.
 *
 * This representation decimates a mesh containing quads to obtain a smaller
 * mesh for faster display. The decimation merges quads based on their
 * associated scalar cell data. The scalar data range is discretized using a
 * customizable number of colors. The decimation starts from an initial cell by
 * checking the neighbors along the four directions. A given cell is merged if
 * its scalar value belongs to the same discretized color bin and if its plane
 * and side edges align with those of the initial cell (a maximum angle can be
 * defined to tolerate slight deviations). The algorithm can also be applied
 * using a logarithmic scale. Note that the topology of the mesh may not be
 * preserved as points are removed. Non-quad polygons are simply copied.
 */

class vtkCallbackCommand;

class VTKREMOTINGVIEWS_EXPORT vtkFastDecimatedSurfaceRepresentation
    : public vtkGeometryRepresentationWithFaces
{
public:
  static vtkFastDecimatedSurfaceRepresentation* New();
  vtkTypeMacro(vtkFastDecimatedSurfaceRepresentation,
               vtkGeometryRepresentationWithFaces);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  /**
   * Set the input data array to process.
   * Overridden to execute the algorithm upon array selection.
   */
  void SetInputArrayToProcess(int idx, int port, int connection,
                              int fieldAssociation, const char* name) override;

  ///@{
  /**
   * Set/Get the number of colors used to discretize the scalar range. A
   * smaller number of colors leads to a stronger decimation.
   * Default is set to 256.
   */
  void SetNumberOfColors(int numColors);
  vtkGetMacro(NumberOfColors, int);
  ///@}

  ///@{
  /**
   * Set/Get the maximum angle (in degrees) between two cells planes and edges
   * at which merging is considered. A larger angle leads to a stronger
   * decimation. Default is set to 10.0. Values are clamped between 0 and 180
   * degrees.
   */
  void SetMaxMergeAngle(double angle);
  vtkGetMacro(MaxMergeAngle, double);
  ///@}

  /**
   * Update the lookup table and add an observer.
   */
  void SetLookupTable(vtkScalarsToColors* val) override;

protected:
  vtkFastDecimatedSurfaceRepresentation();
  virtual ~vtkFastDecimatedSurfaceRepresentation();

  /**
   * Update logarithmic scale information if the color map is modified.
   * Also forces an update of the view.
   */
  void UpdateLogScale();

  int RequestData(vtkInformation*, vtkInformationVector**,
                  vtkInformationVector*) override;

  int NumberOfColors = 256;
  double MaxMergeAngle = 10.0; // Angle in degrees
  bool LogScale = false;
  unsigned long ObserverId = 0;
  vtkScalarsToColors* LookupTable = nullptr;
  vtkNew<vtkCallbackCommand> LogObserver;

private:
  vtkFastDecimatedSurfaceRepresentation(
      const vtkFastDecimatedSurfaceRepresentation&) = delete;
  void operator=(const vtkFastDecimatedSurfaceRepresentation&) = delete;

  /**
   * Reexecute the algorithm if the logarithmic scale is changed in the color
   * map.
   */
  static void LogCallback(vtkObject* vtkNotUsed(caller),
                          unsigned long vtkNotUsed(eid), void* clientdata,
                          void* vtkNotUsed(calldata))
  {
    static_cast<vtkFastDecimatedSurfaceRepresentation*>(clientdata)
        ->UpdateLogScale();
  }
};

#endif
