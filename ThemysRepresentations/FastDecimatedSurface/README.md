# Fast Decimated Surface Representation Plugin

## Overview

This plugin adds a new representation for `vtkPolyData` composed of quads.
It performs a decimation on quad meshes based on colors (cell scalar data) rather
than on the geometry. The scalar data range is first discretized with a customizable
number of colors. The decimation starts from an initial cell by checking the
neighbors along the four directions. A given cell is merged if its scalar value
belongs to the same discretized color bin and if its plane and side edges
align with those of the initial cell (a maximum angle can be defined to tolerate
slight deviations).

Shared vertices are eliminated from the merged cell. Additionally, resulting cells
(both merged and original) are assigned the average value of the discrete scalar
bin to which they belong. The geometry of non-quad cells is simply copied.

Different data set types are supported through a conversion to `vtkPolyData`,
including multiblock, partitioned, image data, structured and unstructured
grid data sets.

## Additional details

The following options are available in the `Display` panel:

- `Number Of Colors`: Sets the number of colors for the discretization of the scalar data range.
- `Maximum Angle`: Sets the maximum angle (in degrees) tolerated for the merge.

Note that less colors and/or a larger maximum angle result in more candidates for decimation.
