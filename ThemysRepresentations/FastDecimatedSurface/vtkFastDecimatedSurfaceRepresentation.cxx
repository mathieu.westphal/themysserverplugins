#include "vtkFastDecimatedSurfaceRepresentation.h"

#include <array>
#include <cmath>    // for pow
#include <iostream> // for std::cerr et std::endl
#include <set>
#include <vector>

#include "vtkCallbackCommand.h"
#include "vtkCellArrayIterator.h"
#include "vtkCellData.h"
#include "vtkDataObject.h"
#include "vtkDataSetSurfaceFilter.h"
#include "vtkDoubleArray.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkMapper.h"
#include "vtkMath.h"
#include "vtkMergeBlocks.h"
#include "vtkObjectFactory.h"
#include "vtkPoints.h"
#include "vtkPolyData.h"
#include "vtkPolygon.h"
#include "vtkScalarsToColors.h"
#include "vtkVector.h"
#include "vtkView.h"

vtkStandardNewMacro(vtkFastDecimatedSurfaceRepresentation);

//----------------------------------------------------------------------------
vtkFastDecimatedSurfaceRepresentation::vtkFastDecimatedSurfaceRepresentation()
{
  // Setup observer for logarithmic scale
  this->LogObserver->SetCallback(
      &vtkFastDecimatedSurfaceRepresentation::LogCallback);
  this->LogObserver->SetClientData(this);
}

//----------------------------------------------------------------------------
vtkFastDecimatedSurfaceRepresentation::~vtkFastDecimatedSurfaceRepresentation()
{
  if (this->LookupTable)
  {
    this->LookupTable->RemoveAllObservers();
  }
}

//----------------------------------------------------------------------------
void vtkFastDecimatedSurfaceRepresentation::SetInputArrayToProcess(
    int idx, int port, int connection, int fieldAssociation, const char* name)
{
  this->Superclass::SetInputArrayToProcess(idx, port, connection,
                                           fieldAssociation, name);
  this->MarkModified();
}

//----------------------------------------------------------------------------
void vtkFastDecimatedSurfaceRepresentation::SetNumberOfColors(int numColors)
{
  if (this->NumberOfColors != numColors)
  {
    this->NumberOfColors = numColors;
    this->MarkModified();
  }
}

//----------------------------------------------------------------------------
void vtkFastDecimatedSurfaceRepresentation::SetMaxMergeAngle(double angle)
{
  if (this->MaxMergeAngle != angle)
  {
    // Values are clamped between 0 and 180 degrees
    if (angle < 0.0)
    {
      this->MaxMergeAngle = 0.0;
    } else if (angle > 180.0)
    {
      this->MaxMergeAngle = 180.0;
    } else
    {
      this->MaxMergeAngle = angle;
    }

    this->MarkModified();
  }
}

//----------------------------------------------------------------------------
void vtkFastDecimatedSurfaceRepresentation::SetLookupTable(
    vtkScalarsToColors* val)
{
  this->Superclass::SetLookupTable(val);

  // Update lookup table and set the observer
  if (this->LookupTable != val)
  {
    // Remove observer if there is one
    if (this->ObserverId)
    {
      this->LookupTable->RemoveObserver(this->ObserverId);
    }

    this->LookupTable = val;
    this->ObserverId = this->LookupTable->AddObserver(vtkCommand::ModifiedEvent,
                                                      this->LogObserver);
  }
}

//----------------------------------------------------------------------------
void vtkFastDecimatedSurfaceRepresentation::UpdateLogScale()
{
  if (this->Mapper && this->Mapper->GetLookupTable() && this->GetView())
  {
    if (this->LogScale != this->Mapper->GetLookupTable()->UsingLogScale())
    {
      this->LogScale = this->Mapper->GetLookupTable()->UsingLogScale();
      this->MarkModified();

      // This is necessary to ensure reexecution of the algorithm when
      // different changes to the color map are made
      this->GetView()->Update();
    }
  }
}

//----------------------------------------------------------------------------
int vtkFastDecimatedSurfaceRepresentation::RequestData(
    vtkInformation* request, vtkInformationVector** inputVector,
    vtkInformationVector* outputVector)
{
  // Retrieve input
  vtkDataObject* inputObject = vtkDataObject::GetData(inputVector[0]);

  // Check if input is empty
  if (!inputObject)
  {
    vtkErrorMacro(<< "Invalid input!");
    return 1;
  }

  // Prepare input if needed
  vtkSmartPointer<vtkPolyData> input;

  // Copy the input given that the representation is based on polygonal meshes
  if (inputObject->IsA("vtkPolyData"))
  {
    input = vtkPolyData::SafeDownCast(inputObject);
  }
  // Extract the surface if the input is an unstructured grid
  else if (inputObject->IsA("vtkUnstructuredGrid") ||
           inputObject->IsA("vtkStructuredGrid") ||
           inputObject->IsA("vtkImageData"))
  {
    vtkNew<vtkDataSetSurfaceFilter> surfaceFilter;
    surfaceFilter->SetInputData(inputObject);
    surfaceFilter->Update();
    input = surfaceFilter->GetOutput();
  }
  // Merge blocks to obtain a polygonal mesh
  else if (inputObject->IsA("vtkDataObjectTree"))
  {
    // Merge blocks without merging points, so that two cells belonging to
    // different blocks are not considered as neighbors
    vtkNew<vtkMergeBlocks> mergeFilter;
    mergeFilter->SetInputData(inputObject);
    mergeFilter->SetMergePoints(false);

    // Extract the surface of the merge output
    vtkNew<vtkDataSetSurfaceFilter> surfaceFilter;
    surfaceFilter->SetInputConnection(mergeFilter->GetOutputPort());
    surfaceFilter->Update();
    input = surfaceFilter->GetOutput();
  } else
  {
    vtkErrorMacro(<< "Unsupported input type.");
    return 1;
  }

  // Setup initial data set
  vtkIdType numInCells = input->GetNumberOfCells();
  vtkCellArray* inCells = input->GetPolys();
  vtkPoints* inPoints = input->GetPoints();
  vtkDataArray* inScalars = input->GetCellData()->GetScalars(
      this->GetInputArrayInformation(0)->Get(vtkDataObject::FIELD_NAME()));

  // Display initial input if there is no cell data
  if (!inScalars)
  {
    vtkWarningMacro(<< "No cell scalar array selected!");
    this->GeometryFilter->SetInputDataObject(0, input);
    this->MultiBlockMaker->Update();
    return 1;
  }

  // Create PolyData output for the representation
  vtkNew<vtkPolyData> output;
  vtkNew<vtkPoints> outPoints;
  vtkNew<vtkCellArray> outCells;
  vtkNew<vtkDoubleArray> outScalars;
  vtkIdType numOutPoints = 0;
  vtkIdType numOutCells = 0;

  outPoints->SetNumberOfPoints(inPoints->GetNumberOfPoints());
  outCells->AllocateEstimate(numInCells, inCells->GetMaxCellSize());
  outScalars->SetName(inScalars->GetName());
  outScalars->Allocate(numInCells);

  // Compute cosine of the maximum angle between cell planes and cell edges for
  // merging
  double maxCosAngle =
      std::cos(vtkMath::RadiansFromDegrees(this->MaxMergeAngle));

  // Retrieve scalar data range
  double* scalarRange = inScalars->GetRange();

  // If there are non positive range values, do not use log scale
  if (this->LogScale && scalarRange[0] <= 0.0)
  {
    vtkWarningMacro(<< "The scalar range contains negative values. Changing "
                       "range to remove negative values.");

    // Change values to match color map editor when log scale is activated on
    // negative values
    scalarRange[0] = 0.0001;
    scalarRange[1] = scalarRange[1] <= 0.0 ? 1.0 : scalarRange[1];
  }

  if (this->LogScale)
  {
    scalarRange[0] = std::log10(scalarRange[0]);
    scalarRange[1] = std::log10(scalarRange[1]);
  }

  // The ith element of binValues is the center value of the ith bin.
  // These discrete values become the new cell scalar values for each cell of
  // the final dataset.
  std::vector<double> binValues(this->NumberOfColors);

  for (vtkIdType binId = 0; binId < this->NumberOfColors; ++binId)
  {
    if (this->LogScale)
    {
      binValues[binId] = std::pow(
          10.0, scalarRange[0] + (2.0 * binId + 1.0) *
                                     (scalarRange[1] - scalarRange[0]) /
                                     (2.0 * this->NumberOfColors));
    } else
    {
      binValues[binId] =
          scalarRange[0] + (2.0 * binId + 1.0) *
                               (scalarRange[1] - scalarRange[0]) /
                               (2.0 * this->NumberOfColors);
    }
  }

  // Assign each cell to a bin based on the scalar data range
  std::vector<vtkIdType> cellToBin(numInCells);
  vtkIdType binId;

  for (vtkIdType cellId = 0; cellId < numInCells; ++cellId)
  {
    if (this->LogScale)
    {
      // If the cell value is negative or zero, the cell is assigned to the
      // first bin
      if (inScalars->GetTuple(cellId)[0] > 0.0)
      {
        binId = vtkMath::Floor(
            (std::log10(inScalars->GetTuple(cellId)[0]) - scalarRange[0]) /
            (scalarRange[1] - scalarRange[0]) * this->NumberOfColors);
      } else
      {
        binId = 0;
      }
    } else
    {
      binId = vtkMath::Floor((inScalars->GetTuple(cellId)[0] - scalarRange[0]) /
                             (scalarRange[1] - scalarRange[0]) *
                             this->NumberOfColors);
    }

    // Values are clamped to the scalar data range
    if (binId < 0)
    {
      cellToBin[cellId] = 0;
    } else if (binId >= this->NumberOfColors)
    {
      cellToBin[cellId] = this->NumberOfColors - 1;
    } else
    {
      cellToBin[cellId] = binId;
    }
  }

  // Compute input cells normals
  vtkNew<vtkDoubleArray> inNormals;
  vtkVector3d normal;
  const vtkIdType* cellPointIds = nullptr;
  vtkIdType numCellPts = 0;

  inNormals->SetNumberOfComponents(3);
  inNormals->Allocate(3 * numInCells);

  auto cellIter = vtk::TakeSmartPointer(inCells->NewIterator());
  for (cellIter->GoToFirstCell(); !cellIter->IsDoneWithTraversal();
       cellIter->GoToNextCell())
  {
    cellIter->GetCurrentCell(numCellPts, cellPointIds);
    vtkPolygon::ComputeNormal(inPoints, numCellPts, cellPointIds,
                              normal.GetData());
    inNormals->InsertTuple(cellIter->GetCurrentCellId(), normal.GetData());
  }

  // Prepare for cell traversal
  input->BuildLinks();
  vtkNew<vtkIdList> neiCellIds;
  vtkNew<vtkIdList> neiPointIds;
  vtkNew<vtkIdList> nonQuadCellIds;
  vtkNew<vtkIdList> startingCellPointIds;
  vtkIdType numPoints = 0;
  vtkIdType cellId = 0;
  vtkIdType neiCellId = 0;
  vtkIdType newCellId = 0;
  vtkIdType startingCellId = 0;
  vtkIdType newCellPointIds[4];
  std::array<vtkIdType, 4> startingCellIds;
  std::array<vtkIdType[2], 4> startingEdgePointIds;
  vtkVector3d cellNormal;
  vtkVector3d neiNormal;
  vtkVector3d neiFirstSideEdge;
  vtkVector3d neiSecondSideEdge;
  vtkVector3d outPtCoords;
  std::array<vtkVector3d, 4> cellEdges;
  std::array<vtkVector3d, 4> cellPtCoords;
  bool allDirectionsChecked = false;
  std::array<bool, 4> isDirectionValid;
  std::set<vtkIdType> markedCells;
  std::set<vtkIdType> candidateCells;
  std::vector<vtkIdType> outPointsIds(inPoints->GetNumberOfPoints(), -1);

  // Merged cells are always quads
  nonQuadCellIds->Allocate(4);

  // Traverse cells for merging. During traversal, eligible cells are marked and
  // merged progressively. For each cell, the search for merge candidates is
  // done in a counterclockwise manner, alternating the search direction to
  // favor the creation of new cells as square as possible.
  cellIter = vtk::TakeSmartPointer(inCells->NewIterator());
  for (cellIter->GoToFirstCell(); !cellIter->IsDoneWithTraversal();
       cellIter->GoToNextCell())
  {
    cellIter->GetCurrentCell(numPoints, cellPointIds);
    cellId = cellIter->GetCurrentCellId();

    // Ignore cell if it has already been visited or merged
    if (!markedCells.count(cellId))
    {
      if (input->GetCellType(cellId) == VTK_QUAD)
      {
        // Mark cell as visited
        markedCells.insert(cellId);

        // Store information about the initial cell
        inNormals->GetTuple(cellId, cellNormal.GetData());

        for (vtkIdType idx = 0; idx < 4; ++idx)
        {
          inPoints->GetPoint(cellPointIds[idx], cellPtCoords[idx].GetData());
        }

        for (vtkIdType dir = 0; dir < 4; ++dir)
        {
          // Initialize edge vectors of the initial cell for angle checks
          for (vtkIdType coord = 0; coord < 3; ++coord)
          {
            cellEdges[dir][coord] =
                cellPtCoords[(dir + 1) % 4][coord] - cellPtCoords[dir][coord];
          }

          cellEdges[dir].Normalize();

          // Initialize vertices for new cell definition
          newCellPointIds[dir] = cellPointIds[dir];

          // Initialize edge points and corresponding cell to start searching
          startingEdgePointIds[dir][0] = cellPointIds[dir];
          startingEdgePointIds[dir][1] = cellPointIds[(dir + 1) % 4];
          startingCellIds[dir] = cellId;

          // Reset search information
          isDirectionValid[dir] = true;
        }

        allDirectionsChecked = false;

        // Continue searching if there are still directions to explore
        while (!allDirectionsChecked)
        {
          for (vtkIdType dir = 0; dir < 4; ++dir)
          {
            // Skip if this direction has already been invalidated
            if (!isDirectionValid[dir])
            {
              continue;
            }

            // Start with the neighbor cell
            input->GetCellEdgeNeighbors(
                startingCellIds[dir], startingEdgePointIds[dir][0],
                startingEdgePointIds[dir][1], neiCellIds);

            // A cell merge is only considered when the edge is shared by
            // exactly two cells
            if (neiCellIds->GetNumberOfIds() != 1)
            {
              isDirectionValid[dir] = false;
              continue;
            }

            neiCellId = neiCellIds->GetId(0);

            // Ignore neighbor if it has already been visited or merged, if it
            // is not a quad, or if it does not belong to the same scalar bin
            if (markedCells.count(neiCellId) ||
                input->GetCellType(neiCellId) != VTK_QUAD ||
                cellToBin[cellId] != cellToBin[neiCellId])
            {
              isDirectionValid[dir] = false;
              continue;
            }

            // Define additional vectors used to verify angles between cells
            input->GetCellPoints(neiCellId, neiPointIds);

            for (vtkIdType idx = 0; idx < 4; ++idx)
            {
              inPoints->GetPoint(neiPointIds->GetId(idx),
                                 cellPtCoords[idx].GetData());
            }

            for (vtkIdType coord = 0; coord < 3; ++coord)
            {
              neiFirstSideEdge[coord] =
                  cellPtCoords[dir][coord] - cellPtCoords[(dir - 1) % 4][coord];
              neiSecondSideEdge[coord] =
                  cellPtCoords[(dir + 1) % 4][coord] - cellPtCoords[dir][coord];
            }

            neiFirstSideEdge.Normalize();
            neiSecondSideEdge.Normalize();
            inNormals->GetTuple(neiCellId, neiNormal.GetData());

            // Check whether the angles between cell planes and edges are small
            // enough
            if (cellNormal.Dot(neiNormal) < maxCosAngle ||
                cellEdges[(dir - 1) % 4].Dot(neiFirstSideEdge) < maxCosAngle ||
                cellEdges[dir].Dot(neiSecondSideEdge) < maxCosAngle)
            {
              isDirectionValid[dir] = false;
              continue;
            }

            // Mark the cell as valid candidate
            candidateCells.insert(neiCellId);
            startingCellId = neiCellId;

            // Now that the neighbor has been validated, we need to extend the
            // search along the side of the original cell to avoid having a
            // non-convex cell
            while (neiPointIds->GetId((dir + 2) % 4) !=
                   newCellPointIds[(dir + 1) % 4])
            {
              // Start with the neighbor cell
              input->GetCellEdgeNeighbors(
                  neiCellId, neiPointIds->GetId((dir + 1) % 4),
                  neiPointIds->GetId((dir + 2) % 4), neiCellIds);

              // Stop if the edge is not shared by exactly two cells
              if (neiCellIds->GetNumberOfIds() != 1)
              {
                isDirectionValid[dir] = false;
                candidateCells.clear();
                break;
              }

              neiCellId = neiCellIds->GetId(0);

              // Stop if the neighbor has already been visited or merged, if it
              // is not a quad, or if it does not belong to the same scalar bin
              if (markedCells.count(neiCellId) ||
                  candidateCells.count(neiCellId) ||
                  input->GetCellType(neiCellId) != VTK_QUAD ||
                  cellToBin[cellId] != cellToBin[neiCellId])
              {
                isDirectionValid[dir] = false;
                candidateCells.clear();
                break;
              }

              // Define additional vectors to verify angles between cells
              input->GetCellPoints(neiCellId, neiPointIds);

              for (vtkIdType idx = 0; idx < 4; ++idx)
              {
                inPoints->GetPoint(neiPointIds->GetId(idx),
                                   cellPtCoords[idx].GetData());
              }

              for (vtkIdType coord = 0; coord < 3; ++coord)
              {
                neiFirstSideEdge[coord] = cellPtCoords[(dir + 1) % 4][coord] -
                                          cellPtCoords[dir][coord];
              }

              neiFirstSideEdge.Normalize();
              inNormals->GetTuple(neiCellId, neiNormal.GetData());

              // Stop if the angles between cell planes and edges are too large
              if (cellNormal.Dot(neiNormal) < maxCosAngle ||
                  cellEdges[dir].Dot(neiFirstSideEdge) < maxCosAngle)
              {
                isDirectionValid[dir] = false;
                candidateCells.clear();
                break;
              }

              candidateCells.insert(neiCellId);
            }

            // Check that we are still good to go
            if (isDirectionValid[dir])
            {
              for (vtkIdType coord = 0; coord < 3; ++coord)
              {
                neiSecondSideEdge[coord] = cellPtCoords[(dir + 2) % 4][coord] -
                                           cellPtCoords[(dir + 1) % 4][coord];
              }

              neiSecondSideEdge.Normalize();

              // Final angle check
              if (cellEdges[(dir + 1) % 4].Dot(neiSecondSideEdge) >=
                  maxCosAngle)
              {
                // Update new cell
                inCells->GetCellAtId(startingCellId, startingCellPointIds);
                newCellPointIds[dir] = startingCellPointIds->GetId(dir);
                newCellPointIds[(dir + 1) % 4] =
                    neiPointIds->GetId((dir + 1) % 4);

                // Mark all merged cells as treated
                markedCells.insert(candidateCells.begin(),
                                   candidateCells.end());

                // Update information to start searching for the next iteration
                startingCellIds[dir] = startingCellId;
                startingEdgePointIds[dir][0] = startingCellPointIds->GetId(dir);
                startingEdgePointIds[dir][1] =
                    startingCellPointIds->GetId((dir + 1) % 4);

                startingCellIds[(dir + 1) % 4] = neiCellId;
                startingEdgePointIds[(dir + 1) % 4][0] =
                    neiPointIds->GetId((dir + 1) % 4);
                startingEdgePointIds[(dir + 1) % 4][1] =
                    neiPointIds->GetId((dir + 2) % 4);
              } else
              {
                isDirectionValid[dir] = false;
              }

              candidateCells.clear();
            }
          }

          // Stop if there are no more valid directions to explore
          allDirectionsChecked = !(isDirectionValid[0] || isDirectionValid[1] ||
                                   isDirectionValid[2] || isDirectionValid[3]);
        }

        // Add points to the output if needed
        for (vtkIdType newPtId = 0; newPtId < 4; ++newPtId)
        {
          if (outPointsIds[newCellPointIds[newPtId]] == -1)
          {
            outPointsIds[newCellPointIds[newPtId]] = numOutPoints;
            inPoints->GetPoint(newCellPointIds[newPtId], outPtCoords.GetData());
            outPoints->SetPoint(numOutPoints, outPtCoords.GetData());
            numOutPoints++;
          }
        }

        // Update new point IDs and add new cell
        for (vtkIdType idx = 0; idx < 4; ++idx)
        {
          newCellPointIds[idx] = outPointsIds[newCellPointIds[idx]];
        }

        newCellId = outCells->InsertNextCell(numPoints, newCellPointIds);
      } else
      {
        // Non-quad cells are simply copied
        for (vtkIdType idx = 0; idx < numPoints; ++idx)
        {
          // Add new point
          if (outPointsIds[cellPointIds[idx]] == -1)
          {
            outPointsIds[cellPointIds[idx]] = numOutPoints;
            inPoints->GetPoint(cellPointIds[idx], outPtCoords.GetData());
            outPoints->SetPoint(numOutPoints, outPtCoords.GetData());
            numOutPoints++;
          }
        }

        // Update new point IDs and add new cell
        nonQuadCellIds->SetNumberOfIds(numPoints);
        for (vtkIdType idx = 0; idx < numPoints; ++idx)
        {
          nonQuadCellIds->SetId(idx, outPointsIds[cellPointIds[idx]]);
        }

        newCellId = outCells->InsertNextCell(nonQuadCellIds);

        // Mark cell as visited
        markedCells.insert(cellId);
      }

      // Assign corresponding bin scalar value to the new cell
      outScalars->InsertNextValue(binValues[cellToBin[cellId]]);
      numOutCells++;
    }
  }

  // Define output
  outPoints->SetNumberOfPoints(numOutPoints);
  output->SetPoints(outPoints);
  output->SetPolys(outCells);
  output->GetCellData()->SetScalars(outScalars);
  output->Squeeze();

  this->GeometryFilter->SetInputDataObject(0, output);
  this->GeometryFilter->Modified();
  this->MultiBlockMaker->Update();

  return vtkPVDataRepresentation::RequestData(request, inputVector,
                                              outputVector);
}

//----------------------------------------------------------------------------
void vtkFastDecimatedSurfaceRepresentation::PrintSelf(ostream& os,
                                                      vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
  os << indent << "NumberOfColors: " << this->NumberOfColors << endl;
  os << indent << "MaxMergeAngle: " << this->MaxMergeAngle << endl;
  os << indent << "LogScale: " << this->LogScale << endl;
}
