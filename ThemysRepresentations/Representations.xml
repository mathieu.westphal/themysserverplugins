<ParaViewPlugin>
  <ServerManagerConfiguration>
    <ProxyGroup name="representations">
      <RepresentationProxy name="FastDecimatedSurfaceRepresentation"
                           class="vtkFastDecimatedSurfaceRepresentation"
                           processes="client|renderserver|dataserver"
                           base_proxygroup="representations"
                           base_proxyname="SurfaceRepresentation">
        <Documentation>
          This representation decimates a mesh containing quads to obtain a smaller
          mesh for faster display. The decimation merges quads based on their associated
          scalar cell data. The scalar data range is discretized using a customizable
          number of colors. The decimation starts from an initial cell by checking the
          neighbors along the four directions. A given cell is merged if its scalar
          value belongs to the same discretized color bin and if its plane and side
          edges align with those of the initial cell (a maximum angle can be defined
          to tolerate slight deviations).
          The algorithm can also be applied using a logarithmic scale.
          Note that the topology of the mesh may not be preserved as points are removed.
          Non-quad polygons are simply copied.
        </Documentation>
        <IntVectorProperty command="SetNumberOfColors"
                           default_values="256"
                           name="NumberOfColors"
                           number_of_elements="1">
          <IntRangeDomain min="1"
                          name="range">
          </IntRangeDomain>
          <Documentation>
            The number of discrete colors to use for the decimation.
            Less colors result in a stronger decimation.
          </Documentation>
        </IntVectorProperty>
        <DoubleVectorProperty command="SetMaxMergeAngle"
                              default_values="10"
                              name="MaximumAngle"
                              number_of_elements="1">
          <DoubleRangeDomain min="1"
                             max="180"
                             name="range">
          </DoubleRangeDomain>
          <Documentation>
            The maximum angle (in degrees) between two cells planes and edges for which
            merging is considered. A larger angle results in a stronger decimation.
          </Documentation>
        </DoubleVectorProperty>
      </RepresentationProxy>

      <Extension name="GeometryRepresentation">
        <Documentation>
          Extends standard GeometryRepresentation by adding
          FastDecimatedSurfaceRepresentation as a new type of representation.
        </Documentation>
        <RepresentationType subproxy="FastDecimatedSurfaceRepresentation"
                            text="Fast Decimated Surface With Edges"
                            subtype="Surface With Edges"/>
        <RepresentationType subproxy="FastDecimatedSurfaceRepresentation"
                            text="Fast Decimated Surface"
                            subtype="Surface"/>
        <SubProxy>
          <Proxy name="FastDecimatedSurfaceRepresentation"
                 proxygroup="representations"
                 proxyname="FastDecimatedSurfaceRepresentation">
          </Proxy>
          <ExposedProperties>
            <PropertyGroup label="Fast Decimated Surface">
              <Property name="NumberOfColors" />
              <Property name="MaximumAngle" />
            </PropertyGroup>
          </ExposedProperties>
          <ShareProperties subproxy="SurfaceRepresentation">
            <Exception name="Input"/>
            <Exception name="Visibility"/>
            <Exception name="Representation"/>
          </ShareProperties>
        </SubProxy>
      </Extension>

      <Extension name="UnstructuredGridRepresentation">
        <Documentation>
          Extends standard UnstructuredGridRepresentation by adding
          FastDecimatedSurfaceRepresentation as a new type of representation.
        </Documentation>
        <RepresentationType subproxy="FastDecimatedSurfaceRepresentation"
                            text="Fast Decimated Surface With Edges"
                            subtype="Surface With Edges"/>
        <RepresentationType subproxy="FastDecimatedSurfaceRepresentation"
                            text="Fast Decimated Surface"
                            subtype="Surface"/>
        <SubProxy>
          <Proxy name="FastDecimatedSurfaceRepresentation"
                 proxygroup="representations"
                 proxyname="FastDecimatedSurfaceRepresentation">
          </Proxy>
          <ExposedProperties>
            <PropertyGroup label="Fast Decimated Surface">
              <Property name="NumberOfColors" />
              <Property name="MaximumAngle" />
            </PropertyGroup>
          </ExposedProperties>
          <ShareProperties subproxy="SurfaceRepresentation">
            <Exception name="Input"/>
            <Exception name="Visibility"/>
            <Exception name="Representation"/>
          </ShareProperties>
        </SubProxy>
      </Extension>

      <Extension name="StructuredGridRepresentation">
        <Documentation>
          Extends standard StructuredGridRepresentation by adding
          FastDecimatedSurfaceRepresentation as a new type of representation.
        </Documentation>
        <RepresentationType subproxy="FastDecimatedSurfaceRepresentation"
                            text="Fast Decimated Surface With Edges"
                            subtype="Surface With Edges"/>
        <RepresentationType subproxy="FastDecimatedSurfaceRepresentation"
                            text="Fast Decimated Surface"
                            subtype="Surface"/>
        <SubProxy>
          <Proxy name="FastDecimatedSurfaceRepresentation"
                 proxygroup="representations"
                 proxyname="FastDecimatedSurfaceRepresentation">
          </Proxy>
          <ExposedProperties>
            <PropertyGroup label="Fast Decimated Surface">
              <Property name="NumberOfColors" />
              <Property name="MaximumAngle" />
            </PropertyGroup>
          </ExposedProperties>
          <ShareProperties subproxy="SurfaceRepresentation">
            <Exception name="Input"/>
            <Exception name="Visibility"/>
            <Exception name="Representation"/>
          </ShareProperties>
        </SubProxy>
      </Extension>

      <Extension name="UniformGridRepresentation">
        <Documentation>
          Extends standard UniformGridRepresentation by adding
          FastDecimatedSurfaceRepresentation as a new type of representation.
        </Documentation>
        <RepresentationType subproxy="FastDecimatedSurfaceRepresentation"
                            text="Fast Decimated Surface With Edges"
                            subtype="Surface With Edges"/>
        <RepresentationType subproxy="FastDecimatedSurfaceRepresentation"
                            text="Fast Decimated Surface"
                            subtype="Surface"/>
        <SubProxy>
          <Proxy name="FastDecimatedSurfaceRepresentation"
                 proxygroup="representations"
                 proxyname="FastDecimatedSurfaceRepresentation">
          </Proxy>
          <ExposedProperties>
            <PropertyGroup label="Fast Decimated Surface">
              <Property name="NumberOfColors" />
              <Property name="MaximumAngle" />
            </PropertyGroup>
          </ExposedProperties>
          <ShareProperties subproxy="SurfaceRepresentation">
            <Exception name="Input"/>
            <Exception name="Visibility"/>
            <Exception name="Representation"/>
          </ShareProperties>
        </SubProxy>
      </Extension>
    </ProxyGroup>
  </ServerManagerConfiguration>
</ParaViewPlugin>
