#include "vtkComparisonFilter.h"

#include <vtkArrayRename.h>
#include <vtkDataArray.h>
#include <vtkDataObjectTreeIterator.h>
#include <vtkFieldData.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkMergeTimeFilter.h>
#include <vtkMultiBlockDataSet.h>
#include <vtkNew.h>
#include <vtkObjectFactory.h>
#include <vtkReflectionFilter.h>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>

static const char* DISTANCE_ARRAY_NAME = "DistanceToOrigin";
static const char* PLANE_ARRAY_NAME = "Plane";

class vtkComparisonFilter::vtkInternals
{
public:
  vtkNew<vtkMergeTimeFilter> MergeTimeFilter;
  vtkNew<vtkArrayRename> ArrayRenameFilter;
  vtkNew<vtkReflectionFilter> ReflectionFilter;
};

//------------------------------------------------------------------------------
vtkStandardNewMacro(vtkComparisonFilter);

//------------------------------------------------------------------------------
vtkComparisonFilter::vtkComparisonFilter()
    : Internals(new vtkComparisonFilter::vtkInternals())
{
  // Connect internal filters
  this->Internals->ArrayRenameFilter->SetInputConnection(
      this->Internals->MergeTimeFilter->GetOutputPort());
  this->Internals->ReflectionFilter->SetCopyInput(false);
}

//------------------------------------------------------------------------------
void vtkComparisonFilter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
  this->Internals->MergeTimeFilter->PrintSelf(os, indent.GetNextIndent());
  this->Internals->ArrayRenameFilter->PrintSelf(os, indent.GetNextIndent());
  this->Internals->ReflectionFilter->PrintSelf(os, indent.GetNextIndent());
}

//------------------------------------------------------------------------------
int vtkComparisonFilter::FillInputPortInformation(int vtkNotUsed(port),
                                                  vtkInformation* info)
{
  info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkDataObject");
  info->Set(vtkAlgorithm::INPUT_IS_REPEATABLE(), 1);
  return 1;
}

//------------------------------------------------------------------------------
int vtkComparisonFilter::RequestDataObject(vtkInformation* request,
                                           vtkInformationVector** inputVector,
                                           vtkInformationVector* outputVector)
{
  return this->Internals->ReflectionFilter->ProcessRequest(request, inputVector,
                                                           outputVector);
}

//------------------------------------------------------------------------------
int vtkComparisonFilter::RequestInformation(vtkInformation* request,
                                            vtkInformationVector** inputVector,
                                            vtkInformationVector* outputVector)
{
  return this->Internals->MergeTimeFilter->ProcessRequest(request, inputVector,
                                                          outputVector);
}

//------------------------------------------------------------------------------
int vtkComparisonFilter::RequestUpdateExtent(vtkInformation* request,
                                             vtkInformationVector** inputVector,
                                             vtkInformationVector* outputVector)
{
  return this->Internals->MergeTimeFilter->ProcessRequest(request, inputVector,
                                                          outputVector);
}

//------------------------------------------------------------------------------
int vtkComparisonFilter::RequestData(vtkInformation* vtkNotUsed(request),
                                     vtkInformationVector** inputVector,
                                     vtkInformationVector* outputVector)
{
  //////////////////// Check inputs / output ////////////////////

  vtkDataObject* input1 = vtkDataObject::GetData(inputVector[0], 0);
  if (!input1)
  {
    vtkErrorMacro("Unable to retrieve the first input !");
    return 0;
  }

  vtkDataObject* input2 = vtkDataObject::GetData(inputVector[0], 1);
  if (!input2)
  {
    vtkErrorMacro("Unable to retrieve the second input !");
    return 0;
  }

  vtkMultiBlockDataSet* output = vtkMultiBlockDataSet::GetData(outputVector, 0);
  if (!output)
  {
    vtkErrorMacro("Unable to retrieve the output !");
    return 0;
  }

  //////////////////// MergeTime + ArrayRename filters ////////////////////

  this->Internals->MergeTimeFilter->RemoveAllInputs();
  this->Internals->MergeTimeFilter->AddInputDataObject(input1);
  this->Internals->MergeTimeFilter->AddInputDataObject(input2);

  // Apply merge time + rename arrays filters
  this->Internals->ArrayRenameFilter->Update();
  auto* arrayRenameOutput = vtkMultiBlockDataSet::SafeDownCast(
      this->Internals->ArrayRenameFilter->GetOutput());

  if (!arrayRenameOutput)
  {
    vtkErrorMacro(
        "Error casting vtkArrayRename filter output to vtkMultiBlockDataSet.");
    return 0;
  }

  //////////////////// Reflection filter ////////////////////

  output->SetNumberOfBlocks(2);

  //////// Forward the first base (input) ////////

  vtkMultiBlockDataSet* inputBaseMB =
      vtkMultiBlockDataSet::SafeDownCast(arrayRenameOutput->GetBlock(0));
  vtkSmartPointer<vtkMultiBlockDataSet> inputBase =
      vtkSmartPointer<vtkMultiBlockDataSet>::New();

  if (!inputBaseMB)
  {
    // If the base is not composite, we wrap it with a MBDS
    inputBase->SetBlock(0, arrayRenameOutput->GetBlock(0));
  } else
  {
    inputBase = inputBaseMB;
  }

  output->SetBlock(0, inputBase);

  //////// Reflect the second base ////////

  vtkNew<vtkDataObjectTreeIterator> leafIter;
  leafIter->SkipEmptyNodesOn();
  leafIter->VisitOnlyLeavesOn();

  inputBaseMB =
      vtkMultiBlockDataSet::SafeDownCast(arrayRenameOutput->GetBlock(1));
  inputBase = vtkSmartPointer<vtkMultiBlockDataSet>::New();

  if (!inputBaseMB)
  {
    // If the base is not composite, we wrap it with a MBDS
    inputBase->SetBlock(0, arrayRenameOutput->GetBlock(1));
  } else
  {
    inputBase = inputBaseMB;
  }

  vtkNew<vtkMultiBlockDataSet> outputBase;
  outputBase->CopyStructure(inputBase);

  double bounds[6] = {0, 0, 0, 0, 0, 0};
  inputBase->GetBounds(bounds);

  leafIter->SetDataSet(inputBase);
  for (leafIter->InitTraversal(); !leafIter->IsDoneWithTraversal();
       leafIter->GoToNextItem())
  {
    auto inputLeaf = leafIter->GetCurrentDataObject();

    // Recover distance to the origin from the data array
    double distance = 0.0;
    if (this->UseEnteredValues)
    {
      distance = this->DistanceToOrigin;
    } else
    {
      auto distanceArray =
          inputLeaf->GetFieldData()->GetArray(DISTANCE_ARRAY_NAME);
      if (!distanceArray)
      {
        vtkWarningMacro("Unable to retrieve the \"DistanceToOrigin\" value "
                        "from field data.");
      } else if (distanceArray->GetNumberOfTuples() != 1 ||
                 distanceArray->GetNumberOfComponents() != 1)
      {
        vtkWarningMacro(
            "\"DistanceToOrigin\" field should only contain one tuple and one "
            "component, ignoring.");
      } else
      {
        distance = distanceArray->GetTuple1(0);
      }
    }

    // Recover the plane from the data array
    int plane = 0;
    if (this->UseEnteredValues)
    {
      plane = this->Plane;
    } else
    {
      auto planeArray = inputLeaf->GetFieldData()->GetArray(PLANE_ARRAY_NAME);
      if (!planeArray)
      {
        vtkWarningMacro(
            "Unable to retrieve the \"Plane\" value from field data.");
      } else if (planeArray->GetNumberOfTuples() != 1 ||
                 planeArray->GetNumberOfComponents() != 1)
      {
        vtkWarningMacro("\"Plane\" field should only contain one tuple and one "
                        "component, ignoring.");
      } else
      {
        plane = planeArray->GetTuple1(0);
      }
    }

    // Update the distance and plane values if needed
    switch (plane)
    {
    case 0: // Xmin
      distance = bounds[0];
      plane = 6;
      break;
    case 1: // Ymin
      distance = bounds[2];
      plane = 7;
      break;
    case 2: // Zmin
      distance = bounds[4];
      plane = 8;
      break;
    case 3: // Xmax
      distance = bounds[1];
      plane = 6;
      break;
    case 4: // Ymax
      distance = bounds[3];
      plane = 7;
      break;
    case 5: // Zmax
      distance = bounds[5];
      plane = 8;
      break;
    default:
      break;
    }

    // Apply the reflection filter to the source
    this->Internals->ReflectionFilter->SetCenter(distance);
    this->Internals->ReflectionFilter->SetPlane(plane);
    this->Internals->ReflectionFilter->SetInputData(inputLeaf);
    this->Internals->ReflectionFilter->Update();

    vtkNew<vtkUnstructuredGrid> outputLeaf;
    outputLeaf->DeepCopy(this->Internals->ReflectionFilter->GetOutput());
    outputBase->SetDataSet(leafIter, outputLeaf);
  }

  output->SetBlock(1, outputBase);

  return 1;
}

//------------------------------------------------------------------------------
void vtkComparisonFilter::SetTolerance(double tolerance)
{
  this->Internals->MergeTimeFilter->SetTolerance(tolerance);
  this->Modified();
}

//------------------------------------------------------------------------------
double vtkComparisonFilter::GetTolerance()
{
  return this->Internals->MergeTimeFilter->GetTolerance();
}

//------------------------------------------------------------------------------
void vtkComparisonFilter::SetUseRelativeTolerance(bool useRelativeTolerance)
{
  this->Internals->MergeTimeFilter->SetUseRelativeTolerance(
      useRelativeTolerance);
  this->Modified();
}

//------------------------------------------------------------------------------
bool vtkComparisonFilter::GetUseRelativeTolerance()
{
  return this->Internals->MergeTimeFilter->GetUseRelativeTolerance();
}

//------------------------------------------------------------------------------
void vtkComparisonFilter::SetUseIntersection(bool useIntersection)
{
  this->Internals->MergeTimeFilter->SetUseRelativeTolerance(useIntersection);
  this->Modified();
}

//------------------------------------------------------------------------------
bool vtkComparisonFilter::GetUseIntersection()
{
  return this->Internals->MergeTimeFilter->GetUseIntersection();
}

//------------------------------------------------------------------------------
void vtkComparisonFilter::SetPointArrayName(const char* inputName,
                                            const char* newName)
{
  this->Internals->ArrayRenameFilter->SetPointArrayName(inputName, newName);
  this->Modified();
}

//------------------------------------------------------------------------------
void vtkComparisonFilter::SetCellArrayName(const char* inputName,
                                           const char* newName)
{
  this->Internals->ArrayRenameFilter->SetCellArrayName(inputName, newName);
  this->Modified();
}

//------------------------------------------------------------------------------
void vtkComparisonFilter::SetFieldArrayName(const char* inputName,
                                            const char* newName)
{
  this->Internals->ArrayRenameFilter->SetFieldArrayName(inputName, newName);
  this->Modified();
}

//------------------------------------------------------------------------------
void vtkComparisonFilter::SetVertexArrayName(const char* inputName,
                                             const char* newName)
{
  this->Internals->ArrayRenameFilter->SetVertexArrayName(inputName, newName);
  this->Modified();
}

//------------------------------------------------------------------------------
void vtkComparisonFilter::SetEdgeArrayName(const char* inputName,
                                           const char* newName)
{
  this->Internals->ArrayRenameFilter->SetEdgeArrayName(inputName, newName);
  this->Modified();
}

//------------------------------------------------------------------------------
void vtkComparisonFilter::SetRowArrayName(const char* inputName,
                                          const char* newName)
{
  this->Internals->ArrayRenameFilter->SetRowArrayName(inputName, newName);
  this->Modified();
}

//------------------------------------------------------------------------------
void vtkComparisonFilter::ClearPointMapping()
{
  this->Internals->ArrayRenameFilter->ClearPointMapping();
  this->Modified();
}

//------------------------------------------------------------------------------
void vtkComparisonFilter::ClearCellMapping()
{
  this->Internals->ArrayRenameFilter->ClearCellMapping();
  this->Modified();
}

//------------------------------------------------------------------------------
void vtkComparisonFilter::ClearFieldMapping()
{
  this->Internals->ArrayRenameFilter->ClearFieldMapping();
  this->Modified();
}

//------------------------------------------------------------------------------
void vtkComparisonFilter::ClearVertexMapping()
{
  this->Internals->ArrayRenameFilter->ClearVertexMapping();
  this->Modified();
}

//------------------------------------------------------------------------------
void vtkComparisonFilter::ClearEdgeMapping()
{
  this->Internals->ArrayRenameFilter->ClearEdgeMapping();
  this->Modified();
}

//------------------------------------------------------------------------------
void vtkComparisonFilter::ClearRowMapping()
{
  this->Internals->ArrayRenameFilter->ClearRowMapping();
  this->Modified();
}
