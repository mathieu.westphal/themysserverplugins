#ifndef vtkComparisonFilter_h
#define vtkComparisonFilter_h

#include <memory> // std::unique_ptr
#include <vector> // Use of dynamically allocated array

#include <vtkMultiBlockDataSetAlgorithm.h>

#include "ComparisonFilterModule.h" // For export macro

/**
 * The comparison filter is a meta-filter used to compare two bases
 * (vtkDataObjects). It allows in a first place the user to merge the timesteps
 * of the bases (taking account a given tolerance). Then, it applies the
 * FieldBasedReflection on the second base. The first one remains unchanged. It
 * is possible to rename the data arrays attached to the bases when applying
 * this filter by configuring a map between old and new names in the Themys
 * settings.
 */

class vtkComparisonFilter : public vtkMultiBlockDataSetAlgorithm
{
public:
  static vtkComparisonFilter* New();
  vtkTypeMacro(vtkComparisonFilter, vtkMultiBlockDataSetAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  //@{
  /**
   * Set/Get the tolerance for comparing time step
   * values to see if they are close enough to be considered
   * identical. Default is 0.00001
   */
  void SetTolerance(double tolerance);
  double GetTolerance();
  //@}

  //@{
  /**
   * Set/Get if the tolerance is relative to previous input or absolute.
   *
   * Default is false (absolute tolerance).
   */
  void SetUseRelativeTolerance(bool useRelativeTolerance);
  bool GetUseRelativeTolerance();
  //@}

  //@{
  /**
   * Set/Get if the merge use intersection instead of union.
   * Default is false (union is used).
   */
  void SetUseIntersection(bool useIntersection);
  bool GetUseIntersection();
  //@}

  //@{
  /**
   * Set array name mapping
   */
  void SetPointArrayName(const char* inputName, const char* newName);
  void SetCellArrayName(const char* inputName, const char* newName);
  void SetFieldArrayName(const char* inputName, const char* newName);
  void SetVertexArrayName(const char* inputName, const char* newName);
  void SetEdgeArrayName(const char* inputName, const char* newName);
  void SetRowArrayName(const char* inputName, const char* newName);
  //@}

  //@{
  /**
   * Clear array name mapping
   */
  void ClearPointMapping();
  void ClearCellMapping();
  void ClearFieldMapping();
  void ClearVertexMapping();
  void ClearEdgeMapping();
  void ClearRowMapping();
  //@}

  ///@{
  /**
   * If the reflection plane is set to X, Y or Z, this variable
   * is used to set the position of the plane.
   */
  vtkSetMacro(DistanceToOrigin, double);
  vtkGetMacro(DistanceToOrigin, double);
  ///@}

  ///@{
  /**
   * Set the normal of the plane to use as mirror.
   * From 0 to 8, the plane value correspond respectively to
   * Xmin, Ymin, Zmin, Xmax, Ymax, Zmax, X, Y and Z.
   */
  vtkSetClampMacro(Plane, int, 0, 8);
  vtkGetMacro(Plane, int);
  ///@}

  ///@{
  /**
   * Choose to use the plane and distance values from the input
   * field data arrays or those provided via the UI.
   */
  vtkSetMacro(UseEnteredValues, bool);
  vtkGetMacro(UseEnteredValues, bool);
  ///@}

protected:
  vtkComparisonFilter();
  ~vtkComparisonFilter() override = default;

  /**
   * Forwarded to vtkReflectionFilter
   */
  int RequestDataObject(vtkInformation*, vtkInformationVector**,
                        vtkInformationVector*) override;
  /**
   * Forwarded to vtkMergeTimeFilter
   */
  int RequestInformation(vtkInformation* request,
                         vtkInformationVector** inputVector,
                         vtkInformationVector* outputVector) override;
  /**
   * Forwarded to vtkMergeTimeFilter
   */
  int RequestUpdateExtent(vtkInformation* request,
                          vtkInformationVector** inputVector,
                          vtkInformationVector* outputVector) override;
  /**
   * vtkMergeTimeFilter + vtkArrayRename + vtkReflectionFilter
   */
  int RequestData(vtkInformation*, vtkInformationVector**,
                  vtkInformationVector*) override;
  /**
   * Override to allow multiple inputs.
   */
  int FillInputPortInformation(int port, vtkInformation* info) override;

private:
  vtkComparisonFilter(const vtkComparisonFilter&) = delete;
  void operator=(const vtkComparisonFilter&) = delete;

  double DistanceToOrigin = 0.0;
  int Plane = 0;
  bool UseEnteredValues = false;

  class vtkInternals;
  std::unique_ptr<vtkInternals> Internals;
};

#endif
