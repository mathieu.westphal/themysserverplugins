/**
 * @class   vtkDataSelectionExtractionFilter
 * @brief   Extracts all points or cells from a dataset having the same values
 * ​​in a given array as a selection
 *
 * From a selection and a data array chosen by the user, extracts all the points
 * or cells of a dataSet or an CompositeDataSet having the same values ​​in
 * this array as those of the selection.
 */

#ifndef vtkDataSelectionExtractionFilter_h
#define vtkDataSelectionExtractionFilter_h

#include <set>
#include <vector>

#include "vtkMultiBlockDataSetAlgorithm.h"
#include "vtkSelectionNode.h"

class vtkCompositeDataSet;
class vtkVariant;
class vtkDataSet;

class vtkDataSelectionExtractionFilter : public vtkMultiBlockDataSetAlgorithm
{
public:
  static vtkDataSelectionExtractionFilter* New();
  vtkTypeMacro(vtkDataSelectionExtractionFilter, vtkMultiBlockDataSetAlgorithm);

  /**
   * Convenience method to specify the selection connection (second input port).
   */
  void SetSelectionConnection(vtkAlgorithmOutput* algOutput);

protected:
  vtkDataSelectionExtractionFilter();
  ~vtkDataSelectionExtractionFilter() override = default;

  int FillInputPortInformation(int port, vtkInformation* info) override;
  int RequestData(vtkInformation*, vtkInformationVector**,
                  vtkInformationVector*) override;

  /**
   * Initialize InputData from the filter input if its a CompositeDataSet or a
   * DataSet
   */
  int SetupOutput(vtkInformation* inInfo, vtkInformation* outInfo);

  /**
   * Convenient method used to extract all values that we want from an ID
   * Selection.
   */
  int UpdateSelectedValuesFromIDSelection(
      vtkSelectionNode* currentNode, vtkIdType compositeIndex,
      vtkSelectionNode::SelectionField& selectionField);

  /**
   * Convenient method used to extract all values that we want with a node
   * provided from a query or frustrum selection.
   *
   * Because we doesn't have a COMPOSITE_INDEX() information from the
   * currentNode, we need to iterate on all block to extract this data.
   */
  int UpdateSelectedValuesFromQueryOrFrustrumSelection(
      vtkSelectionNode* currentNode, vtkCompositeDataSet* rawCD,
      vtkSelectionNode::SelectionField& selectionField);

  /**
   * Fill recursively blockNames with all non empty blocks names
   */
  static void StoreBlockName(std::vector<std::string>& blockNames,
                             vtkMultiBlockDataSet* mb);

  bool InputIsADataSet = false;
  vtkCompositeDataSet* InputData;
  std::set<vtkVariant> ActiveSelectionValues;

private:
  vtkDataSelectionExtractionFilter(const vtkDataSelectionExtractionFilter&) =
      delete;
  void operator=(const vtkDataSelectionExtractionFilter&) = delete;

  /**
   * Use internally to store from
   */
  int FillActiveSelectionValues(
      vtkDataSet* currentBlock, vtkSelectionNode* currentNode,
      vtkSelectionNode::SelectionField& selectionField);
};

#endif
