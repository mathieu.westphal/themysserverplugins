#include "vtkDataSelectionExtractionFilter.h"

#include "vtkCellData.h"
#include "vtkCompositeDataIterator.h"
#include "vtkCompositeDataPipeline.h"
#include "vtkCompositeDataSet.h"
#include "vtkConvertSelection.h"
#include "vtkDataArray.h"
#include "vtkDataSet.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkMultiBlockDataSet.h"
#include "vtkNew.h"
#include "vtkPVExtractSelection.h"
#include "vtkPointData.h"
#include "vtkSelection.h"
#include "vtkSelectionNode.h"
#include "vtkSortDataArray.h"
#include "vtkStreamingDemandDrivenPipeline.h"
#include "vtkUnstructuredGrid.h"

vtkStandardNewMacro(vtkDataSelectionExtractionFilter);

//----------------------------------------------------------------------------
vtkDataSelectionExtractionFilter::vtkDataSelectionExtractionFilter()
    : InputData{nullptr}
{
  this->SetNumberOfInputPorts(2);
}

//----------------------------------------------------------------------------
void vtkDataSelectionExtractionFilter::SetSelectionConnection(
    vtkAlgorithmOutput* algOutput)
{
  this->SetInputConnection(1, algOutput);
}

//------------------------------------------------------------------------------
int vtkDataSelectionExtractionFilter::FillInputPortInformation(
    int port, vtkInformation* info)
{
  if (port == 0)
  {
    info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkDataSet");
    info->Append(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(),
                 "vtkCompositeDataSet");
  } else if (port == 1)
  {
    info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkSelection");
    info->Set(vtkAlgorithm::INPUT_IS_OPTIONAL(), 1);
  } else
  {
    return 0;
  }

  return 1;
}

//------------------------------------------------------------------------------
int vtkDataSelectionExtractionFilter::SetupOutput(vtkInformation* inInfo,
                                                  vtkInformation* outInfo)
{
  vtkIdType piece =
      outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_PIECE_NUMBER());
  int numPieces =
      outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_PIECES());
  vtkDataObject* input = inInfo->Get(vtkDataObject::DATA_OBJECT());

  if (vtkCompositeDataSet* hdInput = vtkCompositeDataSet::SafeDownCast(input))
  {
    this->InputData = hdInput;
    hdInput->Register(this);
    return 1;
  } else if (vtkDataSet* dsInput = vtkDataSet::SafeDownCast(input))
  {
    this->InputIsADataSet = true;
    auto mb = vtkSmartPointer<vtkMultiBlockDataSet>::New();
    mb->SetNumberOfBlocks(numPieces);
    mb->SetBlock(piece, dsInput);
    this->InputData = mb;
    mb->Register(this);

    return 1;
  } else
  {
    vtkErrorMacro("This filter cannot handle input of type: "
                  << (input ? input->GetClassName() : "(none)"));
    return 0;
  }
}

//----------------------------------------------------------------------------
void vtkDataSelectionExtractionFilter::StoreBlockName(
    std::vector<std::string>& blockNames, vtkMultiBlockDataSet* mb)
{
  int childNbBlock = mb->GetNumberOfBlocks();
  for (int i = 0; i < childNbBlock; i++)
  {
    if (vtkMultiBlockDataSet* child =
            vtkMultiBlockDataSet::SafeDownCast(mb->GetBlock(i)))
    {
      vtkDataSelectionExtractionFilter::StoreBlockName(blockNames, child);
    } else
    {
      auto name = mb->GetMetaData(i)->Get(vtkMultiBlockDataSet::NAME());
      if (mb->GetBlock(i)->GetNumberOfElements(vtkDataSet::POINT) != 0)
      {
        blockNames.push_back(name);
      }
    }
  }
}

//----------------------------------------------------------------------------
int vtkDataSelectionExtractionFilter::UpdateSelectedValuesFromIDSelection(
    vtkSelectionNode* currentNode, vtkIdType compositeIndex,
    vtkSelectionNode::SelectionField& selectionField)
{
  vtkDataSet* currentBlock;
  if (this->InputIsADataSet)
  {
    auto mb = vtkMultiBlockDataSet::SafeDownCast(this->InputData);
    currentBlock = vtkDataSet::SafeDownCast(mb->GetBlock(compositeIndex));
  } else
  {
    currentBlock =
        vtkDataSet::SafeDownCast(this->InputData->GetDataSet(compositeIndex));
  }

  if (!currentBlock)
  {
    return 1;
  }

  return this->FillActiveSelectionValues(currentBlock, currentNode,
                                         selectionField);
}

//----------------------------------------------------------------------------
int vtkDataSelectionExtractionFilter::
    UpdateSelectedValuesFromQueryOrFrustrumSelection(
        vtkSelectionNode* currentNode, vtkCompositeDataSet* rawCD,
        vtkSelectionNode::SelectionField& selectionField)
{
  auto iter = vtk::TakeSmartPointer(rawCD->NewIterator());
  for (iter->InitTraversal(); !iter->IsDoneWithTraversal();
       iter->GoToNextItem())
  {
    vtkDataSet* currentBlock = vtkDataSet::SafeDownCast(
        rawCD->GetDataSet(iter->GetCurrentFlatIndex()));
    if (!currentBlock)
    {
      continue;
    }

    if (!this->FillActiveSelectionValues(currentBlock, currentNode,
                                         selectionField))
    {
      return 0;
    }
  }

  return 1;
}

//----------------------------------------------------------------------------
int vtkDataSelectionExtractionFilter::FillActiveSelectionValues(
    vtkDataSet* currentBlock, vtkSelectionNode* currentNode,
    vtkSelectionNode::SelectionField& selectionField)
{
  // Extract the desired data array in the block
  int association = vtkDataObject::FIELD_ASSOCIATION_NONE;
  vtkDataArray* array =
      this->GetInputArrayToProcess(0, currentBlock, association);
  if (!array)
  {
    vtkWarningMacro(<< "No array can be processed");
    return 0;
  }

  vtkNew<vtkPVExtractSelection> extractSelection;
  vtkNew<vtkSelection> selection;
  selection->AddNode(currentNode);
  extractSelection->SetInputData(0, currentBlock);
  extractSelection->SetInputData(1, selection);
  extractSelection->Update();
  vtkDataSet* selectedBlock =
      vtkDataSet::SafeDownCast(extractSelection->GetOutput());

  // Fill ActiveSelectionValues with the desired data array in the current
  // extracted selection
  vtkDataArray* selectedArray;
  switch (association)
  {
  case vtkDataObject::FIELD_ASSOCIATION_CELLS:
    selectionField = vtkSelectionNode::CELL;
    selectedArray = selectedBlock->GetCellData()->GetArray(array->GetName());
    break;

  case vtkDataObject::FIELD_ASSOCIATION_POINTS:
    selectionField = vtkSelectionNode::POINT;
    selectedArray = selectedBlock->GetPointData()->GetArray(array->GetName());
    break;

  default:
    vtkWarningMacro("Unsupported field association");
    return 0;
  }

  if (selectedArray)
  {
    for (vtkIdType i = 0; i < selectedArray->GetNumberOfValues(); i++)
    {
      this->ActiveSelectionValues.insert(selectedArray->GetVariantValue(i));
    }
  }

  return 1;
}

//----------------------------------------------------------------------------
int vtkDataSelectionExtractionFilter::RequestData(
    vtkInformation* vtkNotUsed(request), vtkInformationVector** inputVector,
    vtkInformationVector* outputVector)
{
  vtkInformation* inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation* outInfo = outputVector->GetInformationObject(0);

  // Setup input/output to handle only a CompositeDataSet
  if (!this->SetupOutput(inInfo, outInfo))
  {
    return 0;
  }

  vtkSelection* activeSelection = vtkSelection::GetData(inputVector[1], 0);
  vtkMultiBlockDataSet* output = vtkMultiBlockDataSet::SafeDownCast(
      outInfo->Get(vtkDataObject::DATA_OBJECT()));
  if (!this->InputData || !output || !activeSelection)
  {
    vtkErrorMacro(<< "Invalid input or output.");
    this->InputData->UnRegister(this);
    return 0;
  }

  int numPieces =
      outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_PIECES());
  output->SetNumberOfBlocks(numPieces);

  this->ActiveSelectionValues.clear();

  // Used later if a SelectionNode is a query/frustrum selection
  vtkNew<vtkPVExtractSelection> extractRawData;
  extractRawData->SetInputData(0, this->InputData);
  extractRawData->SetInputData(1, activeSelection);
  extractRawData->Update();
  vtkCompositeDataSet* rawCD =
      vtkCompositeDataSet::SafeDownCast(extractRawData->GetOutput());

  // Search for each selection node what points/cells was selected in the good
  // block and fill ActiveSelectionValues with the values of the dataArray
  // choosen by the user
  vtkSelectionNode::SelectionField selectionField =
      vtkSelectionNode::NUM_FIELD_TYPES;
  for (int i = 0; i < activeSelection->GetNumberOfNodes(); i++)
  {
    auto currentNode = activeSelection->GetNode(i);

    // Because we can't used the COMPOSITE_INDEX() information from a
    // query/frustrum we need to split the logic
    auto nodeType =
        currentNode->GetProperties()->Get(vtkSelectionNode::CONTENT_TYPE());
    if (nodeType == vtkSelectionNode::QUERY ||
        nodeType == vtkSelectionNode::FRUSTUM)
    {
      if (!this->UpdateSelectedValuesFromQueryOrFrustrumSelection(
              currentNode, rawCD, selectionField))
      {
        break;
      }
    } else
    {
      vtkIdType compositeIndex = currentNode->GetProperties()->Get(
          vtkSelectionNode::COMPOSITE_INDEX());

      if (!this->UpdateSelectedValuesFromIDSelection(
              currentNode, compositeIndex, selectionField))
      {
        break;
      }
    }
  }

  if (this->ActiveSelectionValues.size() == 0)
  {
    this->InputData->UnRegister(this);
    vtkWarningMacro("No data can be extracted");
    return 0;
  }

  std::vector<std::string> blockNames;
  if (!this->InputIsADataSet)
  {
    if (vtkMultiBlockDataSet* mb =
            vtkMultiBlockDataSet::SafeDownCast(this->InputData))
    {
      vtkDataSelectionExtractionFilter::StoreBlockName(blockNames, mb);
    }
  }

  // Extract desired selection for each block and append it in the output
  int blockId = 0;
  auto iter = vtk::TakeSmartPointer(this->InputData->NewIterator());
  for (iter->InitTraversal(); !iter->IsDoneWithTraversal();
       iter->GoToNextItem())
  {
    vtkDataSet* currentBlock =
        vtkDataSet::SafeDownCast(iter->GetCurrentDataObject());
    if (!currentBlock)
    {
      continue;
    }

    vtkDataArray* array = this->GetInputArrayToProcess(0, currentBlock);
    if (!array)
    {
      vtkWarningMacro(<< "No array can be processed");
      return 0;
    }

    // Find all matching indices
    vtkNew<vtkIdTypeArray> indices;
    for (vtkIdType i = 0; i < array->GetNumberOfValues(); i++)
    {
      for (auto itr = this->ActiveSelectionValues.begin();
           itr != this->ActiveSelectionValues.end(); itr++)
      {
        if (array->GetVariantValue(i) == *itr)
        {
          indices->InsertNextValue(i);
          break;
        }
      }
    }

    vtkNew<vtkPVExtractSelection> extractSelection;
    vtkNew<vtkSelection> selection;
    vtkNew<vtkSelectionNode> selectionNode;
    selectionNode->SetFieldType(selectionField);
    selectionNode->SetContentType(vtkSelectionNode::INDICES);
    selectionNode->SetSelectionList(indices);
    selection->AddNode(selectionNode);

    extractSelection->SetInputData(0, currentBlock);
    extractSelection->SetInputData(1, selection);
    extractSelection->Update();

    auto* newBlock = extractSelection->GetOutputDataObject(0);
    if (newBlock && newBlock->GetNumberOfElements(selectionField) != 0)
    {
      output->SetBlock(blockId, newBlock);
      if (blockId < blockNames.size())
      {
        output->GetMetaData(blockId)->Set(vtkCompositeDataSet::NAME(),
                                          blockNames[blockId]);
      }
    }
    blockId++;
  }

  this->InputData->UnRegister(this);
  return 1;
}
