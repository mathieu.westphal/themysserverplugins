/**
 * @class   vtkTJunction
 * @brief   Make it easier to define a junction
 *
 * This filter preserves all the topology of the input.
 */

#ifndef vtkTJunction_h
#define vtkTJunction_h

#include "vtkMultiBlockDataSetAlgorithm.h"

class vtkTJunction : public vtkMultiBlockDataSetAlgorithm
{
public:
  static vtkTJunction* New();
  vtkTypeMacro(vtkTJunction, vtkMultiBlockDataSetAlgorithm);
  void PrintSelf(ostream& oss, vtkIndent indent) override;

  int RequestData(vtkInformation*, vtkInformationVector**,
                  vtkInformationVector*) override;

protected:
  vtkTJunction() = default;
  ~vtkTJunction() override = default;

private:
  vtkTJunction(const vtkTJunction&) = delete;
  void operator=(const vtkTJunction&) = delete;
};

#endif
