//------------------------------------------------------------------------------
#include "vtkObjectFactory.h" // i don't know why !
//------------------------------------------------------------------------------
#include "vtkTJunction.h"
//------------------------------------------------------------------------------
#include "vtkDataObject.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkMultiBlockDataSet.h"
//------------------------------------------------------------------------------
vtkStandardNewMacro(vtkTJunction);
//------------------------------------------------------------------------------
void vtkTJunction::PrintSelf(std::ostream& oss, vtkIndent indent)
{
  this->Superclass::PrintSelf(oss, indent);
}
//------------------------------------------------------------------------------
int vtkTJunction::RequestData(vtkInformation* request,
                              vtkInformationVector** inputVector,
                              vtkInformationVector* outputVector)
{
  vtkInformation* input = inputVector[0]->GetInformationObject(0);
  vtkMultiBlockDataSet* input_mbds = vtkMultiBlockDataSet ::SafeDownCast(
      input->Get(vtkDataObject::DATA_OBJECT()));
  vtkInformation* output = outputVector->GetInformationObject(0);
  vtkMultiBlockDataSet* output_mbds = vtkMultiBlockDataSet ::SafeDownCast(
      output->Get(vtkDataObject::DATA_OBJECT()));
  output_mbds->ShallowCopy(input_mbds);
  return 1;
}
//------------------------------------------------------------------------------
