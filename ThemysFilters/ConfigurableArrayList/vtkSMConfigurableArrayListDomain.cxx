#include "vtkSMConfigurableArrayListDomain.h"

#include <sstream>
#include <string>
#include <vector>

#include "vtkObjectFactory.h"
#include "vtkSMProperty.h"
#include "vtkSMSettings.h"
#include "vtkSMStringVectorProperty.h"

vtkStandardNewMacro(vtkSMConfigurableArrayListDomain);

static const char* SETTING_NAME = ".settings.ThemysSettings.ArrayNames";

//-----------------------------------------------------------------------------
void vtkSMConfigurableArrayListDomain::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}

//-----------------------------------------------------------------------------
int vtkSMConfigurableArrayListDomain::SetDefaultValues(
    vtkSMProperty* prop, bool use_unchecked_values)
{
  auto svp = vtkSMStringVectorProperty::SafeDownCast(prop);
  if (!svp)
  {
    return 0;
  }

  if (this->GetNumberOfStrings() == 0)
  {
    return 1;
  }

  vtkSMSettings* setting = vtkSMSettings::GetInstance();

  int nb = setting->GetSettingNumberOfElements(SETTING_NAME);
  int nbPerCommand = svp->GetNumberOfElementsPerCommand();
  std::vector<std::string> values;
  for (int idx = 0; idx < nb; idx += nbPerCommand)
  {
    auto newName = setting->GetSettingAsString(SETTING_NAME, idx, "NoName");
    auto oldNames =
        setting->GetSettingAsString(SETTING_NAME, idx + 1, "NoName");
    std::istringstream stream;
    stream.str(oldNames);
    std::string name;
    unsigned int tmp;
    while (std::getline(stream, name, ';'))
    {
      values.push_back(name);
      values.push_back(newName);
    }
  }

  svp->SetElements(values);

  return 1;
}
