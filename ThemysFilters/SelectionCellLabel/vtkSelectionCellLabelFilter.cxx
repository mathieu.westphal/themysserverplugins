#include "vtkSelectionCellLabelFilter.h"

#include <cmath>
#include <cstdio>
#include <string>

#include "vtkAppendPolyData.h"
#include "vtkCellCenters.h"
#include "vtkCellData.h"
#include "vtkDataArray.h"
#include "vtkDataSet.h"
#include "vtkDataSetSurfaceFilter.h"
#include "vtkExtractSelection.h"
#include "vtkGeneralTransform.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkMath.h"
#include "vtkMultiBlockDataSet.h"
#include "vtkNew.h"
#include "vtkObjectFactory.h"
#include "vtkPointData.h"
#include "vtkPolyData.h"
#include "vtkPolyDataNormals.h"
#include "vtkSelection.h"
#include "vtkTransformPolyDataFilter.h"
#include "vtkTypeTraits.h"
#include "vtkVectorText.h"

vtkStandardNewMacro(vtkSelectionCellLabelFilter);

//----------------------------------------------------------------------------
vtkSelectionCellLabelFilter::vtkSelectionCellLabelFilter()
{
  this->SetNumberOfInputPorts(2);
}

//----------------------------------------------------------------------------
void vtkSelectionCellLabelFilter::SetSelectionConnection(
    vtkAlgorithmOutput* algOutput)
{
  this->SetInputConnection(1, algOutput);
}

//------------------------------------------------------------------------------
int vtkSelectionCellLabelFilter::FillInputPortInformation(int port,
                                                          vtkInformation* info)
{
  if (port == 0)
  {
    info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkDataSet");
  } else if (port == 1)
  {
    info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkSelection");
    info->Set(vtkAlgorithm::INPUT_IS_OPTIONAL(), 1);
  } else
  {
    return 0;
  }

  return 1;
}

//------------------------------------------------------------------------------
double vtkSelectionCellLabelFilter::ComputeScaleFactor(vtkPolyData* polyData)
{
  // Store the largest dimension/length of the first non-vertex cell used to
  // scale the labels. This is an arbitrary choice given that all labels should
  // have the same size.
  double cellSize = 0.0;
  vtkIdType cellId = 0;

  while (cellSize <= 0.0 && cellId < polyData->GetNumberOfCells())
  {
    double* cellBounds = polyData->GetCell(cellId)->GetBounds();
    cellSize = vtkMath::Max(vtkMath::Max(cellBounds[1] - cellBounds[0],
                                         cellBounds[3] - cellBounds[2]),
                            cellBounds[5] - cellBounds[4]);
    ++cellId;
  }

  // If cellSize is still zero, use an arbitrary non-zero value
  if (cellSize == 0.0)
  {
    vtkWarningMacro(<< "All cells have size equal to zero. Setting reference "
                       "cell size for label "
                       "scaling to 0.1.");
    cellSize = 0.1;
  }

  return cellSize * 0.05;
}

//----------------------------------------------------------------------------
int vtkSelectionCellLabelFilter::RequestData(vtkInformation* request,
                                             vtkInformationVector** inputVector,
                                             vtkInformationVector* outputVector)
{
  vtkDataSet* input = vtkDataSet::GetData(inputVector[0], 0);
  vtkSelection* selection = vtkSelection::GetData(inputVector[1], 0);
  vtkMultiBlockDataSet* output = vtkMultiBlockDataSet::GetData(outputVector, 0);

  if (!input || !output)
  {
    vtkErrorMacro(<< "Invalid input or output!");
    return 1;
  }

  if (!selection)
  {
    vtkErrorMacro(<< "No selection!");
    return 1;
  }

  // Extract the active selection
  vtkNew<vtkExtractSelection> extractFilter;
  extractFilter->SetInputData(0, input);
  extractFilter->SetInputData(1, selection);
  extractFilter->Update();
  vtkDataSet* selectedInput =
      vtkDataSet::SafeDownCast(extractFilter->GetOutput());

  // Extract surface of the selection
  vtkNew<vtkDataSetSurfaceFilter> surfaceFilter;
  surfaceFilter->SetInputConnection(extractFilter->GetOutputPort());
  surfaceFilter->Update();

  // Create filter to compute cell centers
  vtkNew<vtkCellCenters> centerFilter;

  // Compute face normals if they are not already available
  if (!surfaceFilter->GetOutput()->GetCellData()->GetNormals())
  {
    vtkNew<vtkPolyDataNormals> normalFilter;
    normalFilter->ComputePointNormalsOff();
    normalFilter->ComputeCellNormalsOn();
    normalFilter->SetInputConnection(surfaceFilter->GetOutputPort());
    centerFilter->SetInputConnection(normalFilter->GetOutputPort());
  } else
  {
    centerFilter->SetInputConnection(surfaceFilter->GetOutputPort());
  }

  // Apply filters
  centerFilter->Update();
  vtkIdType numCellCenters = centerFilter->GetOutput()->GetNumberOfPoints();

  // Check that normals are available (non available e.g. in case of vertices)
  if (!centerFilter->GetOutput()->GetPointData()->GetNormals())
  {
    vtkErrorMacro(<< "Cell normals cannot be computed, aborting.");
    return 1;
  }

  // No labels to create since the selection is probably empty
  if (numCellCenters == 0)
  {
    output->SetNumberOfBlocks(1);
    output->SetBlock(0, selectedInput);
    return 1;
  }

  // Retrieve scalar values
  if (!this->GetInputArrayToProcess(0, inputVector))
  {
    vtkDebugMacro(<< "Invalid input array.");
    return 1;
  }

  vtkDataArray* cellScalars =
      centerFilter->GetOutput()->GetPointData()->GetScalars(
          this->GetInputArrayToProcess(0, inputVector)->GetName());

  if (!cellScalars)
  {
    vtkDebugMacro(<< "Invalid cell scalar data.");
    return 1;
  }

  // Define scale factor for the labels
  double scaleFactor =
      this->AutomaticScaling
          ? this->ComputeScaleFactor(surfaceFilter->GetOutput())
          : this->ScaleFactor;

  // Labels are appended with this filter
  vtkNew<vtkAppendPolyData> appendFilter;

  for (vtkIdType faceId = 0; faceId < numCellCenters; ++faceId)
  {
    // Create label
    std::string labelText = cellScalars->GetVariantValue(faceId).ToString();
    vtkNew<vtkVectorText> label;
    label->SetText(labelText.c_str());
    label->Update();

    // Define the transform that will be applied to the current label
    vtkNew<vtkGeneralTransform> transform;
    transform->PostMultiply();

    // Center label at origin and add offset along normal to avoid overlap with
    // mesh surface. Note that upon generation, the label is in the XY plane
    // with a normal along the Z axis and its lower left corner placed at the
    // origin.
    double* bounds = label->GetOutput()->GetBounds();
    transform->Translate(-(bounds[1] - bounds[0]) * 0.5,
                         -(bounds[3] - bounds[2]) * 0.5, 0.1);

    // Scale label
    transform->Scale(scaleFactor, scaleFactor, scaleFactor);

    // Determine angle and axis of rotation to orient the label along the face
    // normal
    double* faceNormal =
        centerFilter->GetOutput()->GetPointData()->GetNormals()->GetTuple(
            faceId);
    double labelNormal[3] = {0.0, 0.0, 1.0};
    double rotAxis[3] = {0.0, 1.0, 0.0};
    double rotAngle = 0.0;

    // Face normal is flipped compared to label normal
    if (faceNormal[0] == 0.0 && faceNormal[1] == 0.0 && faceNormal[2] < 0.0)
    {
      rotAngle = 180.0;
    }
    // In the case where faceNormal == { 0.0, 0.0, >0.0 }, then rotAngle = 0
    else if (faceNormal[0] != 0.0 || faceNormal[1] != 0.0 ||
             faceNormal[2] < 0.0)
    {
      // Compute rotation axis
      vtkMath::Cross(labelNormal, faceNormal, rotAxis);

      // Compute rotation angle in degrees
      rotAngle = vtkMath::DegreesFromRadians(
          std::acos(vtkMath::Dot(labelNormal, faceNormal)));
    }

    // Define the rotation
    transform->RotateWXYZ(rotAngle, rotAxis[0], rotAxis[1], rotAxis[2]);

    // Place the label at face center
    double* facePos = centerFilter->GetOutput()->GetPoint(faceId);
    transform->Translate(facePos[0], facePos[1], facePos[2]);

    // Setup transform filter
    vtkNew<vtkTransformPolyDataFilter> transformFilter;
    transformFilter->SetTransform(transform);
    transformFilter->SetInputData(label->GetOutput());
    transformFilter->Update();

    // Add label to the append filter
    appendFilter->AddInputData(transformFilter->GetOutput());
  }

  // Append all labels
  appendFilter->Update();

  // First block consists of the selection while second block contains labels
  output->SetNumberOfBlocks(2);
  output->SetBlock(0, selectedInput);
  output->SetBlock(1, appendFilter->GetOutput());

  return 1;
}
