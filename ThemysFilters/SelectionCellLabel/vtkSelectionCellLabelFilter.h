/**
 * @class   vtkSelectionCellLabelFilter
 * @brief   Extracts the current selection and labels faces with scalar values.
 *
 * This filter can be applied on any vtkDataSet where an active selection is
 * present. Labels are created at the center of each cell face, displaying the
 * cell scalar value of the selected data array. The output is a multiblock data
 * set containing the selection as well as a block defining all labels as
 * vtkVectorText.
 */

#ifndef vtkSelectionCellLabelFilter_h
#define vtkSelectionCellLabelFilter_h

#include "vtkMultiBlockDataSetAlgorithm.h"

class vtkPolyData;

class vtkSelectionCellLabelFilter : public vtkMultiBlockDataSetAlgorithm
{
public:
  static vtkSelectionCellLabelFilter* New();
  vtkTypeMacro(vtkSelectionCellLabelFilter, vtkMultiBlockDataSetAlgorithm);

  /**
   * Convenience method to specify the selection connection (second input port).
   */
  void SetSelectionConnection(vtkAlgorithmOutput* algOutput);

  ///@{
  /**
   * Set/get whether label scaling is done automatically.
   * Default is true.
   */
  vtkGetMacro(AutomaticScaling, bool);
  vtkSetMacro(AutomaticScaling, bool);
  ///@}

  ///@{
  /**
   * Set/get the scale factor for the labels. Only used if AutomaticScaling is
   * Off. Default is 1.0.
   */
  vtkGetMacro(ScaleFactor, double);
  vtkSetMacro(ScaleFactor, double);
  ///@}

  /**
   * Compute a scaling factor for labels based on cell size.
   */
  double ComputeScaleFactor(vtkPolyData* polyData);

protected:
  vtkSelectionCellLabelFilter();
  ~vtkSelectionCellLabelFilter() override = default;

  int FillInputPortInformation(int port, vtkInformation* info) override;
  int RequestData(vtkInformation*, vtkInformationVector**,
                  vtkInformationVector*) override;

  bool AutomaticScaling = true;
  double ScaleFactor = 1.0;

private:
  vtkSelectionCellLabelFilter(const vtkSelectionCellLabelFilter&) = delete;
  void operator=(const vtkSelectionCellLabelFilter&) = delete;
};

#endif
