"""
This module tests the CEACellDataToPointData filter with quadrangle projection
and no boundary condition.

The goal is to compute the same projection as it would be done on
2D structured grid (i.e quadrangle cells)
"""
# state file generated using paraview version 5.11.1-1037-g855d440756
import paraview

paraview.compatibility.major = 5
paraview.compatibility.minor = 11

#### import the simple module from the paraview
from paraview.simple import *

from common import CMAKE_SOURCE_DIR, launch_comparison

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# Create a new 'Render View'
renderView1 = CreateView("RenderView")
renderView1.ViewSize = [2016, 1171]
renderView1.InteractionMode = "2D"
renderView1.AxesGrid = "GridAxes3DActor"
renderView1.CenterOfRotation = [0.5, 0.5019831010140479, 0.0]
renderView1.StereoType = "Crystal Eyes"
renderView1.CameraPosition = [
    0.8213093654499063,
    -0.024687229557731036,
    2.7266382576652797,
]
renderView1.CameraFocalPoint = [0.8213093654499063, -0.024687229557731036, 0.0]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 0.10641597058404478

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name="Layout #1")
layout1.AssignView(0, renderView1)
layout1.SetSize(2016, 1171)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'XML MultiBlock Data Reader'
data = XMLMultiBlockDataReader(
    registrationName="4MatsVortexWithNodeField.vtm",
    FileName=[
        f"{CMAKE_SOURCE_DIR}/Data/Testing/MaterialFilters/4MatsVortexWithNodeField/4MatsVortexWithNodeField.vtm"
    ],
)

# create a new 'Cell Data to Point Data'
CEACellDatatoPointData1 = CEACellDatatoPointData(
    registrationName="CEACellDatatoPointData1", Input=data
)
CEACellDatatoPointData1.ProcessAllArrays = 0
CEACellDatatoPointData1.CellDataArraytoprocess = ["vtkInterfaceFraction"]
CEACellDatatoPointData1.WeightCellOption = "Quad"

# create a new 'Iso Volume'
isoVolume1 = IsoVolume(registrationName="IsoVolume1", Input=CEACellDatatoPointData1)
isoVolume1.InputScalars = ["POINTS", "vtkInterfaceFraction"]
isoVolume1.ThresholdRange = [0.5, 2.0]

# create a new 'Transform'
transform1 = Transform(registrationName="Transform1", Input=isoVolume1)
transform1.Transform = "Transform"

# init the 'Transform' selected for 'Transform'
transform1.Transform.Rotate = [180.0, 0.0, 0.0]

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from isoVolume1
isoVolume1Display = Show(isoVolume1, renderView1, "UnstructuredGridRepresentation")

# get 2D transfer function for 'vtkInterfaceFraction'
vtkInterfaceFractionTF2D = GetTransferFunction2D("vtkInterfaceFraction")

# get color transfer function/color map for 'vtkInterfaceFraction'
vtkInterfaceFractionLUT = GetColorTransferFunction("vtkInterfaceFraction")
vtkInterfaceFractionLUT.TransferFunction2D = vtkInterfaceFractionTF2D
vtkInterfaceFractionLUT.RGBPoints = [
    2.2190418544468695e-08,
    0.231373,
    0.298039,
    0.752941,
    0.5000000110952095,
    0.865003,
    0.865003,
    0.865003,
    1.0,
    0.705882,
    0.0156863,
    0.14902,
]
vtkInterfaceFractionLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'vtkInterfaceFraction'
vtkInterfaceFractionPWF = GetOpacityTransferFunction("vtkInterfaceFraction")
vtkInterfaceFractionPWF.Points = [
    2.2190418544468695e-08,
    0.0,
    0.5,
    0.0,
    1.0,
    1.0,
    0.5,
    0.0,
]
vtkInterfaceFractionPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
isoVolume1Display.Representation = "Surface"
isoVolume1Display.ColorArrayName = ["POINTS", "vtkInterfaceFraction"]
isoVolume1Display.LookupTable = vtkInterfaceFractionLUT
isoVolume1Display.SelectTCoordArray = "None"
isoVolume1Display.SelectNormalArray = "None"
isoVolume1Display.SelectTangentArray = "None"
isoVolume1Display.OSPRayScaleArray = "vtkInterfaceFraction"
isoVolume1Display.OSPRayScaleFunction = "PiecewiseFunction"
isoVolume1Display.BlockSelectors = [
    "/Root/M1/_0BLOCVACUUM",
    "/Root/M1/_1NE",
    "/Root/M1/_2NW",
    "/Root/M1/_4SW",
    "/Root/M1/_5global_M1noloaded",
]
isoVolume1Display.SelectOrientationVectors = "None"
isoVolume1Display.ScaleFactor = 0.1
isoVolume1Display.SelectScaleArray = "vtkInterfaceFraction"
isoVolume1Display.GlyphType = "Arrow"
isoVolume1Display.GlyphTableIndexArray = "vtkInterfaceFraction"
isoVolume1Display.GaussianRadius = 0.005
isoVolume1Display.SetScaleArray = ["POINTS", "vtkInterfaceFraction"]
isoVolume1Display.ScaleTransferFunction = "PiecewiseFunction"
isoVolume1Display.OpacityArray = ["POINTS", "vtkInterfaceFraction"]
isoVolume1Display.OpacityTransferFunction = "PiecewiseFunction"
isoVolume1Display.DataAxesGrid = "GridAxesRepresentation"
isoVolume1Display.PolarAxes = "PolarAxesRepresentation"
isoVolume1Display.ScalarOpacityFunction = vtkInterfaceFractionPWF
isoVolume1Display.ScalarOpacityUnitDistance = 0.09787157135833419
isoVolume1Display.OpacityArrayName = ["POINTS", "vtkInterfaceFraction"]
isoVolume1Display.SelectInputVectors = ["POINTS", ""]
isoVolume1Display.WriteLog = ""

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
isoVolume1Display.ScaleTransferFunction.Points = [
    0.4999999999999999,
    0.0,
    0.5,
    0.0,
    1.0,
    1.0,
    0.5,
    0.0,
]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
isoVolume1Display.OpacityTransferFunction.Points = [
    0.4999999999999999,
    0.0,
    0.5,
    0.0,
    1.0,
    1.0,
    0.5,
    0.0,
]

# show data from transform1
transform1Display = Show(transform1, renderView1, "UnstructuredGridRepresentation")

# trace defaults for the display properties.
transform1Display.Representation = "Surface"
transform1Display.ColorArrayName = ["POINTS", "vtkInterfaceFraction"]
transform1Display.LookupTable = vtkInterfaceFractionLUT
transform1Display.SelectTCoordArray = "None"
transform1Display.SelectNormalArray = "None"
transform1Display.SelectTangentArray = "None"
transform1Display.OSPRayScaleArray = "vtkInterfaceFraction"
transform1Display.OSPRayScaleFunction = "PiecewiseFunction"
transform1Display.BlockSelectors = [
    "/Root/M1/_0BLOCVACUUM",
    "/Root/M1/_1NE",
    "/Root/M1/_2NW",
    "/Root/M1/_4SW",
    "/Root/M1/_5global_M1noloaded",
]
transform1Display.SelectOrientationVectors = "None"
transform1Display.ScaleFactor = 0.1
transform1Display.SelectScaleArray = "vtkInterfaceFraction"
transform1Display.GlyphType = "Arrow"
transform1Display.GlyphTableIndexArray = "vtkInterfaceFraction"
transform1Display.GaussianRadius = 0.005
transform1Display.SetScaleArray = ["POINTS", "vtkInterfaceFraction"]
transform1Display.ScaleTransferFunction = "PiecewiseFunction"
transform1Display.OpacityArray = ["POINTS", "vtkInterfaceFraction"]
transform1Display.OpacityTransferFunction = "PiecewiseFunction"
transform1Display.DataAxesGrid = "GridAxesRepresentation"
transform1Display.PolarAxes = "PolarAxesRepresentation"
transform1Display.ScalarOpacityFunction = vtkInterfaceFractionPWF
transform1Display.ScalarOpacityUnitDistance = 0.09789320760781947
transform1Display.OpacityArrayName = ["POINTS", "vtkInterfaceFraction"]
transform1Display.SelectInputVectors = [None, ""]
transform1Display.WriteLog = ""

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
transform1Display.ScaleTransferFunction.Points = [
    0.49999999999999994,
    0.0,
    0.5,
    0.0,
    1.0,
    1.0,
    0.5,
    0.0,
]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
transform1Display.OpacityTransferFunction.Points = [
    0.49999999999999994,
    0.0,
    0.5,
    0.0,
    1.0,
    1.0,
    0.5,
    0.0,
]

# setup the color legend parameters for each legend in this view

# get color legend/bar for vtkInterfaceFractionLUT in view renderView1
vtkInterfaceFractionLUTColorBar = GetScalarBar(vtkInterfaceFractionLUT, renderView1)
vtkInterfaceFractionLUTColorBar.Title = "vtkInterfaceFraction"
vtkInterfaceFractionLUTColorBar.ComponentTitle = ""

# set color bar visibility
vtkInterfaceFractionLUTColorBar.Visibility = 1

# show color legend
isoVolume1Display.SetScalarBarVisibility(renderView1, True)

# show color legend
transform1Display.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# restore active source
SetActiveSource(CEACellDatatoPointData1)
# ----------------------------------------------------------------

launch_comparison(renderView1, baseline_relative_dir="CEACellDataToPointData")
