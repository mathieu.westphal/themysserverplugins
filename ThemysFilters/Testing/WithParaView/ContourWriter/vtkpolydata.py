"""
This module tests that the ContourWriter filter is functional.

It loads a vtkPolyData then saves this contour using the ContourWriter filter
"""
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 11

from pathlib import Path
from tempfile import NamedTemporaryFile

#### import the simple module from the paraview
from paraview.simple import *

from common import THEMYSSERVERPLUGINS_TESTING_PATH

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

DB_NAME = "PolyData"
# create a new 'XML PolyData Reader'
PolyData = XMLPolyDataReader(
    registrationName=f"{DB_NAME}.vtp",
    FileName=[
        f"{THEMYSSERVERPLUGINS_TESTING_PATH}/ContourWriter/PolyData/{DB_NAME}.vtp"
    ],
)
PolyData.CellArrayStatus = [
    "vtkCellId",
    "vtkDomainId",
    "Milieu:Density",
    "Milieu:Pressure",
    "vtkInterfaceNormal",
    "vtkInterfaceDistance",
    "vtkInterfaceOrder",
    "vtkInterfaceFraction",
]
PolyData.PointArrayStatus = ["vtkNodeId"]

# Properties modified on hDepnTemps_usp0001_000005_1_0vtp
PolyData.TimeArray = "None"

# get active view
renderView1 = GetActiveViewOrCreate("RenderView")

# show data in view
PolyDataDisplay = Show(PolyData, renderView1, "GeometryRepresentation")

# update the view to ensure updated data information
renderView1.Update()

# Path toward reference contour file
CONTOUR_REFERENCE_PATH = Path(
    f"{THEMYSSERVERPLUGINS_TESTING_PATH}/ContourWriter/PolyData/{DB_NAME}_contour.dat"
)

with NamedTemporaryFile(mode="a+", delete=True, suffix=".dat") as result:
    if not result.delete:
        print(f"Temporary file: {result.name}")
    # Save the contour
    SaveData(result.name, proxy=PolyData)

    # Compare the obtained contour with the reference
    with CONTOUR_REFERENCE_PATH.open() as file_ref:
        assert file_ref.readlines() == result.readlines()
