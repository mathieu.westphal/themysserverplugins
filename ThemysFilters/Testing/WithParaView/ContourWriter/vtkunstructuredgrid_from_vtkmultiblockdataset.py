"""
This module tests that the ContourWriter filter is functional.

It loads a vtkMultiBlockDataSet, then extract a block and merge it into an UnstructuredGrid.
Then its saves the contour using the ContourWriter filter
"""
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 11

from pathlib import Path
from tempfile import NamedTemporaryFile

#### import the simple module from the paraview
from paraview.simple import *

from common import THEMYSSERVERPLUGINS_TESTING_PATH

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

BD_NAME = "MBDS"
# create a new 'XML MultiBlock Data Reader'
DataSet = XMLMultiBlockDataReader(
    registrationName=f"{BD_NAME}.vtm",
    FileName=[
        f"{THEMYSSERVERPLUGINS_TESTING_PATH}/ContourWriter/MultiBlockDataSet/{BD_NAME}.vtm"
    ],
)

# get active view
renderView1 = GetActiveViewOrCreate("RenderView")

# show data in view
hDepnTemps_usp0001_000005vtmDisplay = Show(
    DataSet, renderView1, "GeometryRepresentation"
)

# update the view to ensure updated data information
renderView1.Update()

# create a new 'Extract Block'
extractBlock1 = ExtractBlock(registrationName="ExtractBlock1", Input=DataSet)

# Properties modified on extractBlock1
extractBlock1.Selectors = ["/Root/M1/_1NE"]

# show data in view
extractBlock1Display = Show(extractBlock1, renderView1, "GeometryRepresentation")

# hide data in view
Hide(DataSet, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# create a new 'Merge Blocks'
mergeBlocks1 = MergeBlocks(registrationName="MergeBlocks1", Input=extractBlock1)

# show data in view
mergeBlocks1Display = Show(mergeBlocks1, renderView1, "UnstructuredGridRepresentation")

# hide data in view
Hide(extractBlock1, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# Path toward reference contour file
CONTOUR_REFERENCE_PATH = Path(
    f"{THEMYSSERVERPLUGINS_TESTING_PATH}/ContourWriter/PolyData/PolyData_contour.dat"
)

with NamedTemporaryFile(mode="a+", delete=True, suffix=".dat") as result:
    if not result.delete:
        print(f"Temporary file: {result.name}")
    # Save the contour
    SaveData(result.name, proxy=mergeBlocks1)

    # Compare the obtained contour with the reference
    with CONTOUR_REFERENCE_PATH.open() as file_ref:
        assert file_ref.readlines() == result.readlines()
