"""
This module tests that the ContourWriter filter is functional.

It generate an UnstructuredGrid which shape is a simple square then
saves its contour using the ContourWriter filter
"""
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 11

from pathlib import Path
from tempfile import NamedTemporaryFile

#### import the simple module from the paraview
from paraview.simple import *

from common import THEMYSSERVERPLUGINS_TESTING_PATH

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'Unstructured Cell Types'
unstructuredCellTypes1 = UnstructuredCellTypes(
    registrationName="UnstructuredCellTypes1"
)

# Properties modified on unstructuredCellTypes1
unstructuredCellTypes1.CellType = "Quad"
unstructuredCellTypes1.BlocksDimensions = [100, 100, 100]
unstructuredCellTypes1.CellOrder = 5

# get active view
renderView1 = GetActiveViewOrCreate("RenderView")

# show data in view
unstructuredCellTypes1Display = Show(
    unstructuredCellTypes1, renderView1, "UnstructuredGridRepresentation"
)

# update the view to ensure updated data information
renderView1.Update()

# Path toward reference contour file
CONTOUR_REFERENCE_PATH = Path(
    f"{THEMYSSERVERPLUGINS_TESTING_PATH}/ContourWriter/UnstructuredGrid/UnstructuredGrid_contour.dat"
)

with NamedTemporaryFile(mode="a+", delete=True, suffix=".dat") as result:
    if not result.delete:
        print(f"Temporary file: {result.name}")
    # Save the contour
    SaveData(result.name, proxy=unstructuredCellTypes1)

    # Compare the obtained contour with the reference
    with CONTOUR_REFERENCE_PATH.open() as file_ref:
        assert file_ref.readlines() == result.readlines()
