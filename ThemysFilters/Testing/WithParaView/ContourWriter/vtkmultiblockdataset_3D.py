"""
This module tests that the ContourWriter filter is functional.

It loads a vtkMultiBlockDataSet, then its saves the contour using the
ContourWriter filter
"""
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 11

from pathlib import Path
from tempfile import NamedTemporaryFile

#### import the simple module from the paraview
from paraview.simple import *

from common import THEMYSSERVERPLUGINS_TESTING_PATH

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

HDEP_NAME = "MBDS_3D"
# create a new 'XML MultiBlock Data Reader'
MBDS_3D = XMLMultiBlockDataReader(
    registrationName=f"{HDEP_NAME}.vtm",
    FileName=[
        f"{THEMYSSERVERPLUGINS_TESTING_PATH}/ContourWriter/MultiBlockDataSet_3D/{HDEP_NAME}.vtm"
    ],
)

# get active view
renderView1 = GetActiveViewOrCreate("RenderView")

# show data in view
MBDS_3DDisplay = Show(MBDS_3D, renderView1, "GeometryRepresentation")

# update the view to ensure updated data information
renderView1.Update()

# Path toward reference contour file
CONTOUR_REFERENCE_PATH = Path(
    f"{THEMYSSERVERPLUGINS_TESTING_PATH}/ContourWriter/MultiBlockDataSet_3D/{HDEP_NAME}.dat"
)

with NamedTemporaryFile(mode="a+", delete=True, suffix=".dat") as result:
    if not result.delete:
        print(f"Temporary file: {result.name}")
    # Save the contour
    SaveData(result.name, proxy=MBDS_3D)

    # Compare the obtained contour with the reference
    with CONTOUR_REFERENCE_PATH.open() as file_ref:
        assert file_ref.readlines() == result.readlines()
