"""
This module tests that the ContourWriter filter is functional.

It loads a vtkMultiBlockDataSet made of 4 mats (vortex) each of them being made
of 4 blocks (16 blocks), then its saves the contour using the
ContourWriter filter.

Each material is made of 4 blocks because the database is derived from an Hercule
database that has been visualized with 4 pvservers and then saved into .vtm files

This is also the reason why in the obtained contour we can see the frontiers of the
4 subdomains even if paraview is launched just as a client (no pvservers).

To remove those frontiers we would need to use the "GhostCellGenerator" filter even
if parallelism is not used (paraview used as a client).
"""
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 11

from pathlib import Path
from tempfile import NamedTemporaryFile

#### import the simple module from the paraview
from paraview.simple import *

from common import THEMYSSERVERPLUGINS_TESTING_PATH

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

HDEP_NAME = "4mats_vortex_8procs"
# create a new 'XML MultiBlock Data Reader'
a4mats_vortex_8procsvtm = XMLMultiBlockDataReader(
    registrationName=f"{HDEP_NAME}.vtm",
    FileName=[
        f"{THEMYSSERVERPLUGINS_TESTING_PATH}/ContourWriter/4MatsVortex_8procs/4mats_vortex_8procs.vtm"
    ],
)

# get animation scene
animationScene1 = GetAnimationScene()

# update animation scene based on data timesteps
animationScene1.UpdateAnimationUsingDataTimeSteps()

# get active view
renderView1 = GetActiveViewOrCreate("RenderView")

# show data in view
a4mats_vortex_8procsvtmDisplay = Show(
    a4mats_vortex_8procsvtm, renderView1, "GeometryRepresentation"
)

# trace defaults for the display properties.
a4mats_vortex_8procsvtmDisplay.Representation = "Surface"

# reset view to fit data
renderView1.ResetCamera(False, 0.9)

# update the view to ensure updated data information
renderView1.Update()

# set scalar coloring
ColorBy(a4mats_vortex_8procsvtmDisplay, ("FIELD", "vtkBlockColors"))

# show color bar/color legend
a4mats_vortex_8procsvtmDisplay.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'vtkBlockColors'
vtkBlockColorsLUT = GetColorTransferFunction("vtkBlockColors")

# get opacity transfer function/opacity map for 'vtkBlockColors'
vtkBlockColorsPWF = GetOpacityTransferFunction("vtkBlockColors")

# get 2D transfer function for 'vtkBlockColors'
vtkBlockColorsTF2D = GetTransferFunction2D("vtkBlockColors")

# Path toward reference contour file
CONTOUR_REFERENCE_PATH = Path(
    f"{THEMYSSERVERPLUGINS_TESTING_PATH}/ContourWriter/4MatsVortex_8procs/{HDEP_NAME}.dat"
)

with NamedTemporaryFile(mode="a+", delete=True, suffix=".dat") as result:
    if not result.delete:
        print(f"Temporary file: {result.name}")
    # Save the contour
    SaveData(result.name, proxy=a4mats_vortex_8procsvtm)

    # Compare the obtained contour with the reference
    with CONTOUR_REFERENCE_PATH.open() as file_ref:
        assert file_ref.readlines() == result.readlines()
