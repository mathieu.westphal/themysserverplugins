# Registers the pvpython (python) tests
if(${PARAVIEW_USE_PYTHON})
  set(PythonTestNames donut_2D_YZ shell_3D)

  # Necessary so that the common module is found. Necessitates also to copy each module test in the same directory
  set(_vtk_build_TEST_FILE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
  configure_file(${CMAKE_CURRENT_SOURCE_DIR}/../common.py.in ${_vtk_build_TEST_FILE_DIRECTORY}/common.py)

  foreach(test_name ${PythonTestNames})
    register_paraview_python_test(${test_name})
  endforeach()
endif()
