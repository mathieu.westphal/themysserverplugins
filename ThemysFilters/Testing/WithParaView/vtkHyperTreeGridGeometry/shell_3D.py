"""
This test check that the vtkHyperTreeGridGeometry filters is functional when
working on a 3D HTG base.
It opens a 3D HTG base and applies the filter and then ensures the result is
a vtkPolyData
"""
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 11

#### import the simple module from the paraview
from paraview.simple import *

from paraview import servermanager as sm
from paraview.vtk.numpy_interface import dataset_adapter as dsa

from common import THEMYSSERVERPLUGINS_TESTING_PATH, launch_comparison

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

HTG_NAME = "shell"
# create a new 'HyperTreeGrid Reader'
shellhtg = HyperTreeGridReader(
    registrationName=f"{HTG_NAME}.htg",
    FileNames=[
        f"{THEMYSSERVERPLUGINS_TESTING_PATH}/vtkHyperTreeGridGeometry/{HTG_NAME}.htg"
    ],
)

# get active view
renderView1 = GetActiveViewOrCreate("RenderView")

# show data in view
shellhtgDisplay = Show(shellhtg, renderView1, "HyperTreeGridRepresentation")

# trace defaults for the display properties.
shellhtgDisplay.Representation = "Surface"

# reset view to fit data
renderView1.ResetCamera(False, 0.9)

# update the view to ensure updated data information
renderView1.Update()

# set scalar coloring
ColorBy(shellhtgDisplay, ("CELLS", "level"))

# rescale color and/or opacity maps used to include current data range
shellhtgDisplay.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
shellhtgDisplay.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'level'
levelLUT = GetColorTransferFunction("level")

# get opacity transfer function/opacity map for 'level'
levelPWF = GetOpacityTransferFunction("level")

# get 2D transfer function for 'level'
levelTF2D = GetTransferFunction2D("level")

# create a new 'vtk HTG Geometry'
vtkHTGGeometry1 = HyperTreeGridGeometryFilter(
    registrationName="vtkHTGGeometry1", Input=shellhtg
)

# show data in view
vtkHTGGeometry1Display = Show(vtkHTGGeometry1, renderView1, "GeometryRepresentation")

# trace defaults for the display properties.
vtkHTGGeometry1Display.Representation = "Surface"

# hide data in view
Hide(shellhtg, renderView1)

# show color bar/color legend
vtkHTGGeometry1Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# ================================================================
# addendum: following script captures some of the application
# state to faithfully reproduce the visualization during playback
# ================================================================

# get layout
layout1 = GetLayout()

# --------------------------------
# saving layout sizes for layouts

# layout/tab size in pixels
layout1.SetSize(2178, 1144)

# -----------------------------------
# saving camera placements for views

# current camera placement for renderView1
renderView1.CameraPosition = [
    -4.021940800231706,
    2.891637821917155,
    0.08365747991871222,
]
renderView1.CameraFocalPoint = [
    0.9999999999999999,
    -8.542861893107945e-17,
    -1.9342328814584027e-17,
]
renderView1.CameraViewUp = [
    -0.20758581082526945,
    -0.33391590956431394,
    -0.9194608727313264,
]
renderView1.CameraParallelScale = 1.5

launch_comparison(renderView1, baseline_relative_dir="vtkHyperTreeGridGeometry")

result = dsa.WrapDataObject(sm.Fetch(vtkHTGGeometry1))
assert result.GetClassName() == "vtkPolyData"
