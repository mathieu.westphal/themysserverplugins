"""
This python module tests the interface visualization
(MaterialFilters) in the case of a unique cell
containing three materials (NE, NW, SE) separated by two interfaces.
Only material SE is displayed
"""
from common import run

DB_NAME = "ThreeMatsTwoInterfaces.vtm"
DB_PATH = "Data/Testing/MaterialFilters/SingleCell/" + DB_NAME
SL_BLOCK = "_3SE"
CAM_POS = [0.5099999904632568, 0.4699999988079071, 10000.0]
CAM_FP = [0.5099999904632568, 0.4699999988079071, 0.0]
CAM_PAR_SCALE = 0.014142122136739427


def interface_filter_configuration(material_interface_filter):
    material_interface_filter.FillMaterialOn = 1


run(
    DB_PATH,
    DB_NAME,
    SL_BLOCK,
    CAM_POS,
    CAM_FP,
    CAM_PAR_SCALE,
    interface_filter_configuration,
)
