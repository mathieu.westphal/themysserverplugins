"""
This python module tests the interface visualization
(MaterialFilters) in the case of a unique cell
containing three materials (NE, NW, SE) separated by two interfaces.
Only material NW is displayed,
included the comparison of the list of fields available under cell and their ranges
"""
from paraview import servermanager as sm
from paraview.vtk.numpy_interface import dataset_adapter as dsa

from common import run

DB_NAME = "ThreeMatsTwoInterfaces_PedigreeAndGlobalIds.vtm"
DB_PATH = "Data/Testing/MaterialFilters/SingleCell/" + DB_NAME
SL_BLOCK = "_2NW"
CAM_POS = [0.5099999904632568, 0.4699999988079071, 10000.0]
CAM_FP = [0.5099999904632568, 0.4699999988079071, 0.0]
CAM_PAR_SCALE = 0.014142122136739427
expected_names_celldata = [
    "Milieu:Density",
    "Milieu:DomainUniqueId",
    "Milieu:InternalEnergy",
    "Milieu:Mass",
    "Milieu:Pressure",
    "Milieu:Volume",
    "Milieu:VolumicFraction",
    "vtkCellId",
    "vtkDomainId",
    "vtkInterfaceDistance",
    "vtkInterfaceFraction",
    "vtkInterfaceNormal",
    "vtkInterfaceOrder",
    "vtkMaterialId",
]
offset_material = 2
expected_range_celldata_material = (
    (0.9390341780174376, 0.9390341780174376),
    (0.0, 0.0),
    (2.79895660979887, 2.79895660979887),
    (0.0001016052620825488, 0.0001016052620825488),
    (1.0513263677555822, 1.0513263677555822),
    (0.00010820187854830351, 0.00010820187854830351),
    (0.2705046963707591, 0.2705046963707591),
    (1175.0, 1175.0),
    (0.0, 0.0),
    (-0.6129764347324197, -0.6129764347324197),
    (0.2705046963707591, 0.2705046963707591),
    (0.3455646020734835, 0.3455646020734835),
    (0.0, 0.0),
    (3.0, 3.0),
)


def db_asserter(database_vtm):
    # Step database_vtm
    database = dsa.WrapDataObject(sm.Fetch(database_vtm))
    for block in database:
        cd = block.CellData
        if "vtkCellId" in expected_names_celldata:
            assert cd.GetPedigreeIds()
            assert cd.GetPedigreeIds().GetName() == "vtkCellId"
        else:
            if cd.GetPedigreeIds():
                print("Not expected PedigreeIds: ", cd.GetPedigreeIds().GetName())
            else:
                print("No PedigreeIds!")
        if "vtkInternalGlobalCellId" in expected_names_celldata:
            assert cd.GetGlobalIds()
            assert cd.GetGlobalIds().GetName() == "vtkInternalGlobalCellId"
        else:
            if cd.GetGlobalIds():
                print("Not expected GlobalIds: ", cd.GetGlobalIds().GetName())
            else:
                print("No GlobalIds!")


def filter_asserter(filter):
    # Step material_interface
    material_interface = dsa.WrapDataObject(sm.Fetch(filter))
    for block in material_interface:
        cd = block.CellData
        if "vtkCellId" in expected_names_celldata:
            assert cd.GetPedigreeIds()
            assert cd.GetPedigreeIds().GetName() == "vtkCellId"
        else:
            if cd.GetPedigreeIds():
                print("Not expected PedigreeIds: ", cd.GetPedigreeIds().GetName())
            else:
                print("No PedigreeIds!")
        assert cd.GetGlobalIds() is None
    # Step names cell data
    if material_interface.CellData.keys() == expected_names_celldata:
        assert material_interface.CellData.keys() == expected_names_celldata
    else:
        print("Not expected names cell data! ", material_interface.CellData.keys())
    # Step ranges cell data
    for name_celldata, range_celldata in zip(
        expected_names_celldata, expected_range_celldata_material
    ):
        assert material_interface.CellData[name_celldata].Arrays[
            offset_material
        ].GetRange() == tuple(range_celldata)


# Check the consistency of the expected parameters
assert len(expected_names_celldata) == len(expected_range_celldata_material)
run(
    DB_PATH,
    DB_NAME,
    SL_BLOCK,
    CAM_POS,
    CAM_FP,
    CAM_PAR_SCALE,
    database_asserter=db_asserter,
    interface_filter_asserter=filter_asserter,
)
