"""
This python module test the fact that the vtkMaterialInterface plugin
passes the field data from input to output
"""
from paraview.simple import GetActiveSource
from common import load_data_and_apply_filter, set_camera_up

DB_NAME = "HDep-n=Temps_u=s.p-0001_000005.vtm"
DB_PATH = "Data/Testing/MaterialFilters/4MatsVortex/" + DB_NAME
SL_BLOCK = None
CAM_POS = [0.5, 0.5, 10000.0]
CAM_FP = [0.5, 0.5, 0.0]
CAM_PAR_SCALE = 0.7071067811865476
RENDER_VIEW, _, _ = load_data_and_apply_filter(DB_PATH, DB_NAME, SL_BLOCK)
set_camera_up(RENDER_VIEW, CAM_POS, CAM_FP, CAM_PAR_SCALE)

FD_INFOS_BIS = GetActiveSource().GetFieldDataInformation()
VTKMATERIALIDBIS = FD_INFOS_BIS["vtkMaterialId"]
assert VTKMATERIALIDBIS is not None
