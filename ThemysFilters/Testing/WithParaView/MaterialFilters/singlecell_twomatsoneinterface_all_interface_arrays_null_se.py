"""
This python module tests the interface visualization
(MaterialFilters) in the case of a unique cell
containing two materials (SE, SW) separated by one interface.
All materials are lacking the three interface arrays (order, distance, normale).
This may occur when using PV in // on a database resulting from // computation.
One (or more) server may have materials that have no interface arrays while
their neighbours have them.
In this case the output material mesh has to be a copy (shallow) of the input
material mesh to avoid Emmental aspect after applying the filter.
This test display the SE block.
"""
from common import run

DB_NAME = "TwoMatsOneInterface.vtm"
DB_PATH = (
    "Data/Testing/MaterialFilters/SingleCell_WrongData/MatWithAllInterfaceQuantitiesNull/"
    + DB_NAME
)
SL_BLOCK = "_3SE"
CAM_POS = [0.17000000178813934, 0.45000000298023224, 10000.0]
CAM_FP = [0.17000000178813934, 0.45000000298023224, 0.0]
CAM_PARA_SCALE = 0.014142143210163683


def interface_filter_configuration(material_interface_filter):
    material_interface_filter.FillMaterialOn = 1
    material_interface_filter.NormalArray = "vtkInterfaceNormal"
    material_interface_filter.DistanceArray = "vtkInterfaceDistance"
    material_interface_filter.OrderArray = "vtkInterfaceOrder"


run(
    DB_PATH,
    DB_NAME,
    SL_BLOCK,
    CAM_POS,
    CAM_FP,
    CAM_PARA_SCALE,
    interface_filter_configuration,
)
