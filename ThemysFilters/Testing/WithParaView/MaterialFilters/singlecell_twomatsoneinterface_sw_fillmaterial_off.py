"""
This python module tests the interface visualization
(MaterialFilters) in the case of a unique cell
containing two materials (SE, SW) separated by one interface.
Only material SW is displayed
"""
from common import run

DB_NAME = "TwoMatsOneInterface.vtm"
DB_PATH = "Data/Testing/MaterialFilters/SingleCell/" + DB_NAME
SL_BLOCK = "_4SW"
CAM_POS = [0.17000000178813934, 0.45000000298023224, 10000.0]
CAM_FP = [0.17000000178813934, 0.45000000298023224, 0.0]
CAM_PAR_SCALE = 0.014142143210163683


def interface_filter_configuration(material_interface_filter):
    material_interface_filter.FillMaterialOn = 0


def interface_display_configuration(material_interface_display):
    material_interface_display.LineWidth = 5.0


run(
    DB_PATH,
    DB_NAME,
    SL_BLOCK,
    CAM_POS,
    CAM_FP,
    CAM_PAR_SCALE,
    interface_filter_configuration,
    interface_display_configuration,
)
