"""
This python module tests the interface visualization
(MaterialFilters) in the case of a unique cell
containing two materials (SE, SW) separated by one interface.
Only material NE is displayed. This material is splitted into
three triangles
"""
from common import run

DB_NAME = "TwoMatsThreeInterfaces.vtm"
DB_PATH = "Data/Testing/MaterialFilters/SingleCell_1/" + DB_NAME
SL_BLOCK = "_4SW"
CAM_POS = [0.029999999329447746, 0.26999999582767487, 0.05464103019658135]
CAM_FP = [0.029999999329447746, 0.26999999582767487, 0.0]
CAM_PARA_SCALE = 0.014142139258897186


def interface_filter_configuration(material_interface_filter):
    material_interface_filter.FillMaterialOn = 1


run(
    DB_PATH,
    DB_NAME,
    SL_BLOCK,
    CAM_POS,
    CAM_FP,
    CAM_PARA_SCALE,
    interface_filter_configuration,
)
