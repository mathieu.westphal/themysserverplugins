"""
This test checks the vtkMaterialInterface filter behavior, using global algorithm
and with clip output (FillMaterial=On)
Identical to MaterialFilters_vtkMaterialInterface_4MatsVortex.xml but checks also
the type of each block in the MBDS before and after the filter application.
Identical to materials_vtkmaterialinterface_4matsvortex.py but use a database with
node fields in it.
"""
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 11

#### import the simple module from the paraview
from paraview.simple import *
from paraview import servermanager as sm
from paraview.vtk.numpy_interface import dataset_adapter as dsa

from common import CMAKE_SOURCE_DIR, launch_comparison

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'XML MultiBlock Data Reader'
hDepnTemps_usp0001_000005vtm = XMLMultiBlockDataReader(
    registrationName="HDep-n=Temps_u=s.p-0001_000005.vtm",
    FileName=[
        f"{CMAKE_SOURCE_DIR}/Data/Testing/MaterialFilters/4MatsVortexWithNodeField/4MatsVortexWithNodeField.vtm"
    ],
)

database = dsa.WrapDataObject(sm.Fetch(hDepnTemps_usp0001_000005vtm))
for block in database:
    assert block.GetClassName() == "vtkPolyData"

# get active view
renderView1 = GetActiveViewOrCreate("RenderView")

# show data in view
hDepnTemps_usp0001_000005vtmDisplay = Show(
    hDepnTemps_usp0001_000005vtm, renderView1, "GeometryRepresentation"
)

# reset view to fit data
renderView1.ResetCamera(False)

# update the view to ensure updated data information
renderView1.Update()

# create a new 'Material Interface (LOVE)'
materialInterfaceLOVE1 = MaterialInterfaceLOVE(
    registrationName="MaterialInterfaceLOVE1", Input=hDepnTemps_usp0001_000005vtm
)
materialInterfaceLOVE1.MaskArray = ["POINTS", "None"]
materialInterfaceLOVE1.NormalArray2 = ["POINTS", "None"]
materialInterfaceLOVE1.DistanceArray2 = ["POINTS", "None"]
materialInterfaceLOVE1.FillMaterialOn = 1  # Clip method

# show data in view
materialInterfaceLOVE1Display = Show(
    materialInterfaceLOVE1, renderView1, "GeometryRepresentation"
)

# trace defaults for the display properties.
materialInterfaceLOVE1Display.Representation = "Surface"
materialInterfaceLOVE1Display.ColorArrayName = [None, ""]
materialInterfaceLOVE1Display.SelectTCoordArray = "None"
materialInterfaceLOVE1Display.SelectNormalArray = "None"
materialInterfaceLOVE1Display.SelectTangentArray = "None"
materialInterfaceLOVE1Display.OSPRayScaleFunction = "PiecewiseFunction"
materialInterfaceLOVE1Display.SelectOrientationVectors = "None"
materialInterfaceLOVE1Display.ScaleFactor = 0.1
materialInterfaceLOVE1Display.SelectScaleArray = "None"
materialInterfaceLOVE1Display.GlyphType = "Arrow"
materialInterfaceLOVE1Display.GlyphTableIndexArray = "None"
materialInterfaceLOVE1Display.GaussianRadius = 0.005
materialInterfaceLOVE1Display.SetScaleArray = [None, ""]
materialInterfaceLOVE1Display.ScaleTransferFunction = "PiecewiseFunction"
materialInterfaceLOVE1Display.OpacityArray = [None, ""]
materialInterfaceLOVE1Display.OpacityTransferFunction = "PiecewiseFunction"
materialInterfaceLOVE1Display.DataAxesGrid = "GridAxesRepresentation"
materialInterfaceLOVE1Display.PolarAxes = "PolarAxesRepresentation"
materialInterfaceLOVE1Display.SelectInputVectors = [None, ""]
materialInterfaceLOVE1Display.WriteLog = ""

# hide data in view
Hide(hDepnTemps_usp0001_000005vtm, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# set scalar coloring
ColorBy(materialInterfaceLOVE1Display, ("POINTS", "Noeud:NodeVelocity", "Magnitude"))

# show color bar/color legend
materialInterfaceLOVE1Display.SetScalarBarVisibility(renderView1, True)

# Properties modified on materialInterfaceLOVE1Display
materialInterfaceLOVE1Display.PointSize = 5.0

# Properties modified on materialInterfaceLOVE1Display
materialInterfaceLOVE1Display.LineWidth = 5.0

# Properties modified on materialInterfaceLOVE1Display
materialInterfaceLOVE1Display.BlockSelectors = [
    "/Root/M1/_1NE",
    "/Root/M1/_2NW",
    "/Root/M1/_3SE",
    "/Root/M1/_4SW",
]

# get layout
layout1 = GetLayout()

# layout/tab size in pixels
layout1.SetSize(1482, 867)

# current camera placement for renderView1
renderView1.InteractionMode = "2D"
renderView1.CameraPosition = [0.5, 0.5, 10000.0]
renderView1.CameraFocalPoint = [0.5, 0.5, 0.0]
renderView1.CameraParallelScale = 0.7071067811865476

# With Clip method the output meshes types should be the same as those in input
filter = dsa.WrapDataObject(sm.Fetch(materialInterfaceLOVE1))
for block in filter:
    assert block.GetClassName() == "vtkPolyData"

launch_comparison(renderView1, baseline_relative_dir="MaterialFilters")
