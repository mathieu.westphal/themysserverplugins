# Registers the pvpython (python) tests
if(${PARAVIEW_USE_PYTHON})
  set(PythonTestNames ceareaderdat ceareaderdat_2d educational_4matvortex_8procs)

  # Necessary so that the common module is found. Necessitates also to copy each module test in the same directory
  set(_vtk_build_TEST_FILE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
  configure_file(${CMAKE_CURRENT_SOURCE_DIR}/../common.py.in ${_vtk_build_TEST_FILE_DIRECTORY}/common.py)

  foreach(test_name ${PythonTestNames})
    register_paraview_python_test(${test_name})
    set_tests_properties(
      "Python-${test_name}"
      PROPERTIES
        ENVIRONMENT
        "PARAVIEW_DATA_ROOT=${CMAKE_SOURCE_DIR}/Plugin;PV_PLUGIN_PATH=$<TARGET_FILE_DIR:ThemysFilters>\;${CMAKE_CURRENT_BINARY_DIR}/../../../PluginsPython"
        FIXTURES_REQUIRED
        "${test_name}_fixture"
    )
  endforeach()

endif()
