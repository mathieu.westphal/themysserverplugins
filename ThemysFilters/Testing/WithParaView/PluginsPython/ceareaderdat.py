"""
This module tests the CEAReaderDat filter.

It opens a previously saved contour and compare the image obtained to the reference
The saved contour has 3 coordinates per point so the Z Epsilon property is not tested
"""
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 11

#### import the simple module from the paraview
from paraview.simple import *
from paraview.vtk.test import Testing

from common import THEMYSSERVERPLUGINS_TESTING_PATH, launch_comparison

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

CONTOUR_FILENAME = "PolyData_contour"
# create a new 'CEA Reader DAT (Python)'
CONTOUR = CEAReaderDATPython(
    registrationName=f"{CONTOUR_FILENAME}.dat",
    FileName=f"{THEMYSSERVERPLUGINS_TESTING_PATH}/ContourWriter/PolyData/{CONTOUR_FILENAME}.dat",
)

# get active view
renderView1 = GetActiveViewOrCreate("RenderView")

# show data in view
CONTOURDisplay = Show(CONTOUR, renderView1, "GeometryRepresentation")

# trace defaults for the display properties.
CONTOURDisplay.Representation = "Surface"
CONTOURDisplay.ColorArrayName = [None, ""]
CONTOURDisplay.SelectTCoordArray = "None"
CONTOURDisplay.SelectNormalArray = "None"
CONTOURDisplay.SelectTangentArray = "None"
CONTOURDisplay.OSPRayScaleFunction = "PiecewiseFunction"
CONTOURDisplay.SelectOrientationVectors = "None"
CONTOURDisplay.ScaleFactor = 0.1
CONTOURDisplay.SelectScaleArray = "None"
CONTOURDisplay.GlyphType = "Arrow"
CONTOURDisplay.GlyphTableIndexArray = "None"
CONTOURDisplay.GaussianRadius = 0.005
CONTOURDisplay.SetScaleArray = [None, ""]
CONTOURDisplay.ScaleTransferFunction = "PiecewiseFunction"
CONTOURDisplay.OpacityArray = [None, ""]
CONTOURDisplay.OpacityTransferFunction = "PiecewiseFunction"
CONTOURDisplay.DataAxesGrid = "GridAxesRepresentation"
CONTOURDisplay.PolarAxes = "PolarAxesRepresentation"
CONTOURDisplay.SelectInputVectors = [None, ""]
CONTOURDisplay.WriteLog = ""

# reset view to fit data
renderView1.ResetCamera(False)

# changing interaction mode based on data extents
renderView1.InteractionMode = "2D"
renderView1.CameraPosition = [0.6500000059604645, 0.5, 10000.0]
renderView1.CameraFocalPoint = [0.6500000059604645, 0.5, 0.0]

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on hDepnTemps_usp0001_000005_1_0_contourdatDisplay
CONTOURDisplay.LineWidth = 4.0

# ================================================================
# addendum: following script captures some of the application
# state to faithfully reproduce the visualization during playback
# ================================================================

# get layout
layout1 = GetLayout()

# --------------------------------
# saving layout sizes for layouts

# layout/tab size in pixels
layout1.SetSize(1374, 693)

# -----------------------------------
# saving camera placements for views

# current camera placement for renderView1
renderView1.InteractionMode = "2D"
renderView1.CameraPosition = [0.6500000059604645, 0.5, 10000.0]
renderView1.CameraFocalPoint = [0.6500000059604645, 0.5, 0.0]
renderView1.CameraParallelScale = 0.6103277773685832

launch_comparison(renderView1, baseline_relative_dir="PluginsPython")
