"""
This test checks the HyperTreeGridGenerateMaskLeavesCells filter on a 2D base, with only
one material and one subdomain.
"""
# trace generated using paraview version 5.11.1-1280-gd982f83420
# import paraview
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 11

#### import the simple module from the paraview
from paraview.simple import *

from common import CMAKE_SOURCE_DIR, launch_comparison

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'XML MultiBlock Data Reader'
vtm_basename = "armen3D_thresholdRho10"
data = XMLMultiBlockDataReader(
    registrationName=f"{vtm_basename}.vtm",
    FileName=f"{CMAKE_SOURCE_DIR}/Data/Testing/HyperTreeGridFragmentation/3D/{vtm_basename}.vtm",
)

assert "rho" in data.CellData

dataGMLC = CEAHyperTreeGridGenerateMaskLeavesCells(
    registrationName="dataGMLC", Input=data
)

assert "rho" in dataGMLC.CellData
assert "vtkValidCell" in dataGMLC.CellData
assert "vtkVolume" in dataGMLC.CellData

# SumWrong
dataPCWrongSum = PythonCalculator(registrationName="dataPCWrongSum", Input=dataGMLC)
dataPCWrongSum.Expression = "sum(rho)"
dataPCWrongSum.ArrayName = "WrongSumRho"

assert "rho" in dataPCWrongSum.CellData
assert "vtkValidCell" in dataPCWrongSum.CellData
assert "vtkVolume" in dataPCWrongSum.CellData
assert "WrongSumRho" in dataPCWrongSum.CellData

# SumValid
dataPCValidSum = PythonCalculator(
    registrationName="dataPCValidSum", Input=dataPCWrongSum
)
dataPCValidSum.Expression = "sum(rho*vtkValidCell)"
dataPCValidSum.ArrayName = "ValidSumRho"

assert "rho" in dataPCValidSum.CellData
assert "vtkValidCell" in dataPCValidSum.CellData
assert "vtkVolume" in dataPCValidSum.CellData
assert "WrongSumRho" in dataPCValidSum.CellData
if "ValidSumRho" not in dataPCValidSum.CellData:
    print("WARNING Exclusive execution parallel bi-servers")
    exit()
assert "ValidSumRho" in dataPCValidSum.CellData

# AvgWrong
dataPCWrongAvg = PythonCalculator(
    registrationName="dataPCWrongAvg", Input=dataPCValidSum
)
dataPCWrongAvg.Expression = "sum(rho*vtkVolume)/sum(vtkVolume)"
dataPCWrongAvg.ArrayName = "WrongAvgRho"

assert "rho" in dataPCWrongAvg.CellData
assert "vtkValidCell" in dataPCWrongAvg.CellData
assert "vtkVolume" in dataPCWrongAvg.CellData
assert "WrongSumRho" in dataPCWrongAvg.CellData
assert "ValidSumRho" in dataPCWrongAvg.CellData
assert "WrongAvgRho" in dataPCWrongAvg.CellData

# AvgValid
dataPCValidAvg = PythonCalculator(
    registrationName="dataPCValidAvg", Input=dataPCWrongAvg
)
dataPCValidAvg.Expression = (
    "sum(rho*vtkVolume*vtkValidCell)/sum(vtkVolume*vtkValidCell)"
)
dataPCValidAvg.ArrayName = "ValidAvgRho"

assert "rho" in dataPCValidAvg.CellData
assert "vtkValidCell" in dataPCValidAvg.CellData
assert "vtkVolume" in dataPCValidAvg.CellData
assert "WrongSumRho" in dataPCValidAvg.CellData
assert "ValidSumRho" in dataPCValidAvg.CellData
assert "WrongAvgRho" in dataPCValidAvg.CellData
assert "ValidAvgRho" in dataPCValidAvg.CellData

last = dataPCValidAvg

renderView1 = CreateView("RenderView")

Show(last, renderView1, "HyperTreeGridRepresentation")

import math

block = last

assert "WrongSumRho" in block.CellData
assert "ValidSumRho" in block.CellData
assert "WrongAvgRho" in block.CellData
assert "ValidAvgRho" in block.CellData

valW = block.CellData["WrongSumRho"].GetRange()[0]
print("WrongSumRho", valW)
# seq: 1457999.5006462706
# //2: 1457999.5006462706
assert math.fabs(valW - 1457999.5) < 1e-1
valV = block.CellData["ValidSumRho"].GetRange()[0]
print("ValidSumRho", valV)
assert math.fabs(valV - 274999.6934308876) < 1e-7
if valW < valV:
    print(
        'Inconsistent values ​​because the "BAD VALUE" ',
        valW,
        ' must be greater than or equal to the "GOOD VALUE" ',
        valV,
    )
    assert valW >= valV
print("WrongSumRho/ValidSumRho", valW / valV)
# seq: 5.30182238177934
# //2: 5.3018222764408
assert math.fabs(valW / valV - 5.301822) < 1e-6
assert valW / valV >= 1 + 1 / 8  # 8 = f^d, f=2, d=3

valAvgW = block.CellData["WrongAvgRho"].GetRange()[0]
print("WrongAvgRho", valAvgW)
# seq: 999.9998267206645
# //2: 999.999821562533
assert math.fabs(valAvgW - 999.99982) < 1e-5
valAvgV = block.CellData["ValidAvgRho"].GetRange()[0]
print("ValidAvgRho", valAvgV)
assert math.fabs(valAvgV - 999.9988852032276) < 1e-7

##--------------------------------------------
## You may need to add some code at the end of this python script depending on your usage, eg:
#
## Render all views to see them appears
# RenderAllViews()
#
## Interact with the view, usefull when running from pvpython
# Interact()
#
## Save a screenshot of the active view
# SaveScreenshot("path/to/screenshot.png")
#
## Save a screenshot of a layout (multiple splitted view)
# SaveScreenshot("path/to/screenshot.png", GetLayout())
#
## Save all "Extractors" from the pipeline browser
# SaveExtracts()
#
## Save a animation of the current active view
# SaveAnimation()
#
## Please refer to the documentation of paraview.simple
## https://kitware.github.io/paraview-docs/latest/python/paraview.simple.html
##--------------------------------------------
