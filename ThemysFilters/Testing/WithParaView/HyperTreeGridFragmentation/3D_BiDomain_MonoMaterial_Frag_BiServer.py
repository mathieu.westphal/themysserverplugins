"""
This test checks the HyperTreeGridFragmentation filter on a 3D base, with only
one material and two subdomain.
The sequential execution produces a ERR cause no equivalent "mono-domain" on
each server.
"""
# trace generated using paraview version 5.11.1-1280-gd982f83420
# import paraview
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 11

#### import the simple module from the paraview
from paraview.simple import *

from common import CMAKE_SOURCE_DIR, launch_comparison

# ----------------------------------------------------------------
from mpi4py import MPI

comm = MPI.COMM_WORLD
nprocs = comm.Get_size()
# ----------------------------------------------------------------

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'XML MultiBlock Data Reader'
vtm_basename = "armen3D_thresholdRho10"
data = XMLMultiBlockDataReader(
    registrationName=f"{vtm_basename}.vtm",
    FileName=f"{CMAKE_SOURCE_DIR}/Data/Testing/HyperTreeGridFragmentation/3D/{vtm_basename}.vtm",
)

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# Create a new 'Render View'
renderView1 = CreateView("RenderView")
renderView1.ViewSize = [800, 600]
renderView1.AxesGrid = "Grid Axes 3D Actor"
renderView1.CenterOfRotation = [2e-6, 3e-5, 2.8e-05]
renderView1.StereoType = "Crystal Eyes"
renderView1.CameraPosition = [-1e-4, 1.5e-4, 1.5e-4]
renderView1.CameraFocalPoint = [0, 0, 0]
renderView1.CameraViewUp = [0.4, 0, -0.3708818076863428]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 6.782329972706415e-05
renderView1.LegendGrid = "Legend Grid Actor"

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name="Layout #1")
layout1.AssignView(0, renderView1)
layout1.SetSize(800, 600)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------
Show(data, renderView1, "HyperTreeGridRepresentation")
Hide(data, renderView1)

# create a new 'CEA Hyper Tree Grid - Ghost Cells Generator Filter'
dataFrag = CEAFragmentationFilter(registrationName="dataFrag", Input=data)

# Properties modified on cEAFragmentationFilter1
dataFrag.ExtractType = "Edge Extraction"  # 1 # EXTRACT_EDGE_FRAGMENT
dataFrag.ExtractWithGlobalFields = 0
dataFrag.UseMass = 0  # if 1 then cEAFragmentationFilter1.Mass = "..."
dataFrag.UseDensity = 1
dataFrag.Density = "rho"
dataFrag.UseVelocity = 1
dataFrag.Velocity = "Vitesse"

import math

assert "Pounds" in dataFrag.PointData
pounds = dataFrag.PointData["Pounds"].GetRange()[1]
print("pounds", pounds)
assert pounds == 94

assert "Volume" in dataFrag.PointData
volume = dataFrag.PointData["Volume"].GetRange()[1]
relatif = math.fabs((volume - 6.016e-15) / 6.016e-15)
print("Volume", volume, relatif)
assert relatif < 1e-7

assert "FormFactorRadius" in dataFrag.PointData
form = dataFrag.PointData["FormFactorRadius"].GetRange()[1]
relatif = math.fabs((form - 1.128252763904278e-05) / 1.128252763904278e-05)
print("FormFactorRadius", form, relatif)
assert relatif < 1e-7

assert "AvgCenter" in dataFrag.PointData
avgcenter = dataFrag.PointData["AvgCenter"].GetRange()[1]
relatif = math.fabs((avgcenter - 4.6e-05) / 4.6e-05)
print("AvgCenter", avgcenter, relatif)
assert relatif < 1e-7

if "Mass" in dataFrag.PointData:
    mass = dataFrag.PointData["Mass"].GetRange()[1]
    relatif = math.fabs((mass - 6.0159918e-12) / 6.0159918e-12)
    print("Mass", mass, relatif)
    assert relatif < 1e-7

    assert "Density" in dataFrag.PointData
    density = dataFrag.PointData["Density"].GetRange()[1]
    relatif = math.fabs((density - 999.9995) / 999.9995)
    print("Density", density, relatif)
    assert relatif < 1e-7

    assert "Barycenter" in dataFrag.PointData
    barycenter = dataFrag.PointData["Barycenter"].GetRange()[1]
    relatif = math.fabs((barycenter - 4.6e-5) / 4.6e-5)
    print("Barycenter", barycenter, relatif)
    assert relatif < 1e-7

if "AvgVelocity" in dataFrag.PointData:
    avgvelo = dataFrag.PointData["AvgVelocity"].GetRange()[1]
    relatif = math.fabs((avgvelo - 0.72124775) / 0.72124775)
    print("AvgVelocity", avgvelo, relatif)
    assert relatif < 1e-7

    if "Mass" in dataFrag.PointData:
        assert "Velocity" in dataFrag.PointData
        velo = dataFrag.PointData["Velocity"].GetRange()[1]
        relatif = math.fabs((velo - 0.72124775) / 0.72124775)
        print("Velocity", velo, relatif)
        assert relatif < 1e-7

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# get 2D transfer function for 'FragmentId'
FragmentIdTF2D = GetTransferFunction2D("FragmentId")

# get color transfer function/color map for 'FragmentId'
FragmentIdLUT = GetColorTransferFunction("FragmentId")
FragmentIdLUT.TransferFunction2D = FragmentIdTF2D
FragmentIdLUT.RGBPoints = [
    0.0,
    0.231373,
    0.298039,
    0.752941,
    0.5001220703125,
    0.865003,
    0.865003,
    0.865003,
    1.000244140625,
    0.705882,
    0.0156863,
    0.14902,
]
FragmentIdLUT.ScalarRangeInitialized = 1.0

# show data from dataFrag
dataFragDisplay = Show(dataFrag, renderView1, "HyperTreeGridRepresentation")

# trace defaults for the display properties.
dataFragDisplay.Representation = "Surface"
dataFragDisplay.ColorArrayName = ["CELLS", "FragmentId"]
dataFragDisplay.LookupTable = FragmentIdLUT
dataFragDisplay.SelectTCoordArray = "None"
dataFragDisplay.SelectNormalArray = "None"
dataFragDisplay.SelectTangentArray = "None"
dataFragDisplay.OSPRayScaleFunction = "Piecewise Function"
dataFragDisplay.Assembly = "Hierarchy"
dataFragDisplay.SelectOrientationVectors = "None"
dataFragDisplay.ScaleFactor = 2.4e-05
dataFragDisplay.SelectScaleArray = "None"
dataFragDisplay.GlyphType = "Arrow"
dataFragDisplay.GlyphTableIndexArray = "None"
dataFragDisplay.GaussianRadius = 1.2e-06
dataFragDisplay.SetScaleArray = [None, ""]
dataFragDisplay.ScaleTransferFunction = "Piecewise Function"
dataFragDisplay.OpacityArray = [None, ""]
dataFragDisplay.OpacityTransferFunction = "Piecewise Function"
dataFragDisplay.DataAxesGrid = "Grid Axes Representation"
dataFragDisplay.PolarAxes = "Polar Axes Representation"

# setup the color legend parameters for each legend in this view

# get color legend/bar for FragmentIdLUT in view renderView1
FragmentIdLUTColorBar = GetScalarBar(FragmentIdLUT, renderView1)
FragmentIdLUTColorBar.Title = "FragmentId"
FragmentIdLUTColorBar.ComponentTitle = ""

# set color bar visibility
FragmentIdLUTColorBar.Visibility = 0

# hide data in view
Hide(data, renderView1)

# hide data in view
Hide(dataFrag, renderView1)

# show data in view
Show(dataFrag, renderView1)
# ----------------------------------------------------------------
# setup color maps and opacity maps used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# get opacity transfer function/opacity map for 'FragmentId'
FragmentIdPWF = GetOpacityTransferFunction("FragmentId")
FragmentIdPWF.Points = [0.0, 0.0, 0.5, 0.0, 101.0, 1.0, 0.5, 0.0]
FragmentIdPWF.ScalarRangeInitialized = 1

# ----------------------------------------------------------------
# restore active source
SetActiveSource(dataFrag)
# ----------------------------------------------------------------

# layout/tab size in pixels
layout1.SetSize(800, 600)

# ----------------------------------------------------------------
if nprocs > 1:
    baseline = "HyperTreeGridFragmentation/MPI_" + str(nprocs)
else:
    baseline = "HyperTreeGridFragmentation"
print("Use baseline ", baseline)

launch_comparison(renderView1, baseline_relative_dir=baseline)

##--------------------------------------------
## You may need to add some code at the end of this python script depending on your usage, eg:
#
## Render all views to see them appears
# RenderAllViews()
#
## Interact with the view, usefull when running from pvpython
# Interact()
#
## Save a screenshot of the active view
# SaveScreenshot("path/to/screenshot.png")
#
## Save a screenshot of a layout (multiple splitted view)
# SaveScreenshot("path/to/screenshot.png", GetLayout())
#
## Save all "Extractors" from the pipeline browser
# SaveExtracts()
#
## Save a animation of the current active view
# SaveAnimation()
#
## Please refer to the documentation of paraview.simple
## https://kitware.github.io/paraview-docs/latest/python/paraview.simple.html
##--------------------------------------------
