"""
This test checks the HyperTreeGridFragmentation filter on a 2D base, with only
one material and one subdomain
"""
# trace generated using paraview version 5.11.1-1280-gd982f83420
# import paraview
# paraview.compatibility.major = 5
# paraview.compatibility.minor = 11

#### import the simple module from the paraview
from paraview.simple import *

from common import CMAKE_SOURCE_DIR, launch_comparison

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'HyperTreeGrid Reader'
htg_basename = "aquarius_2D_XSlice0.015_Psup1e-5"
data = HyperTreeGridReader(
    registrationName=f"{htg_basename}.htg",
    FileNames=[
        f"{CMAKE_SOURCE_DIR}/Data/Testing/HyperTreeGridFragmentation/2D/{htg_basename}.htg"
    ],
)

# get active view
renderView1 = GetActiveViewOrCreate("RenderView")

# show data in view
aquarius_2D_XSlice0015_Psup1e5htgDisplay = Show(
    data, renderView1, "HyperTreeGridRepresentation"
)

# trace defaults for the display properties.
aquarius_2D_XSlice0015_Psup1e5htgDisplay.Representation = "Surface"

# reset view to fit data
renderView1.ResetCamera(False, 0.9)

# changing interaction mode based on data extents
renderView1.InteractionMode = "2D"
renderView1.CameraPosition = [3.365, 0.5, 0.5]
renderView1.CameraFocalPoint = [0.015, 0.5, 0.5]
renderView1.CameraViewUp = [0.0, 0.0, 1.0]

# update the view to ensure updated data information
renderView1.Update()

# reset view to fit data
renderView1.ResetCamera(False, 0.9)

# change representation type
aquarius_2D_XSlice0015_Psup1e5htgDisplay.SetRepresentationType("HTG Surface With Edges")

# create a new 'CEA Fragmentation Filter'
cEAFragmentationFilter1 = CEAFragmentationFilter(
    registrationName="CEAFragmentationFilter1", Input=data
)

# Properties modified on cEAFragmentationFilter1
cEAFragmentationFilter1.ExtractType = "Edge Extraction"  # 1 # EXTRACT_EDGE_FRAGMENT
cEAFragmentationFilter1.ExtractWithGlobalFields = 0
cEAFragmentationFilter1.UseMass = 0  # if 1 then cEAFragmentationFilter1.Mass = "..."
cEAFragmentationFilter1.UseDensity = (
    0  # if 1 then cEAFragmentationFilter1.Density = "..."
)
cEAFragmentationFilter1.UseVelocity = (
    0  # if 1 then cEAFragmentationFilter1.Velocity = "..."
)

# Properties modified on cEAFragmentationFilter1
cEAFragmentationFilter1.UseMass = 0
cEAFragmentationFilter1.UseDensity = 0
cEAFragmentationFilter1.UseVelocity = 0

# show data in view
cEAFragmentationFilter1Display = Show(
    cEAFragmentationFilter1, renderView1, "GeometryRepresentation"
)

# trace defaults for the display properties.
cEAFragmentationFilter1Display.Representation = "Surface"

# hide data in view
Hide(data, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# set scalar coloring
ColorBy(cEAFragmentationFilter1Display, ("FIELD", "vtkBlockColors"))

# show color bar/color legend
cEAFragmentationFilter1Display.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'vtkBlockColors'
vtkBlockColorsLUT = GetColorTransferFunction("vtkBlockColors")

# get opacity transfer function/opacity map for 'vtkBlockColors'
vtkBlockColorsPWF = GetOpacityTransferFunction("vtkBlockColors")

# get 2D transfer function for 'vtkBlockColors'
vtkBlockColorsTF2D = GetTransferFunction2D("vtkBlockColors")

# Properties modified on cEAFragmentationFilter1Display
cEAFragmentationFilter1Display.PointSize = 8.0

# ================================================================
# addendum: following script captures some of the application
# state to faithfully reproduce the visualization during playback
# ================================================================

# get layout
layout1 = GetLayout()

# --------------------------------
# saving layout sizes for layouts

# layout/tab size in pixels
layout1.SetSize(800, 600)

# -----------------------------------
# saving camera placements for views

# current camera placement for renderView1
renderView1.InteractionMode = "2D"
renderView1.CameraPosition = [0.08826127724076727, 0.015625, 0.0126953125]
renderView1.CameraFocalPoint = [0.014999999664723873, 0.015625, 0.0126953125]
renderView1.CameraViewUp = [0.0, 0.0, 1.0]
renderView1.CameraParallelScale = 0.018961413905222265

launch_comparison(renderView1, baseline_relative_dir="HyperTreeGridFragmentation")
