#include "vtkBoxLinesSource.h"

#include "vtkCellArray.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkLineSource.h"
#include "vtkNew.h"
#include "vtkObjectFactory.h"
#include "vtkPoints.h"
#include "vtkPolyData.h"
#include "vtkTransform.h"
#include "vtkTransformFilter.h"
#include "vtkVector.h"
#include "vtkVectorOperators.h"

vtkStandardNewMacro(vtkBoxLinesSource);

//----------------------------------------------------------------------------
vtkBoxLinesSource::vtkBoxLinesSource() { this->SetNumberOfInputPorts(0); }

//----------------------------------------------------------------------------
int vtkBoxLinesSource::RequestData(vtkInformation* request,
                                   vtkInformationVector** inputVector,
                                   vtkInformationVector* outputVector)
{
  vtkPolyData* output = vtkPolyData::GetData(outputVector, 0);

  if (!output)
  {
    vtkErrorMacro("Missing output!");
    return 1;
  }

  // Retrieve plane parameters
  const vtkVector3d center(this->PlaneCenter[0], this->PlaneCenter[1],
                           this->PlaneCenter[2]);
  const vtkVector3d normal(this->PlaneNormal[0], this->PlaneNormal[1],
                           this->PlaneNormal[2]);

  // Compute two orthogonal vectors in the plane using the x or y vector as
  // starting vectors
  vtkVector3d refVec(1.0, 0.0, 0.0);

  if (normal == refVec)
  {
    refVec[0] = 0.0;
    refVec[1] = 1.0;
  }

  vtkVector3d planeVec1(normal.Cross(refVec));
  planeVec1.Normalize();
  vtkVector3d planeVec2(normal.Cross(planeVec1));
  planeVec2.Normalize();

  // Compute a corner of the square
  vtkVector3d corner =
      center - (planeVec1 + planeVec2) * (this->SideLength * 0.5);
  int nbLines = this->NumberOfLinesPerSide * this->NumberOfLinesPerSide;

  // Create points
  vtkNew<vtkPoints> points;
  points->SetNumberOfPoints(nbLines * 2);
  double coords[3] = {0.0, 0.0, 0.0};
  vtkIdType currentNbPts = 0;

  // Create lines
  vtkNew<vtkCellArray> lines;
  lines->AllocateEstimate(nbLines, 2);

  // Create line generator
  vtkNew<vtkLineSource> lineSource;

  // If there is only on line, create it at the center
  if (this->NumberOfLinesPerSide == 1)
  {
    // Compute line extremities
    vtkVector3d point1 = center - (this->LinesLength * 0.5) * normal;
    vtkVector3d point2 = center + (this->LinesLength * 0.5) * normal;

    // Generate line
    lineSource->SetPoint1(point1.GetData());
    lineSource->SetPoint2(point2.GetData());
    lineSource->Update();

    // Retrieve line
    vtkPolyData* line = lineSource->GetOutput();
    vtkPoints* linePoints = line->GetPoints();
    vtkCellArray* lineCells = line->GetLines();

    // Add line points and segments to the output
    for (vtkIdType pt = 0; pt < 2; pt++)
    {
      linePoints->GetPoint(pt, coords);
      points->SetPoint(pt, coords);
    }

    lines->Append(lineCells, 0);
  } else
  {
    double spacing = this->SideLength / (this->NumberOfLinesPerSide - 1);

    for (vtkIdType m = 0; m < this->NumberOfLinesPerSide; m++)
    {
      for (vtkIdType n = 0; n < this->NumberOfLinesPerSide; n++)
      {
        // Compute line extremities and center
        vtkVector3d pointOnPlane =
            corner + spacing * (m * planeVec1 + n * planeVec2);
        vtkVector3d point1 = pointOnPlane - (this->LinesLength * 0.5) * normal;
        vtkVector3d point2 = pointOnPlane + (this->LinesLength * 0.5) * normal;

        // Generate line
        lineSource->SetPoint1(point1.GetData());
        lineSource->SetPoint2(point2.GetData());
        lineSource->Update();

        // Retrieve line
        vtkPolyData* line = lineSource->GetOutput();
        vtkPoints* linePoints = line->GetPoints();
        vtkCellArray* lineCells = line->GetLines();

        // Add line points and segments to the output
        for (vtkIdType pt = 0; pt < 2; pt++)
        {
          linePoints->GetPoint(pt, coords);
          points->SetPoint(currentNbPts + pt, coords);
        }

        lines->Append(lineCells, currentNbPts);
        currentNbPts += 2;
      }
    }
  }

  // Fill output
  output->SetPoints(points);
  output->SetLines(lines);

  // Rotate lines if needed
  if (this->Angle != 0.0)
  {
    const vtkVector3d minusCenter = -1.0 * center;

    vtkNew<vtkTransform> transform;
    transform->PostMultiply();
    transform->Translate(minusCenter.GetData());
    transform->RotateWXYZ(this->Angle, normal.GetData());
    transform->Translate(center.GetData());

    vtkNew<vtkTransformFilter> transformer;
    transformer->SetTransform(transform);
    transformer->SetInputData(output);
    transformer->Update();
    output->ShallowCopy(transformer->GetOutput());
  }

  return 1;
}
