/**
 * @class   vtkBoxLinesSource
 * @brief   Creates a box-shaped bundle of lines.
 *
 * This source filter creates a vtkPolyData composed of parallel lines forming
 * a box shape. The lines are defined orthogonal to a given plane with
 * configurable length and distribution.
 */

#ifndef vtkBoxLinesSource_h
#define vtkBoxLinesSource_h

#include "vtkPolyDataAlgorithm.h"

class vtkBoxLinesSource : public vtkPolyDataAlgorithm
{
public:
  static vtkBoxLinesSource* New();
  vtkTypeMacro(vtkBoxLinesSource, vtkPolyDataAlgorithm);

  ///@{
  /**
   * Get/set the length of the side of the square containing the lines.
   * Default is 10.0.
   */
  vtkGetMacro(SideLength, double);
  vtkSetClampMacro(SideLength, double, 0.0, VTK_INT_MAX);
  ///@}

  ///@{
  /**
   * Get/set the length of each line.
   * Default is 10.0.
   */
  vtkGetMacro(LinesLength, double);
  vtkSetClampMacro(LinesLength, double, 0.0, VTK_INT_MAX);
  ///@}

  ///@{
  /**
   * Get/set the number of lines along each side of the square containing the
   * lines. If the number is N, the total number of lines will be N * N. Default
   * is 3.
   */
  vtkGetMacro(NumberOfLinesPerSide, int);
  vtkSetClampMacro(NumberOfLinesPerSide, int, 0, VTK_INT_MAX);
  ///@}

  ///@{
  /**
   * Get/Set the center for the plane.
   * Default is { 0.0, 0.0, 0.0 }.
   */
  vtkSetVector3Macro(PlaneCenter, double);
  vtkGetVector3Macro(PlaneCenter, double);
  ///@}

  ///@{
  /**
   * Get/Set the normal for the plane.
   * Default is { 0.0, 0.0, 1.0 }.
   */
  vtkSetVector3Macro(PlaneNormal, double);
  vtkGetVector3Macro(PlaneNormal, double);
  ///@}

  ///@{
  /**
   * Get/set the angle of rotation (counterclockwise) for the box of lines in
   * degrees. The rotation axis corresponds to the plane normal. Default is 0.0.
   */
  vtkGetMacro(Angle, double);
  vtkSetMacro(Angle, double);
  ///@}

protected:
  vtkBoxLinesSource();
  ~vtkBoxLinesSource() override = default;

  int RequestData(vtkInformation*, vtkInformationVector**,
                  vtkInformationVector*) override;

  double SideLength = 10.0;
  double LinesLength = 10.0;
  int NumberOfLinesPerSide = 3;
  double PlaneCenter[3] = {0.0, 0.0, 0.0};
  double PlaneNormal[3] = {0.0, 0.0, 1.0};
  double Angle = 0.0;

private:
  vtkBoxLinesSource(const vtkBoxLinesSource&) = delete;
  void operator=(const vtkBoxLinesSource&) = delete;
};

#endif
