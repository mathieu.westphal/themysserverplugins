/**
 * @class   vtkSphereLinesSource
 * @brief   Creates a sphere-shaped bundle of lines.
 *
 * This source filter creates a vtkPolyData composed of lines forming a sphere.
 * The lines start at the center of the sphere and extend along the radius.
 * The length and distribution of the lines can be configured.
 */

#ifndef vtkSphereLinesSource_h
#define vtkSphereLinesSource_h

#include "vtkPolyDataAlgorithm.h"

class vtkSphereLinesSource : public vtkPolyDataAlgorithm
{
public:
  static vtkSphereLinesSource* New();
  vtkTypeMacro(vtkSphereLinesSource, vtkPolyDataAlgorithm);

  ///@{
  /**
   * Get/set the number of lines along the longitudinal direction of the sphere
   * (theta) that goes from 0 to 360 degrees.
   * Default is 10.
   */
  vtkGetMacro(NumberOfAzimuthalLines, int);
  vtkSetClampMacro(NumberOfAzimuthalLines, int, 1, VTK_INT_MAX);
  ///@}

  ///@{
  /**
   * Get/set the number of lines along the latitudinal direction of the sphere
   * (phi) that goes from 0 to 180 degrees.
   * Default is 10.
   */
  vtkGetMacro(NumberOfPolarLines, int);
  vtkSetClampMacro(NumberOfPolarLines, int, 1, VTK_INT_MAX);
  ///@}

  ///@{
  /**
   * Get/Set the center of the sphere.
   * Default is { 0.0, 0.0, 0.0 }.
   */
  vtkSetVector3Macro(Center, double);
  vtkGetVector3Macro(Center, double);
  ///@}

  ///@{
  /**
   * Get/set the radius of the sphere.
   * Default is 1.0.
   */
  vtkGetMacro(Radius, double);
  vtkSetClampMacro(Radius, double, 0.0, VTK_INT_MAX);
  ///@}

  ///@{
  /**
   * Get/set the azimuth for the rotation of the lines in degrees.
   * Default is 0.0.
   */
  vtkGetMacro(AzimuthalAngle, double);
  vtkSetMacro(AzimuthalAngle, double);
  ///@}

  ///@{
  /**
   * Get/set the inclination for the rotation of the lines in degrees.
   * Default is 0.0.
   */
  vtkGetMacro(PolarAngle, double);
  vtkSetMacro(PolarAngle, double);
  ///@}

protected:
  vtkSphereLinesSource();
  ~vtkSphereLinesSource() override = default;

  int RequestData(vtkInformation*, vtkInformationVector**,
                  vtkInformationVector*) override;

  int NumberOfAzimuthalLines = 10;
  int NumberOfPolarLines = 10;
  double Center[3] = {0.0, 0.0, 0.0};
  double Radius = 1.0;
  double AzimuthalAngle = 0.0;
  double PolarAngle = 0.0;

private:
  vtkSphereLinesSource(const vtkSphereLinesSource&) = delete;
  void operator=(const vtkSphereLinesSource&) = delete;
};

#endif
