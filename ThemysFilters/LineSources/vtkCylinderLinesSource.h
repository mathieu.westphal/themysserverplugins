/**
 * @class   vtkCylinderLinesSource
 * @brief   Creates a cylinder-shaped bundle of lines.
 *
 * This source filter creates a vtkPolyData composed of lines forming a
 * cylinder. The lines start on the cylinder axis and extend outwards. The
 * distribution of the lines can be configured.
 */

#ifndef vtkCylinderLinesSource_h
#define vtkCylinderLinesSource_h

#include "vtkPolyDataAlgorithm.h"

class vtkCylinderLinesSource : public vtkPolyDataAlgorithm
{
public:
  static vtkCylinderLinesSource* New();
  vtkTypeMacro(vtkCylinderLinesSource, vtkPolyDataAlgorithm);

  ///@{
  /**
   * Get/set the length of the cylinder.
   * Default is 10.0.
   */
  vtkGetMacro(CylinderLength, double);
  vtkSetClampMacro(CylinderLength, double, 0.0, VTK_DOUBLE_MAX);
  ///@}

  ///@{
  /**
   * Get/set the number of lines along the axis of the cylinder.
   * Default is 10.
   */
  vtkGetMacro(NumberOfLinesAlongAxis, int);
  vtkSetClampMacro(NumberOfLinesAlongAxis, int, 0, VTK_INT_MAX);
  ///@}

  ///@{
  /**
   * Get/set the number of lines along the perimeter of the base of the
   * cylinder. Default is 10.
   */
  vtkGetMacro(NumberOfLinesAlongPerimeter, int);
  vtkSetClampMacro(NumberOfLinesAlongPerimeter, int, 0, VTK_INT_MAX);
  ///@}

  ///@{
  /**
   * Get/Set the center of the cylinder.
   * Default is { 0.0, 0.0, 0.0 }.
   */
  vtkSetVector3Macro(Center, double);
  vtkGetVector3Macro(Center, double);
  ///@}

  ///@{
  /**
   * Get/Set the axis of the cylinder.
   * Default is { 0.0, 0.0, 1.0 }.
   */
  vtkSetVector3Macro(Axis, double);
  vtkGetVector3Macro(Axis, double);
  ///@}

  ///@{
  /**
   * Get/set the radius of the cylinder.
   * Default is 1.0.
   */
  vtkGetMacro(Radius, double);
  vtkSetClampMacro(Radius, double, 0.0, VTK_INT_MAX);
  ///@}

  ///@{
  /**
   * Get/set the angle of rotation (counterclockwise) for the lines in degrees.
   * The rotation axis corresponds to the cylinder axis.
   * Default is 0.0.
   */
  vtkGetMacro(Angle, double);
  vtkSetMacro(Angle, double);
  ///@}

protected:
  vtkCylinderLinesSource();
  ~vtkCylinderLinesSource() override = default;

  int RequestData(vtkInformation*, vtkInformationVector**,
                  vtkInformationVector*) override;

  double CylinderLength = 10.0;
  int NumberOfLinesAlongAxis = 10;
  int NumberOfLinesAlongPerimeter = 10;
  double Center[3] = {0.0, 0.0, 0.0};
  double Axis[3] = {0.0, 0.0, 1.0};
  double Radius = 1.0;
  double Angle = 0.0;

private:
  vtkCylinderLinesSource(const vtkCylinderLinesSource&) = delete;
  void operator=(const vtkCylinderLinesSource&) = delete;
};

#endif
