#include "vtkSphereLinesSource.h"

#include "vtkCellArray.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkNew.h"
#include "vtkObjectFactory.h"
#include "vtkPoints.h"
#include "vtkPolyData.h"
#include "vtkSphereSource.h"
#include "vtkTransform.h"
#include "vtkTransformFilter.h"
#include "vtkVector.h"
#include "vtkVectorOperators.h"

vtkStandardNewMacro(vtkSphereLinesSource);

//----------------------------------------------------------------------------
vtkSphereLinesSource::vtkSphereLinesSource() { this->SetNumberOfInputPorts(0); }

//----------------------------------------------------------------------------
int vtkSphereLinesSource::RequestData(vtkInformation* request,
                                      vtkInformationVector** inputVector,
                                      vtkInformationVector* outputVector)
{
  vtkPolyData* output = vtkPolyData::GetData(outputVector, 0);

  if (!output)
  {
    vtkErrorMacro("Missing output!");
    return 1;
  }

  // Retrieve sphere center
  const vtkVector3d center(this->Center[0], this->Center[1], this->Center[2]);

  // Create sphere based on parameters
  vtkNew<vtkSphereSource> sphereSource;
  sphereSource->SetCenter(this->Center);
  sphereSource->SetRadius(this->Radius);
  sphereSource->SetThetaResolution(this->NumberOfAzimuthalLines);
  sphereSource->SetPhiResolution(this->NumberOfPolarLines);
  sphereSource->Update();

  // Retrieve sphere points
  vtkPolyData* sphere = sphereSource->GetOutput();
  vtkPoints* spherePoints = sphere->GetPoints();

  // Determine number of lines and indices to retrieve from the sphere source
  int nbLines = sphere->GetNumberOfPoints();
  vtkIdType startIdx = 0;
  vtkIdType endIdx = nbLines;

  // Since the sphere source always produces at least 3 azimuthal and
  // 3 polar points, we need to retrieve only the points required when
  // there are less than 3
  if (this->NumberOfAzimuthalLines < 3)
  {
    nbLines = this->NumberOfPolarLines;
    startIdx = 0;
    endIdx = this->NumberOfPolarLines;
  } else if (this->NumberOfPolarLines < 3)
  {
    nbLines = this->NumberOfAzimuthalLines;
    startIdx = 2;
    endIdx = this->NumberOfAzimuthalLines + 2;
  }

  // Create points (the central point is shared between all lines)
  vtkNew<vtkPoints> points;
  points->SetNumberOfPoints(nbLines + 1);
  double coords[3] = {0.0, 0.0, 0.0};

  // Add central point
  points->SetPoint(0, this->Center);
  vtkIdType currentNbPts = 1;

  // Create lines
  vtkNew<vtkCellArray> lines;
  lines->AllocateEstimate(nbLines, 2);

  for (vtkIdType ptIdx = startIdx; ptIdx < endIdx; ptIdx++)
  {
    // Retrieve point on sphere
    spherePoints->GetPoint(ptIdx, coords);
    points->SetPoint(currentNbPts, coords);

    // Add line going from the center to the current point
    lines->InsertNextCell(2);
    lines->InsertCellPoint(0);
    lines->InsertCellPoint(currentNbPts);

    currentNbPts += 1;
  }

  // Assign points and lines to the output
  output->SetPoints(points);
  output->SetLines(lines);

  // Rotate lines if needed
  if (this->PolarAngle != 0.0 || this->AzimuthalAngle != 0.0)
  {
    const vtkVector3d minusCenter = -1.0 * center;
    const vtkVector3d yVec(0, 1, 0);
    const vtkVector3d zVec(0, 0, 1);

    vtkNew<vtkTransform> transform;
    transform->PostMultiply();
    transform->Translate(minusCenter.GetData());
    transform->RotateWXYZ(this->PolarAngle, yVec.GetData());
    transform->RotateWXYZ(this->AzimuthalAngle, zVec.GetData());
    transform->Translate(center.GetData());

    vtkNew<vtkTransformFilter> transformer;
    transformer->SetTransform(transform);
    transformer->SetInputData(output);
    transformer->Update();
    output->ShallowCopy(transformer->GetOutput());
  }

  return 1;
}
