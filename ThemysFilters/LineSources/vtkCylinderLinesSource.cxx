#include "vtkCylinderLinesSource.h"

#include "vtkCellArray.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkLineSource.h"
#include "vtkNew.h"
#include "vtkObjectFactory.h"
#include "vtkPoints.h"
#include "vtkPolyData.h"
#include "vtkTransform.h"
#include "vtkTransformFilter.h"
#include "vtkVector.h"
#include "vtkVectorOperators.h"

vtkStandardNewMacro(vtkCylinderLinesSource);

//----------------------------------------------------------------------------
vtkCylinderLinesSource::vtkCylinderLinesSource()
{
  this->SetNumberOfInputPorts(0);
}

//----------------------------------------------------------------------------
int vtkCylinderLinesSource::RequestData(vtkInformation* request,
                                        vtkInformationVector** inputVector,
                                        vtkInformationVector* outputVector)
{
  vtkPolyData* output = vtkPolyData::GetData(outputVector, 0);

  if (!output)
  {
    vtkErrorMacro("Missing output!");
    return 1;
  }

  if (this->NumberOfLinesAlongAxis == 0 ||
      this->NumberOfLinesAlongPerimeter == 0)
  {
    return 1;
  }

  // Retrieve cylinder parameters
  const vtkVector3d center(this->Center[0], this->Center[1], this->Center[2]);
  const vtkVector3d axis(this->Axis[0], this->Axis[1], this->Axis[2]);
  const vtkVector3d minusCenter = -1.0 * center;

  // Compute a vector orthogonal to the cylinder axis using the x or y vector
  vtkVector3d refVec(1.0, 0.0, 0.0);

  if (axis == refVec)
  {
    refVec[0] = 0.0;
    refVec[1] = 1.0;
  }

  vtkVector3d orthoVec(axis.Cross(refVec));
  orthoVec.Normalize();

  // Create points (points on the cylinder axis are shared between several
  // lines)
  int nbLines =
      this->NumberOfLinesAlongAxis * this->NumberOfLinesAlongPerimeter;
  vtkNew<vtkPoints> points;
  points->SetNumberOfPoints(nbLines + this->NumberOfLinesAlongAxis);
  double coords[3] = {0.0, 0.0, 0.0};

  // Create lines
  vtkNew<vtkCellArray> lines;
  lines->AllocateEstimate(nbLines, 2);

  // Assign points and lines to the output
  output->SetPoints(points);
  output->SetLines(lines);

  // Add first point on one extremity of the cylinder axis
  const vtkVector3d firstAxisPt = center - 0.5 * this->CylinderLength * axis;
  points->SetPoint(0, firstAxisPt.GetData());
  vtkIdType currentNbPts = 1;

  // Create line generator
  vtkNew<vtkLineSource> lineSource;

  // Create first line connected to the first point at one extremity of the
  // cylinder
  const vtkVector3d firstTipPt = firstAxisPt + this->Radius * orthoVec;
  points->SetPoint(currentNbPts, firstTipPt.GetData());
  lines->InsertNextCell(2);
  lines->InsertCellPoint(0);
  lines->InsertCellPoint(currentNbPts);
  currentNbPts += 1;

  // Create reference point to rotate
  vtkNew<vtkPoints> refPt;
  refPt->SetNumberOfPoints(1);
  refPt->SetPoint(0, firstTipPt.GetData());
  vtkNew<vtkPolyData> refPtData;
  refPtData->SetPoints(refPt);

  // Create complete first set of lines by rotation
  if (this->NumberOfLinesAlongPerimeter > 1)
  {
    // Create transform and corresponding filter
    vtkNew<vtkTransform> transform;
    transform->PostMultiply();

    vtkNew<vtkTransformFilter> transformer;
    transformer->SetTransform(transform);
    transformer->SetInputData(refPtData);

    double angleSpacing = 360.0 / this->NumberOfLinesAlongPerimeter;

    for (vtkIdType n = 1; n < this->NumberOfLinesAlongPerimeter; n++)
    {
      // Rotate reference point
      transform->Translate(minusCenter.GetData());
      transform->RotateWXYZ(angleSpacing, axis.GetData());
      transform->Translate(center.GetData());
      transformer->Update();

      // Retrieve line
      vtkPolyData* rotatedData =
          vtkPolyData::SafeDownCast(transformer->GetOutput());
      vtkPoints* rotatedPt = rotatedData->GetPoints();

      // Add point
      rotatedPt->GetPoint(0, coords);
      points->SetPoint(currentNbPts, coords);

      // Create line
      lines->InsertNextCell(2);
      lines->InsertCellPoint(0);
      lines->InsertCellPoint(currentNbPts);
      currentNbPts += 1;
    }
  }

  // Create reference lines to fill the cylinder
  vtkNew<vtkPolyData> refLines;
  refLines->DeepCopy(output);

  // Fill the cylinder by creating lines through translation
  if (this->NumberOfLinesAlongAxis > 1)
  {
    // Create transform and corresponding filter
    vtkNew<vtkTransform> transform;
    transform->PostMultiply();

    vtkNew<vtkTransformFilter> transformer;
    transformer->SetTransform(transform);
    transformer->SetInputData(refLines);

    // Define translation displacement
    double spacing = this->CylinderLength / (this->NumberOfLinesAlongAxis - 1);
    const vtkVector3d displacement = spacing * axis;

    for (vtkIdType n = 1; n < this->NumberOfLinesAlongAxis; n++)
    {
      // Translate starting set of lines
      transform->Translate(displacement.GetData());
      transformer->Update();

      // Retrieve line
      vtkPolyData* line = vtkPolyData::SafeDownCast(transformer->GetOutput());
      vtkPoints* linePoints = line->GetPoints();
      vtkCellArray* lineCells = line->GetLines();

      // Add line points and lines to the output
      for (vtkIdType pt = 0; pt < this->NumberOfLinesAlongPerimeter + 1; pt++)
      {
        linePoints->GetPoint(pt, coords);
        points->SetPoint(currentNbPts + pt, coords);
      }

      lines->Append(lineCells, currentNbPts);
      currentNbPts += this->NumberOfLinesAlongPerimeter + 1;
    }
  }

  // Rotate lines if needed
  if (this->Angle != 0.0)
  {
    vtkNew<vtkTransform> transform;
    transform->PostMultiply();
    transform->Translate(minusCenter.GetData());
    transform->RotateWXYZ(this->Angle, axis.GetData());
    transform->Translate(center.GetData());

    vtkNew<vtkTransformFilter> transformer;
    transformer->SetTransform(transform);
    transformer->SetInputData(output);
    transformer->Update();
    output->ShallowCopy(transformer->GetOutput());
  }

  return 1;
}
