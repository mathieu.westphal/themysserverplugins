<ServerManagerConfiguration>
  <ProxyGroup name="themys_line_sources">

    <!-- ==================================================================== -->
    <SourceProxy class="vtkBoxLinesSource"
                 label="Box Lines"
                 name="BoxLinesSource">
      <Documentation short_help="Creates a box shaped bundle of lines.">
        This source creates lines orthogonal to the given plane with the specified
        length. The number and extent of the lines can be configured.
      </Documentation>

      <DoubleVectorProperty command="SetSideLength"
                            name="SideLength"
                            label="Side Length"
                            number_of_elements="1"
                            default_values="10.0">
        <DoubleRangeDomain name="range" min="0.0"/>
        <Documentation>
          Length of the side of the square containing the lines.
        </Documentation>
      </DoubleVectorProperty>

      <DoubleVectorProperty command="SetLinesLength"
                            name="LinesLength"
                            label="Lines Length"
                            number_of_elements="1"
                            default_values="10.0">
        <DoubleRangeDomain name="range" min="0.0"/>
        <Documentation>
          Length of each line.
        </Documentation>
      </DoubleVectorProperty>

      <IntVectorProperty command="SetNumberOfLinesPerSide"
                         name="NumberOfLinesPerSide"
                         label="Number Of Lines Per Side"
                         number_of_elements="1"
                         default_values="3">
        <IntRangeDomain name="range" min="1"/>
        <Documentation>
          Number of lines for each side of the square containing the lines.
        </Documentation>
      </IntVectorProperty>

      <DoubleVectorProperty command="SetAngle"
                            name="Angle"
                            label="Rotation Angle"
                            number_of_elements="1"
                            default_values="0.0">
        <DoubleRangeDomain name="range" min="0.0" max="360.0"/>
        <Documentation>
          Angle of rotation (counterclockwise) in degrees of the box of lines around
          their axis.
        </Documentation>
      </DoubleVectorProperty>

      <DoubleVectorProperty name="PlaneCenter"
                            command="SetPlaneCenter"
                            label="Center"
                            number_of_elements="3"
                            default_values="0 0 0">
        <Documentation>
          Point on the plane. Used to position the plane in space.
        </Documentation>
      </DoubleVectorProperty>

      <DoubleVectorProperty name="PlaneNormal"
                            command="SetPlaneNormal"
                            label="Normal"
                            number_of_elements="3"
                            default_values="0 0 1">
        <Documentation>
          Plane normal. Used to orient the plane in space.
        </Documentation>
      </DoubleVectorProperty>

      <InputProperty is_internal="1" name="DummyInput">
        <!-- Used when this source is added to a proxy list domain. -->
      </InputProperty>

      <PropertyGroup label="Plane Parameters" panel_widget="InteractivePlane">
        <Property function="Origin" name="PlaneCenter" />
        <Property function="Normal" name="PlaneNormal" />
        <Property function="Input" name="DummyInput" />
      </PropertyGroup>

      <Hints>
        <ProxyList>
          <Link name="DummyInput" with_property="Input" />
        </ProxyList>
      </Hints>

      <!-- End BoxLinesSource -->
    </SourceProxy>
    <!-- ==================================================================== -->
    <SourceProxy class="vtkCylinderLinesSource"
                 label="Cylinder Lines"
                 name="CylinderLinesSource">
      <Documentation short_help="Creates a cylinder shaped bundle of lines.">
        This source creates lines inside of the given cylinder, starting from the
        cylinder axis and extending outwards. The number of lines along the axis
        and around the perimeter can be configured.
      </Documentation>

      <DoubleVectorProperty command="SetCylinderLength"
                            name="CylinderLength"
                            label="Cylinder Length"
                            number_of_elements="1"
                            default_values="10.0">
        <DoubleRangeDomain name="range" min="0.0"/>
        <Documentation>
          Length of the cylinder.
        </Documentation>
      </DoubleVectorProperty>

      <IntVectorProperty command="SetNumberOfLinesAlongAxis"
                         name="NumberOfLinesAlongAxis"
                         label="Number Of Lines Along Axis"
                         number_of_elements="1"
                         default_values="10">
        <IntRangeDomain name="range" min="1"/>
        <Documentation>
          Number of lines along the axis of the cylinder.
        </Documentation>
      </IntVectorProperty>

      <IntVectorProperty command="SetNumberOfLinesAlongPerimeter"
                         name="NumberOfLinesAlongPerimeter"
                         label="Number Of Lines Along Perimeter"
                         number_of_elements="1"
                         default_values="10">
        <IntRangeDomain name="range" min="0"/>
        <Documentation>
          Number of lines around the perimeter of the cylinder.
        </Documentation>
      </IntVectorProperty>

      <DoubleVectorProperty command="SetAngle"
                            name="Angle"
                            label="Rotation Angle"
                            number_of_elements="1"
                            default_values="0.0">
        <DoubleRangeDomain name="range" min="0.0" max="360.0"/>
        <Documentation>
          Angle of rotation (counterclockwise) in degrees of the bundle of lines
          around the cylinder axis.
        </Documentation>
      </DoubleVectorProperty>

      <DoubleVectorProperty name="Center"
                            command="SetCenter"
                            label="Center"
                            number_of_elements="3"
                            default_values="0 0 0">
        <Documentation>
          Center of the cylinder.
        </Documentation>
      </DoubleVectorProperty>

      <DoubleVectorProperty name="Axis"
                            command="SetAxis"
                            label="Axis"
                            number_of_elements="3"
                            default_values="0 0 1">
        <Documentation>
          Axis of the cylinder.
        </Documentation>
      </DoubleVectorProperty>

      <DoubleVectorProperty command="SetRadius"
                            default_values="1.0"
                            name="Radius"
                            number_of_elements="1">
        <DoubleRangeDomain name="range" min="0.0" />
        <Documentation>
          Radius of the cylinder.
        </Documentation>
      </DoubleVectorProperty>

      <InputProperty is_internal="1" name="DummyInput">
        <!-- Used when this source is added to a proxy list domain. -->
      </InputProperty>

      <PropertyGroup label="Cylinder Parameters" panel_widget="InteractiveCylinder">
        <Property function="Center" name="Center" />
        <Property function="Axis" name="Axis" />
        <Property function="Radius" name="Radius" />
        <Property function="Input" name="DummyInput" />
      </PropertyGroup>

      <Hints>
        <ProxyList>
          <Link name="DummyInput" with_property="Input" />
        </ProxyList>
      </Hints>

      <!-- End CylinderLinesSource -->
    </SourceProxy>
    <!-- ==================================================================== -->
    <SourceProxy class="vtkSphereLinesSource"
                 label="Sphere Lines"
                 name="SphereLinesSource">
      <Documentation short_help="Creates a sphere shaped bundle of lines.">
        This source creates lines inside of the given sphere with the specified
        length. The number of lines along both spherical angles (polar and azimuthal)
        can be configured.
      </Documentation>

      <IntVectorProperty command="SetNumberOfAzimuthalLines"
                         name="NumberOfAzimuthalLines"
                         label="Number Of Azimuthal Lines"
                         number_of_elements="1"
                         default_values="10">
        <IntRangeDomain name="range" min="1"/>
        <Documentation>
          Number of lines along the longitudinal direction of the sphere (theta)
          that goes from 0 to 360 degrees.
        </Documentation>
      </IntVectorProperty>

      <IntVectorProperty command="SetNumberOfPolarLines"
                         name="NumberOfPolarLines"
                         label="Number Of Polar Lines"
                         number_of_elements="1"
                         default_values="10">
        <IntRangeDomain name="range" min="1"/>
        <Documentation>
          Number of lines along the latitudinal direction of the sphere (phi)
          that goes from 0 to 180 degrees.
        </Documentation>
      </IntVectorProperty>

      <DoubleVectorProperty command="SetAzimuthalAngle"
                            name="AzimuthalAngle"
                            label="Azimuthal Rotation Angle"
                            number_of_elements="1"
                            default_values="0.0">
        <DoubleRangeDomain name="range" min="0.0" max="360.0"/>
        <Documentation>
          Azimuth for the rotation of the lines in degrees.
        </Documentation>
      </DoubleVectorProperty>

      <DoubleVectorProperty command="SetPolarAngle"
                            name="PolarAngle"
                            label="Polar Rotation Angle"
                            number_of_elements="1"
                            default_values="0.0">
        <DoubleRangeDomain name="range" min="0.0" max="360.0"/>
        <Documentation>
          Inclination for the rotation of the lines in degrees.
        </Documentation>
      </DoubleVectorProperty>

      <DoubleVectorProperty name="Center"
                            command="SetCenter"
                            label="Center"
                            number_of_elements="3"
                            default_values="0 0 0">
        <Documentation>
          Center of the sphere.
        </Documentation>
      </DoubleVectorProperty>

      <DoubleVectorProperty command="SetRadius"
                            default_values="1.0"
                            name="Radius"
                            number_of_elements="1">
        <DoubleRangeDomain name="range" min="0.0" />
        <Documentation>
          Radius of the sphere.
        </Documentation>
      </DoubleVectorProperty>

      <InputProperty is_internal="1" name="DummyInput">
        <!-- Used when this source is added to a proxy list domain. -->
      </InputProperty>

      <PropertyGroup label="Sphere Parameters" panel_widget="InteractiveSphere">
        <Property function="Center" name="Center" />
        <Property function="Radius" name="Radius" />
        <Property function="Input" name="DummyInput" />
      </PropertyGroup>

      <Hints>
        <ProxyList>
          <Link name="DummyInput" with_property="Input" />
        </ProxyList>
      </Hints>

      <!-- End SphereLinesSource -->
    </SourceProxy>

  </ProxyGroup>
</ServerManagerConfiguration>
