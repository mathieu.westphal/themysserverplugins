import numpy as np
from paraview.util.vtkAlgorithm import *
from vtkmodules.numpy_interface import dataset_adapter as dsa
from vtkmodules.vtkCommonDataModel import vtkDataSet


@smproxy.filter(label="CEA Custom Line Integrator (Python)")
@smproperty.input(name="Input")
class SimplePythonFilter(VTKPythonAlgorithmBase):
    def __init__(self):
        VTKPythonAlgorithmBase.__init__(self)
        self.ArrayToProcess = None
        self.Operators = ["np.cumsum(inputArray)", "np.cumsum(inputArray*dl)", "custom"]
        self.IndexOperator = None

    def RequestData(self, request, inInfo, outInfo):
        inputLine = dsa.WrapDataObject(vtkDataSet.GetData(inInfo[0]))
        outData = dsa.WrapDataObject(self.GetOutputData(outInfo, 0))
        outData.ShallowCopy(inputLine.VTKObject)

        if self.IndexOperator == 0:
            arr = np.cumsum(inputLine.PointData[self.ArrayToProcess])
            outData.PointData.append(arr, "Integral")
            outData.FieldData.append(arr[-1], "GlobalIntegral")
            return 1

        if self.IndexOperator == 1:
            arr = inputLine.PointData[self.ArrayToProcess]
            arc = inputLine.PointData["arc_length"]

            dpos = np.zeros(len(arr))
            dpos[0:-1] = arc[1:] - arc[0:-1]
            intlin = np.cumsum(arr * dpos)

            outData.PointData.append(intlin, "LinearIntegral")
            outData.FieldData.append(intlin[-1], "GlobalIntegral")
            return 1

        # set your custom operator
        arr = inputLine.PointData[self.ArrayToProcess]
        arc = inputLine.PointData["arc_length"]

        dpos = np.zeros(len(arr))
        dpos[0:-1] = arc[1:] - arc[0:-1]
        custom = np.maximum.accumulate(arr * dpos)

        A = np.array([1, 0, 3, 2, 1, 5])
        print(np.maximum.accumulate(A))

        outData.PointData.append(custom, "Custom")
        outData.FieldData.append(np.max(custom), "GlobalCustom")
        return 1

    # Information only property adds a read-only widget in the properties panel.
    @smproperty.stringvector(
        name="Version", panel_visibility="advanced", information_only="1"
    )
    def GetVersion(self):
        return "1.0 (c)JBL 2021"

    # Information only property adds a read-only widget in the properties panel.
    @smproperty.stringvector(
        name="Used Formulae", panel_visibility="default", information_only="1"
    )
    def GetFormulae(self):
        if self.IndexOperator == 0:
            return "np.cumsum({0})".format(self.ArrayToProcess)
        elif self.IndexOperator == 1:
            return "np.cumsum({0}*dl)".format(self.ArrayToProcess)
        return "np.cummax({0}*dl)".format(self.ArrayToProcess)

    @smproperty.stringvector(name="StringInfo", information_only="1")
    def GetStrings(self):
        return self.Operators

    @smproperty.stringvector(name="Operator", number_of_elements="1")
    @smdomain.xml(
        """<StringListDomain name="list">
             <RequiredProperties>
                <Property name="StringInfo" function="StringInfo"/>
             </RequiredProperties>
          </StringListDomain>
       """
    )
    def SetString(self, _operator):
        if _operator is None:
            return
        print(_operator)
        print(self.Operators)
        print(self.Operators.index(_operator))
        index = self.Operators.index(_operator)
        if index != self.IndexOperator:
            self.IndexOperator = index
            self.Modified()

    @smproperty.stringvector(name="InputArray", default_values="RTData")
    @smdomain.xml(
        """<ArrayListDomain name="array_list" attribute_type="Scalars" >
            <RequiredProperties>
               <Property name="Input" function="Input"/>
            </RequiredProperties>
         </ArrayListDomain>
      """
    )
    def SetInputArrayToProcess(self, arrayName):
        if arrayName != self.ArrayToProcess:
            self.ArrayToProcess = arrayName
            self.Modified()
