/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include "vtkMaterialInterface_InterfaceData.h"

#include <array>

#include <vtkDoubleArray.h>

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
InterfaceData::InterfaceData(vtkDoubleArray* normale, vtkDoubleArray* distance)
    : m_normale{nullptr}, m_distance{nullptr}
{
  if (normale == nullptr)
    return;

  constexpr std::array<double, 2> null_vector_range{0., 0.};

  // Do not store normale if all values are null vector
  auto normale_range{null_vector_range};
  // -1 means that we want to compute the L2 norm over all components
  normale->GetFiniteRange(normale_range.data(), -1);

  if (normale_range == null_vector_range)
    return;

  m_normale = normale;
  m_distance = distance;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
double InterfaceData::getDistanceInCell(const vtkIdType& cell_index) const
{
  if (m_distance == nullptr)
  {
    return 0.;
  }

  return *m_distance->GetTuple(cell_index);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
InterfaceNormale
InterfaceData::getNormaleInCell(const vtkIdType& cell_index) const
{
  if (m_normale == nullptr)
  {
    return {0., 0., 0.};
  }

  auto* normale_v{m_normale->GetTuple(cell_index)};
  // No choice for pointer arithmetic here. The array is returned from Hercule
  // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
  return {*normale_v, *(normale_v + 1), *(normale_v + 2)};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out, const InterfaceData& if_data)
{
  out << "{Normale: " << if_data.m_normale
      << ", Distance: " << if_data.m_distance << "}";
  return out;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
