// IWYU pragma: no_include <vtkIOStream.h>
#include "vtkMaterialInterface_InterfaceInfos.h"

#include <ostream>
#include <string>

#include "vtkMaterialInterface_Utils.h"
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::string interface_status_to_string(InterfaceStatus status)
{
  switch (status)
  {
  case InterfaceStatus::UNDEFINED:
    return "UNDEFINED";
  case InterfaceStatus::WITH_ONE_INTERFACE:
    return "WITH_ONE_INTERFACE";
  case InterfaceStatus::WITH_TWO_INTERFACES:
    return "WITH_TWO_INTERFACES";
  case InterfaceStatus::WITH_ORDER:
    return "WITH_ORDER";
  default:
    return "UNKNOWN";
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out, InterfaceStatus status)
{
  out << "Interface status: " << interface_status_to_string(status);
  return out;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out, const InterfaceInfos& infos)
{
  out << "{Normale: " << infos.normale << " / "
      << "Distance: " << infos.distance << "}";
  return out;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool InterfaceInfos::isValid() const
{
  return !is_empty_name(this->distance) && !is_empty_name(this->normale);
}
