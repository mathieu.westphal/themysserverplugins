/*=========================================================================

Program:   Visualization Toolkit
Module:    vtkMaterialInterface.cxx

Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
All rights reserved.
See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .SECTION Thanks
// This file includes the material construction algorithm taking into account
// sub-cell information which allows a cell to be clip or cut according to at
// most two interfaces. This implementation is proposed by Jacques-Bernard
// Lekien of the society : CEA, DAM, DIF, F-91297 Arpajon, France

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

// IWYU pragma: no_include <array>
// IWYU pragma: no_include <fwd>
// IWYU pragma: no_include <functional>
// IWYU pragma: no_include <tuple>
// IWYU pragma: no_include <string_view>
// IWYU pragma: no_include <type_traits>
// IWYU pragma: no_include <vtkLogger.h>
// IWYU pragma: no_include "vtkSystemIncludes.h"

#include "vtkMaterialInterface.h"

#include <map>
#include <numeric>
#include <ostream>
#include <sstream>
#include <string>
#include <utility>

#include <vtkCompositeDataIterator.h>
#include <vtkCompositeDataSet.h>
#include <vtkDataObject.h>
#include <vtkDataSet.h>
#include <vtkIndent.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkMultiBlockDataSet.h>
#include <vtkObjectFactory.h>
#include <vtkPVLogger.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>

#include "vtkMaterialInterface_InterfaceInfos.h"
#include "vtkMaterialInterface_InterfaceInfosPair.h"
#include "vtkMaterialInterface_MaterialClipMethod.h"
#include "vtkMaterialInterface_MaterialContourMethod.h"
#include "vtkMaterialInterface_SamplingPattern.h"
#include "vtkMaterialInterface_Types.h"

/*----------------------------------------------------------------------------*/
/**
 * @brief Internal implementation of the vtkMaterialInterface filter
 *
 * @tparam MaterialMethodT : type of Material object required
 * (MaterialContourMethod or MaterialClipMethod)
 */
/*----------------------------------------------------------------------------*/
template <MethodType method> class vtkMaterialInterface::vtkInternal
{
public:
  vtkInternal() = default;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Create the Material object and record it into a collection for
   * further processing.
   *
   * Recording in the collection occurs only if the necessary arrays have
   * been successfully retrieved and if those array are compatible with the
   * algorithm required (global or local)
   *
   * @param _negative_value_mask_on : if true and if mask is used then a cell
   * is masked if the mask array value is lower than 0. Otherwise a cell
   * is masked if the mask array value is equal to 0.
   * @param _mask_name : name of the mask array
   * @param if_status : interface status
   * @param _material_index : index of the material in the composite data set
   * @param interfaces_infos : informations (name of arrays) of the interfaces
   * @param _input_material_mesh : input material mesh
   * @param _inputIterator : iterator to position in the ouput data set
   * where the output material mesh has to be inserted
   * @warning requires the use of CopyStructure method (called before)
   * @param _compositeOutput : data set into which the output material mesh
   * has to be inserted
   *
   */
  /*----------------------------------------------------------------------------*/
  void registerMaterial(bool _negative_value_mask_on,
                        const std::string& _mask_name,
                        const InterfaceStatus if_status, int _material_index,
                        const InterfaceInfosPair& interfaces_infos,
                        vtkDataSet* _input_material_mesh,
                        vtkCompositeDataIterator* _inputIterator,
                        vtkMultiBlockDataSet* _compositeOutput)
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "Registering material " << _material_index);

    std::unique_ptr<MaterialAbstractMethod> current_material;
    if constexpr (method == MethodType::CONTOUR)
    {
      current_material =
          std::make_unique<MaterialContourMethod>(_input_material_mesh);
    } else if constexpr (method == MethodType::CLIP)
    {
      if (vtkPolyData::SafeDownCast(_input_material_mesh) != nullptr)
      {
        current_material = std::make_unique<MaterialClipMethod<vtkPolyData>>(
            _input_material_mesh);
      } else if (vtkUnstructuredGrid::SafeDownCast(_input_material_mesh) !=
                 nullptr)
      {
        current_material =
            std::make_unique<MaterialClipMethod<vtkUnstructuredGrid>>(
                _input_material_mesh);
      } else
      {
        throw std::runtime_error(
            "The clip method is only available for output meshes of type "
            "vtkPolyData or vtkUnstructuredGrid");
      }
    }
    current_material->initOutputMaterialMesh();

    // Retrieve the needed arrays for later interface computing
    if (!current_material->fetchArraysFromCellData(
            _mask_name, _negative_value_mask_on, interfaces_infos))
    {
      vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_ERROR,
              "Fetching arrays for material " << _material_index
                                              << " has failed!");
      return;
    }

    if (current_material->allInterfaceArraysNull())
    {
      vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_WARNING,
              "All interface arrays are null. Shallow copy of the input "
              "material mesh");
      if constexpr (method == MethodType::CLIP)
      {
        // In case the user specified existing interface arrays (or not but
        // using directly the plugin without ParaView) but those arrays do not
        // exist on the current pv server just copy the input to avoid holes in
        // the final result
        _compositeOutput->SetDataSet(_inputIterator, _input_material_mesh);
      }
      return;
    }

    // If the arrays retrieved are not those that the selected mode request
    // then the interface cannot be computed
    if (!current_material->checkInterfaceStatusRequirements(if_status))
    {
      vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_ERROR,
              "Requirements for filter treatment of material "
                  << _material_index << " are not fullfilled!");
      return;
    }

    // Store the material in the dedicated collection
    const auto [it_material, insertion_success] = m_material_collection.insert(
        {_material_index, std::move(current_material)});
    if (!insertion_success)
    {
      vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_ERROR,
              "Unable to insert material with index " << _material_index);
      return;
    }

    // Insert the current material output mesh in the global output
    _compositeOutput->SetDataSet(_inputIterator,
                                 it_material->second->getOutputMaterialMesh());
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Build pure cells (if necessary, not in case of contouring (i.e
   * polydata output)) and call the local algorithm to build mixed cells
   *
   */
  /*----------------------------------------------------------------------------*/
  int executeLocalAlgorithm()
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "Executing the local algorithm");
    if constexpr (method == MethodType::CLIP)
    {
      this->buildPureCells();
    }
    this->buildMixedCellsLocalAlgorithm();
    return 1;
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Build pure cells (if necessary, not in case of contouring (i.e
   * polydata output)) and call the global algorithm to build mixed cells
   */
  /*----------------------------------------------------------------------------*/
  int executeGlobalAlgorithm()
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "Executing the global algorithm");
    if constexpr (method == MethodType::CLIP)
    {
      this->buildPureCells();
    }
    this->fillInMixedCellsLocalization();
    if (!this->checkMixedCellsZones())
    {
      return 0;
    }
    this->buildMixedCellsGlobalAlgorithm();
    return 1;
  }

private:
  /*----------------------------------------------------------------------------*/
  /**
   * @brief Fills in the m_mixed_cell_zones structure by retrieving the mixed
   * cells informations on each material
   *
   * @todo: mutualize the call to separateMixedAndPureCells with the
   * buildPureCells method
   *
   */
  /*----------------------------------------------------------------------------*/
  void fillInMixedCellsLocalization()
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "Filling in the mixed cells localization variable");
    for (const auto& mpair : m_material_collection)
    {
      const auto& [material_id, material] = mpair;
      const auto& [_, mcells_lids] = material->separateMixedAndPureCells();
      for (const auto& l_id : mcells_lids)
      {
        const int order = material->getOrder(l_id);
        const auto& g_id = material->getGlobalCellId(l_id);
        vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_7,
                "Inserting {material_id (" << material_id << "), l_id (" << l_id
                                           << ")} for cell with global id "
                                           << g_id << " and for order "
                                           << order);
        m_mixed_cell_zones[g_id][order] = {material_id, l_id};
      }
    }
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Build the pure cells of each material mesh
   *
   * @note: used only for UnstructuredGrid output (i.e FillMaterial is required
   * by the user)
   */
  /*----------------------------------------------------------------------------*/
  void buildPureCells()
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "Building pure cells");
    for (auto& mpair : m_material_collection)
    {
      auto& [material_id, material] = mpair;
      const auto& [pcells_lids, _] = material->separateMixedAndPureCells();
      vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_7,
              "Building pure cells for material " << material_id);
      material->buildPureCells(pcells_lids);
    }
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Build mixed cells using the local algorithm
   *
   * This algorithm requires each material to owns necessary informations to
   * build at least one interface and at most two interfaces
   *
   * There is no limitation in the number of materials inside the mixed cell.
   *
   * For example with the following mixed cell:
   *
   *                  Interface 1
   *  ┌───────────────**──────────────┐
   *  │              **               │
   *  │            ***                │
   *  │ MatA      **                  │
   *  │          **                  ** Interface 2
   *  │         **                  **│
   *  │        **                  ** │
   *  │        *                  **  │
   *  │       **                 **   │
   *  │      **    MatB         **    │
   *  │     **                 **     │
   *  │    **                 **      │
   *  │   *                  **       │
   *  │ **                 ***        │
   *  │**                 **          │
   *  **                 **   MatC    │
   *  │                 **            │
   *  │                **             │
   *  └───────────────**──────────────┘
   *
   * MatA owns a normal and a distance for the "Interface 1" (first interface
   * data)
   * MatB owns a normal and a distance for the "Interface 1" (first
   * interface data) and a normal and a distance for the "Interface 2" (second
   * interface data)
   * MatC owns a normal and a distance for the "Interface 2" (first interface
   * data)
   *
   * For mat in (MatA, MatB, MatC):
   *  if mat has first interface data:
   *    build the interface using first interface data
   *  if mat has second interface data:
   *    build the interface using second interface data
   */
  /*----------------------------------------------------------------------------*/
  void buildMixedCellsLocalAlgorithm()
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "Building mixed cell with local algorithm");
    for (auto& mpair : m_material_collection)
    {
      auto& [material_id, material] = mpair;
      const auto& [_, mcells_lids] = material->separateMixedAndPureCells();
      for (const auto& cell_index : mcells_lids)
      {
        if (m_material_collection.at(material_id)->isMasked(cell_index))
        {
          continue;
        }
        vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_7,
                "material_id = " << material_id
                                 << ", cell index = " << cell_index);
        material->applyLocalAlgorithm(cell_index);
      }
    }
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Build mixed cells using the global algorithm
   *
   * This algorithm supposes that all materials in the mixed cells share the
   * same normale vector (onion skin).
   *
   * Each material of the mixed cell owns the necessary fields (distance and
   * normale) to build only one interface (by convention its "right" interface)
   *
   * To build both interfaces delimiting the material it is assumed that the
   * "left" interface is the same as the "right" interface of the previous
   * material.
   *
   * Thus it is imperative that this method is called successively on
   * all materials of the mixed cell in a sorted order (given by the order
   * array).
   *
   * There is no limitation in the number of materials inside the mixed cell.
   *
   * For example with the following mixed cell:
   *
   *                  Interface 1
   *  ┌───────────────**──────────────┐
   *  │              **               │
   *  │            ***                │
   *  │ MatA      **                  │
   *  │          **                  ** Interface 2
   *  │         **                  **│
   *  │        **                  ** │
   *  │        *                  **  │
   *  │       **                 **   │
   *  │      **    MatB         **    │
   *  │     **                 **     │
   *  │    **                 **      │
   *  │   *                  **       │
   *  │ **                 ***        │
   *  │**                 **          │
   *  **                 **   MatC    │
   *  │                 **            │
   *  │                **             │
   *  └───────────────**──────────────┘
   *
   * MatA owns a normal and a distance for the "Interface 1". It as order 0
   * MatB owns a normal and a distance for the "Interface 2". It as order 1
   * No matter what MatC owns. It as order 2
   *
   * For order in (0, 1, 2):
   *  if order == 0:
   *    Get the material MatA
   *      build the interface 1 using MatA's distance
   *  if order !0 and order !2:
   *    Get the material MatB
   *      build the interface 1 using previous mat distance
   *      (i.e MatA's distance)
   *      build the interface 2 using current mat distance
   *      (i.e MatB's * distance)
   *  if order == 2:
   *    Get the material MatC
   *      build the interface 2 using previous mat distance
   *      (i.e MatB's distance)
   *
   */
  /*----------------------------------------------------------------------------*/
  void buildMixedCellsGlobalAlgorithm()
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "Building mixed cell with global algorithm");
    for (const auto& [global_cell_index, zones] : m_mixed_cell_zones)
    {
      const auto nb_zones = zones.size();
      vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_7,
              "Cell " << global_cell_index << " has " << nb_zones << " zones");
      double prev_distance{0.};
      // This algorithm requires walking through all the materials in the cell
      // in ascending order
      for (const auto& [order, zone] : zones)
      {
        vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_7, "order = " << order);
        const auto& [material_index, cell_index] = zone;
        vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_7,
                "material_index = " << material_index);
        if (m_material_collection.at(material_index)->isMasked(cell_index))
        {
          continue;
        }
        const auto& current_distance =
            m_material_collection.at(material_index)
                ->applyGlobalAlgorithm(prev_distance, order, nb_zones,
                                       cell_index);
        prev_distance = current_distance;
      }
    }
  }

  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  /**
   * @brief Checks that the orders in a mixed cell start at zero and are
   * consecutives.
   *
   * The global algorithm treats one material after each other and needs
   * information about previous (in order sense) material in the cell.
   *
   * @return true: if orders are as expeceted
   * @return false: otherwise
   */
  /*----------------------------------------------------------------------------*/
  bool checkMixedCellsZones()
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "Checking if the mixed cells zones are correctly defined");

    // This lambda creates a vector of consecutive values from O to
    // upper_val - 1
    const auto consecutive_orders_maker = [](const size_t upper_val) {
      std::vector<OrderType> consecutives(upper_val);
      std::iota(std::begin(consecutives), std::end(consecutives), 0);
      return consecutives;
    };

    // This lambda builds a vector made of the orders presents in zones
    // They are sorted due to the fact that ZonesCollectionType is a std::map
    const auto cell_orders_packer = [](const ZonesCollectionType& zones) {
      std::vector<OrderType> pack{};
      for (const auto& [order, zone] : zones)
      {
        pack.push_back(order);
      }
      return pack;
    };

    // This lambda just dump the vector into a string
    const auto vector_dumper = [](const std::vector<OrderType>& data) {
      std::ostringstream msg;
      msg << "[";
      for (const auto& val : data)
      {
        msg << val << ", ";
      }
      msg << "]";
      return msg.str();
    };

    for (const auto& [global_cell_index, zones] : m_mixed_cell_zones)
    {
      const auto expected_consecutive_orders{
          consecutive_orders_maker(zones.size())};
      const auto cell_orders{cell_orders_packer(zones)};
      if (cell_orders != expected_consecutive_orders)
      {
        vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_ERROR,
                "The order is not respected inside cell with global id "
                    << global_cell_index << ". Orders are "
                    << vector_dumper(cell_orders) << " when expecting "
                    << vector_dumper(expected_consecutive_orders) << "\n"
                    << "This error is usually due to a badly formed database "
                       "or to an error occuring while reading the database");
        return false;
      }
    }
    return true;
  }

  /// @brief Collection of MaterialAbstractMethod object. Either
  /// MaterialContourMethod or MaterialClipMethod
  std::map<MaterialIdType, std::unique_ptr<MaterialAbstractMethod>>
      m_material_collection;

  /// @brief Structure holding mixed cells informations. It associates the
  /// global mixed cell id, to a map itself associating order number to a pair
  /// (material id, local cell id)
  MixedCellsLocalizationType m_mixed_cell_zones;
};

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
vtkStandardNewMacro(vtkMaterialInterface);

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
vtkMaterialInterface::vtkMaterialInterface() = default;

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
vtkMaterialInterface::~vtkMaterialInterface() = default;

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// NOLINTNEXTLINE(readability-named-parameter)
int vtkMaterialInterface::RequestData(vtkInformation* vtkNotUsed(request),
                                      vtkInformationVector** inputVector,
                                      vtkInformationVector* outputVector)
{
  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "vtkMaterialInterface::RequestData");

  // Needs arithmetic pointer operations to walk through vtkInformationVector
  // NOLINTBEGIN
  auto* compositeInput = vtkCompositeDataSet::SafeDownCast(
      inputVector[0]->GetInformationObject(0)->Get(
          vtkDataObject::DATA_OBJECT()));
  // NOLINTEND

  auto* compositeOutput = vtkMultiBlockDataSet::SafeDownCast(
      outputVector->GetInformationObject(0)->Get(vtkDataObject::DATA_OBJECT()));

  if (compositeInput == nullptr || compositeOutput == nullptr)
  {
    vtkErrorMacro(<< "Invalid algorithm connection\n");
    return 0;
  }
  compositeOutput->CopyStructure(compositeInput);

  const InterfaceInfos interface_1{this->NormalArrayName,
                                   this->DistanceArrayName};
  const InterfaceInfos interface_2{this->NormalArrayName2,
                                   this->DistanceArrayName2};
  const InterfaceInfosPair interfaces_infos{interface_1, interface_2,
                                            this->OrderArrayName};
  const SamplingPatternEnum mode{this->Mode};
  const auto& if_status = interfaces_infos.getInterfaceStatus(mode);

  // Ensure that the informations about interfaces are those required
  // for the selected mode (i.e ensure the user entered coherent choices for
  // Mode and array names. This is usefull only for batch invocation. If the
  // user uses the GUI then he cannot enter uncoherent choices)
  if (if_status == InterfaceStatus::UNDEFINED)
  {
    vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_ERROR,
            "The interfaces informations "
                << interfaces_infos
                << "are not in accordance with the selected mode " << mode);
    return 0;
  }

  using InternalClipT = vtkMaterialInterface::vtkInternal<MethodType::CLIP>;
  using InternalContourT =
      vtkMaterialInterface::vtkInternal<MethodType::CONTOUR>;

  if (this->FillMaterialOn != 0)
  {
    auto internal = std::make_unique<InternalClipT>();
    return this->RequestDataDispatch(compositeInput, compositeOutput, if_status,
                                     interfaces_infos, std::move(internal));
  } else
  {
    auto internal = std::make_unique<InternalContourT>();
    return this->RequestDataDispatch(compositeInput, compositeOutput, if_status,
                                     interfaces_infos, std::move(internal));
  }

  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
          "vtkMaterialInterface::RequestData terminated");
  return 1;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void vtkMaterialInterface::PrintSelf(ostream& output, vtkIndent indent)
{
  this->Superclass::PrintSelf(output, indent);
  output << indent << "Mask: " << this->Mask << "\n";
  output << indent << "NegativeValueMaskOn: " << this->NegativeValueMaskOn
         << "\n";
  if (this->MaskArrayName != nullptr)
  {
    output << indent << "MaskArrayName: " << this->MaskArrayName << "\n";
  }
  output << indent << "Mode: " << this->Mode << "\n";
  output << indent << "FillMaterialOn: " << this->FillMaterialOn << "\n";
  if (this->OrderArrayName != nullptr)
  {
    output << indent << "OrderArrayName: " << this->OrderArrayName << "\n";
  }
  if (this->DistanceArrayName != nullptr)
  {
    output << indent << "DistanceArrayName: " << this->DistanceArrayName
           << "\n";
  }
  if (this->NormalArrayName != nullptr)
  {
    output << indent << "NormalArrayName: " << this->NormalArrayName << "\n";
  }
  if (this->DistanceArrayName2 != nullptr)
  {
    output << indent << "DistanceArrayName2: " << this->DistanceArrayName2
           << "\n";
  }
  if (this->NormalArrayName2 != nullptr)
  {
    output << indent << "NormalArrayName2: " << this->NormalArrayName2 << "\n";
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class InternalOutputType>
int vtkMaterialInterface::RequestDataDispatch(
    vtkCompositeDataSet* composite_input,
    vtkMultiBlockDataSet* composite_output, const InterfaceStatus if_status,
    const InterfaceInfosPair& interfaces_infos,
    std::unique_ptr<InternalOutputType> internal)
{
  auto input_iterator{vtk::TakeSmartPointer(composite_input->NewIterator())};
  for (input_iterator->InitTraversal(); !input_iterator->IsDoneWithTraversal();
       input_iterator->GoToNextItem())
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "CurrentDataObject is "
                << input_iterator->GetCurrentDataObject()->GetClassName());
    vtkDataSet* input =
        vtkDataSet::SafeDownCast(input_iterator->GetCurrentDataObject());
    const auto material_index = input_iterator->GetCurrentFlatIndex();
    internal->registerMaterial(this->NegativeValueMaskOn, this->MaskArrayName,
                               if_status, material_index, interfaces_infos,
                               input, input_iterator, composite_output);
  }

  if (if_status == InterfaceStatus::WITH_ORDER)
  {
    return internal->executeGlobalAlgorithm();
  } else if (if_status == InterfaceStatus::WITH_ONE_INTERFACE ||
             if_status == InterfaceStatus::WITH_TWO_INTERFACES)
  {
    return internal->executeLocalAlgorithm();
  }
  return 0;
}
