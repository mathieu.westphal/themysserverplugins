/**
 * @class vtkMetaMaterialInterface
 * @brief a meta filter that let user choose between material filters
 */

#ifndef VTK_META_MATERIAL_INTERFACE_H
#define VTK_META_MATERIAL_INTERFACE_H

#include <vtkMultiBlockDataSetAlgorithm.h>
#include <vtkYoungsMaterialInterface.h>

#include "vtkMaterialInterface.h"

class vtkMetaMaterialInterface : public vtkMultiBlockDataSetAlgorithm
{
public:
  static vtkMetaMaterialInterface* New();
  vtkTypeMacro(vtkMetaMaterialInterface, vtkMultiBlockDataSetAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  ///@{
  /**
   * Control if youngs or material interface should be used
   * Default is standard material interface
   */
  vtkSetMacro(UseYoungs, bool);
  vtkGetMacro(UseYoungs, bool);
  ///@}

  ///@{
  /**
   * Set/Get the MaterialInterface to use
   */
  vtkSetSmartPointerMacro(MaterialInterface, vtkMaterialInterface);
  vtkGetSmartPointerMacro(MaterialInterface, vtkMaterialInterface);
  ///@}

  ///@{
  /**
   * Set/Get the YoungsMaterialInterface to use
   */
  vtkSetSmartPointerMacro(YoungsMaterialInterface, vtkYoungsMaterialInterface);
  vtkGetSmartPointerMacro(YoungsMaterialInterface, vtkYoungsMaterialInterface);
  ///@}

protected:
  vtkMetaMaterialInterface();
  ~vtkMetaMaterialInterface() override;

  int RequestData(vtkInformation*, vtkInformationVector**,
                  vtkInformationVector*) override;

  void ReportReferences(vtkGarbageCollector*) override;

private:
  vtkMetaMaterialInterface(const vtkMetaMaterialInterface&) = delete;
  void operator=(const vtkMetaMaterialInterface&) = delete;

  bool UseYoungs = false;
  vtkSmartPointer<vtkMaterialInterface> MaterialInterface;
  vtkSmartPointer<vtkYoungsMaterialInterface> YoungsMaterialInterface;
};

#endif /* VTK_MATERIAL_INTERFACE_H */
