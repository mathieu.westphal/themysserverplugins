/*=========================================================================

Program:   Visualization Toolkit
            Module:    vtkMaterialInterface.h

                       Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
    All rights reserved.
    See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

            This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class vtkMaterialInterface
 * @brief adjusts the mesh of a material according to the defined interfaces
 *
 * This file includes the algorithm for adjusting the mesh of each material
 * according to the definition of the interfaces. Interfaces define sub-cell
 * information. There will be at most two interfaces per cell for a material.
 *
 * This algorithm requires that a quantity be assigned to the PedigreeIds
 * attribute of the cells in order to identify the mixed cells between
 * materials. This attribute describes the globally unique identifier of a cell
 * seen by the code.
 *
 * In the CEA historical mode, if a mixed cell comprises more than one material,
 * it is considered that the normal of the interfaces between these materials
 * resets the same, this is the case of an onion-skin interface.
 * This description of the interfaces is done by defining three value fields
 * at the cell level:
 * - order,
 * - normal, and
 * - distance (distance from the interface plane to the origin)
 * In the case of a pure cell, the values of these fields are all zero.
 * The order describes the order in which the materials traverse in a mixed cell
 * following the normal (outgoing) thus described.
 * For a normal nonzero, the value of the order is positive, with a continuous
 * numbering starting from the first material whose order is 0 and according to
 * the outgoing normal of the interface. The value of the normal remains
 * unchanged for 2 and more materials. The value of the distance from the
 * interface to the origin associated with a material of a mixed cell
 * corresponds either to the unique interface or to the interface with the
 * material having a higher order in this mixed cell. Sometimes the value of
 * order can nevertheless be used as a mask to hide cells when its value is
 * negative.
 *
 * In the MODE_WITH_TWO_OUTGOING_INTERFACES mode, if a mixed cell includes more
 * than one material, we can differentiate the normals of the interfaces with
 * the constraint that the two interfaces must not meet in the cell.
 * This description of an interface is done by defining two value fields at the
 * cell level:
 * - normal,
 * - distance (distance from the interface plane to the origin)
 * One description for MODE_WITH_ONE_OUTGOING_INTERFACE and two description
 * for MODE_WITH_TWO_OUTGOING_INTERFACES.
 * In the case of a pure cell, the values of these fields are all zero.
 *
 * If the normal is null, it is because we are dealing with a pure cell, a
 * single material.
 *
 * This filter also makes it possible to use a field of values ​​to mask, to
 * remove certain cells from the output. This is done if the value is null or if
 * it is strickly negative.
 *
 * @par Thanks
 * This implementation is proposed by Jacques-Bernard Lekien of the society :
 * CEA, DAM, DIF, F-91297 Arpajon, France
 */

#ifndef VTK_MATERIAL_INTERFACE_H
#define VTK_MATERIAL_INTERFACE_H

// IWYU pragma: no_include <vtkIOStream.h>
// IWYU pragma: no_include <vtkObject.h>

#include <memory> // for std::unique_ptr
#include <ostream>

#include <vtkSetGet.h>

#include "vtkMultiBlockDataSetAlgorithm.h"

class vtkCompositeDataSet;
class vtkMultiBlockDataSet;
class vtkIndent;
class vtkInformation;
class vtkInformationVector;
class InterfaceInfosPair;
enum class InterfaceStatus;

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
enum class MethodType { CLIP, CONTOUR };

/*----------------------------------------------------------------------------*/
/**
 * @brief This filter proposes two algorithms to construct interfaces in a mixed
 * cell.
 *
 * It requires a CompositeDataSet as input. Each member of this dataset
 * is supposed to represent a material mesh holding a certain amount of cells
 * that are shared with other materials (i.e mixed cells).
 *
 * Inside a material mesh, a mixed cell will hold at most two interfaces.
 * A pure cell, is made of only one material and thus has no interface.
 *
 * Two algorithms are available to construct the interfaces.
 *
 * First one is global, because it implies to walk through each and every
 * material of the mixed cell in a sorted way (the sort order is given by the
 * order array). It means that all material meshes have to be available to
 * construct a correct interface.
 *
 * This global algorithm requires for each and every of the material meshes to
 * have interface arrays:
 * - an array defining the distance of the interface to the origin in the cell;
 * - an array defining the normal of the interface in the cell;
 * - an array defining the order of the material in the cell;
 *
 * This global algorithm require every material mesh (involved in mixed cells)
 * to have those interface arrays. More over it requires a quantity to have
 * PedigreeIds attribute. This quantity is the global unique identifier of a
 * cell. It means that two (or more) cells of two (or more) different material
 * meshes should have the same PedigreeId if they are part of the same mixed
 * cell.
 *
 * Second algorithm is local. Each material interface may be constructed
 * independtly from other materials. Thus it is not necessary to have all
 * material meshes available.
 * @warning: this second algorithm is experimental for now.
 *
 * This local algorithm requires for a material mesh to have interface arrays
 * defined:
 * - one or two arrays defining the distance of the first and second interfaces
 *   to the origin in the cell;
 * - one or tow arrays defining the normal of the first and second interface in
 * the cell;
 *
 * This local algorithm do not require every material mesh to have those
 * interface arrays defined.
 *
 * Both algorithms consider a cell is a mixed cell if one of its interfaces has
 * a non zero normale. Otherwise the cell is pure.
 *
 */
/*----------------------------------------------------------------------------*/
class vtkMaterialInterface : public vtkMultiBlockDataSetAlgorithm
{
public:
  static vtkMaterialInterface* New();

  // clang-format off
  vtkTypeMacro(vtkMaterialInterface, vtkMultiBlockDataSetAlgorithm)

  void PrintSelf(ostream& output, vtkIndent indent) override;
  // clang-format on

  ///@{
  /**
   * Enabling the use of a mask or not
   */
  vtkSetMacro(Mask, int);
  vtkGetMacro(Mask, int);
  vtkBooleanMacro(Mask, int);
  //@}

  ///@{
  /**
   * When NegativeValueMaskOn is checked, cells with a strickly negative mask
   * value are not output (considered hidden from view).
   */
  vtkSetMacro(NegativeValueMaskOn, int);
  vtkGetMacro(NegativeValueMaskOn, int);
  vtkBooleanMacro(NegativeValueMaskOn, int);
  //@}

  ///@{
  /**
   * Choice of a mask array name
   */
  void SetMaskArrayName(int, int, int, int, const char* name)
  {
    this->SetMaskArrayName(name);
  }
  vtkSetStringMacro(MaskArrayName);
  vtkGetStringMacro(MaskArrayName);
  //@}

  ///@{
  /**
   * When FillMaterialOn is set to 1, the volume containing material is output
   * and not only the interface surface.
   */
  vtkSetMacro(FillMaterialOn, int);
  vtkGetMacro(FillMaterialOn, int);
  vtkBooleanMacro(FillMaterialOn, int);
  //@}

  ///@{
  /**
   * Choice of a possible sampling scheme
   */
  vtkGetMacro(Mode, int);
  vtkSetClampMacro(Mode, int, 0, 2);
  //@}

  ///@{
  /**
   * Choice of a order array name
   */
  void SetOrderArrayName(int, int, int, int, const char* name)
  {
    this->SetOrderArrayName(name);
  }
  vtkSetStringMacro(OrderArrayName);
  vtkGetStringMacro(OrderArrayName);
  //@}

  // First interface by material
  ///@{
  /**
   * Choice of the distance array name from the first interface
   */
  void SetDistanceArrayName(int, int, int, int, const char* name)
  {
    this->SetDistanceArrayName(name);
  }
  vtkSetStringMacro(DistanceArrayName);
  vtkGetStringMacro(DistanceArrayName);
  //@}

  ///@{
  /**
   * Choice of the normal array name from the first interface
   */
  void SetNormalArrayName(int, int, int, int, const char* name)
  {
    this->SetNormalArrayName(name);
  }
  vtkSetStringMacro(NormalArrayName);
  vtkGetStringMacro(NormalArrayName);
  //@}

  // Second interface by material
  ///@{
  /**
   * Choice of the distance array name from the second interface
   */
  void SetDistanceArrayName2(int, int, int, int, const char* name)
  {
    this->SetDistanceArrayName2(name);
  }
  vtkSetStringMacro(DistanceArrayName2);
  vtkGetStringMacro(DistanceArrayName2);
  //@}

  ///@{
  /**
   * Choice of the normal array name from the second interface
   */
  void SetNormalArrayName2(int, int, int, int, const char* name)
  {
    this->SetNormalArrayName2(name);
  }
  vtkSetStringMacro(NormalArrayName2);
  vtkGetStringMacro(NormalArrayName2);
  //@}

protected:
  template <MethodType> class vtkInternal;

  vtkMaterialInterface();
  ~vtkMaterialInterface() override;

  int RequestData(vtkInformation*, vtkInformationVector**,
                  vtkInformationVector*) override;

  int Mask = 0;

  int NegativeValueMaskOn = 0;
  char* MaskArrayName = nullptr; // TODO No free by destructor

  int Mode = 2; // => MODE_OLD_SCOOL_CEA_INTERFACES

  int FillMaterialOn = 0;
  char* OrderArrayName = nullptr; // TODO No free by destructor

  char* DistanceArrayName = nullptr; // TODO No free by destructor
  char* NormalArrayName = nullptr;   // TODO No free by destructor

  char* DistanceArrayName2 = nullptr; // TODO No free by destructor
  char* NormalArrayName2 = nullptr;   // TODO No free by destructor

private:
  vtkMaterialInterface(const vtkMaterialInterface&) = delete;
  void operator=(const vtkMaterialInterface&) = delete;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Dispatch the RequestData treatment to the Internal class with type
   * in argument
   *
   * @tparam InternalOutputType : type of the Internal class
   * @param composite_input : input object
   * @param composite_output : output object
   * @param if_status : interface status
   * @param interfaces_infos : interfaces informations
   * @param internal : pointer to internal class that should treats the
   * RequestData
   */
  /*----------------------------------------------------------------------------*/
  template <class InternalOutputType>
  int RequestDataDispatch(vtkCompositeDataSet* composite_input,
                          vtkMultiBlockDataSet* composite_output,
                          const InterfaceStatus if_status,
                          const InterfaceInfosPair& interfaces_infos,
                          std::unique_ptr<InternalOutputType> internal);
};

#endif /* VTK_MATERIAL_INTERFACE_H */
