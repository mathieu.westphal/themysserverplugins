/*----------------------------------------------------------------------------*/
/**
 * @file vtkMaterialInterface_MaterialAbstract.h
 * @brief Declares the MaterialAbstractMethod class
 * @date 2023-02-13
 *
 * @author Guillaume Peillex (guillaume.peillex@cea.fr)
 * @copyright Copyright CEA, DAM, DIF, F-91297 Arpajon, France 2023
 *
 */
/*----------------------------------------------------------------------------*/
#ifndef VTK_MATERIAL_INTERFACE_MATERIAL_ABSTRACT_H
#define VTK_MATERIAL_INTERFACE_MATERIAL_ABSTRACT_H
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// IWYU pragma: no_include <fwd>
#include <array>
#include <string> // IWYU pragma: keep
#include <vector>

#include <vtkSmartPointer.h>

#include "vtkMaterialInterface_InterfaceData.h"
#include "vtkMaterialInterface_InterfaceInfos.h"
#include "vtkMaterialInterface_Types.h"

class vtkDataSet;
class vtkPointSet;
class vtkDataArray;
class vtkIdTypeArray;
class vtkMergePoints;
class InterfaceInfosPair;

/*----------------------------------------------------------------------------*/
/**
 * @brief The MaterialAbstractMethod class represent a Material object used to
 * construct interfaces
 *
 * This class is abstract and has to be inherited (as in the
 * MaterialClipMethod and MaterialContourMethod cases)
 */
/*----------------------------------------------------------------------------*/
class MaterialAbstractMethod
{
public:
  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  // Do not authorize a MaterialTempalte to be built with a nullptr value for
  // the m_input_material_mesh
  MaterialAbstractMethod() = delete;

  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  MaterialAbstractMethod(vtkDataSet* input_material_mesh);

  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  virtual ~MaterialAbstractMethod() = default;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Build and/or initialize the output mesh
   *
   */
  /*----------------------------------------------------------------------------*/
  virtual void initOutputMaterialMesh() = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the Output Mesh object
   *
   * @return vtkSmartPointer<vtkPointSet>
   */
  /*----------------------------------------------------------------------------*/
  virtual vtkSmartPointer<vtkPointSet> getOutputMaterialMesh() = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return true if all the interface arrays are null
   *
   * @return true
   * @return false
   */
  /*----------------------------------------------------------------------------*/
  bool allInterfaceArraysNull();

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Build the pure cells in the output mesh
   *
   * @param pcells_lids : local cell ids
   */
  /*----------------------------------------------------------------------------*/
  virtual void buildPureCells(const std::vector<LocalIdType>& pcells_lids) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Implementation of the global algorithm for the current material
   *
   * @param previous_distance: interface distance to the origin of the
   * previous material
   * @param _order: order of this material
   * @param _number_of_zones: number of zones (i.e materials) present in the
   * mixed cell
   * @param _cell_index: local id of the mixed cell (i.e id of the mixed cell in
   * this material mesh)
   * @return double: interface distance to the origin of this material (will be
   * the previous distance for the next material)
   */
  /*----------------------------------------------------------------------------*/
  virtual double applyGlobalAlgorithm(const double& previous_distance,
                                      OrderType _order, int _number_of_zones,
                                      LocalIdType _cell_index) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Implementation of the local algorithm for the current material
   *
   * @param _cell_index: local id of the mixed cell (i.e id of the mixed cell in
   * this material mesh)
   */
  /*----------------------------------------------------------------------------*/
  virtual void applyLocalAlgorithm(LocalIdType _cell_index) = 0;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns true if the arrays necessary to the filter treatment are
   * available
   *
   * @param if_status : interfaces status
   * @return true
   * @return false
   */
  /*----------------------------------------------------------------------------*/
  bool checkInterfaceStatusRequirements(InterfaceStatus if_status);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns two vectors. First one contains local cell index of each
   * pure cell, second one contains local cell index of each mixed cell
   *
   * @return std::array<std::vector<vtkIdType>, 2>
   */
  /*----------------------------------------------------------------------------*/
  std::array<std::vector<LocalIdType>, 2> separateMixedAndPureCells() const;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return true if the cell is masked, false otherwise
   *
   * @param l_id : local id of the cell
   * @return true
   * @return false
   */
  /*----------------------------------------------------------------------------*/
  bool isMasked(LocalIdType l_id);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return the material order value in the cell in argument
   *
   * @param cell_index : local index of the cell
   * @return int
   */
  /*----------------------------------------------------------------------------*/
  int getOrder(LocalIdType cell_index) const;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the Global Cell Id of the local cell in argument
   *
   * @param l_id : local cell id
   * @return GlobalIdType
   */
  /*----------------------------------------------------------------------------*/
  GlobalIdType getGlobalCellId(LocalIdType l_id) const;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Initialize the members which are arrays.
   *
   * @param mask_name : name of the array holding mask values
   * @param negative_value_mask_on : whether to compare the value of the mask
   * array with less than zero (true) or equal to zero (false)
   * @param order_name : name of the array holding order infos
   * @param interface : first interface infos
   * @param interface_2 : second interface infos
   * @return true : in case of success
   * @return false : in cas of failure
   */
  /*----------------------------------------------------------------------------*/
  bool fetchArraysFromCellData(const std::string& mask_name,
                               bool negative_value_mask_on,
                               const InterfaceInfosPair& infos_pair);

private:
  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns true if the arrays necessary to the filter treatment
   * in the "one interface" case are available
   *
   * @return true
   * @return false
   */
  /*----------------------------------------------------------------------------*/
  bool checkOneInterfaceRequirements();

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns true if the arrays necessary to the filter treatment
   * in the "two interfaces" case are available
   *
   * @return true
   * @return false
   */
  /*----------------------------------------------------------------------------*/
  bool checkTwoInterfacesRequirements();

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns true if the arrays necessary to the filter treatment
   * in the "order" case are available
   *
   * @return true
   * @return false
   */
  /*----------------------------------------------------------------------------*/
  bool checkOrderRequirements();

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Retrieve the order array from the cell data
   *
   * @note : this is a wrapper around getArrayFromCellData that checks that
   * the order array is different from a uniform array filled with -1
   * @param order_name : name of the order array
   * @return vtkDataArray*
   */
  /*----------------------------------------------------------------------------*/
  vtkDataArray* getOrderFromCellData(const std::string& order_name);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Retrieve the array from the cell data and downcast it to the desired
   * type.
   * Returns nullptr if the array name is empty (or "None") or if the array
   * is not found in the cell data
   *
   * @tparam ArrayType : the type to downcast to
   * @param array_name : name of the array
   * @return ArrayType*
   */
  /*----------------------------------------------------------------------------*/
  template <class ArrayType>
  ArrayType* getArrayFromCellData(const std::string& array_name);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Build the mask variable by comparing values of the threshold array
   * to zero
   *
   * @tparam ArrayType : type of the array holding the values to be compared to
   * zero
   * @param use_less_comparator : if true then the comparison is made using the
   * less comparator, i.e the value of the threshold array should be strictly
   * less than zero for the mask to be true. Otherwise the equal comparator is
   * used
   * @param threshold_array : array holding the values to be compared to zero
   * @return std::vector<bool>
   */
  /*----------------------------------------------------------------------------*/
  template <class ArrayType>
  std::vector<bool> buildMaskArray(bool use_less_comparator,
                                   ArrayType* threshold_array);

protected:
  std::vector<bool> m_mask;
  vtkDataSet* m_input_material_mesh;
  InterfaceData m_interface_data;
  InterfaceData m_interface2_data;
  vtkDataArray* m_order;
  vtkIdTypeArray* m_global_cell_index;
  vtkSmartPointer<vtkMergePoints> m_output_point_locator;
};

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // VTK_MATERIAL_INTERFACE_MATERIAL_ABSTRACT_H
