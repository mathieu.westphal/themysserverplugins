#ifndef VTK_MATERIAL_INTERFACE_MATERIAL_CLIP_METHOD_IMPL_H
#define VTK_MATERIAL_INTERFACE_MATERIAL_CLIP_METHOD_IMPL_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// IWYU pragma: no_include <vtkGenericDataArray.txx>
// IWYU pragma: no_include "vtkSystemIncludes.h"
// IWYU pragma: no_include "vtkMaterialInterface_Utils-Impl.h"
// IWYU pragma: no_include <vtkLogger.h>
#include "vtkMaterialInterface_MaterialClipMethod.h"

#include <array>
#include <cassert>
#include <stdexcept>
#include <tuple>

#include <vtkCell.h>
#include <vtkCellArray.h>
#include <vtkCellArrayIterator.h>
#include <vtkCellData.h>
#include <vtkDataSet.h>
#include <vtkDoubleArray.h>
#include <vtkIdList.h>
#include <vtkLine.h>
#include <vtkMergePoints.h>
#include <vtkNew.h>
#include <vtkPVLogger.h>
#include <vtkPointData.h>
#include <vtkPoints.h>
#include <vtkPolyLine.h>
#include <vtkPolyVertex.h>
#include <vtkPolygon.h>
#include <vtkQuad.h>
#include <vtkSmartPointer.h>
#include <vtkTetra.h>
#include <vtkTriangle.h>
#include <vtkType.h>
#include <vtkUnsignedCharArray.h>
#include <vtkUnstructuredGrid.h>
#include <vtkVertex.h>
#include <vtkWedge.h>

#include "vtkMaterialInterface_InterfaceData.h"
#include "vtkMaterialInterface_Types.h"
#include "vtkMaterialInterface_Utils.h"

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class OutputGridT>
MaterialClipMethod<OutputGridT>::MaterialClipMethod(
    vtkDataSet* input_material_mesh)
    : MaterialAbstractMethod(input_material_mesh)
{
  static_assert(std::is_same_v<OutputGridT, vtkPolyData> ||
                    std::is_same_v<OutputGridT, vtkUnstructuredGrid>,
                "The clip method can not handle output mesh that are not "
                "vtkPolyData nor vtkUnstructuredGrid!");
  // Need to do a copy of the input mesh and an interpolation of the point data
  // in order to have an output that is the same as the input.
  // For more information please look at the comment in vtkClipDataSet.cxx in
  // VTK in the RequestData method. Here m_real_input_material_mesh is a class
  // member in order to keep a reference on it and so to have the underlying
  // pointer always valid.
  m_real_input_material_mesh.TakeReference(
      m_input_material_mesh->NewInstance());
  m_real_input_material_mesh->CopyStructure(m_input_material_mesh);
  m_real_input_material_mesh->GetFieldData()->PassData(
      m_input_material_mesh->GetFieldData());
  m_real_input_material_mesh->GetCellData()->PassData(
      m_input_material_mesh->GetCellData());
  m_real_input_material_mesh->GetPointData()->InterpolateAllocate(
      m_input_material_mesh->GetPointData(), 0, 0, 1);
  m_input_material_mesh = m_real_input_material_mesh.GetPointer();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class OutputGridT>
void MaterialClipMethod<OutputGridT>::initOutputMaterialMesh()
{
  m_output_material_mesh =
      build_output_mesh<OutputGridT>(m_input_material_mesh);

  m_output_point_locator = vtkSmartPointer<vtkMergePoints>::New();
  m_output_point_locator->InitPointInsertion(
      m_output_material_mesh->GetPoints(), m_input_material_mesh->GetBounds());
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class OutputGridT>
void MaterialClipMethod<OutputGridT>::buildPureCells(
    const std::vector<LocalIdType>& pcells_lids)
{
  for (const auto local_index : pcells_lids)
  {
    if (isMasked(local_index))
    {
      continue;
    }
    auto* current_cell = m_input_material_mesh->GetCell(local_index);
    vtkIdList* pointIds = current_cell->GetPointIds();
    vtkPoints* points = current_cell->GetPoints();
    const vtkIdType nbPts = current_cell->GetNumberOfPoints();
    std::vector<vtkIdType> pts(nbPts);
    for (int iPt = 0; iPt < nbPts; ++iPt)
    {
      std::array<double, 3> coords{};
      points->GetPoint(iPt, coords.data());
      if (m_output_point_locator->InsertUniquePoint(coords.data(), pts[iPt]) !=
          0)
      {
        m_output_material_mesh->GetPointData()->CopyData(
            m_input_material_mesh->GetPointData(), pointIds->GetId(iPt),
            pts[iPt]);
      }
    }
    m_output_material_mesh->InsertNextCell(current_cell->GetCellType(), nbPts,
                                           pts.data());
    m_output_material_mesh->GetCellData()->CopyData(
        m_input_material_mesh->GetCellData(), local_index,
        m_output_material_mesh->GetNumberOfCells() - 1);
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// NOLINTBEGIN(bugprone-easily-swappable-parameters)
template <class OutputGridT>
double MaterialClipMethod<OutputGridT>::applyGlobalAlgorithm(
    const double& previous_distance, const OrderType _order,
    const int _number_of_zones, const LocalIdType _cell_index)
// NOLINTEND(bugprone-easily-swappable-parameters)
{
  if (m_input_material_mesh->GetCell(_cell_index)->GetCellType() ==
      VTK_POLYHEDRON)
  {
    vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_ERROR,
            "Unable to create clip cell when it is a VTK_POLYHEDRON");
    throw std::runtime_error(
        "Unable to deal with cell having VTK_POLYHEDRON type!");
  }
  // In theory this should never failed because the verification is done
  // before
  assert(this->m_order && this->m_interface_data &&
         "Need the order array and interface data");
  const auto normale{this->m_interface_data.getNormaleInCell(_cell_index)};
  const double distance{this->m_interface_data.getDistanceInCell(_cell_index)};

  const vtkIdType& first_new_cell_index{
      m_output_material_mesh->GetNumberOfCells()};
  if (_order == 0)
  {
    // For the first material of the cell (order = 0) the interface is the
    // right boundary. Use current distance with no reversal of the normal
    vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_7,
            "Clip the cell to build the right boundary");
    this->clipInCellForSideMaterial(normale, distance, false, _cell_index);
  } else if (_order == _number_of_zones - 1)
  {
    // For the last material of the cell (order = nb_zones - 1) the interface
    // is the left boundary. This boundary is in fact the right interface of
    // the previous material. Use previous distance and reverse the normal
    vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_7,
            "Clip the cell to build the left boundary");
    this->clipInCellForSideMaterial(normale, previous_distance, true,
                                    _cell_index);
  } else
  {
    // For material that are not first nor last (ie embedded in the cell) left
    // and right interfaces have to be built
    vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_7,
            "Clip the cell to build both left and right boundaries");
    this->clipInCellForEmbeddedMaterial(previous_distance, normale, true,
                                        distance, normale, false, _cell_index);
  }

  const vtkIdType last_new_cell_index{
      m_output_material_mesh->GetNumberOfCells()};
  vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_7,
          "The clip of the cell produced "
              << last_new_cell_index - first_new_cell_index << " new cells");

  if constexpr (std::is_same_v<OutputGridT, vtkUnstructuredGrid>)
  {
    // Adds a type for the cells that have been created by the clip process
    this->assignNewCellsTypes(first_new_cell_index, last_new_cell_index,
                              _cell_index);
  }
  return distance;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class OutputGridT>
void MaterialClipMethod<OutputGridT>::applyLocalAlgorithm(
    const LocalIdType _cell_index)
{
  assert((m_interface_data || m_interface2_data) &&
         "Should have been checked previously!");
  if (m_input_material_mesh->GetCell(_cell_index)->GetCellType() ==
      VTK_POLYHEDRON)
  {
    vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_ERROR,
            "Unable to create clip cell when it is a VTK_POLYHEDRON");
    throw std::runtime_error(
        "Unable to deal with cell having VTK_POLYHEDRON type!");
  }

  const vtkIdType first_new_cell_index{
      m_output_material_mesh->GetNumberOfCells()};

  if (!m_interface2_data)
  {
    const auto& normale = m_interface_data.getNormaleInCell(_cell_index);
    const auto& distance = m_interface_data.getDistanceInCell(_cell_index);
    this->clipInCellForSideMaterial(normale, distance, false, _cell_index);
  } else
  {
    const auto& normale = m_interface_data.getNormaleInCell(_cell_index);
    const auto& distance = m_interface_data.getDistanceInCell(_cell_index);
    const auto& normale_2 = m_interface2_data.getNormaleInCell(_cell_index);
    const auto& distance_2 = m_interface2_data.getDistanceInCell(_cell_index);

    this->clipInCellForEmbeddedMaterial(distance, normale, false, distance_2,
                                        normale_2, false, _cell_index);
  }

  const vtkIdType last_new_cell_index{
      m_output_material_mesh->GetNumberOfCells()};
  vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_7,
          "The clip of the cell produced "
              << last_new_cell_index - first_new_cell_index << " new cells");

  if constexpr (std::is_same_v<OutputGridT, vtkUnstructuredGrid>)
  {
    // browse the last cells that have been generated to associate them with a
    // cell type
    this->assignNewCellsTypes(first_new_cell_index, last_new_cell_index,
                              _cell_index);
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class OutputGridT>
void MaterialClipMethod<OutputGridT>::clipInCellForSideMaterial(
    const InterfaceNormale& normale, const double& distance,
    const bool opposite, const LocalIdType cell_lid)
{
  auto* cell{m_input_material_mesh->GetCell(cell_lid)};
  auto cell_conn{this->getCells(cell)};

  const auto& pts_coords =
      extract_nodes_coordinates<vtkDataSet>(m_input_material_mesh, cell);
  auto pts_distance =
      compute_distance_to_plan(pts_coords, normale, distance, opposite);
  cell->Clip(0, pts_distance.Get(), m_output_point_locator, cell_conn,
             m_input_material_mesh->GetPointData(),
             m_output_material_mesh->GetPointData(),
             m_input_material_mesh->GetCellData(), cell_lid,
             m_output_material_mesh->GetCellData(),
             1); // 1 => inside, cell_scalars<=0 is in
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class OutputGridT>
void MaterialClipMethod<OutputGridT>::clipInCellForEmbeddedMaterial(
    const double& left_distance, const InterfaceNormale& left_normale,
    const bool left_opposite, const double& right_distance,
    const InterfaceNormale& right_normale, const bool right_opposite,
    const LocalIdType cell_lid)
{
  // Case of the cell that must be cut twice to define an area, the part of
  // a material in a cell. The modus operandi is that defined for the "onion
  // skin" mixed cell type. The two cutting planes have the same normal,
  // only the value of the distance to the origin of each of the cutting
  // planes differs. The normal is directed in an outgoing way to the zone
  // having for sequence number (order) 0.

  // Clip the cell with its left interface (use previous_distance as the left
  // interface for this material is the right interface of previous material)
  // Clipping results in one or more temporary cells with same topological
  // dimension
  // Those temporary cells are not yet true cells objects, but their points,
  // cell connectivity and data (on cells and points) are obtained as clip
  // results
  auto tmp_cells_build_data = this->clipInterfaceToTemporaryCell(
      left_normale, left_distance, left_opposite, cell_lid);
  auto [tmp_pts, tmp_conn, tmp_point_data, tmp_cell_data] =
      tmp_cells_build_data;

  // Iterating over each temporary cell through the temporary cells connectivity
  auto tmp_conn_iter{vtk::TakeSmartPointer(tmp_conn->NewIterator())};
  for (tmp_conn_iter->GoToFirstCell(); !tmp_conn_iter->IsDoneWithTraversal();
       tmp_conn_iter->GoToNextCell())
  {
    // Retrieve temporary cell id and points and build the temporary cell object
    const auto tmp_cell_lid{tmp_conn_iter->GetCurrentCellId()};
    vtkIdType tmp_cell_points_nb{};
    const vtkIdType* tmp_cell_points{};
    tmp_conn_iter->GetCurrentCell(tmp_cell_points_nb, tmp_cell_points);
    const auto& origin_cell_dim{
        this->m_input_material_mesh->GetCell(cell_lid)->GetCellDimension()};
    auto tmp_cell{buildTemporaryCell(origin_cell_dim, tmp_cell_points,
                                     tmp_cell_points_nb, tmp_pts)};
    // Clip the temporary cell and put the newly created cells into the output
    // mesh
    clipInterfaceBackToOutputMesh(right_normale, right_distance, right_opposite,
                                  tmp_cells_build_data, tmp_cell_lid, tmp_cell);
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class OutputGridT>
DataForCellBuildType
MaterialClipMethod<OutputGridT>::clipInterfaceToTemporaryCell(
    const InterfaceNormale& normale, const double& distance,
    const bool opposite, const LocalIdType origin_cell_lid)
{
  auto* origin_cell{m_input_material_mesh->GetCell(origin_cell_lid)};

  auto tmp_points = vtkSmartPointer<vtkPoints>::New();
  constexpr vtkIdType tmp_points_number{8};
  tmp_points->SetDataType(VTK_DOUBLE);
  tmp_points->Allocate(tmp_points_number);

  vtkNew<vtkMergePoints> tmp_point_locator;
  tmp_point_locator->InitPointInsertion(tmp_points, origin_cell->GetBounds(),
                                        tmp_points_number);

  auto tmp_conn = vtkSmartPointer<vtkCellArray>::New();
  tmp_conn->Initialize();

  auto tmp_point_data = vtkSmartPointer<vtkPointData>::New();
  tmp_point_data->InterpolateAllocate(m_input_material_mesh->GetPointData(),
                                      tmp_points_number);

  auto tmp_cell_data = vtkSmartPointer<vtkCellData>::New();

  // In case of the clipping of a side material
  // (i.e via method clipInCellForSideMaterial) there is only one clip
  // operation. The cell data of the clipped cells (i.e cell data of the
  // output mesh) are directly copied from the cell data of the input mesh.
  // In this case the copy operation, that occurs during the clip, between
  // output mesh and input mesh is set up through the call of the CopyAllocate
  // method. This method ensure some fields will be copied while other are not.
  // For example, by default, the Global fields are not copied.

  // Here the situation is a bit more complicated. Two clips operations are
  // involved. The first one produced clipped cells that are temporary.
  // Each of these clipped cells will itself be clipped one more time to
  // produce the final cells of the output mesh.
  // As the call to CopyAllocate on the output mesh has already been done
  // (depending on the input mesh), a selection of cell data fields will occur
  // during the last clip. Thus the first clip have to keep all the fields.
  // So the call to SetCopyGlobalIds, that explicitly ask to keep the Global
  // fields.

  // If the SetCopyGlobalIds method is not call, then the operation of
  // clipping back to output mesh will raise a SIGSEGV due to missing array
  // Here it seems it shows up a drawback of this implementation using tmp cells
  // The output mesh is CopyAllocate on the input mesh thus the CopyData
  // operation should only occur between output_mesh and input_mesh but in case
  // of tmp cells the copyData operates between tmp cells and output mesh.
  tmp_cell_data->SetCopyGlobalIds(1);
  tmp_cell_data->CopyAllocate(m_input_material_mesh->GetCellData(),
                              tmp_points_number);

  const auto& pts_coords =
      extract_nodes_coordinates<vtkDataSet>(m_input_material_mesh, origin_cell);
  auto pts_distance =
      compute_distance_to_plan(pts_coords, normale, distance, opposite);
  origin_cell->Clip(0, pts_distance.Get(), tmp_point_locator, tmp_conn,
                    m_input_material_mesh->GetPointData(), tmp_point_data,
                    m_input_material_mesh->GetCellData(), origin_cell_lid,
                    tmp_cell_data, 1); // insideOut is true
  return DataForCellBuildType{tmp_points, tmp_conn, tmp_point_data,
                              tmp_cell_data};
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class OutputGridT>
void MaterialClipMethod<OutputGridT>::clipInterfaceBackToOutputMesh(
    const InterfaceNormale& normale, const double& distance,
    const bool opposite, const DataForCellBuildType& tmp_data,
    const LocalIdType tmp_cell_lid, const vtkSmartPointer<vtkCell>& tmp_cell)
{
  vtkCellArray* cell_conn{this->getCells(tmp_cell)};

  auto [tmp_pts, tmp_conn, tmp_point_data, tmp_cell_data] = tmp_data;
  const auto& pts_coords =
      extract_nodes_coordinates<vtkPoints>(tmp_pts, tmp_cell);
  auto pts_distance =
      compute_distance_to_plan(pts_coords, normale, distance, opposite);
  tmp_cell->Clip(0, pts_distance.Get(), m_output_point_locator, cell_conn,
                 tmp_point_data, m_output_material_mesh->GetPointData(),
                 tmp_cell_data, tmp_cell_lid,
                 m_output_material_mesh->GetCellData(), 1); // insideOut is true
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class OutputGridT>
vtkSmartPointer<vtkCell> MaterialClipMethod<OutputGridT>::buildTemporaryCell(
    const int origin_cell_dim, const vtkIdType* tmp_cell_points_ids,
    const vtkIdType tmp_cell_points_nb,
    const vtkSmartPointer<vtkPoints>& tmp_points)
{
  // For each new cell added, got to set the type of the cell
  switch (origin_cell_dim)
  {
  case 0: {
    if (tmp_cell_points_nb > 1)
    {
      return this->createAndInitCell<vtkPolyVertex>(
          tmp_cell_points_nb, tmp_cell_points_ids, tmp_points);
    }
    return this->createAndInitCell<vtkVertex>(tmp_cell_points_nb,
                                              tmp_cell_points_ids, tmp_points);
  }
  case 1: {
    if (tmp_cell_points_nb > 2)
    {
      return this->createAndInitCell<vtkPolyLine>(
          tmp_cell_points_nb, tmp_cell_points_ids, tmp_points);
    }
    return this->createAndInitCell<vtkLine>(tmp_cell_points_nb,
                                            tmp_cell_points_ids, tmp_points);
  }
  case 2: {
    if (tmp_cell_points_nb == 3)
    {
      return this->createAndInitCell<vtkTriangle>(
          tmp_cell_points_nb, tmp_cell_points_ids, tmp_points);
    }
    if (tmp_cell_points_nb == 4)
    {
      return this->createAndInitCell<vtkQuad>(tmp_cell_points_nb,
                                              tmp_cell_points_ids, tmp_points);
    }
    return this->createAndInitCell<vtkPolygon>(tmp_cell_points_nb,
                                               tmp_cell_points_ids, tmp_points);
  }
  case 3: {
    if (tmp_cell_points_nb == 4)
    {
      return this->createAndInitCell<vtkTetra>(tmp_cell_points_nb,
                                               tmp_cell_points_ids, tmp_points);
    }
    return this->createAndInitCell<vtkWedge>(tmp_cell_points_nb,
                                             tmp_cell_points_ids, tmp_points);
  }
  default:
    vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_ERROR,
            "Unknown dimension " << origin_cell_dim);
    throw std::runtime_error("Unknown dimension!");
  }

  assert(false && "Should never get here!");
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class OutputGridT>
VTKCellType MaterialClipMethod<OutputGridT>::getNewlyInsertedCellType(
    const LocalIdType origin_cell_lid, const vtkIdType new_cell_points_nb)
{
  auto* origin_cell{m_input_material_mesh->GetCell(origin_cell_lid)};
  assert(origin_cell->GetCellType() != VTK_POLYHEDRON &&
         "The case should not happen as a runtime_error should have been throw "
         "earlier");
  const auto origin_cell_dimension{origin_cell->GetCellDimension()};
  switch (origin_cell_dimension)
  {
  case 0: {
    // points are generated--------------------------------
    if (new_cell_points_nb > 1)
    {
      return VTK_POLY_VERTEX;
    }
    return VTK_VERTEX;
  }
  case 1: {
    // lines are generated---------------------------------
    if (new_cell_points_nb > 2)
    {
      return VTK_POLY_LINE;
    }
    return VTK_LINE;
  }
  case 2: {
    // polygons are generated------------------------------
    if (new_cell_points_nb == 3)
    {
      return VTK_TRIANGLE;
    }
    if (new_cell_points_nb == 4)
    {
      return VTK_QUAD;
    }
    return VTK_POLYGON;
  }
  case 3: {
    // tetrahedra or wedges are generated------------------
    if (new_cell_points_nb == 4)
    {
      return VTK_TETRA;
    }
    return VTK_WEDGE;
  }
  default: {
    vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_ERROR,
            "Unknown dimension " << origin_cell_dimension);
  }
  }
  return VTK_EMPTY_CELL;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class OutputGridT>
void MaterialClipMethod<OutputGridT>::assignNewCellsTypes(
    vtkIdType begin_lid, vtkIdType end_lid, LocalIdType origin_cell_lid)
{
  static_assert(
      std::is_same_v<OutputGridT, vtkUnstructuredGrid>,
      "Assigning new cells type is required for UnstructuredGrid only!");
  vtkCellArray* cell_conn{m_output_material_mesh->GetCells()};
  for (vtkIdType iCell = begin_lid; iCell < end_lid; ++iCell)
  {
    vtkIdType new_cell_points_nb{0};
    const vtkIdType* new_cell_points{nullptr};
    cell_conn->GetCellAtId(iCell, new_cell_points_nb, new_cell_points);
    const vtkIdType& cell_type{
        this->getNewlyInsertedCellType(origin_cell_lid, new_cell_points_nb)};
    m_output_material_mesh->GetCellTypesArray()->InsertNextValue(cell_type);
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class OutputGridT>
vtkCellArray* MaterialClipMethod<OutputGridT>::getCells(vtkCell* current_cell)
{
  if constexpr (std::is_same_v<OutputGridT, vtkPolyData>)
  {
    if (current_cell->GetCellDimension() == 0)
    {
      return m_output_material_mesh->GetVerts();
    }
    if (current_cell->GetCellDimension() == 1)
    {
      return m_output_material_mesh->GetLines();
    }
    if (current_cell->GetCellDimension() == 2)
    {
      return m_output_material_mesh->GetPolys();
    }
  }
  if constexpr (std::is_same_v<OutputGridT, vtkUnstructuredGrid>)
  {
    return m_output_material_mesh->GetCells();
  }

  // Cannot occur. OutputGridT is assured to be vtkPolyData or
  // vtkUnstructuredGrid by static_assert in the ctor
  return nullptr;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class OutputGridT>
template <class CellType>
vtkSmartPointer<CellType> MaterialClipMethod<OutputGridT>::createAndInitCell(
    vtkIdType cell_points_nb, const vtkIdType* cell_points_ids,
    vtkSmartPointer<vtkPoints> points)
{
  auto tmp_cell = vtkSmartPointer<CellType>::New();
  tmp_cell->Initialize(cell_points_nb, cell_points_ids, points);
  return tmp_cell;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // VTK_MATERIAL_INTERFACE_MATERIAL_CLIP_METHOD_IMPL_H
