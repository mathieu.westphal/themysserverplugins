/*----------------------------------------------------------------------------*/
/**
 * @file vtkMaterialInterface_MaterialContourMethod.h
 * @brief Declares the MaterialContourMethod class
 * @date 2023-01-27
 *
 * @author Guillaume Peillex (guillaume.peillex@cea.fr)
 * @copyright Copyright CEA, DAM, DIF, F-91297 Arpajon, France 2023
 *
 */
/*----------------------------------------------------------------------------*/
#ifndef VTK_MATERIAL_INTERFACE_MATERIAL_CONTOUR_METHOD_H
#define VTK_MATERIAL_INTERFACE_MATERIAL_CONTOUR_METHOD_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// IWYU pragma: no_include <fwd>
#include <vector>

#include <vtkSmartPointer.h>

#include "vtkMaterialInterface_MaterialAbstractMethod.h"
#include "vtkMaterialInterface_Types.h"

class vtkDataSet;
class vtkPointSet;
class vtkPolyData;
class InterfaceNormale;

/*----------------------------------------------------------------------------*/
/**
 * @brief MaterialContourMethod represents the material that is concerned with
 * interface construction. The ouput material mesh is a vtkPolyData.
 *
 * This class is used when the user does not tick the FillMaterial option.
 * This way the user indicates he wants to see the interfaces as lines (in 2D)
 * or faces (in 3D)
 *
 */
/*----------------------------------------------------------------------------*/
class MaterialContourMethod : public MaterialAbstractMethod
{
public:
  MaterialContourMethod() = delete;
  virtual ~MaterialContourMethod() = default;

  explicit MaterialContourMethod(vtkDataSet* input_material_mesh);

  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  void initOutputMaterialMesh() override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the Output Mesh object
   *
   * @return vtkPointSet*
   */
  /*----------------------------------------------------------------------------*/
  vtkSmartPointer<vtkPointSet> getOutputMaterialMesh() override
  {
    return m_output_material_mesh;
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Build the pure cells in the output mesh
   *
   * This a just a no-op because the user just wants to see the interface
   * as lines (2D) or surfaces (3D). He does't want to see the pure cells.
   *
   * @param pcells_lids : local cell ids
   */
  /*----------------------------------------------------------------------------*/
  void buildPureCells(
      [[maybe_unused]] const std::vector<LocalIdType>& pcells_lids) override{};

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Apply the global algorithm for the current material.
   *
   * This algorithm uses the Contour method to compute the "left/first"
   * interface of this material. To do this it uses the interface distance of
   * the previous material.
   * Then it uses the Contour method to compute the "right/second" interface
   * of this material. To do this it uses its own interface distance.
   *
   * @param previous_distance: interface distance to the origin of the
   * previous material
   * @param _order: order of this material
   * @param _number_of_zones: number of zones (i.e materials) present in the
   * mixed cell
   * @param _cell_index: local id of the mixed cell (i.e id of the mixed cell in
   * this material mesh)
   * @return double: interface distance to the origin of this material (will be
   * the previous distance for the next material)
   */
  /*----------------------------------------------------------------------------*/
  double applyGlobalAlgorithm(const double& previous_distance, OrderType _order,
                              int _number_of_zones,
                              LocalIdType _cell_index) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Apply the local algorithm for the current material
   *
   * This algorithm uses the Contour method to compute the "left/first"
   * interface of this material. To do this it uses the first normal/distance
   * couple of this material. It a second normal/distance couple exist for this
   * material then it is used to compute the "right/second" interface thanks to
   * the Contour method.
   *
   * @param _cell_index: local id of the mixed cell (i.e id of the mixed cell in
   * this material mesh)
   */
  /*----------------------------------------------------------------------------*/
  void applyLocalAlgorithm(LocalIdType _cell_index) override;

private:
  /*----------------------------------------------------------------------------*/
  /**
   * @brief Given the interface normale and the interface distance to the origin
   * in the current cell, the sign of the normal of the interface, build the
   * contour of the interface in the cell
   *
   * @param normale: interface normale
   * @param distance: interface distance to the origin in the cell
   * @param opposite: should the normal be reversed
   * @param cell_id: local id of the cell
   */
  /*----------------------------------------------------------------------------*/
  void buildContourInCell(const InterfaceNormale& normale,
                          const double& distance, bool opposite,
                          LocalIdType cell_id);

  vtkSmartPointer<vtkPolyData> m_output_material_mesh;
};

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif //  VTK_MATERIAL_INTERFACE_MATERIAL_CONTOUR_METHOD_H
