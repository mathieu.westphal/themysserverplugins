#ifndef VTK_MATERIAL_INTERFACE_UTILS_IMPL_H
#define VTK_MATERIAL_INTERFACE_UTILS_IMPL_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include "vtkMaterialInterface_Utils.h"

#include <vector>

#include <vtkCell.h>
#include <vtkDataSet.h>
#include <vtkSmartPointer.h>
#include <vtkType.h>

#include "vtkMaterialInterface_Types.h"

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class T>
std::vector<Vec3> extract_nodes_coordinates(T* input_mesh, vtkCell* cell)
{
  std::vector<Vec3> res(0);
  res.reserve(cell->GetNumberOfPoints());
  for (vtkIdType iPt = 0; iPt < cell->GetNumberOfPoints(); ++iPt)
  {
    Vec3 buffer{};
    input_mesh->GetPoint(cell->GetPointId(iPt), buffer.data());
    res.push_back(buffer);
  }
  return res;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class T> vtkSmartPointer<T> build_output_mesh(vtkDataSet* i_mesh)
{
  const auto& estimated_size = size_estimation(i_mesh->GetNumberOfCells());

  auto o_mesh = vtkSmartPointer<T>::New();
  o_mesh->Allocate(estimated_size);
  vtkNew<vtkPoints> newPts;
  newPts->SetDataType(VTK_DOUBLE);
  newPts->Allocate(estimated_size);
  o_mesh->SetPoints(newPts);

  o_mesh->GetPointData()->InterpolateAllocate(i_mesh->GetPointData(),
                                              estimated_size);
  o_mesh->GetCellData()->CopyAllocate(i_mesh->GetCellData(), estimated_size);
  o_mesh->GetFieldData()->PassData(i_mesh->GetFieldData());

  return o_mesh;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // VTK_MATERIAL_INTERFACE_UTILS_IMPL_H
