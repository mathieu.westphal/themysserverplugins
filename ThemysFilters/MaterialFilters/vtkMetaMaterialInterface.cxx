#include "vtkMetaMaterialInterface.h"

#include <vtkGarbageCollector.h>
#include <vtkObjectFactory.h>

//--------------------------------------------------------------------------------------------------
vtkStandardNewMacro(vtkMetaMaterialInterface);

//--------------------------------------------------------------------------------------------------
vtkMetaMaterialInterface::vtkMetaMaterialInterface() = default;

// --------------------------------------------------------------------------------------------------
vtkMetaMaterialInterface::~vtkMetaMaterialInterface() = default;

// --------------------------------------------------------------------------------------------------
int vtkMetaMaterialInterface::RequestData(vtkInformation* request,
                                          vtkInformationVector** inputVector,
                                          vtkInformationVector* outputVector)
{
  if (this->UseYoungs)
  {
    if (!this->YoungsMaterialInterface)
    {
      vtkErrorMacro("No YoungsMaterialInterface defined");
      return 0;
    }
    return this->YoungsMaterialInterface->ProcessRequest(request, inputVector,
                                                         outputVector);
  } else
  {
    if (!this->MaterialInterface)
    {
      vtkErrorMacro("No MaterialInterface defined");
      return 0;
    }
    return this->MaterialInterface->ProcessRequest(request, inputVector,
                                                   outputVector);
  }
}
//--------------------------------------------------------------------------------------------------
void vtkMetaMaterialInterface::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
  os << indent << "UseYoungs: " << this->UseYoungs << "\n";

  if (this->YoungsMaterialInterface)
  {
    os << indent << "YoungsMaterialInterface:" << endl;
    this->YoungsMaterialInterface->PrintSelf(os, indent.GetNextIndent());
  } else
  {
    os << indent << "YoungsMaterialInterface: (null)" << endl;
  }

  if (this->MaterialInterface)
  {
    os << indent << "MaterialInterface:" << endl;
    this->MaterialInterface->PrintSelf(os, indent.GetNextIndent());
  } else
  {
    os << indent << "MaterialInterface: (null)" << endl;
  }
}

//--------------------------------------------------------------------------------------------------
void vtkMetaMaterialInterface::ReportReferences(vtkGarbageCollector* collector)
{
  this->Superclass::ReportReferences(collector);
  vtkGarbageCollectorReport(collector, this->YoungsMaterialInterface,
                            "MetaYoungsMaterialInterface");
  vtkGarbageCollectorReport(collector, this->MaterialInterface,
                            "MetaMaterialInterface");
}
