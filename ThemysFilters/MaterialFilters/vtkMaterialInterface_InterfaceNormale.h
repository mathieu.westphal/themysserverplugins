/*----------------------------------------------------------------------------*/
/**
 * @file vtkMaterialInterface_InterfaceNormale.h
 * @brief Declares the InterfaceNormale class
 * @date 2023-01-06
 *
 * @author Guillaume Peillex (guillaume.peillex@cea.fr)
 * @copyright Copyright CEA, DAM, DIF, F-91297 Arpajon, France 2023
 *
 */
/*----------------------------------------------------------------------------*/
#ifndef VTK_MATERIAL_INTERFACE_INTERFACE_NORMALE_H
#define VTK_MATERIAL_INTERFACE_INTERFACE_NORMALE_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// IWYU pragma: no_include <vtkIOStream.h>
#include <ostream>

#include "vtkMaterialInterface_Types.h"

/*----------------------------------------------------------------------------*/
/**
 * @brief An InterfaceNormale object is an array of 3 floating number (double)
 * each one being normale vector coordinate
 *
 * This class adds the bool conversion operator defining a null interface
 * and printing capabilities
 *
 */
/*----------------------------------------------------------------------------*/
class InterfaceNormale : public Vec3
{
public:
  /*----------------------------------------------------------------------------*/
  /**
   * @brief An InterfaceNormale is not null if at least one of its three
   * components is not equal to zero.
   *
   * @return true
   * @return false
   */
  /*----------------------------------------------------------------------------*/
  explicit operator bool() const;
};

/*----------------------------------------------------------------------------*/
/**
 * @brief Dump the InterfaceNormale object into the stream
 *
 * @param out : the stream to dump into
 * @param ifn : interface normale
 * @return std::ostream&
 */
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out, const InterfaceNormale& ifn);

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // VTK_MATERIAL_INTERFACE_INTERFACE_NORMALE_H
