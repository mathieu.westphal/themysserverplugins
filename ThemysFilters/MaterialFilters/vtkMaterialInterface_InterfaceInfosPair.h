/*----------------------------------------------------------------------------*/
/**
 * @file vtkMaterialInterface_InterfaceInfosPair.h
 * @brief Declares the InterfaceInfosPair class
 * @date 2023-01-27
 *
 * @author Guillaume Peillex (guillaume.peillex@cea.fr)
 * @copyright Copyright CEA, DAM, DIF, F-91297 Arpajon, France 2023
 *
 */
/*----------------------------------------------------------------------------*/
#ifndef VTK_MATERIAL_INTERFACE_INTERFACE_INFOS_PAIR_H
#define VTK_MATERIAL_INTERFACE_INTERFACE_INFOS_PAIR_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// IWYU pragma: no_include <fwd>
#include <array>
#include <ostream>
#include <string>
#include <string_view>

#include "vtkMaterialInterface_InterfaceInfos.h"

enum class SamplingPatternEnum;

/*----------------------------------------------------------------------------*/
/**
 * @brief The InterfaceInfosPair class is an array of two InterfaceInfos
 * components enriched with the name of the order array
 *
 * It regroups all the array names involved into interface construction.
 *
 */
/*----------------------------------------------------------------------------*/
class InterfaceInfosPair : private std::array<InterfaceInfos, 2>
{
public:
  /*----------------------------------------------------------------------------*/
  /**
   * @brief Construct a new Interface Infos Pair object
   *
   * @param interface_1 : first interface infos
   * @param interface_2 : second interface infos
   * @param order : order array name
   */
  /*----------------------------------------------------------------------------*/
  InterfaceInfosPair(const InterfaceInfos& interface_1,
                     const InterfaceInfos& interface_2,
                     std::string_view order = "None");

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns true if order member is not None nor empty
   *
   * @return true
   * @return false
   */
  /*----------------------------------------------------------------------------*/
  bool hasOrder() const;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the order array name
   *
   * @return const std::string&
   */
  /*----------------------------------------------------------------------------*/
  const std::string& getOrder() const;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns true if the first interface is valid but not the second one
   *
   * @return true
   * @return false
   */
  /*----------------------------------------------------------------------------*/
  bool hasOneInterface() const;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the first Interface object
   *
   * @return const InterfaceInfos&
   */
  /*----------------------------------------------------------------------------*/
  const InterfaceInfos& getFirstInterface() const;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns true if the first and the second interfaces are valid
   *
   * @return true
   * @return false
   */
  /*----------------------------------------------------------------------------*/
  bool hasTwoInterfaces() const;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the Second Interface object
   *
   * @return const InterfaceInfos&
   */
  /*----------------------------------------------------------------------------*/
  const InterfaceInfos& getSecondInterface() const;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns the interface status depending on the sampling pattern mode
   * and the informations of each interface of the pair
   *
   * @param mode : sampling pattern mode
   * @return InterfaceStatus
   */
  /*----------------------------------------------------------------------------*/
  InterfaceStatus getInterfaceStatus(SamplingPatternEnum mode) const;

private:
  std::string m_order;
};

/*----------------------------------------------------------------------------*/
/**
 * @brief Dumps the InterfaceInfosPair object into the stream in argument
 *
 * @param out : the stream into which the InterfaceInfosPair object should be
 * dumped
 * @param infos_pair : the object to dump
 * @return std::ostream&
 */
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out,
                         const InterfaceInfosPair& infos_pair);

#endif // VTK_MATERIAL_INTERFACE_INTERFACE_INFOS_H
