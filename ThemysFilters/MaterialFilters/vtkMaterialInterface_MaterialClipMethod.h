/*----------------------------------------------------------------------------*/
/**
 * @file vtkMaterialInterface_MaterialClipMethod.h
 * @brief Declares the MaterialClipMethod class
 * @date 2023-01-27
 *
 * @author Guillaume Peillex (guillaume.peillex@cea.fr)
 * @copyright Copyright CEA, DAM, DIF, F-91297 Arpajon, France 2023
 *
 */
/*----------------------------------------------------------------------------*/
#ifndef VTK_MATERIAL_INTERFACE_MATERIAL_CLIP_METHOD_H
#define VTK_MATERIAL_INTERFACE_MATERIAL_CLIP_METHOD_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// IWYU pragma: no_include <fwd>
#include <vector> // for vector

#include <vtkCellType.h>     // for VTKCellType
#include <vtkSmartPointer.h> // for vtkSmartPointer
#include <vtkType.h>         // for vtkIdType

#include "vtkMaterialInterface_MaterialAbstractMethod.h" // for MaterialAbstract
#include "vtkMaterialInterface_Types.h" // for LocalIdType, Data...

class InterfaceNormale; // lines 35-35
class vtkCell;          // lines 22-22
class vtkCellArray;     // lines 23-23
class vtkDataSet;       // lines 26-26
class vtkPoints;        // lines 32-32
class vtkUnstructuredGrid;

/*----------------------------------------------------------------------------*/
/**
 * @brief MaterialClipMethod represents the material that is concerned
 * with interface construction. The ouput material mesh is a
 * vtkUnstructuredGrid.
 *
 * This class is used when the user does tick the FillMaterial option.
 * This way the user indicates he wants to see the material mesh cut by the
 * interfaces.
 *
 */
/*----------------------------------------------------------------------------*/
template <class OutputGridT>
class MaterialClipMethod : public MaterialAbstractMethod
{
public:
  MaterialClipMethod() = delete;
  virtual ~MaterialClipMethod() = default;

  explicit MaterialClipMethod(vtkDataSet* input_material_mesh);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Build and/or initialize the output mesh
   *
   */
  /*----------------------------------------------------------------------------*/
  void initOutputMaterialMesh() override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the Output Mesh object
   *
   * @return vtkSmartPointer<vtkPointSet>
   */
  /*----------------------------------------------------------------------------*/
  vtkSmartPointer<vtkPointSet> getOutputMaterialMesh() override
  {
    return m_output_material_mesh;
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Build the pure cells in the output mesh
   *
   * @param pcells_lids : local cell ids
   */
  /*----------------------------------------------------------------------------*/
  void buildPureCells(const std::vector<LocalIdType>& pcells_lids) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Apply the global algorithm for the current material.
   *
   * This algorithm uses the Clip method to cut the cells according to the
   * "left/first" interface of this material. To do this it uses the interface
   * distance of the previous material.
   *
   * Then it uses the Clip method to cut the cells according to the
   * "right/second" interface of this material. To do this it uses its own
   * interface distance.
   *
   * @param previous_distance: interface distance to the origin of the
   * previous material
   * @param _order: order of this material
   * @param _number_of_zones: number of zones (i.e materials) present in the
   * mixed cell
   * @param _cell_index: local id of the mixed cell (i.e id of the mixed cell in
   * this material mesh)
   * @return double: interface distance to the origin of this material (will be
   * the previous distance for the next material)
   */
  /*----------------------------------------------------------------------------*/
  double applyGlobalAlgorithm(const double& previous_distance, OrderType _order,
                              int _number_of_zones,
                              LocalIdType _cell_index) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Apply the local algorithm for the current material
   *
   * This algorithm uses the clip method to cut the "left/first"
   * interface of this material. To do this it uses the first normal/distance
   * couple of this material.
   *
   * If a second normal/distance couple exist for this
   * material then it is used to cut the cell according to the "right/second"
   * interface thanks to the Clip method.
   *
   * @param _cell_index: local id of the mixed cell (i.e id of the mixed cell in
   * this material mesh)
   */
  /*----------------------------------------------------------------------------*/
  void applyLocalAlgorithm(LocalIdType _cell_index) override;

private:
  /*----------------------------------------------------------------------------*/
  /**
   * @brief Given the interface normale and distance to the origin in the cell,
   * the sign of the interface normale, clip the cell of the output mesh
   * along the interface
   *
   * As this method deals with only one interface (one tuple normale, distance,
   * sign) it is used for side material (i.e first or last material of the mixed
   * cell)
   *
   *   ┌───**───*────┐  Here materials A and D are side materials.
   *   │A  *    *    │  Materials B and C are embedded materials.
   *   │  **   **    │
   *   │ **B  **    **
   *   │**   ** C  **│
   *   **    *    ** │
   *   *    **   **  │
   *   │   **   ** D │
   *   └───*────*────┘
   *
   * @param normale: normale of the interface
   * @param distance: distance of the interface to the origin in the cell
   * @param opposite: should the normal be reversed
   * @param cell_id: local id of the cell
   */
  /*----------------------------------------------------------------------------*/
  void clipInCellForSideMaterial(const InterfaceNormale& normale,
                                 const double& distance, bool opposite,
                                 LocalIdType cell_lid);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Given the left/first and right/second interface data (normale,
   * distance to origin and normal sign), clip the cell of the output mesh
   * along those two interfaces
   *
   * As this method deals with two interfaces (two tuples normale, distance,
   * sign) it is used for embedded material
   *
   *   ┌───**───*────┐  Here materials A and D are side materials.
   *   │A  *    *    │  Materials B and C are embedded materials.
   *   │  **   **    │
   *   │ **B  **    **
   *   │**   ** C  **│
   *   **    *    ** │
   *   *    **   **  │
   *   │   **   ** D │
   *   └───*────*────┘
   *
   * @param left_distance: first interface distance to origin
   * @param left_normale: first interface normal vector
   * @param left_opposite: first interface sign
   * @param right_distance: second interface distance to origin
   * @param right_normale: second interface normal vector
   * @param right_opposite: second interface sign
   * @param cell_id: local id of the cell
   */
  /*----------------------------------------------------------------------------*/
  void clipInCellForEmbeddedMaterial(const double& left_distance,
                                     const InterfaceNormale& left_normale,
                                     bool left_opposite,
                                     const double& right_distance,
                                     const InterfaceNormale& right_normale,
                                     bool right_opposite, LocalIdType cell_lid);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Given the interface normale and interface distance to the origin in
   * the cell, the sign of the interface normal, clip the cell and return the
   * data (points, connectivity, point data and cell data) of the cells created
   * by the clip
   *
   * @param normale: interface normale of the cell
   * @param distance: interface distance to the origin in the cell
   * @param opposite: should the normal be reversed
   * @param cell_lid: local id of the cell
   * @return DataForCellBuildType
   */
  /*----------------------------------------------------------------------------*/
  DataForCellBuildType
  clipInterfaceToTemporaryCell(const InterfaceNormale& normale,
                               const double& distance, bool opposite,
                               LocalIdType cell_lid);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Given an interface normale, interface distance to the origin
   * and interface normal sign, clip the temporary cell described by the last
   * three arguments and insert the result in the output material mesh
   *
   * @param normale: interface normale
   * @param distance: interface distance to the origin
   * @param opposite: sign of the interface normale
   * @param tmp_data: data describing temporary cells to be clipped
   * @param tmp_cell_lid: cell id of the temporary cell to be clipped
   * @param tmp_cell: temporary cell to be clipped
   */
  /*----------------------------------------------------------------------------*/
  void clipInterfaceBackToOutputMesh(const InterfaceNormale& normale,
                                     const double& distance, bool opposite,
                                     const DataForCellBuildType& tmp_data,
                                     LocalIdType tmp_cell_lid,
                                     const vtkSmartPointer<vtkCell>& tmp_cell);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Given the origin cell dimension and the temporary points build and
   * returns a temporary cell
   *
   * @param origin_cell_dim: origin cell dimension
   * @param tmp_cell_points_ids: ids of the temporary points that define the
   * temporary cell
   * @param tmp_cell_points_nb: number of temporary points that define the
   * temporary cell
   * @param tmp_points: points collection holding the temporary points
   * @return vtkSmartPointer<vtkCell>
   */
  /*----------------------------------------------------------------------------*/
  vtkSmartPointer<vtkCell>
  buildTemporaryCell(int origin_cell_dim, const vtkIdType* tmp_cell_points_ids,
                     vtkIdType tmp_cell_points_nb,
                     const vtkSmartPointer<vtkPoints>& tmp_points);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the type of the new cell in the connectivity
   *
   * @param origin_cell_lid: local id of the origin cell
   * @param new_cell_points_nb: number of points in the new cell
   * @return VTKCellType
   */
  /*----------------------------------------------------------------------------*/
  VTKCellType getNewlyInsertedCellType(LocalIdType origin_cell_lid,
                                       vtkIdType new_cell_points_nb);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Assign the type of the new cells that have been created by the clip
   * operation
   *
   * @warning: not a necessary/valid operation for vtkPolyData
   * @param begin_lid: local id of the first new cell created
   * @param end_lid: local id of the last new cell created
   * @param origin_cell_lid: local id of the origin cell
   */
  /*----------------------------------------------------------------------------*/
  void assignNewCellsTypes(vtkIdType begin_lid, vtkIdType end_lid,
                           LocalIdType origin_cell_lid);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Get the cells connectivity array
   *
   * In case the output material mesh is a vtkUnstructuredGrid then returns the
   * result of the vtkUnstructuredGrid::GetCells method.
   * In case the ouput material mesh is a vtkPolyData then returns the vertices,
   * lines or polys depending on the dimension of the current cell
   *
   * @param current_cell: current cell
   * @return vtkCellArray*
   */
  /*----------------------------------------------------------------------------*/
  vtkCellArray* getCells(vtkCell* current_cell);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Create an intialize a cell with the chosen type (vtkPolyData,
   * vtkQuad ...)
   *
   * @tparam CellType: type of the cell desired
   * @param cell_points_nb: number of points in the cell
   * @param cell_points_ids: ids of the points defining the cell
   * @param points: points collection
   * @return vtkSmartPointer<CellType>
   */
  /*----------------------------------------------------------------------------*/
  template <class CellType>
  vtkSmartPointer<CellType>
  createAndInitCell(vtkIdType cell_points_nb, const vtkIdType* cell_points_ids,
                    vtkSmartPointer<vtkPoints> points);

  /// @brief input material mesh that is copied and InterpolateAllocated.
  /// The only role of this member is to keep the underlying pointer valid
  vtkSmartPointer<vtkDataSet> m_real_input_material_mesh;
  vtkSmartPointer<OutputGridT> m_output_material_mesh;
};

#include "vtkMaterialInterface_MaterialClipMethod-Impl.h" // IWYU pragma: keep
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif //  VTK_MATERIAL_INTERFACE_MATERIAL_CLIP_METHOD_H
