/*----------------------------------------------------------------------------*/
/**
 * @file vtkMaterialInterface_SamplingPattern.h
 * @brief Declares the SamplingPattern enum and utilities
 * @date 2023-01-27
 *
 * @author Guillaume Peillex (guillaume.peillex@cea.fr)
 * @copyright Copyright CEA, DAM, DIF, F-91297 Arpajon, France 2023
 *
 */
/*----------------------------------------------------------------------------*/
#ifndef VTK_MATERIAL_INTERFACE_SAMPLING_PATTERN_H
#define VTK_MATERIAL_INTERFACE_SAMPLING_PATTERN_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// IWYU pragma: no_include <fwd>
#include <ostream>
#include <string> // IWYU pragma: keep

/*----------------------------------------------------------------------------*/
/**
 * Enumeration of possible sampling patterns
 * - MODE_WITH_AN_OUTGOING_INTERFACE : consideration of mixed cells with at most
 * two materials
 * - MODE_WITH_TWO_OUTGOING_INTERFACES : consideration of mixed cells with more
 * than two materials
 * - MODE_OLD_SCOOL_CEA_INTERFACES : consideration of mixed cells with the
 * historical compact mechanism of the CEA
 */
/*----------------------------------------------------------------------------*/
enum class SamplingPatternEnum {
  MODE_WITH_AN_OUTGOING_INTERFACE = 0,
  MODE_WITH_TWO_OUTGOING_INTERFACES = 1,
  MODE_OLD_SCOOL_CEA_INTERFACES = 2
};

/*----------------------------------------------------------------------------*/
/**
 * @brief Convert the enum into string
 *
 * @param pattern : enum to convert
 * @return std::string
 */
/*----------------------------------------------------------------------------*/
std::string sampling_pattern_enum_to_string(SamplingPatternEnum pattern);

/*----------------------------------------------------------------------------*/
/**
 * @brief Dump the enum into the stream
 *
 * @param out : the stream to dump the enum into
 * @param pattern : the enum to be dumped
 * @return std::ostream&
 */
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out, SamplingPatternEnum pattern);

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // VTK_MATERIAL_INTERFACE_SAMPLING_PATTERN_H
