// IWYU pragma: no_include "vtkMaterialInterface_Utils-Impl.h"
// IWYU pragma: no_include <vtkLogger.h>
#include "vtkMaterialInterface_MaterialContourMethod.h"

#include <vtkCell.h>
#include <vtkDataSet.h>
#include <vtkDoubleArray.h>
#include <vtkMergePoints.h>
#include <vtkPVLogger.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>

#include "vtkMaterialInterface_InterfaceData.h"
#include "vtkMaterialInterface_Utils.h"

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
MaterialContourMethod::MaterialContourMethod(vtkDataSet* input_material_mesh)
    : MaterialAbstractMethod(input_material_mesh)
{
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void MaterialContourMethod::initOutputMaterialMesh()
{
  m_output_material_mesh =
      build_output_mesh<vtkPolyData>(m_input_material_mesh);

  m_output_point_locator = vtkSmartPointer<vtkMergePoints>::New();
  m_output_point_locator->InitPointInsertion(
      m_output_material_mesh->GetPoints(), m_input_material_mesh->GetBounds());
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// NOLINTBEGIN(bugprone-easily-swappable-parameters)
double MaterialContourMethod::applyGlobalAlgorithm(
    const double& previous_distance, const OrderType _order,
    const int _number_of_zones, const LocalIdType _cell_index)
// NOLINTEND(bugprone-easily-swappable-parameters)
{
  const auto& normale{this->m_interface_data.getNormaleInCell(_cell_index)};
  const auto distance{this->m_interface_data.getDistanceInCell(_cell_index)};

  if (_order == 0)
  {
    // Build the contour of the right interface of the first material
    vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_7,
            "Building right interface for first material");
    this->buildContourInCell(normale, distance, false, _cell_index);
  } else if (_order == _number_of_zones - 1)
  {
    // Build the contour of the left interface of the last material.
    // The left interface of this material is the right interface of the
    // previous material.
    // Thus use the previous distance with opposite direction
    vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_7,
            "Building right interface for first material");
    this->buildContourInCell(normale, previous_distance, true, _cell_index);
  } else
  {
    // Build the contour of the left interface of the current material.
    // The left interface of this material is the right interface of the
    // previous material.
    // Thus use the previous distance with opposite direction
    this->buildContourInCell(normale, previous_distance, true, _cell_index);

    // Build the contour of the right boundary of the current material
    this->buildContourInCell(normale, distance, false, _cell_index);
  }

  // Return the current distance that may be used to compute the left boundary
  // of the next material
  return distance;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void MaterialContourMethod::applyLocalAlgorithm(const LocalIdType _cell_index)
{
  if (m_interface_data)
  {
    const auto& normale{m_interface_data.getNormaleInCell(_cell_index)};
    const auto& distance{this->m_interface_data.getDistanceInCell(_cell_index)};
    this->buildContourInCell(normale, distance, false, _cell_index);
  }
  if (m_interface2_data)
  {
    const auto& normale{m_interface2_data.getNormaleInCell(_cell_index)};
    const auto& distance{m_interface2_data.getDistanceInCell(_cell_index)};
    this->buildContourInCell(normale, distance, false, _cell_index);
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void MaterialContourMethod::buildContourInCell(const InterfaceNormale& normale,
                                               const double& distance,
                                               const bool opposite,
                                               const LocalIdType cell_id)
{
  auto* cell{m_input_material_mesh->GetCell(cell_id)};
  auto* newVerts{m_output_material_mesh->GetVerts()};
  auto* newLines{m_output_material_mesh->GetLines()};
  auto* newPolys{m_output_material_mesh->GetPolys()};
  const auto& pts_coords =
      extract_nodes_coordinates<vtkDataSet>(m_input_material_mesh, cell);
  auto pts_distance =
      compute_distance_to_plan(pts_coords, normale, distance, opposite);
  cell->Contour(0, pts_distance.Get(), m_output_point_locator, newVerts,
                newLines, newPolys, m_input_material_mesh->GetPointData(),
                m_output_material_mesh->GetPointData(),
                m_input_material_mesh->GetCellData(), cell_id,
                m_output_material_mesh->GetCellData());
}
