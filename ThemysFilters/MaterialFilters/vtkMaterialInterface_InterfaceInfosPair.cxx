/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// IWYU pragma: no_include "vtkSystemIncludes.h"
// IWYU pragma: no_include <vtkIOStream.h>
#include "vtkMaterialInterface_InterfaceInfosPair.h"

#include <array>
#include <string_view>

#include "vtkMaterialInterface_InterfaceInfos.h"
#include "vtkMaterialInterface_SamplingPattern.h"
#include "vtkMaterialInterface_Utils.h"

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
InterfaceInfosPair::InterfaceInfosPair(const InterfaceInfos& interface_1,
                                       const InterfaceInfos& interface_2,
                                       std::string_view order)
    : std::array<InterfaceInfos, 2>{InterfaceInfosSentinel,
                                    InterfaceInfosSentinel},
      m_order{"None"}
{
  if (interface_1.isValid())
  {
    this->at(0) = interface_1;
  }
  if (interface_2.isValid())
  {
    this->at(1) = interface_2;
  }
  if (!is_empty_name(order))
  {
    m_order = order;
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool InterfaceInfosPair::hasOrder() const { return !is_empty_name(m_order); }

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
const std::string& InterfaceInfosPair::getOrder() const { return m_order; }

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool InterfaceInfosPair::hasOneInterface() const
{
  return this->at(0).isValid() && !this->at(1).isValid();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
const InterfaceInfos& InterfaceInfosPair::getFirstInterface() const
{
  return this->at(0);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool InterfaceInfosPair::hasTwoInterfaces() const
{
  return this->at(0).isValid() && this->at(1).isValid();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
const InterfaceInfos& InterfaceInfosPair::getSecondInterface() const
{
  return this->at(1);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
InterfaceStatus
InterfaceInfosPair::getInterfaceStatus(const SamplingPatternEnum mode) const
{
  if (mode == SamplingPatternEnum::MODE_WITH_AN_OUTGOING_INTERFACE ||
      mode == SamplingPatternEnum::MODE_WITH_TWO_OUTGOING_INTERFACES)
  {
    if (this->hasOrder())
    {
      return InterfaceStatus::UNDEFINED;
    }
    if (this->hasOneInterface())
    {
      return InterfaceStatus::WITH_ONE_INTERFACE;
    }
    if (this->hasTwoInterfaces())
    {
      return InterfaceStatus::WITH_TWO_INTERFACES;
    }
  }
  if (mode == SamplingPatternEnum::MODE_OLD_SCOOL_CEA_INTERFACES &&
      this->hasOrder() && this->hasOneInterface())
  {
    return InterfaceStatus::WITH_ORDER;
  }
  return InterfaceStatus::UNDEFINED;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out,
                         const InterfaceInfosPair& infos_pair)
{
  out << "Order: " << infos_pair.getOrder() << " || ";
  out << "First interface: " << infos_pair.getFirstInterface() << " || ";
  out << "Second interface:" << infos_pair.getSecondInterface();
  return out;
}
