/*----------------------------------------------------------------------------*/
/**
 * @file vtkMaterialInterface_InterfaceInfos.h
 * @brief Declares the InterfaceInfos class
 * @date 2023-01-27
 *
 * @author Guillaume Peillex (guillaume.peillex@cea.fr)
 * @copyright Copyright CEA, DAM, DIF, F-91297 Arpajon, France 2023
 *
 */
/*----------------------------------------------------------------------------*/
#ifndef VTK_MATERIAL_INTERFACE_INTERFACE_INFOS_H
#define VTK_MATERIAL_INTERFACE_INTERFACE_INFOS_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// IWYU pragma: no_include <fwd>
#include <ostream>
#include <string>

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
enum class InterfaceStatus {
  UNDEFINED = -1,
  WITH_ONE_INTERFACE = 0,
  WITH_TWO_INTERFACES = 1,
  WITH_ORDER = 2
};

/*----------------------------------------------------------------------------*/
/**
 * @brief Convert the enum into a string
 *
 * @param status : enum to convert
 * @return std::string
 */
/*----------------------------------------------------------------------------*/
std::string interface_status_to_string(InterfaceStatus status);

/*----------------------------------------------------------------------------*/
/**
 * @brief Dump the interface status enum into the stream
 *
 * @param out : the stream to dump the enum into
 * @param status : the enum to dump
 * @return std::ostream&
 */
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out, InterfaceStatus status);

/*----------------------------------------------------------------------------*/
/**
 * @brief The InterfaceInfos struct aggregates the name of the normale array
 * and the name of the distance array.
 *
 */
/*----------------------------------------------------------------------------*/
struct InterfaceInfos {
  std::string normale;
  std::string distance;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Returns true if both normale and distance names are valid (not empty
   * nor equal to "None")
   *
   * @return true
   * @return false
   */
  /*----------------------------------------------------------------------------*/
  bool isValid() const;
};

/*----------------------------------------------------------------------------*/
/**
 * @brief Dump the interface informations into the stream
 *
 * @param out : the stream to dump the interface informations into
 * @param status : the interface informations to dump
 * @return std::ostream&
 */
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out, const InterfaceInfos& infos);

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
static const InterfaceInfos InterfaceInfosSentinel{"None", "None"};

#endif // VTK_MATERIAL_INTERFACE_INTERFACE_INFOS_H
