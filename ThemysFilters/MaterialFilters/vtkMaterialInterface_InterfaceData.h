/*----------------------------------------------------------------------------*/
/**
 * @file vtkMaterialInterface_InterfaceData.h
 * @brief Declares the InterfaceData class
 * @date 2023-01-06
 *
 * @author Guillaume Peillex (guillaume.peillex@cea.fr)
 * @copyright Copyright CEA, DAM, DIF, F-91297 Arpajon, France 2023
 *
 */
/*----------------------------------------------------------------------------*/
#ifndef VTK_MATERIAL_INTERFACE_INTERFACE_DATA_H
#define VTK_MATERIAL_INTERFACE_INTERFACE_DATA_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// IWYU pragma: no_include <vtkIOStream.h>
#include <ostream>

#include <vtkType.h>

#include "vtkMaterialInterface_InterfaceNormale.h"

class vtkDoubleArray;
/*----------------------------------------------------------------------------*/
/**
 * @brief An InterfaceData object is made of a normale array and a distance
 * array.
 *
 * This class aggregates the data necessary to compute the position of an
 * interface into a cell
 *
 */
/*----------------------------------------------------------------------------*/
class InterfaceData
{
public:
  /// @brief Default constructor
  InterfaceData() = default;

  /// @brief Constructor
  /// @param normale Array of normal vectors of the interface
  /// @param distance Array of distance of the interface to the origin
  InterfaceData(vtkDoubleArray* normale, vtkDoubleArray* distance);

  /// @brief Returns the distance of the interface in the cell to the origin
  /// @param cell_index : index of the cell
  /// @return
  double getDistanceInCell(const vtkIdType& cell_index) const;

  /// @brief Returns the interface normale in the cell
  /// @param cell_index : index of the cell
  /// @return
  InterfaceNormale getNormaleInCell(const vtkIdType& cell_index) const;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief An InterfaceData is considered true if its normale and its distance
   * arrays are not null
   *
   * @return true
   * @return false
   */
  /*----------------------------------------------------------------------------*/
  explicit operator bool() const
  {
    return m_normale != nullptr && m_distance != nullptr;
  }

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Two InterfaceData object are equals if their distance and their
   * normale arrays are equals
   *
   * @param other
   * @return true
   * @return false
   */
  /*----------------------------------------------------------------------------*/
  bool operator==(const InterfaceData& other)
  {
    return m_distance == other.m_distance && m_normale == other.m_normale;
  }

private:
  /// @brief Array of normal vectors of the interface in each cell
  vtkDoubleArray* m_normale;
  /// @brief Array of distances of the interface in each cell to the origin
  vtkDoubleArray* m_distance;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Dump the interface data into the stream
   *
   * @param out : the stream to dump into
   * @param if_data : the interface data
   * @return std::ostream&
   */
  /*----------------------------------------------------------------------------*/
  friend std::ostream& operator<<(std::ostream&, const InterfaceData& if_data);
};

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#endif // VTK_MATERIAL_INTERFACE_INTERFACE_DATA_H
