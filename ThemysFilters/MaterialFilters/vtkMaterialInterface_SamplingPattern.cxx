/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include "vtkMaterialInterface_SamplingPattern.h"

#include <string>

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::string sampling_pattern_enum_to_string(SamplingPatternEnum pattern)
{
  switch (pattern)
  {
  case SamplingPatternEnum::MODE_WITH_AN_OUTGOING_INTERFACE:
    return "MODE_WITH_AN_OUTGOING_INTERFACE";
  case SamplingPatternEnum::MODE_WITH_TWO_OUTGOING_INTERFACES:
    return "MODE_WITH_TWO_OUTGOING_INTERFACES";
  case SamplingPatternEnum::MODE_OLD_SCOOL_CEA_INTERFACES:
    return "MODE_OLD_SCOOL_CEA_INTERFACES";
  default:
    return "MODE_UNKNOWN";
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::ostream& operator<<(std::ostream& out, SamplingPatternEnum pattern)
{
  out << sampling_pattern_enum_to_string(pattern);
  return out;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
