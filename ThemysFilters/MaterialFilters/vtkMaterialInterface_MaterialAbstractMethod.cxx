/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// IWYU pragma: no_include <__bit_reference>
// IWYU pragma: no_include <vtkIOStream.h>
// IWYU pragma: no_include "vtkSystemIncludes.h"
#include "vtkMaterialInterface_MaterialAbstractMethod.h"

#include <functional> // for __base, function
#include <ostream>    // for operator<<, ost...
#include <string>     // for char_traits

#include <vtkAbstractArray.h> // for vtkAbstractArray
#include <vtkCellData.h>      // for vtkCellData
#include <vtkCellIterator.h>  // for vtkCellIterator
#include <vtkDataArray.h>     // for vtkDataArray
#include <vtkDataSet.h>       // for vtkDataSet
#include <vtkDoubleArray.h>   // for vtkDoubleArray
#include <vtkIdTypeArray.h>   // for vtkIdTypeArray
#include <vtkIntArray.h>      // for vtkIntArray
#include <vtkLogger.h>        // for vtkVLog, vtkLog...
#include <vtkMergePoints.h>   // for vtkMergePoints
#include <vtkPVLogger.h>      // for PARAVIEW_LOG_PL...
#include <vtkType.h>          // for vtkIdType

#include "vtkMaterialInterface_InterfaceInfosPair.h" // for InterfaceInfosPair
#include "vtkMaterialInterface_InterfaceNormale.h"   // for InterfaceNormale
#include "vtkMaterialInterface_Utils.h"              // for is_empty_name
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
MaterialAbstractMethod::MaterialAbstractMethod(vtkDataSet* input_material_mesh)
    : m_input_material_mesh{input_material_mesh}, m_interface_data{},
      m_interface2_data{}, m_order{nullptr}, m_global_cell_index{nullptr},
      m_output_point_locator{nullptr}
{
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool MaterialAbstractMethod::allInterfaceArraysNull()
{
  return m_order == nullptr && !m_interface_data && !m_interface2_data;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool MaterialAbstractMethod::checkInterfaceStatusRequirements(
    InterfaceStatus if_status)
{
  switch (if_status)
  {
  case InterfaceStatus::WITH_ORDER:
    return this->checkOrderRequirements();
  case InterfaceStatus::WITH_ONE_INTERFACE:
    return this->checkOneInterfaceRequirements();
  case InterfaceStatus::WITH_TWO_INTERFACES:
    return this->checkTwoInterfacesRequirements();
  case InterfaceStatus::UNDEFINED:
  default:
    return false;
  }
  return false;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::array<std::vector<LocalIdType>, 2>
MaterialAbstractMethod::separateMixedAndPureCells() const
{
  std::array<std::vector<LocalIdType>, 2> res;
  enum CellType { PURE, MIXED };

  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
          "Separating mixed cells from pure cells");
  auto cellIter{
      vtk::TakeSmartPointer(m_input_material_mesh->NewCellIterator())};
  for (cellIter->InitTraversal(); !cellIter->IsDoneWithTraversal();
       cellIter->GoToNextCell())
  {
    // index cell in material
    const LocalIdType& cell_index = cellIter->GetCellId();
    const auto& normale = m_interface_data.getNormaleInCell(cell_index);
    const auto& normale_2 = m_interface2_data.getNormaleInCell(cell_index);

    vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_7, "normale: " << normale);
    vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_7, "normale_2: " << normale_2);

    if (normale || normale_2)
    {
      auto& mix = std::get<MIXED>(res);
      vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_7,
              "Mixed cell detected " << cell_index);
      mix.push_back(cell_index);
    } else
    {
      auto& pure = std::get<PURE>(res);
      vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_7,
              "Pure cell detected " << cell_index);
      pure.push_back(cell_index);
    }
  }
  return res;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool MaterialAbstractMethod::isMasked(const LocalIdType l_id)
{
  return m_mask.empty() ? false : m_mask[l_id];
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
int MaterialAbstractMethod::getOrder(const LocalIdType cell_index) const
{
  if (m_order != nullptr)
  {
    return static_cast<int>(m_order->GetTuple1(cell_index));
  }
  return 0;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
GlobalIdType
MaterialAbstractMethod::getGlobalCellId(const LocalIdType l_id) const
{
  return m_global_cell_index->GetValue(l_id);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool MaterialAbstractMethod::fetchArraysFromCellData(
    const std::string& mask_name, bool negative_value_mask_on,
    const InterfaceInfosPair& infos_pair)
{
  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "Fetching arrays from cell data");

  auto* int_mask = this->getArrayFromCellData<vtkIntArray>(mask_name);
  auto* double_mask = this->getArrayFromCellData<vtkDoubleArray>(mask_name);
  if (int_mask != nullptr && double_mask == nullptr)
  {
    m_mask = this->buildMaskArray(negative_value_mask_on, int_mask);
  } else if (int_mask == nullptr && double_mask != nullptr)
  {
    m_mask = this->buildMaskArray(negative_value_mask_on, double_mask);
  } else if (int_mask != nullptr && double_mask != nullptr)
  {
    vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_ERROR,
            "Can not fill the mask because both integer ou double array "
            "are available!");
    return false;
  }

  const auto& order_name = infos_pair.getOrder();
  const auto& interface = infos_pair.getFirstInterface();
  const auto& interface_2 = infos_pair.getSecondInterface();

  m_order = this->getOrderFromCellData(order_name);

  m_interface_data = InterfaceData{
      this->getArrayFromCellData<vtkDoubleArray>(interface.normale),
      this->getArrayFromCellData<vtkDoubleArray>(interface.distance)};
  m_interface2_data = InterfaceData{
      this->getArrayFromCellData<vtkDoubleArray>(interface_2.normale),
      this->getArrayFromCellData<vtkDoubleArray>(interface_2.distance)};

  vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_7,
          "Interface data 1 has been fetched: " << m_interface_data);
  vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_7,
          "Interface data 2 has been fetched: " << m_interface2_data);

  if (m_interface_data == m_interface2_data)
  {
    // If both interfaces are equal just keep first
    m_interface2_data = {};
  } else if (!m_interface_data && m_interface2_data)
  {
    // If first interface is not defined but second is then swap.
    m_interface_data = m_interface2_data;
    m_interface2_data = {};
  }

  m_global_cell_index = vtkIdTypeArray::SafeDownCast(
      m_input_material_mesh->GetCellData()->GetPedigreeIds());
  if (m_global_cell_index == nullptr)
  {
    vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_WARNING,
            "Global Cell Index Array is not a vtkIdTypeArray");
  }

  return true;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool MaterialAbstractMethod::checkOneInterfaceRequirements()
{
  if (m_order == nullptr && !m_interface2_data &&
      m_global_cell_index != nullptr)
  {
    if (!m_interface_data)
    {
      return true;
    }
    return true;
  }
  return false;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool MaterialAbstractMethod::checkTwoInterfacesRequirements()
{
  if (m_order == nullptr && !m_interface_data)
  {
    if (!m_interface2_data)
    {
      return true;
    }
    return true;
  }
  return false;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool MaterialAbstractMethod::checkOrderRequirements()
{
  bool status{true};
  if (m_order == nullptr)
  {
    vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_ERROR,
            "Unable to find an order array!");
    status = false;
  }
  if (!m_interface_data)
  {
    vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_ERROR,
            "The interface data are null: " << m_interface_data << "!");
    status = false;
  }
  if (m_global_cell_index == nullptr)
  {
    vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_ERROR,
            "Unable to find the cells globals ids!");
    status = false;
  }
  if (m_interface2_data)
  {
    vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_ERROR,
            "Data for a secondary interface are not null: "
                << m_interface2_data << "\n"
                << "As they are not required they should be null!");
    status = false;
  }
  return status;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
vtkDataArray*
MaterialAbstractMethod::getOrderFromCellData(const std::string& order_name)
{
  auto order = this->getArrayFromCellData<vtkDataArray>(order_name);
  constexpr std::array<double, 2> default_order_range{-1, -1};
  auto order_range{default_order_range};
  if (order != nullptr)
  {
    order->GetFiniteRange(order_range.data(), -1);
    if (order_range != default_order_range)
    {
      return order;
    }
  }
  return nullptr;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class ArrayType>
ArrayType*
MaterialAbstractMethod::getArrayFromCellData(const std::string& array_name)
{
  if (is_empty_name(array_name))
  {
    vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_7,
            "" << array_name << " is empty!");
    return nullptr;
  }

  auto* absarray = m_input_material_mesh->GetCellData()->GetAbstractArray(
      array_name.c_str());
  if (absarray == nullptr)
  {
    vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_7,
            "No array named " << array_name << " found in the cell data!");
    return nullptr;
  }

  auto* res = ArrayType::SafeDownCast(absarray);
  if (res == nullptr)
  {
    vtkVLog(vtkPVLogger::Verbosity::VERBOSITY_7,
            "The array named " << array_name << " is not an array made of "
                               << absarray->GetDataTypeAsString());
  }

  return res;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template <class ArrayType>
std::vector<bool>
MaterialAbstractMethod::buildMaskArray(const bool use_less_comparator,
                                       ArrayType* threshold_array)
{
  using ArrayValueType = typename ArrayType::ValueType;

  std::function<bool(const ArrayValueType&)> comparator =
      [](const ArrayValueType& value) -> bool { return value == 0; };
  if (use_less_comparator)
  {
    comparator = [](const ArrayValueType& value) -> bool { return value < 0; };
  }

  const auto nb_tuples = threshold_array->GetNumberOfTuples();
  std::vector<bool> mask(nb_tuples);
  for (vtkIdType tpl_ind{0}; tpl_ind < nb_tuples; ++tpl_ind)
  {
    mask[tpl_ind] = comparator(threshold_array->GetValue(tpl_ind));
  }

  return mask;
}
