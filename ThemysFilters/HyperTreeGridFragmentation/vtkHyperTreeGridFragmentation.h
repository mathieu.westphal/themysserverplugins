/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkHyperTreeGridFragmentation.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class   vtkHyperTreeGridFragmentation
 * @brief   Extract fragments from HyperTreeGrid
 *
 * vtkHyperTreeGridFragmentation est un filtre qui identifie, carctèrise et
 * extrait les fragments d'un maillage.
 *
 * Un fragment est défini comme une partie du maillage qui est topologiquement
 * connectée. C'est ce qui identifie un fragment par rapport à un autre.
 *
 * Un centre et des champs globaux sont caractèrisé pour chacun des fragments :
 * - FragmentId, l'identifiant du fragment défini à l'issu du passage de ce
 * filtre ;
 * - Pounds, le poids en nombre de cellules ;
 * - Volume, le volume ;
 * - Masse, la masse si l'utilisateur a passé en paramètre un nom de champ aux
 *   cellules du maillage décrivant une masse ou une densité ;
 * - AvgCenter, qui est la moyenne des centres des cellules ;
 * - Barycenter, qui est la somme des positions des centres multiplié par la
 * masse par cellule divisée par la masse totale ; uniquement disponible si
 *   l'utilisateur a passé en paramètre un nom de champ aux cellules sur
 *   le maillage décrivant une masse ou une densité ;
 * - un facteur de forme sphérique caractèrisé par :
 *   - un des deux centres décrit plus haut,
 *   - FormFactorRadius, le rayon du facteur de forme sphérique,
 * - AvgVelocity, qui est la moyenne des vitesses des cellules ;
 * - Velocity, qui est la somme des quantités de mouvement (vitesse * masse)
 *   par cellule divisée par la masse totale ; uniquement disponible si
 *   l'utilisateur a passé en paramètre un nom de champ aux cellules sur
 *   le maillage décrivant une masse ou une densité ;
 *
 * Le maillage d'entrée doit être un vtkHyperTreeGrid.
 *
 * Le maillage de sortie est un vtkMultiBlockDataSet comportant :
 * - un bloc "Centers" décrivant un maillage de points où chacun de ces points
 *   correspond à un centre de fragment ; les champs associés à ce point
 *   correspond aux champs globaux qui ont été calculés ; le choix de la
 * position de chacun de ces points est d'abord Barycenter, si l'utilisateur a
 * passé un nom de champ de masse ou de densité, sinon c'est AvgCenter ;
 * - un bloc "Fragment_#" pour chacun des fragments contenant en fonction du
 *   choix d'extraction de l'utilisateur :
 *   - rien (NO_EXTRACT_FRAGMENT),
 *   - le centre des cellules du bord / peau du fragment
 * (EXTRACT_EDGE_FRAGMENT), la bonne application de cette option pour cette
 * valeur nécessite que l'utilisateur ait appliqué un filtre
 * vtkHyperTreeGridGhostCellsGenerator avant ;
 *   - tous les centres des cellules décrivant ce fragment
 * (EXTRACT_ALL_FRAGMENT)
 *
 * Ce filtre est le pendant des filtres vtkPolyDataConnectivityFilter et
 * vtkConnectivity qui ne s'appliquent pas sur un vtkHyperTreeGrid et ne
 * fonctionnent pas en parallèle. L'aspect description de la topologie via des
 * valeurs seuils sur un champ de valeurs qui est proposé par ces deux filtres
 * peut être obtenu avec le filtre vtkHyperTreeGridFragmentation en appliquant
 * un filtre de vtkHyperTreeGridThreshold avant. Il se trouve que le coût
 * mémoire d'emploi du filtre de Threshold n'est que d'un champ boolean par
 * cellules qui va décrire un masque sur le maillage en input.
 *
 * @sa
 * vtkHyperTreeGrid vtkHyperTreeGridAlgorithm vtkConnectivity
 * vtkPolyDataConnectivityFilter
 *
 * @par Thanks:
 * This work was realized by Jacques-Bernard Lekien in 2023 for Commissariat a
 * l'Energie Atomique CEA, DAM, DIF, F-91297 Arpajon, France.
 */

#ifndef VTK_HYPER_TREE_GRID_FRAGMENTATION_H
#define VTK_HYPER_TREE_GRID_FRAGMENTATION_H

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// IWYU pragma: no_include <vtkIOStream.h>
// IWYU pragma: no_include <vtkObject.h>
#include <map>     // for map
#include <memory>  // for std::unique_ptr
#include <ostream> // for ostream

#include <vtkSetGet.h> // for vtkGetMacro, vtkSetMacro, vtk...

#include "vtkHyperTreeGridAlgorithm.h" // for vtkHyperTreeGridAlgorithm

class vtkBitArray;
class vtkDataObject;
class vtkHyperTreeGrid;
class vtkHyperTreeGridNonOrientedGeometryCursor;        // lines 85-85
class vtkHyperTreeGridNonOrientedVonNeumannSuperCursor; // lines 86-86
class vtkIndent;
class vtkInformation;
class vtkInformationVector;
class vtkMultiBlockDataSet; // lines 87-87
class vtkUnsignedCharArray;
class vtkUnsignedIntArray;

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

class vtkHyperTreeGridFragmentation : public vtkHyperTreeGridAlgorithm
{
public:
  vtkTypeMacro(
      vtkHyperTreeGridFragmentation,
      vtkHyperTreeGridAlgorithm) void PrintSelf(ostream& ost,
                                                vtkIndent indent) override;

  static vtkHyperTreeGridFragmentation* New();

  /**
   * @brief Définir le nom d'un champ masse.
   *
   * Définir le nom d'un champ scalaire existant dans l'input au niveau des
   * CellData et ayant une sémantique de masse.
   *
   * @param MassName : nom du champ
   */
  vtkSetStringMacro(MassName);

  /**
   * @brief Retourner le nom du champ masse.
   *
   * @return : Retourne le nom du champ masse
   */
  vtkGetStringMacro(MassName);

  /**
   * @brief Définir le nom d'un champ densité.
   *
   * Définir le nom d'un champ scalaire existant dans l'input au niveau des
   * CellData et ayant une sémantique de densité.
   *
   * Si le champ de masse et le champ de densité sont décrits par l'utilisateur,
   * une vérification aujourd'hui obligatoire est alors réalisée afin de
   * vérifier que la densité est bien la masse sur le volume calculé de la
   * cellule.
   *
   * @param DensityName : nom du champ
   */
  vtkSetStringMacro(DensityName);

  /**
   * @brief Retourner le nom du champ densité.
   *
   * @return : Retourne le nom du champ
   */
  vtkGetStringMacro(DensityName);

  /**
   * @brief Définir le nom d'un champ vitesse.
   *
   * Définir le nom d'un champ vectoriel existant dans l'input au niveau des
   * CellData et ayant une sémantique de vitesse.
   *
   * @param VelocityName : nom du champ
   */
  vtkSetStringMacro(VelocityName);

  /**
   * @brief Retourner le nom du champ vitesse.
   *
   * @return : Retourne le nom du champ
   */
  vtkGetStringMacro(VelocityName);

  /**
   * @brief Class d'énumération du type d'extraction.
   *
   * Differents types d'extraction du fragment sont proposés.
   *
   * De façon systématique, un nuage de points décrivant le centre
   * de chaque fragment est créé :
   * - ce centre est la valeur du barycentre d'un fragment si le nom du champ
   *   masse ou densité a été donné ;
   * - sinon, ce centre est le centre moyen des centres des cellules composant
   *   le fragment.
   *
   * Optionnellement, il est possible de faire le choix :
   * - NO_EXTRACT_FRAGMENT afin de ne pas sortir plus d'information ;
   * - EXTRACT_EDGE_FRAGMENT afin de sortir un maillage de points défini par le
   * centre des cellules de bord/peau composant chacun des fragments ; cette
   * option nécessite l'application d'un filtre
   * vtkHyperTreeGridGhostCellsGenerator ;
   * - EXTRACT_ALL_FRAGMENT afin de sortir un maillage de points défini par le
   * centre de toutes les cellules composant chacun des fragments.
   */
  enum class EnumClassExtractType {
    NO_EXTRACT_FRAGMENT = 0,
    EXTRACT_EDGE_FRAGMENT = 1,
    EXTRACT_ALL_FRAGMENT = 2,
  };

  /**
   * @brief Définir le type d'extraction demandé.
   *
   * @note Le défaut est EXTRACT_ALL_FRAGMENT.
   */
  vtkSetClampMacro(
      ExtractType, int,
      static_cast<int>(EnumClassExtractType::NO_EXTRACT_FRAGMENT),
      static_cast<int>(EnumClassExtractType::EXTRACT_ALL_FRAGMENT));

  /**
   * @brief Retourner le type d'extraction demandé.
   *
   * @return : Retourne le type d'extraction demandé
   */
  vtkGetMacro(ExtractType, int);

  void SetNoExtractFragment()
  {
    this->SetExtractType(
        static_cast<int>(EnumClassExtractType::NO_EXTRACT_FRAGMENT));
  }
  void SetExtractEdgeFragment()
  {
    this->SetExtractType(
        static_cast<int>(EnumClassExtractType::EXTRACT_EDGE_FRAGMENT));
  }
  void SetExtractAllFragment()
  {
    this->SetExtractType(
        static_cast<int>(EnumClassExtractType::EXTRACT_ALL_FRAGMENT));
  }

  /**
   * @brief Définir si on sortira les champs globaux sur les nuages de points
   * définissant chacun des fragments.
   *
   * @param EnableOuputGlobalFields : l'état
   * @note Défaut false.
   */
  vtkSetMacro(EnableOuputGlobalFields, bool);

  /**
   * @brief Retourner si on active les champs globaux sur les nuages de points
   * définissant chacun des fragments.
   *
   * @return true: si on active
   * @return false: si on n'active pas
   */
  vtkGetMacro(EnableOuputGlobalFields, bool);

  /**
   * @brief Définir si on ignore le champ masse.
   *
   * @param EnableMassName : l'état
   * @note Défaut false.
   */
  vtkSetMacro(EnableMassName, bool);

  /**
   * @brief Retourner si on ignore le champ de masse
   *
   * @return true: si le champ de masse est à prendre en compte
   * @return false: si le champ de masse ne doit pas être pris en compte
   */
  vtkGetMacro(EnableMassName, bool);

  /**
   * @brief Définir si on ignore le champ densité.
   *
   * @param EnableDensityName : l'état
   * @note Défaut false.
   */
  vtkSetMacro(EnableDensityName, bool);

  /**
   * @brief Retourner si on ignore le champ de densité
   *
   * @return true: si le champ de densité est à prendre en compte
   * @return false: si le champ de densité ne doit pas être pris en compte
   */
  vtkGetMacro(EnableDensityName, bool);

  /**
   * @brief Définir si on ignore le champ vitesse.
   *
   * @param EnableVelocityName : l'état
   * @note Défaut false.
   */
  vtkSetMacro(EnableVelocityName, bool);

  /**
   * @brief Retourner si on ignore le champ de vitesse
   *
   * @return true: si le champ de vitesse est à prendre en compte
   * @return false: si le champ de vitesse ne doit pas être pris en compte
   */
  vtkGetMacro(EnableVelocityName, bool);

  /**
   * @brief Set MPI MultiProcess Controller.
   */
  int RequestUpdateExtent(vtkInformation*, vtkInformationVector** inputVector,
                          vtkInformationVector*) override;

  /**
   * @brief Redefinir le type d'output du filtre.
   */
  int FillOutputPortInformation(int, vtkInformation*) override;

  /**
   * @brief Traitement d'un HyperTreeGrid.
   */
  int ProcessTrees(vtkHyperTreeGrid*, vtkDataObject*) override;

protected:
  vtkHyperTreeGridFragmentation() = default;
  ~vtkHyperTreeGridFragmentation() override;

private:
  vtkHyperTreeGridFragmentation(const vtkHyperTreeGridFragmentation&) = delete;
  void operator=(const vtkHyperTreeGridFragmentation&) = delete;

  //------------------------------------------------------------------------------------------------------------------
  // Class member

  char* MassName = nullptr;
  char* DensityName = nullptr;
  char* VelocityName = nullptr;

  int ExtractType{static_cast<int>(EnumClassExtractType::EXTRACT_ALL_FRAGMENT)};

  bool EnableOuputGlobalFields = false;

  bool EnableMassName = false;
  bool EnableDensityName = false;
  bool EnableVelocityName = false;

  //------------------------------------------------------------------------------------------------------------------
  // Class method

  /**
   * @brief Process tous les arbres (HyperTree) de l'HyperTreeGrid avec un
   * supercurseur.
   *
   * L'objet de cette méthode est de parcourir tous les cellules de l'HTG
   * afin d'identifier les fragments et construire les champs globaux qui lui
   * sont associés. L'algorithme est relativement simple puisqu'il consiste à
   * parcourir toutes les feuilles afin de leur attribuer un numéro de fragment
   * et de le propager aux cellules voisines. Si une cellule voisine est déjà
   * affectée à un fragment alors les cellules de ce fragment sont rattachées au
   * fragment courant. Cela est rendu possible grâce à l'emploi d'un
   * supercurseur qui parcourt toutes les cellules en donnant accès aux
   * informations des cellules voisines.
   *
   * Pour plus de détail, au cours du parcours, on va attribuer à chaque cellule
   * un identifiant de fragment. Au cours de ce même parcours, on peut être
   * amener à prendre la décision d'associer un fragment voisin (cela se fait à
   * travers une cellule voisine à la courante dont on constate qu'elle
   * appartient déjà à un autre fragment) au fragment courant (toutes les
   * cellules de ce fragment voisin vont donc être associées au fragment en
   * cours de la cellule courante), cela a pour effet de faire "disparaître" de
   * la numérotation un fragment, cela crée donc une discontinuité. En l'état et
   * je pense que c'est à raison, l'algo n'essaie pas de réemployer un numéro de
   * fragments. Par contre, à la fin de l'algo, l'utilisateur souhaite une
   * numérotation continue plus conventionnelle et facile à manipuler.
   *
   * @param _input: le maillage d'entrée de type vtkHyperTreeGrid
   */
  void ProcessAllTree(vtkHyperTreeGrid* _input);

  /**
   * @brief Process récursif sur un arbre (HyperTree) de l'HyperTreeGrid avec un
   * supercurseur.
   *
   * @param _supercursor:
   */
  void RecursivelyProcessTree(
      vtkHyperTreeGridNonOrientedVonNeumannSuperCursor* _supercursor);

  /**
   * @brief Process concernant la gestion particuliere du mode distribué.
   *
   * @param _input: le maillage d'entrée de type vtkHyperTreeGrid
   */
  unsigned int DistributedProcessTrees(vtkHyperTreeGrid* _input,
                                       unsigned int nbLocalFrags);

  /**
   * @brief RecursivelyProcessGhostTree.
   *
   * Cette methode parcourt les cellules afin de déterminer les fragments
   * voisins.
   *
   * @param _masked: un champ sur les cellules indiquant si une cellule est
   * masquée
   * @param _ghosted: un champ sur les cellules indiquant si une cellule est
   * fantôme
   * @param _fragIdGlobal: un champ sur les cellules indiquant l'indice du
   * fragment auquel la cellule appartient
   * @param _supercursor: le super curseur permettant le parcours des cellules
   * de l'HTG
   * @param _sameFrag: décrit les fragments identiques en associant un
   *   indice de fragment (dit max) a un autre fragment (dit min).
   */
  void RecursivelyProcessGhostTree(
      const vtkBitArray* _masked, const vtkUnsignedCharArray* _ghosted,
      const vtkUnsignedIntArray* _fragIdGlobal,
      vtkHyperTreeGridNonOrientedVonNeumannSuperCursor* _supercursor,
      std::map<unsigned int, unsigned int>& _sameFrag);

  /**
   * @brief ProcessAllGhostTree.
   *
   * Parcours les HyperTree une fois qu'un filtre GhostCellGenerator a été
   * appliqué afin de déterminer les fragments voisins.
   *
   * @param _input:
   * @param _sameFrag: décrit les fragments identiques en associant un
   *   indice de fragment (dit max) a un autre fragment (dit min).
   */
  void ProcessAllGhostTree(vtkHyperTreeGrid* _input,
                           std::map<unsigned int, unsigned int>& _sameFrag);

  /**
   * @brief Génére les maillages points par fragment.
   *
   * A partir de l'indexation de la numérotation continue finale vers
   * l'indexation discontinue construite au cours du calcul, on construit la
   * numérotation inverse qui permet de connaître l'indexation suivant la
   * numérotation continue finale à partir de l'identifiant du fragment
   * construit au cours du traitement. A partir de là, on extrait chacun des
   * fragments en ne retenant que le centre de chaque cellule.
   *
   * @param _input
   */
  void GeneratePoints(vtkHyperTreeGrid* _input);

  /**
   * @brief Process récursif sur un arbre (HyperTree) de l'HyperTreeGrid avec un
   * curseur.
   *
   * @param _cursor:
   */
  void RecursivelyProcessGeneratePoints(
      vtkHyperTreeGridNonOrientedGeometryCursor* _cursor);

  /**
   * @brief Calcul et construit le bloc "Centers" décrivant le centre de chaque
   * fragment et ses champs globaux.
   *
   * @param _output:
   */
  void ComputeBlockCenters(vtkMultiBlockDataSet* _output);

  /**
   * @brief Calcul et construit les blocs "Fragment_#" décrivant chaque fragment
   * en fonction de l'option d'extraction choisi par l'utilisateur.
   *
   * @param _nbRealGlobalFrags: le nombre réel de fragment tous serveurs
   * confondus
   * @param _output:
   */
  void ComputeBlocksFragments(const unsigned int _nbRealGlobalFrags,
                              vtkMultiBlockDataSet* _output);

  //------------------------------------------------------------------------------------------------------------------
  // Class internal

  class vtkInternal;
  std::unique_ptr<vtkInternal> Internal;
};

#endif // VTK_HYPER_TREE_GRID_FRAGMENTATION_H
