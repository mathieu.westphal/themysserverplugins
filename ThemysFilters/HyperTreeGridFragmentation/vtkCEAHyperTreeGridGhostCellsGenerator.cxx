// SPDX-FileCopyrightText: Copyright (c) Ken Martin, Will Schroeder, Bill
// Lorensen SPDX-License-Identifier: BSD-3-Clause

// IWYU pragma: no_include <type_traits>
// IWYU pragma: no_include <vtkGenericDataArray.txx>
// IWYU pragma: no_include "vtkSystemIncludes.h"

#include "vtkCEAHyperTreeGridGhostCellsGenerator.h"

#include <cassert>  // for assert
#include <cstring>  // for size_t, memcpy
#include <iostream> // for cerr
#include <map>
#include <unordered_map>
#include <utility>
#include <vector>

#include <vtkBitArray.h>
#include <vtkCellData.h>
#include <vtkCommunicator.h>
#include <vtkDataArray.h>         // for vtkDataArray
#include <vtkDataObject.h>        // for vtkDataObject
#include <vtkDataSetAttributes.h> // for vtkDataSetAttributes
#include <vtkHyperTree.h>
#include <vtkHyperTreeGrid.h>
#include <vtkHyperTreeGridNonOrientedCursor.h>
#include <vtkHyperTreeGridOrientedCursor.h>
#include <vtkIndent.h> // for vtkIndent
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkMultiProcessController.h>
#include <vtkNew.h> // for vtkNew
#include <vtkObjectFactory.h>
#include <vtkStreamingDemandDrivenPipeline.h>
#include <vtkType.h> // for vtkIdType
#include <vtkUnsignedCharArray.h>

//------------------------------------------------------------------------------
namespace {
#ifndef NDEBUG
int trace_process_id{-1};
int trace_number_of_processes{-1};

// Macro for memorize rank and number of processes
#define TRACEINFO(_processId, _numberOfProcesses)                              \
  ::trace_process_id = _processId;                                             \
  ::trace_number_of_processes = _numberOfProcesses;

// Macro for format log
#define TRACESTD(_msg)                                                         \
  {                                                                            \
    if (::trace_process_id < 0)                                                \
    {                                                                          \
      std::cerr << "[#/#] vtkHTGGCG" << _msg << std::endl;                     \
    } else                                                                     \
    {                                                                          \
      std::cerr << "[" << ::trace_process_id << "/"                            \
                << ::trace_number_of_processes << "] vtkHTGGCG" << _msg        \
                << std::endl;                                                  \
    }                                                                          \
  }

#define TRACE(_msg)                                                            \
  TRACESTD(_msg)                                                               \
  {                                                                            \
    if (::trace_process_id < 0)                                                \
    {                                                                          \
      vtkDebugMacro("[#/#] vtkHTGGCG" << _msg);                                \
    } else                                                                     \
    {                                                                          \
      vtkDebugMacro("[" << ::trace_process_id << "/"                           \
                        << ::trace_number_of_processes << "] vtkHTGGCG"        \
                        << _msg);                                              \
    }                                                                          \
  }

#else

#define TRACEINFO(processId, numberOfProcesses)
#define TRACESTD(msg)
#define TRACE(msg)

#endif
} // namespace

//------------------------------------------------------------------------------
namespace {

template <typename MapType>
typename MapType::iterator ProbeFind(vtkMultiProcessController* _controller,
                                     int _tag, MapType& _recvMap)
{
  int processBuff = -1;
  auto targetRecv = _recvMap.end();
  if (_controller->Probe(vtkMultiProcessController::ANY_SOURCE, _tag,
                         &processBuff) != 1)
  {
    vtkErrorWithObjectMacro(nullptr,
                            "Probe failed on reception of tag " << _tag);
    TRACESTD("::ProbeFind FAILED")
    return targetRecv;
  }
  TRACESTD("::ProbeFind processBuff# " << processBuff)
  if (processBuff < 0)
  {
    vtkErrorWithObjectMacro(nullptr, "Probe returned erroneous process ID "
                                         << processBuff << "reception of tag "
                                         << _tag);
    TRACESTD("::ProbeFind FAILED NEGATIVE VALUE")
    return targetRecv;
  }
  targetRecv = _recvMap.find(processBuff);
  if (targetRecv == _recvMap.end())
  {
    vtkErrorWithObjectMacro(nullptr, "Receiving unexpected communication from "
                                         << processBuff << " process on tag "
                                         << _tag << ".");
    TRACESTD("::ProbeFind FAILED NOT FIND")
    return targetRecv;
  }
  return targetRecv;
}

} // namespace

//------------------------------------------------------------------------------
VTK_ABI_NAMESPACE_BEGIN
vtkStandardNewMacro(vtkCEAHyperTreeGridGhostCellsGenerator)

    //------------------------------------------------------------------------------
    namespace
{
  struct SendBuffer {
    SendBuffer()
        : count(0), mask(0), isParent(vtkBitArray::New()),
          isMask(vtkBitArray::New())
    {
    }
    ~SendBuffer()
    {
      isParent->Delete();
      isMask->Delete();
    }
    vtkIdType count;                // Len buffer
    unsigned int mask;              // Ghost mask
    std::vector<vtkIdType> indices; // Indices for selected cells

    // A possible development would be to replace vtkBitArray with that
    // of std::vector<bool>, which is a priori more efficient.
    // This is only possible because these tables are temporarily defined
    // locally to describe HTs which will serve as ghost cells on servers
    // neighboring the current server.
    vtkBitArray* isParent; // Decomposition amr tree ghot cells
    vtkBitArray* isMask;   // Decomposition amr tree mesk cells
  };

  struct RecvBuffer {
    RecvBuffer() : count(0), offset(0) {}
    vtkIdType count;  // Len buffer
    vtkIdType offset; // Offset in field vector
    std::vector<vtkIdType> indices;
  };

  const int HTGGCG_SIZE_EXCHANGE_TAG = 5098;
  const int HTGGCG_DATA_EXCHANGE_TAG = 5099;
  const int HTGGCG_DATA2_EXCHANGE_TAG = 5100;

  // Handling receive and send buffer.
  // The structure is as follows:
  // sendBuffer[id] or recvBuffer[id] == process id of neighbor with who to
  // communicate buffer sendBuffer[id][jd] or recvBuffer[id][jd] tells which
  // tree index is being sent.
  typedef std::map<unsigned int, SendBuffer> SendTreeBufferMap;
  typedef std::map<unsigned int, SendTreeBufferMap> SendProcessBufferMap;
  typedef std::map<unsigned int, RecvBuffer> RecvTreeBufferMap;
  typedef std::map<unsigned int, RecvTreeBufferMap> RecvProcessBufferMap;

  //----------------------------------------------------------------------------
  /**
   * IndexesExtractArray.
   *
   * @brief This method serialize a vtkDataArray.
   *
   * @param _offset: index from which the output array is filled in
   * @param _inArray: Input VTK array.
   * @param _indices: Indexes cells from which we want to extract data.
   * @param _arr: Output pointer double array pre-allocated.
   */
  void IndexesExtractArray(vtkIdType & _offset, vtkDataArray * _inArray,
                           const std::vector<vtkIdType>& _indices, double* _arr)
  {
#ifndef NDEBUG
    vtkIdType nbTuples = _inArray->GetNumberOfTuples();
#endif
    int nbCompos = _inArray->GetNumberOfComponents();
    for (auto& tupleIdx : _indices)
    {
      assert(tupleIdx >= 0 && tupleIdx < nbTuples);
      for (int compIdx = 0; compIdx < nbCompos; ++compIdx)
      {
        _arr[_offset++] = _inArray->GetComponent(tupleIdx, compIdx);
      }
    }
  }

  //----------------------------------------------------------------------------
  /**
   * IndexesSetArray.
   *
   * @brief This method deserializes an array into a vtkDataArray
   *
   * @param _offset: starting index from which the input array is read
   * @param _arr: Input pointer double array.
   * @param _indices: Indexes cells from which we want to set data.
   * @param _outArray: Output VTK array pre-allocated.
   */
  void IndexesSetArray(vtkIdType & _offset, double* _arr,
                       const std::vector<vtkIdType>& _indices,
                       vtkDataArray* _outArray)
  {
#ifndef NDEBUG
    vtkIdType nbTuples = _outArray->GetNumberOfTuples();
#endif
    int nbCompos = _outArray->GetNumberOfComponents();
    for (auto& tupleIdx : _indices)
    {
      assert(tupleIdx >= 0 && tupleIdx < nbTuples);
      for (int compIdx = 0; compIdx < nbCompos; ++compIdx)
      {
        _outArray->SetComponent(tupleIdx, compIdx, _arr[_offset++]);
      }
    }
  }
}

//------------------------------------------------------------------------------
class vtkCEAHyperTreeGridGhostCellsGenerator::vtkInternal
{
public:
  enum FlagType { NOT_TREATED, INITIALIZE_TREE, INITIALIZE_FIELD };

  //----------------------------------------------------------------------------
  /**
   * @brief Constructeur.
   */
  vtkInternal(vtkMultiProcessController* _controller)
      : m_controller(_controller)
  {
    if (this->HasController())
    {
      this->m_number_of_processes = this->m_controller->GetNumberOfProcesses();
      this->m_my_rank = this->m_controller->GetLocalProcessId();
    }
  }

  //----------------------------------------------------------------------------
  /**
   * @brief Destructeur.
   */
  virtual ~vtkInternal() = default;

  //------------------------------------------------------------------------------------------------
  /**
   * @brief HasController.
   *
   * Say if has MPI MultiProcess controller.
   *
   * @return true: if exist MPI MultiProcess controller
   * @return false: if not exist MPI MultiProcess controller
   */
  bool HasController() const { return this->m_controller != nullptr; }

  //------------------------------------------------------------------------------------------------
  /**
   * @brief GetController.
   *
   * Get Current Multi Process Controller.
   * Important for calling the specific Send and Receive method.
   *
   * @return: current MPI multi process controller
   */
  vtkMultiProcessController* GetController() // non const cause VTK
  {
    return this->m_controller;
  }

  //------------------------------------------------------------------------------------------------
  /**
   * @brief GetNumberOfProcesses.
   *
   * Get number of processes/servers MPI.
   *
   * @return: number of processes/servers MPI
   */
  int GetNumberOfProcesses() const { return this->m_number_of_processes; }

  //------------------------------------------------------------------------------------------------
  /**
   * @brief GetMyRank.
   *
   * Get MPI rank of current processes/servers.
   *
   * @return: current rank MPI
   */
  int GetMyRank() const { return this->m_my_rank; }

  //------------------------------------------------------------------------------------------------
  /**
   * @brief Barrier.
   *
   * Applies the barrier method of the controller.
   */
  void Barrier() { this->m_controller->Barrier(); }

  //------------------------------------------------------------------------------------------------
  /**
   * @brief LocalizeHyperTreesOnServers.
   *
   * @brief Find on which server each hypertree is located
   *
   * @param _input:
   * @param _cellDims: (output) la dimension totale de l'HTG
   * @param _hyperTreesMapToProcesses: (output) a vector to index hyper tree to
   * rank serveur ; -1 if hyper undefined or masked by all servers
   */
  void LocalizeHyperTreesOnServers(vtkHyperTreeGrid* _input,
                                   unsigned int* _cellDims,
                                   std::vector<int>& _hyperTreesMapToProcesses)
  {
    vtkHyperTreeGrid::vtkHyperTreeGridIterator inHTs;
    _input->InitializeTreeIterator(inHTs);
    vtkIdType inTreeIndex;
    // Broadcast hyper tree locations to everyone
    _input->GetCellDims(_cellDims);
    vtkIdType nbHTs = _cellDims[0] * _cellDims[1] * _cellDims[2];
    // broadcastHyperTreesMapToProcesses describe iPe (number process/server) by
    // each HT
    std::vector<int> broadcastHyperTreesMapToProcesses(nbHTs, -1);
    _hyperTreesMapToProcesses.resize(nbHTs);
    _input->InitializeTreeIterator(inHTs);
#ifndef NDEBUG
    int nbLocalHTs = 0;
#endif
    vtkNew<vtkHyperTreeGridOrientedCursor> inOrientedCursor;
    while (inHTs.GetNextTree(inTreeIndex))
    {
      _input->InitializeOrientedCursor(inOrientedCursor, inTreeIndex);
      if (inOrientedCursor->IsMasked())
      {
        continue;
      }
      broadcastHyperTreesMapToProcesses[inTreeIndex] = this->m_my_rank;
#ifndef NDEBUG
      ++nbLocalHTs;
#endif
    }
#ifndef NDEBUG
    TRACESTD("::LocalizeHyperTreesOnServers nbLocalHTs##" << nbLocalHTs << "/##"
                                                          << nbHTs);
#endif
    this->m_controller->AllReduce(broadcastHyperTreesMapToProcesses.data(),
                                  _hyperTreesMapToProcesses.data(), nbHTs,
                                  vtkCommunicator::MAX_OP);
  }

  //------------------------------------------------------------------------------------------------
  /**
   * @brief NeighboringTopology.
   *
   * This method ...
   * Cette méthode renseigne les structures d'envoi _sendBuffer et de réception
   * _recvBuffer en déterminant les HyperTrees voisins aux HyperTrees locaux et
   * les serveurs qui les possédent. Pour se faire, on parcourt chacun des
   * HyperTrees locaux en parcourant chacun des HyperTrees voisins à la cellule
   * en cours ce qui permet de construire une nouvelle position (i,j,k) dans la
   * grille de l'HTG qui décrit cet HyperTree voisin. A partir de cette
   * position, on détermine un index d'HyperTree puis on vérifie sa localisation
   * sur un des serveurs donnée par la table _hyperTreesMapToProcesses. S'il se
   * trouve sur le même serveur, on passe à l'HyperTree suivant. S'il se trouve
   * qu'il n'est pas sur ce serveur, alors on renseigne les structures
   * d'échanges.
   *
   * A faire remarquer qu'il est crucial d'utiliser la méthode
   * GetIndexFromLevelZeroCoordinates afin de déterminer l'index d'un HyperTree
   * par rapport à sa position (i,j,k), de la même façon qu'il est crucial
   * d'utiliser la méthode GetLevelZeroCoordinatesFromIndex afin de déterminer
   * la position (i,j,k) à partir d'un index. En effet, la description de la
   * grille HTG peut être parcouru au choix de l'utilisateur suivant les axes
   * (I,J,K) ou (K,J,I). Ces méthodes prennent en compte cela.
   *
   * @param _input:
   * @param _cellDims: (output) la dimension totale de l'HTG
   * @param _hyperTreesMapToProcesses: (input) a vector to index hyper tree to
   * rank serveur ; -1 if hyper undefined or masked by all servers
   * @param _flags: (output) falg d'état
   * @param _sendBuffer: (output) structure de communication pour l'envoi de
   * message
   * @param _recvBuffer: (output) structure de communication pour réception de
   * message
   */
  void NeighboringTopology(vtkHyperTreeGrid* _input,
                           const unsigned int* _cellDims,
                           const std::vector<int>& _hyperTreesMapToProcesses,
                           std::unordered_map<unsigned, FlagType>& _flags,
                           ::SendProcessBufferMap& _sendBuffer,
                           ::RecvProcessBufferMap& _recvBuffer)
  {
    vtkHyperTreeGrid::vtkHyperTreeGridIterator inHTs;
    _input->InitializeTreeIterator(inHTs);
    vtkIdType inTreeIndex;
    vtkNew<vtkHyperTreeGridOrientedCursor> inOrientedCursor;
    // Determining who are my neighbors
    unsigned i, j, k = 0;
    _input->InitializeTreeIterator(inHTs);
    switch (_input->GetDimension())
    {
    case 2: {
      while (inHTs.GetNextTree(inTreeIndex))
      {
        _input->InitializeOrientedCursor(inOrientedCursor, inTreeIndex);
        if (inOrientedCursor->IsMasked())
        {
          continue;
        }
        _input->GetLevelZeroCoordinatesFromIndex(inTreeIndex, i, j, k);
        // Avoiding over / under flowing the grid
        for (int rj = ((j > 0) ? -1 : 0);
             rj < (((j + 1) < _cellDims[1]) ? 2 : 1); ++rj)
        {
          for (int ri = ((i > 0) ? -1 : 0);
               ri < (((i + 1) < _cellDims[0]) ? 2 : 1); ++ri)
          {
            vtkIdType neighbor = -1;
            _input->GetIndexFromLevelZeroCoordinates(neighbor, i + ri, j + rj,
                                                     0);
            int id = _hyperTreesMapToProcesses.at(neighbor);
            if (id >= 0 && id != this->m_my_rank)
            {
              // Construction a neighborhood mask to extract the interface in
              // ExtractInterface later on Same encoding as
              // vtkHyperTreeGrid::GetChildMask
              _sendBuffer[id][inTreeIndex].mask |=
                  1 << (8 * sizeof(int) - 1 - (ri + 1 + (rj + 1) * 3));
              // Not receiving anything from this guy since we will send him
              // stuff
              _recvBuffer[id][neighbor].count = 0;
              // Process not treated yet, yielding the flag
              _flags[id] = NOT_TREATED;
            }
          }
        }
      }
      break;
    }
    case 3: {
      while (inHTs.GetNextTree(inTreeIndex))
      {
        _input->InitializeOrientedCursor(inOrientedCursor, inTreeIndex);
        if (inOrientedCursor->IsMasked())
        {
          continue;
        }
        _input->GetLevelZeroCoordinatesFromIndex(inTreeIndex, i, j, k);
        // Avoiding over / under flowing the grid
        for (int rk = ((k > 0) ? -1 : 0);
             rk < (((k + 1) < _cellDims[2]) ? 2 : 1); ++rk)
        {
          for (int rj = ((j > 0) ? -1 : 0);
               rj < (((j + 1) < _cellDims[1]) ? 2 : 1); ++rj)
          {
            for (int ri = ((i > 0) ? -1 : 0);
                 ri < (((i + 1) < _cellDims[0]) ? 2 : 1); ++ri)
            {
              vtkIdType neighbor = -1;
              _input->GetIndexFromLevelZeroCoordinates(neighbor, i + ri, j + rj,
                                                       k + rk);
              int iServer = _hyperTreesMapToProcesses.at(neighbor);
              if (iServer >= 0 && iServer != this->m_my_rank)
              {
                // Construction a neighborhood mask to extract the interface in
                // ExtractInterface later on Same encoding as
                // vtkHyperTreeGrid::GetChildMask
                _sendBuffer[iServer][inTreeIndex].mask |=
                    1 << (8 * sizeof(int) - 1 -
                          (ri + 1 + (rj + 1) * 3 + (rk + 1) * 9));
                // Not receiving anything from this guy since we will send him
                // stuff
                _recvBuffer[iServer][neighbor].count = 0;
                TRACESTD("::LocalizeHyperTreesOnServers HT#"
                         << inTreeIndex << " have neighbor HT# " << neighbor
                         << " in iServer#" << iServer)
                // Process not treated yet, yielding the flag
                _flags[iServer] = NOT_TREATED;
              }
            }
          }
        }
      }
#ifndef NDEBUG
      {
        for (const auto& [key, val] : _sendBuffer)
        {
          TRACESTD("::LocalizeHyperTreesOnServers send to #"
                   << key << " size:## " << val.size())
        }
      }
#endif
      break;
    }
    }
  }

  //------------------------------------------------------------------------------
  /**
   * Lecture de la description des interfaces entre processus/serveurs voisins
   * du maillage passé en entrée au filtre afin que cette description soit par
   * la suite envoyé à un des serveurs voisins pour être utilisé comme des
   * cellules fantômes. Cette méthode est la version miroir de ce qui sera fait
   * au niveau des serveurs via la méthode
   * vtkCEAHyperTreeGridGhostCellsGenerator::CreateGhostTree.
   *
   * A noter que ces cellules qui deviendront fantômes pour d'autres serveurs
   * appartiennent à des HTs dont toutes les cellules le sont, des HTs fantômes.
   * C'est la contrainte qu'exploite ce filtre : une distribution avec une
   * présence d'un HT sur un unique serveur.
   *
   * @param inCursor Cursor on the current tree to read from the input
   * @param isParent A bit array being produced by this filter,
   * telling if the corresponding node is parent or not. A node is
   * a parent if it is not a leaf. The map of the tracking is stored in indices.
   * For example, if the data array of the input is called inArray,
   * isParent->GetValue(m) equals one if inArray->GetTuple1(indices[m]) is not a
   * leaf.
   * @param indices An array produced by this filter mapping the nodes of the
   * interface with their location in the input data array.
   * @param grid Input vtkHyperTreeGrid used to have the neighborhood profile.
   * This neighborhood profile is tested with the mask parameter to know whether
   * to descend or not in the current hyper tree.
   * @param mask Input parameter which should be shaped as
   * vtkHyperTreeGrid::GetChildMask() of the input. This parameter is used to
   * only descend on the interface with the other processes.
   * @param pos This parameter will be equal to the number of nodes in the hyper
   * tree to send to the other processes.
   */
  void ExtractInterface(vtkHyperTreeGridNonOrientedCursor* inCursor,
                        vtkBitArray* isParent, vtkBitArray* isMask,
                        std::vector<vtkIdType>& indices, vtkHyperTreeGrid* grid,
                        unsigned int mask, vtkIdType& pos)
  {
    isParent->InsertTuple1(pos, !inCursor->IsLeaf());
    isMask->InsertTuple1(pos, inCursor->IsMasked());
    indices[pos] = inCursor->GetGlobalNodeIndex();
    ++pos;
    if (!inCursor->IsLeaf())
    {
      for (int ichild = 0; ichild < inCursor->GetNumberOfChildren(); ++ichild)
      {
        inCursor->ToChild(ichild);
        unsigned int newMask = mask & grid->GetChildMask(ichild);
        if (newMask)
        {
          this->ExtractInterface(inCursor, isParent, isMask, indices, grid,
                                 newMask, pos);
        } else
        {
          isParent->InsertTuple1(pos, 0);
          // Le fait de changer un coarse en leaf le rend masque car il ne doit
          // pas etre traite et concerne la structure interne des ghosts et non
          // l'interface ghost a proprement parle
          isMask->InsertTuple1(pos, 1);
          indices[pos] = inCursor->GetGlobalNodeIndex();
          ++pos;
        }
        inCursor->ToParent();
      }
    }
  }

  //------------------------------------------------------------------------------------------------
  /**
   * @brief ExchangeSizeWithNeighbors.
   *
   * Cette méthode calcule le nombre de cellules qui devront être échangé entre
   * le serveur courant et chacun des serveurs voisins.
   * Cette information est ensuite envoyée afin que chaque serveur sache le
   * nombre de cellules qu'ils recevront de chacun des serveurs voisins.
   *
   * @param _input:
   * @param _sendBuffer: (input/output)
   * @param _recvBuffer: (input/output)
   * @param _iRecvError: (output) the iServer in receive error
   *
   * @return 1 si tout s'est bien passé
   */
  int ExchangeSizeWithNeighbors(vtkHyperTreeGrid* _input,
                                ::SendProcessBufferMap& _sendBuffer,
                                ::RecvProcessBufferMap& _recvBuffer,
                                std::size_t& _iRecvError)
  {
    vtkNew<vtkHyperTreeGridNonOrientedCursor> inCursor;
    // Send
    for (int iServer = 0; iServer < this->m_number_of_processes; ++iServer)
    {
      if (iServer == this->m_my_rank)
      {
        continue;
      }
      auto sendIt = _sendBuffer.find(iServer);
      if (sendIt == _sendBuffer.end())
      {
        continue;
      }

      ::SendTreeBufferMap& sendTreeMap = sendIt->second;
      std::vector<vtkIdType> counts(sendTreeMap.size());
      int cpt = 0;
      for (auto&& sendTreeBufferPair : sendTreeMap)
      {
        vtkIdType treeId = sendTreeBufferPair.first;
        TRACESTD("::ExchangeSizeWithNeighbors SEND treeId#" << treeId)
        auto&& sendTreeBuffer = sendTreeBufferPair.second;
        _input->InitializeNonOrientedCursor(inCursor, treeId);
        assert(!inCursor->IsMasked());
        // Extracting the tree interface with its neighbors
        sendTreeBuffer.count = 0;
        vtkHyperTree* tree = inCursor->GetTree();
        // We store the isParent profile along the interface to know when to
        // subdivide later indices store the indices in the input of the nodes
        // on the interface
        vtkIdType nbVertices = tree->GetNumberOfVertices();
        sendTreeBuffer.indices.resize(nbVertices); // surdimension
        this->ExtractInterface(inCursor, sendTreeBuffer.isParent,
                               sendTreeBuffer.isMask, sendTreeBuffer.indices,
                               _input, sendTreeBuffer.mask,
                               sendTreeBuffer.count);
        sendTreeBuffer.indices.resize(sendTreeBuffer.count); // surdimension
        // Telling my neighbors how much data I will send later
        TRACESTD("::ExchangeSizeWithNeighbors SEND to "
                 << iServer << " cpt#" << cpt << " HT#" << treeId
                 << " sendCount##" << sendTreeBuffer.count)
        counts[cpt++] = sendTreeBuffer.count;
      }
      this->m_controller->Send(counts.data(), cpt, iServer,
                               HTGGCG_SIZE_EXCHANGE_TAG);
    }

    // Recv by Probe
    // Receiving size info from my neighbors
    std::size_t iRecv = 0;
    for (auto itRecvBuffer = _recvBuffer.begin();
         itRecvBuffer != _recvBuffer.end(); ++itRecvBuffer)
    {
      auto targetRecvBuffer = itRecvBuffer;

      if (this->m_controller->CanProbe())
      {
        targetRecvBuffer = ::ProbeFind(this->m_controller,
                                       HTGGCG_SIZE_EXCHANGE_TAG, _recvBuffer);
        if (targetRecvBuffer == _recvBuffer.end())
        {
          _iRecvError = iRecv;
          return 0;
        }
      }

      int iServer = targetRecvBuffer->first;
      auto&& recvTreeMap = targetRecvBuffer->second;
      std::vector<vtkIdType> counts(recvTreeMap.size());
      TRACESTD("::ExchangeSizeWithNeighbors RECV to #" << iServer << " ##"
                                                       << recvTreeMap.size())
      this->m_controller->Receive(counts.data(),
                                  static_cast<vtkIdType>(recvTreeMap.size()),
                                  iServer, HTGGCG_SIZE_EXCHANGE_TAG);
      int cpt = 0;
      for (auto&& recvBufferPair : recvTreeMap)
      {
        // treeId: recvBufferPair.first)
        // toProcess: iServer
        // recvCount: counts[cpt]
        recvBufferPair.second.count = counts[cpt++];
        TRACESTD("::ExchangeSizeWithNeighbors RECV to #"
                 << iServer << " cpt#" << cpt << " HT#" << recvBufferPair.first
                 << " recvCount##" << counts[cpt])
      }
      ++iRecv;
    }
    return 1;
  }

  //------------------------------------------------------------------------------------------------
  /**
   * Creates a ghost tree in the output. It is built in mirror with
   * vtkCEAHyperTreeGridGhostCellsGenerator::ExtractInterface.
   *
   * @param outCursor Cursor on the output tree that will create the hyper tree.
   * @param isParent Input vtkBitArray produced by a neighbor process to tell if
   * the current node is a leaf or not.
   * @param indices Output array mapping the created nodes to their position in
   * the output data arrays.
   * @param pos Parameter which should be left untouched, it is used to keep
   * track of the number of inserted data.
   */
  vtkIdType CreateGhostTree(vtkHyperTreeGridNonOrientedCursor* outCursor,
                            vtkBitArray* outMask, vtkBitArray* isParent,
                            vtkBitArray* isMask, vtkIdType* indices,
                            vtkIdType&& pos = 0)
  {
    indices[pos] = outCursor->GetGlobalNodeIndex();
    outMask->InsertValue(indices[pos], isMask->GetValue(pos));
    if (isParent->GetValue(pos++))
    {
      outCursor->SubdivideLeaf();
      for (int ichild = 0; ichild < outCursor->GetNumberOfChildren(); ++ichild)
      {
        outCursor->ToChild(ichild);
        this->CreateGhostTree(outCursor, outMask, isParent, isMask, indices,
                              std::forward<vtkIdType&&>(pos));
        outCursor->ToParent();
      }
    }
    return pos;
  }

  //------------------------------------------------------------------------------------------------
  /**
   * @brief ExchangeIsParentIsMaskWithNeighbors.
   *
   * Cette méthode transmet aux serveurs voisins la description des HTs fantômes
   * (exclusivement constituées de cellules fantômes) via les tableaux isParent
   * et isMask constitués dans la structure d'envoi _sendBuffer. Ces tableaux
   * représentent des binaires et sont donc transmis comme des tableaux
   * d'unsigned char par paquet de 8 valeurs binaires.
   *
   * Les serveurs recoivent alors les données des serveurs voisins afin de
   * construire les HTs fantômes via la méthode CreateGhostTree.
   *
   * @param _numberOfValues: (in/ouput) current number of values
   * @param _sendBuffer: (input)
   * @param _recvBuffer: (input)
   * @param _flags: (in/output)
   * @param _output: (in/output)
   * @param _outputMask: (in/output)
   * @param _iRecvError: (output) the iServer in receive error
   */
  int ExchangeIsParentIsMaskWithNeighbors(
      vtkIdType& _numberOfValues, ::SendProcessBufferMap& _sendBuffer,
      ::RecvProcessBufferMap& _recvBuffer,
      std::unordered_map<unsigned, vtkCEAHyperTreeGridGhostCellsGenerator::
                                       vtkInternal::FlagType>& _flags,
      vtkHyperTreeGrid* _output, vtkBitArray* _outputMask,
      std::size_t& _iRecvError)
  {
    // Sending masks and parent state of each node
    for (int iServer = 0; iServer < this->m_number_of_processes; ++iServer)
    {
      if (iServer == this->m_my_rank)
      {
        continue;
      }
      auto sendIt = _sendBuffer.find(iServer);
      if (sendIt == _sendBuffer.end())
      {
        continue;
      }
      ::SendTreeBufferMap& sendTreeMap = sendIt->second;
      std::vector<unsigned char> buf;
      // Accumulated length
      vtkIdType len = 0;
      for (auto&& sendTreeBufferPair : sendTreeMap)
      {
        auto&& sendTreeBuffer = sendTreeBufferPair.second;
        if (sendTreeBuffer.count)
        {
          // We send the bits packed in unsigned char
          vtkIdType dlen = (sendTreeBuffer.count + 7) / 8;
          TRACESTD("::ExchangeIsParentIsMaskWithNeighbors SEND to #"
                   << iServer << " count##" << sendTreeBuffer.count
                   << " sizeof(uichar)##" << sizeof(unsigned char) << " dlen## "
                   << dlen << " len## " << len)
          buf.resize(len + 2 * dlen);
          memcpy(buf.data() + len, sendTreeBuffer.isParent->GetPointer(0),
                 dlen);
          memcpy(buf.data() + len + dlen, sendTreeBuffer.isMask->GetPointer(0),
                 dlen);
          len += 2 * dlen;
        }
      }
      TRACESTD("::ExchangeIsParentIsMaskWithNeighbors SEND to #"
               << iServer << " len## " << len)
      this->m_controller->Send(buf.data(), len, iServer,
                               HTGGCG_DATA_EXCHANGE_TAG);
    }

    if (_outputMask == nullptr)
    {
      _outputMask = vtkBitArray::New();
      _outputMask->Resize(_numberOfValues);
      for (vtkIdType ii = 0; ii < _numberOfValues; ++ii)
      {
        _outputMask->SetValue(ii, 0);
      }
    }

    vtkNew<vtkHyperTreeGridNonOrientedCursor> outCursor;
    // Receiving masks
    std::size_t iRecv = 0;
    for (auto itRecvBuffer = _recvBuffer.begin();
         itRecvBuffer != _recvBuffer.end(); ++itRecvBuffer)
    {
      auto targetRecvBuffer = itRecvBuffer;
      if (this->m_controller->CanProbe())
      {
        targetRecvBuffer = ::ProbeFind(this->m_controller,
                                       HTGGCG_DATA_EXCHANGE_TAG, _recvBuffer);
        if (targetRecvBuffer == _recvBuffer.end())
        {
          _iRecvError = iRecv;
          return 0;
        }
      }
      int iServer = targetRecvBuffer->first;
      auto&& recvTreeMap = targetRecvBuffer->second;
      TRACESTD("::ExchangeIsParentIsMaskWithNeighbors RECV to "
               << iServer << " ##" << recvTreeMap.size())
      // If we have not dealt with iServer yet,
      // we prepare for receiving with appropriate length
      if (_flags[iServer] == NOT_TREATED)
      {
        vtkIdType len = 0;
        for (auto&& recvTreeBufferPair : recvTreeMap)
        {
          auto&& recvTreeBuffer = recvTreeBufferPair.second;
          // bit message is packed in unsigned char, getting the correct length
          // of the message
          len += (recvTreeBuffer.count + 7) / 8;
          len += (recvTreeBuffer.count + 7) / 8;
        }
        TRACESTD("::ExchangeIsParentIsMaskWithNeighbors RECV to "
                 << iServer << " real##" << len)
        std::vector<unsigned char> buf(len);
        this->m_controller->Receive(buf.data(), len, iServer,
                                    HTGGCG_DATA_EXCHANGE_TAG);
        vtkIdType cpt = 0;
        // Distributing receive data among my trees, i.e. creating my ghost
        // trees with this data Remember: we only have the nodes / leaves at the
        // inverface with our neighbor
        for (auto&& recvTreeBufferPair : recvTreeMap)
        {
          vtkIdType treeId = recvTreeBufferPair.first;
          auto&& recvTreeBuffer = recvTreeBufferPair.second;
          if (recvTreeBuffer.count != 0)
          {
            vtkIdType dlen = (recvTreeBuffer.count + 7) / 8;

            vtkNew<vtkBitArray> isParent;
            isParent->SetArray(buf.data() + cpt, recvTreeBuffer.count,
                               1); // 1 for use directly external buf

            vtkNew<vtkBitArray> isMask;
            isMask->SetArray(buf.data() + cpt + dlen, recvTreeBuffer.count,
                             1); // 1 for use directly external buf

            recvTreeBuffer.offset = _numberOfValues;
            recvTreeBuffer.indices.resize(recvTreeBuffer.count);

#ifndef NDEBUG
            _output->InitializeNonOrientedCursor(outCursor, treeId);
            assert(!outCursor->HasTree());
#endif

            _output->InitializeNonOrientedCursor(outCursor, treeId, true);

            outCursor->SetGlobalIndexStart(_numberOfValues);

            vtkIdType numberOfValuesHT =
                this->CreateGhostTree(outCursor, _outputMask, isParent, isMask,
                                      recvTreeBuffer.indices.data());
            assert(numberOfValuesHT == recvTreeBuffer.count);
            _numberOfValues += numberOfValuesHT;

            cpt += 2 * dlen;
          }
        }
        assert(cpt == len);
        _flags[iServer] = INITIALIZE_TREE;
      }
      ++iRecv;
    }
    return 1;
  }

  //------------------------------------------------------------------------------------------------
  /**
   * @brief ExchangeValuesWithNeighbors.
   *
   * Cette méthode transmet aux serveurs voisins les valeurs des cellules des
   * HTs qui deviendront des fantômes sur les serveurs voisins. Tous les champs
   * sont transmis sous la forme de double.
   *
   * @param _input: (input)
   * @param _sendBuffer: (input)
   * @param _recvBuffer: (input)
   * @param _flags: (in/output)
   * @param _output: (in/output)
   * @param _outputMask: (in/output)
   * @param _iRecvError: (output) the iServer in receive error
   */
  int ExchangeValuesWithNeighbors(
      vtkHyperTreeGrid* _input, ::SendProcessBufferMap& _sendBuffer,
      ::RecvProcessBufferMap& _recvBuffer,
      std::unordered_map<unsigned, FlagType>& _flags, vtkHyperTreeGrid* _output,
      std::size_t& _iRecvError)
  {
    TRACESTD("::ExchangeValuesWithNeighbors")
    // We now send the data store on each node
    for (int iServer = 0; iServer < this->m_number_of_processes; ++iServer)
    {
      if (iServer == this->m_my_rank)
      {
        continue;
      }
      auto sendIt = _sendBuffer.find(iServer);
      if (sendIt == _sendBuffer.end())
      {
        continue;
      }
      TRACESTD("::ExchangeValuesWithNeighbors SEND to #"
               << iServer << "/##" << this->m_number_of_processes)
      ::SendTreeBufferMap& sendTreeMap = sendIt->second;
      std::vector<double> buf;
      vtkIdType len = 0;
      for (auto&& sendTreeBufferPair : sendTreeMap)
      {
        auto&& sendTreeBuffer = sendTreeBufferPair.second;
        if (sendTreeBuffer.count)
        {
          vtkCellData* pd = _input->GetCellData();
          int nbArray = pd->GetNumberOfArrays();
          vtkIdType first = len;
          vtkIdType crtlen = 0;
          for (int iArray = 0; iArray < nbArray; ++iArray)
          {
            vtkDataArray* inArray = pd->GetArray(iArray);
            crtlen += inArray->GetNumberOfComponents();
          }
          crtlen *= sendTreeBuffer.count;

          len += crtlen;
          buf.resize(len);
          TRACESTD("::ExchangeValuesWithNeighbors to #"
                   << iServer << "/##" << this->m_number_of_processes
                   << " len:" << len)
          // All values send as double
          double* arr = buf.data() + first;
          vtkIdType offset = 0;
          for (int iArray = 0; iArray < nbArray; ++iArray)
          {
            TRACESTD("::ExchangeValuesWithNeighbors to #"
                     << iServer << "/##" << this->m_number_of_processes
                     << " iArray# " << iArray << " "
                     << pd->GetArrayName(iArray))
            vtkDataArray* inArray = pd->GetArray(iArray);
            assert(vtkIdType(sendTreeBuffer.indices.size()) ==
                   sendTreeBuffer.count);
            ::IndexesExtractArray(offset, inArray, sendTreeBuffer.indices, arr);
          }
          assert(arr + offset == buf.data() + len);
          assert(offset == crtlen);
        }
      }
      this->m_controller->Send(buf.data(), len, iServer,
                               HTGGCG_DATA2_EXCHANGE_TAG);
    }

    // We receive the data
    std::size_t iRecv = 0;
    for (auto itRecvBuffer = _recvBuffer.begin();
         itRecvBuffer != _recvBuffer.end(); ++itRecvBuffer)
    {
      auto targetRecvBuffer = itRecvBuffer;
      if (this->m_controller->CanProbe())
      {
        targetRecvBuffer = ::ProbeFind(this->m_controller,
                                       HTGGCG_DATA2_EXCHANGE_TAG, _recvBuffer);
        if (targetRecvBuffer == _recvBuffer.end())
        {
          _iRecvError = iRecv;
          return 0;
        }
      }
      int iServer = targetRecvBuffer->first;
      TRACESTD("::ExchangeValuesWithNeighbors RECV to #"
               << iServer << "/##" << this->m_number_of_processes)
      auto&& recvTreeMap = targetRecvBuffer->second;
      if (_flags[iServer] == INITIALIZE_TREE)
      {
        vtkIdType len = 0;
        for (auto&& recvTreeBufferPair : recvTreeMap)
        {
          vtkCellData* pd = _output->GetCellData();
          int nbArray = pd->GetNumberOfArrays();
          vtkIdType crtlen = 0;
          for (int iArray = 0; iArray < nbArray; ++iArray)
          {
            vtkDataArray* inArray = pd->GetArray(iArray);
            crtlen += inArray->GetNumberOfComponents();
          }
          crtlen *= recvTreeBufferPair.second.count;
          len += crtlen;
        }
        std::vector<double> buf(len);
        this->m_controller->Receive(buf.data(), len, iServer,
                                    HTGGCG_DATA2_EXCHANGE_TAG);

        len = 0;
        for (auto&& recvTreeBufferPair : recvTreeMap)
        {
          auto&& recvTreeBuffer = recvTreeBufferPair.second;
          vtkCellData* pd = _output->GetCellData();
          int nbArray = pd->GetNumberOfArrays();
          double* arr = buf.data() + len;

          vtkIdType crtlen = 0;
          for (int iArray = 0; iArray < nbArray; ++iArray)
          {
            vtkDataArray* outArray = pd->GetArray(iArray);
            outArray->SetNumberOfTuples(outArray->GetNumberOfTuples() +
                                        recvTreeBufferPair.second.count *
                                            outArray->GetNumberOfComponents());
            ::IndexesSetArray(crtlen, arr, recvTreeBuffer.indices, outArray);
          }
          len += crtlen;
        }
        _flags[iServer] = INITIALIZE_FIELD;
      }
      ++iRecv;
    }
    return 1;
  }

private:
  vtkMultiProcessController* m_controller = nullptr;

  int m_number_of_processes{0};
  int m_my_rank{0};
};

//------------------------------------------------------------------------------
vtkCEAHyperTreeGridGhostCellsGenerator::vtkCEAHyperTreeGridGhostCellsGenerator()
{
  this->AppropriateOutput = true;
}

//------------------------------------------------------------------------------
vtkCEAHyperTreeGridGhostCellsGenerator::
    ~vtkCEAHyperTreeGridGhostCellsGenerator() = default;

//------------------------------------------------------------------------------
void vtkCEAHyperTreeGridGhostCellsGenerator::PrintSelf(ostream& ost,
                                                       vtkIndent indent)
{
  this->Superclass::PrintSelf(ost, indent);
}

//------------------------------------------------------------------------------
int vtkCEAHyperTreeGridGhostCellsGenerator::FillOutputPortInformation(
    int, vtkInformation* info)
{
  info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkHyperTreeGrid");
  return 1;
}

//--------------------------------------------------------------------------------------------------
int vtkCEAHyperTreeGridGhostCellsGenerator::RequestUpdateExtent(
    vtkInformation*, vtkInformationVector** inputVector, vtkInformationVector*)
{
  if (this->Internal == nullptr)
  {
    this->Internal = std::make_unique<vtkInternal>(
        vtkMultiProcessController::GetGlobalController());
    if (this->Internal->HasController())
    {
      // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
      vtkInformation* info = inputVector[0]->GetInformationObject(0);
      info->Set(vtkStreamingDemandDrivenPipeline::UPDATE_PIECE_NUMBER(),
                this->Internal->GetMyRank());
      info->Set(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_PIECES(),
                this->Internal->GetNumberOfProcesses());
    }
  }
  return 1;
}

//------------------------------------------------------------------------------
int vtkCEAHyperTreeGridGhostCellsGenerator::ProcessTrees(
    vtkHyperTreeGrid* input, vtkDataObject* outputDO)
{
  TRACE("::ProcessTrees");
  assert(input->GetDimension() > 1);

  // Downcast output data object to hyper tree grid
  vtkHyperTreeGrid* output = vtkHyperTreeGrid::SafeDownCast(outputDO);
  if (!output)
  {
    vtkErrorMacro("Incorrect type of output: " << outputDO->GetClassName());
    return 0;
  }

  // We only need the structure of the input with no data in it
  output->Initialize();

  // Local handle on the controller
  int processId = this->Internal->GetMyRank();
  int numberOfProcesses = this->Internal->GetNumberOfProcesses();
  TRACEINFO(processId, numberOfProcesses)
  if (numberOfProcesses == 1)
  {
    output->DeepCopy(
        input); // Ne devrait on pas s'attendre plutôt à un ShallowCopy ?
    return 1;
  } else
  {
    output->CopyEmptyStructure(input);
    output->GetCellData()->CopyStructure(input->GetCellData());
  }

  TRACE("::ProcessTrees Link HyperTrees")

  // Link HyperTrees
  vtkHyperTreeGrid::vtkHyperTreeGridIterator inHTs;
  input->InitializeTreeIterator(inHTs);
  vtkIdType inTreeIndex;

  // To keep track of the number of nodes in the htg
  vtkIdType numberOfValues = 0;

  vtkNew<vtkHyperTreeGridOrientedCursor> inOrientedCursor;
  vtkNew<vtkHyperTreeGridNonOrientedCursor> outCursor, inCursor;

  vtkBitArray *outputMask = input->HasMask() ? vtkBitArray::New() : nullptr,
              *inputMask = input->HasMask() ? input->GetMask() : nullptr;

  // First, we copy the input htg into the output
  // We do it "by hand" to fill gaps if they exist
  while (inHTs.GetNextTree(inTreeIndex))
  {
    input->InitializeNonOrientedCursor(inCursor, inTreeIndex);
    if (inCursor->IsMasked())
    {
      continue;
    }
    output->InitializeNonOrientedCursor(outCursor, inTreeIndex, true);
    outCursor->SetGlobalIndexStart(numberOfValues);
    this->CopyInputTreeToOutput(inCursor, outCursor, input->GetCellData(),
                                output->GetCellData(), inputMask, outputMask);
    numberOfValues += outCursor->GetTree()->GetNumberOfVertices();
  }

  vtkIdType numberOfValuesLocal = numberOfValues;
  TRACE("::ProcessTrees ouput cellsLocal##" << numberOfValuesLocal
                                            << " (input cells##"
                                            << input->GetNumberOfCells())
  assert(numberOfValuesLocal == output->GetNumberOfCells());
  assert(numberOfValuesLocal <= input->GetNumberOfCells());

  ::SendProcessBufferMap sendBuffer;
  ::RecvProcessBufferMap recvBuffer;

  std::unordered_map<
      unsigned, vtkCEAHyperTreeGridGhostCellsGenerator::vtkInternal::FlagType>
      flags;

  unsigned int cellDims[3];
  std::vector<int> hyperTreesMapToProcesses;
  TRACE("::ProcessTrees LocalizeHyperTreesOnServers");
  this->Internal->LocalizeHyperTreesOnServers(input, cellDims,
                                              hyperTreesMapToProcesses);

  TRACE("::ProcessTrees NeighboringTopology");
  this->Internal->NeighboringTopology(input, cellDims, hyperTreesMapToProcesses,
                                      flags, sendBuffer, recvBuffer);

  std::size_t iRecvError;

  TRACE("::ProcessTreesExchangeSizeWithNeighbors");
  if (this->Internal->ExchangeSizeWithNeighbors(input, sendBuffer, recvBuffer,
                                                iRecvError) == 0)
  {
    vtkErrorMacro("ERROR Reception probe on process "
                  << processId << " failed on " << iRecvError
                  << "th iteration.");
    return 0;
  }

#ifdef NDEBUG
  vtkDebugMacro("Barrier");
  this->Internal->Barrier();
#endif

  vtkDebugMacro("ExchangeIsParentIsMaskWithNeighbors");
  if (this->Internal->ExchangeIsParentIsMaskWithNeighbors(
          numberOfValues, sendBuffer, recvBuffer, flags, output, outputMask,
          iRecvError) == 0)
  {
    vtkErrorMacro("ERROR Reception probe on process "
                  << processId << " failed on " << iRecvError
                  << "th iteration.");
    return 0;
  }

#ifdef NDEBUG
  TRACE("Barrier");
  this->Internal->Barrier();
#endif

  TRACE("::ExchangeValuesWithNeighbors");
  if (this->Internal->ExchangeValuesWithNeighbors(
          input, sendBuffer, recvBuffer, flags, output, iRecvError) == 0)
  {
    vtkErrorMacro("ERROR Reception probe on process "
                  << processId << " failed on " << iRecvError
                  << "th iteration.");
    return 0;
  }

#ifdef NDEBUG
  TRACE("Barrier")
  this->Internal->Barrier();
#endif

  if (this->GetEnableVisibilityGhostCells())
  {
    vtkNew<vtkUnsignedCharArray> scalars;
    scalars->SetNumberOfComponents(1);
    scalars->SetName("vtkIsGhostCell");
    scalars->SetNumberOfTuples(numberOfValues);
    for (vtkIdType ii = 0; ii < numberOfValuesLocal; ++ii)
    {
      scalars->InsertValue(ii, processId * 100);
    }
    for (vtkIdType ii = numberOfValuesLocal; ii < numberOfValues; ++ii)
    {
      scalars->InsertValue(ii, processId * 100 + 1);
    }
    output->GetCellData()->AddArray(scalars);
  } else
  {
    vtkNew<vtkUnsignedCharArray> scalars;
    scalars->SetNumberOfComponents(1);
    scalars->SetName(vtkDataSetAttributes::GhostArrayName());
    scalars->SetNumberOfTuples(numberOfValues);
    for (vtkIdType ii = 0; ii < numberOfValuesLocal; ++ii)
    {
      scalars->InsertValue(ii, 0);
    }
    for (vtkIdType ii = numberOfValuesLocal; ii < numberOfValues; ++ii)
    {
      scalars->InsertValue(ii, 1);
    }
    output->GetCellData()->AddArray(scalars);
  }

  output->SetMask(outputMask);

  TRACE("::ProcessTrees input cells## " << input->GetNumberOfCells()
                                        << " output cells## "
                                        << output->GetNumberOfCells())

  this->UpdateProgress(1.);
  return 1;
}

//------------------------------------------------------------------------------
void vtkCEAHyperTreeGridGhostCellsGenerator::CopyInputTreeToOutput(
    vtkHyperTreeGridNonOrientedCursor* inCursor,
    vtkHyperTreeGridNonOrientedCursor* outCursor, vtkCellData* inCellData,
    vtkCellData* outCellData, vtkBitArray* inMask, vtkBitArray* outMask)
{
  vtkIdType outIdx = outCursor->GetGlobalNodeIndex(),
            inIdx = inCursor->GetGlobalNodeIndex();
  outCellData->InsertTuple(outIdx, inIdx, inCellData);
  if (inMask)
  {
    outMask->InsertTuple1(outIdx, inMask->GetValue(inIdx));
  }
  if (!inCursor->IsLeaf())
  {
    outCursor->SubdivideLeaf();
    for (int ichild = 0; ichild < inCursor->GetNumberOfChildren(); ++ichild)
    {
      outCursor->ToChild(ichild);
      inCursor->ToChild(ichild);
      this->CopyInputTreeToOutput(inCursor, outCursor, inCellData, outCellData,
                                  inMask, outMask);
      outCursor->ToParent();
      inCursor->ToParent();
    }
  }
}
VTK_ABI_NAMESPACE_END
