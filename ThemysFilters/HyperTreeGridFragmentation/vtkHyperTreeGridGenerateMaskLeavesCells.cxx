/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkHyperTreeGridGenerateMaskLeavesCells.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkHyperTreeGridGenerateMaskLeavesCells.h"

#include <algorithm> // for max
#include <set>
#include <vector> // for vector, vecto...

#include <vtkAOSDataArrayTemplate.h> // for vtkAOSDataArr...
#include <vtkBitArray.h>
#include <vtkCellData.h>
#include <vtkDataArray.h>     // for vtkDataArray
#include <vtkDataArrayMeta.h> // for GetAPIType
#include <vtkDataObject.h>    // for vtkDataObject
#include <vtkDoubleArray.h>
#include <vtkGenericDataArray.txx> // for vtkGenericDat...
#include <vtkHyperTreeGrid.h>
#include <vtkHyperTreeGridNonOrientedGeometryCursor.h>
#include <vtkImplicitArray.h>
#include <vtkImplicitArray.txx> // for vtkImplicitAr...
#include <vtkIndent.h>          // for vtkIndent
#include <vtkInformation.h>
#include <vtkNew.h>
#include <vtkObject.h> // for vtkObject
#include <vtkObjectFactory.h>
#include <vtkType.h> // for vtkIdType
#include <vtkUnsignedCharArray.h>

VTK_ABI_NAMESPACE_BEGIN
vtkStandardNewMacro(vtkHyperTreeGridGenerateMaskLeavesCells)

#define WITH_IMPLICIT_ARRY 1

    //--------------------------------------------------------------------------------------------------
    namespace
{
  /**
   * Comme son nom l'indique, la classe vtkScalarBooleanImplicitBackend définit
   * la partie backend, c'est-à-dire le fonctionnement spécifique et caché, d'un
   * tableau implicite, d'où le vtkImplicitArray, spécifique à la définition
   * d'un tableau implicit de sclaire boolean. L'intérêt de passer par ce
   * tableau implicit est de limiter l'allocation mémoire à un tableau vector de
   * boolean de la STL tout en répondant comme un tableau classique VTK non
   * boolean, ici des unsigned char (le gain est d'un facteur 8, sans parler
   * d'un accès accéléré par l'emploi du vector de la STL). Contre un coût
   * d'accès plus élevé, on aurait pu envisager de mettre en place un mode de
   * compression qui au final n'aurait pas apporter un gain réellement
   * significatif... par rapport au coût de chargement d'un champ de valeurs
   * double. Le mieux (ici la compression absolue) peut être l'ennemi du bien
   * (complexité de la maintenance et performance). Ici,le problème étant que
   * ParaView ne supporte pas les tableaux vtkBitArray. A ce jour, il n'a pas
   * été abordé la possibilité de sérialiser/désérialiser ce type d'objet à des
   * fins de sauvegardes que ce soit au format VTK XML ou autre. La sauvegarde
   * d'un tel tableau produit explicitement le tableau équivalent en mémoire
   * avant son écriture et comme lors de sa lecture future.
   */
  template <typename ValueType> struct vtkScalarBooleanImplicitBackend final {
    /**
     * A non-trivially contructible constructor
     *
     * @param _values: le tableau de valeurs sous sa forme booleenne
     */
    vtkScalarBooleanImplicitBackend(const std::vector<bool>& _values)
        : m_values(_values)
    {
    }

    /**
     * The main call method for the backend
     *
     * @param _index: the offset in array
     * \return la valeur boolean suivant le type souhaité
     */
    ValueType operator()(const int _index) const { return m_values[_index]; }

    /**
     * The real boolean vector stored in the backend
     */
    const std::vector<bool> m_values;
  };

  template <typename T>
  using vtkScalarBooleanArray =
      vtkImplicitArray<vtkScalarBooleanImplicitBackend<T>>;
}

//------------------------------------------------------------------------------
class vtkHyperTreeGridGenerateMaskLeavesCells::vtkInternal
{
public:
  //------------------------------------------------------------------------------------------------
  /**
   * @brief Constructeur.
   */
  vtkInternal() {}

  //------------------------------------------------------------------------------------------------
  /**
   * @brief Destructeur.
   */
  virtual ~vtkInternal() {}

  //------------------------------------------------------------------------------------------------
  /**
   * @brief Initialize.
   *
   * Cette methode dimensionne le tableau de boolean en le reinitialisant à 0.
   * D'autres informations relatives à la source sont mise à jour comme
   * le nombre de cellules filles lors d'un raffinement, l'existence d'un
   * masquage sur les cellules ou de la définition de cellules fantômes.
   *
   * @param _input: input mesh
   */
  void Initialize(vtkHyperTreeGrid* _input)
  {
    this->m_validcell_bool.clear();
    this->m_validcell_bool.resize(_input->GetNumberOfCells(), 0);

    this->m_with_discretes_values = true;
    this->m_discretes_values.clear();
    this->m_volume_double.clear();
    this->m_volume_double.resize(_input->GetNumberOfCells(), 0);

    this->m_number_of_children = _input->GetNumberOfChildren();
    this->m_mask_array = _input->HasMask() ? _input->GetMask() : nullptr;
    this->m_ghost_array = _input->GetGhostCells();
  }

  //------------------------------------------------------------------------------------------------
  /**
   * @brief GetAndFinalizeValidMaskArray.
   *
   * Cette methode construit le tableau de boolean suivant VTK décrivant si
   * une cellule est valide ou non.
   *
   * @return le tableau VTK décrivant un masque sur les cellules valides (valeur
   * True) face aux cellules non valides (valeur False, les cellules coarses,
   * masquées ou fantômes).
   */
  vtkDataArray* GetAndFinalizeValidMaskArray()
  {
#ifdef WITH_IMPLICIT_ARRY
    this->m_vtkvalidcell->ConstructBackend(this->m_validcell_bool);
#endif
    this->m_vtkvalidcell->SetName("vtkValidCell");
    this->m_vtkvalidcell->SetNumberOfComponents(1);
    this->m_vtkvalidcell->SetNumberOfTuples(this->m_validcell_bool.size());
#ifndef WITH_IMPLICIT_ARRY
    for (vtkIdType iCell = 0;
         iCell < (vtkIdType)(this->m_validcell_bool.size()); ++iCell)
    {
      this->m_vtkvalidcell->SetTuple1(iCell, this->m_validcell_bool[iCell]);
    }
#endif
    this->m_validcell_bool.clear();
    return this->m_vtkvalidcell;
  }

  //------------------------------------------------------------------------------------------------
  /**
   * @brief GetAndFinalizeVolumArray.
   *
   * Cette methode construit le tableau de double suivant VTK décrivant un champ
   * Volume.
   *
   * @return le tableau VTK décrivant un volume.
   */
  vtkDataArray* GetAndFinalizeVolumArray()
  {
#ifdef WITH_IMPLICIT_ARRY
    // généralement les valeurs prises par le volume des cellules décrit un
    // ensemble de valeurs discrétes :
    // - dans le cas uniforme, une valeur de volume par niveau ;
    // - dans le cas générale, une valeur de volume par niveau par nombre de
    // cellules par axe.
    if (!this->m_with_discretes_values)
    {
      // Implicit array by discrete double values
      // return ... ;
    }
    // Classic array by double
#endif
    this->m_volume->SetName("vtkVolume");
    this->m_volume->SetNumberOfComponents(1);
    this->m_volume->SetNumberOfTuples(this->m_volume_double.size());
    for (vtkIdType iCell = 0; iCell < (vtkIdType)(this->m_volume_double.size());
         ++iCell)
    {
      this->m_volume->SetTuple1(iCell, this->m_volume_double[iCell]);
    }
    this->m_with_discretes_values = true;
    this->m_discretes_values.clear();
    this->m_volume_double.clear();
    return this->m_volume;
  }

  //------------------------------------------------------------------------------------------------
  /**
   * @brief ComputeVolume.
   *
   * Cette methode calcule le volume de la cellule pointé par le cursor.
   *
   * @param _idC: index de la cellule
   */
  void ComputeVolume(vtkHyperTreeGridNonOrientedGeometryCursor* _cursor)
  {
    vtkIdType idC = _cursor->GetGlobalNodeIndex();
    bool checkVolume = false;
    double cellVolume{1};
    double* ptr{_cursor->GetSize()};
    for (unsigned int iDim = 0; iDim < 3; ++iDim, ++ptr)
    {
      if (*ptr != 0)
      {
        cellVolume *= *ptr;
        checkVolume = true;
      }
    }
    if (!checkVolume)
    {
      cellVolume = 0;
    }
    if (this->m_with_discretes_values)
    {
      this->m_discretes_values.insert(cellVolume);
      if (this->m_discretes_values.size() > 256)
      {
        this->m_with_discretes_values = false;
        this->m_discretes_values.clear();
      }
    }
    this->m_volume_double[idC] = cellVolume;
  }

  //------------------------------------------------------------------------------------------------
  /**
   * @brief SetValidMaskArray.
   *
   * Cette methode positionne la valeur du masque à true à l'index passé en
   * paramètre.
   *
   * @param _idC: index de la cellule
   */
  void SetValidMaskArray(const vtkIdType _idC)
  {
    this->m_validcell_bool[_idC] = true;
  }

  //------------------------------------------------------------------------------------------------
  /**
   * @brief GetNumberOfChildren.
   *
   * Cette methode permet de récupère le nombre de cellules filles (more fine)
   * lors du raffinement d'une cellule (mère, coarse).
   *
   * @return the value
   */
  unsigned int GetNumberOfChildren() const
  {
    return this->m_number_of_children;
  }

  //------------------------------------------------------------------------------------------------
  /**
   * @brief GetInputIsValidCell.
   *
   * Cette methode permet de récupère si une cellule est valide (c'est une
   * cellule feuille, non masquée et non fantome) par rapport aux informations
   * recuperees au niveau de l'input. ATTENTION La notion de cellule feuille est
   * relatif à l'application ou non du filtre vtkDepthLimiter.
   *
   * @param _idC: index de la cellule
   * @return the value
   */
  bool GetInputIsValidCell(const vtkIdType _idC) const
  {
    // This cell is valid:
    // - which means not masked;
    if (this->m_mask_array != nullptr &&
        this->m_mask_array->GetTuple1(_idC) != 0)
    {
      return false;
    }
    // - which means not ghosted.
    if (this->m_ghost_array != nullptr &&
        this->m_ghost_array->GetTuple1(_idC) != 0)
    {
      return false;
    }
    return true;
  }

private:
  // Data input
  unsigned int m_number_of_children{0};
  vtkBitArray* m_mask_array = nullptr;
  vtkUnsignedCharArray* m_ghost_array = nullptr;
  // Data intermediate
  std::vector<bool> m_validcell_bool;

  bool m_with_discretes_values{true};
  std::set<double> m_discretes_values;
  std::vector<double> m_volume_double;
  // Data output
#ifdef WITH_IMPLICIT_ARRY
  using SourceT = vtk::GetAPIType<vtkDoubleArray>;
  vtkNew<::vtkScalarBooleanArray<SourceT>> m_vtkvalidcell;
#else
  vtkNew<vtkUnsignedCharArray> m_vtkvalidcell;
#endif
  vtkNew<vtkDoubleArray> m_volume;
};

//------------------------------------------------------------------------------
vtkHyperTreeGridGenerateMaskLeavesCells::
    vtkHyperTreeGridGenerateMaskLeavesCells() = default;

//------------------------------------------------------------------------------
vtkHyperTreeGridGenerateMaskLeavesCells::
    ~vtkHyperTreeGridGenerateMaskLeavesCells() = default;

//------------------------------------------------------------------------------
void vtkHyperTreeGridGenerateMaskLeavesCells::PrintSelf(ostream& ost,
                                                        vtkIndent indent)
{
  this->Superclass::PrintSelf(ost, indent);
}

//------------------------------------------------------------------------------
int vtkHyperTreeGridGenerateMaskLeavesCells::FillOutputPortInformation(
    int, vtkInformation* info)
{
  info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkHyperTreeGrid");
  if (this->Internal == nullptr)
  {
    this->Internal = std::make_unique<vtkInternal>();
  }
  return 1;
}

//------------------------------------------------------------------------------
int vtkHyperTreeGridGenerateMaskLeavesCells::ProcessTrees(
    vtkHyperTreeGrid* input, vtkDataObject* outputDO)
{
  // Downcast output data object to hypertree grid
  vtkHyperTreeGrid* output = vtkHyperTreeGrid::SafeDownCast(outputDO);
  if (output == nullptr)
  {
    vtkErrorMacro("Incorrect type of output: " << outputDO->GetClassName());
    return 0;
  }

  output->ShallowCopy(input);

  this->Internal->Initialize(input);

  // Iterate over all input and output hyper trees
  vtkIdType index = -1; // This value imposed by clang-tidy
  vtkHyperTreeGrid::vtkHyperTreeGridIterator hypertree_iterator;
  output->InitializeTreeIterator(hypertree_iterator);
  vtkNew<vtkHyperTreeGridNonOrientedGeometryCursor> outCursor;
  while (hypertree_iterator.GetNextTree(index) != nullptr)
  {
    if (this->CheckAbort())
    {
      break;
    }
    // Initialize new cursor at root of current output tree
    output->InitializeNonOrientedGeometryCursor(outCursor, index);
    // Recursively
    this->ProcessNode(outCursor);
    // Clean up
  }

  // Build and set cell data valid mask array
  output->GetCellData()->AddArray(
      this->Internal->GetAndFinalizeValidMaskArray());
  // Build and set volum data valid mask array
  output->GetCellData()->AddArray(this->Internal->GetAndFinalizeVolumArray());

  this->UpdateProgress(1.);
  return 1;
}

//------------------------------------------------------------------------------
void vtkHyperTreeGridGenerateMaskLeavesCells::ProcessNode(
    vtkHyperTreeGridNonOrientedGeometryCursor* outCursor)
{
  vtkIdType idC = outCursor->GetGlobalNodeIndex();
  this->Internal->ComputeVolume(outCursor);
  // Is Leaf
  if (outCursor->IsLeaf())
  {
    if (this->Internal->GetInputIsValidCell(idC))
    {
      this->Internal->SetValidMaskArray(idC);
    }
    return;
  }
  // Is Coarse
  for (unsigned int iChild = 0; iChild < this->Internal->GetNumberOfChildren();
       ++iChild)
  {
    if (this->CheckAbort())
    {
      break;
    }
    outCursor->ToChild(iChild);
    // We go through the children's cells
    this->ProcessNode(outCursor);
    outCursor->ToParent();
  }
}

VTK_ABI_NAMESPACE_END
