/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkHyperTreeGridFragmentation.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

// #include <format> C++ 20 (see BeforeC++20)
// IWYU wants to include algorithm for max but there is no max use here
// IWYU pragma: no_include <algorithm>
#include <array>     // for array
#include <cassert>   // for assert
#include <cstdio>    // for snprintf
#include <limits>    // for numeri...
#include <map>       // for map
#include <memory>    // for allocator
#include <ostream>   // for ostream
#include <stdexcept> // for runtim...
#include <string>    // for string
#include <utility>   // for pair
#include <vector>    // for vector

#include <vtkBitArray.h>                                      // for vtkBit...
#include <vtkCellArray.h>                                     // for vtkCel...
#include <vtkCellData.h>                                      // for vtkCel...
#include <vtkCommunicator.h>                                  // for vtkCom...
#include <vtkCompositeDataSet.h>                              // for vtkCom...
#include <vtkConstantArray.h>                                 // for vtkCon...
#include <vtkDataArray.h>                                     // for vtkDat...
#include <vtkDataArrayMeta.h>                                 // for GetAPI...
#include <vtkDataObject.h>                                    // for vtkDat...
#include <vtkDoubleArray.h>                                   // for vtkDou...
#include <vtkFieldData.h>                                     // for vtkFie...
#include <vtkGenericDataArray.txx>                            // for vtkGen...
#include <vtkHyperTreeGrid.h>                                 // for vtkHyp...
#include <vtkHyperTreeGridFragmentation.h>                    // for vtkHyp...
#include <vtkHyperTreeGridFragmentationInternal.h>            // for vtkHyp...
#include <vtkHyperTreeGridNonOrientedCursor.h>                // for vtkHyp...
#include <vtkHyperTreeGridNonOrientedGeometryCursor.h>        // for vtkHyp...
#include <vtkHyperTreeGridNonOrientedVonNeumannSuperCursor.h> // for vtkHyp...
#include <vtkHyperTreeGridOrientedGeometryCursor.h>           // for vtkHyp...
#include <vtkImplicitArray.h>                                 // for vtkImp...
#include <vtkImplicitArray.txx>                               // for vtkImp...
#include <vtkIndent.h>                                        // for vtkIndent
#include <vtkInformation.h>                                   // for vtkInf...
#include <vtkInformationVector.h>                             // for vtkInf...
#include <vtkMultiBlockDataSet.h>                             // for vtkMul...
#include <vtkMultiPieceDataSet.h>                             // for vtkMul...
#include <vtkMultiProcessController.h>                        // for vtkMul...
#include <vtkNew.h>                                           // for vtkNew
#include <vtkObjectFactory.h>                                 // for vtkSta...
#include <vtkPointData.h>                                     // for vtkPoi...
#include <vtkPoints.h>                                        // for vtkPoints
#include <vtkPolyData.h>                                      // for vtkPol...
#include <vtkSmartPointer.h>                                  // for vtkSma...
#include <vtkStreamingDemandDrivenPipeline.h>                 // for vtkStr...
#include <vtkType.h>                                          // for vtkIdType
#include <vtkUnsignedCharArray.h>                             // for vtkUns...
#include <vtkUnsignedIntArray.h>                              // for vtkUns...
#include <vtkUnsignedLongArray.h>                             // for vtkUns...

#include "vtkCEAHyperTreeGridGhostCellsGenerator.h" // for vtkCEA...

//-------------------------------------------------------------------------------------------------
// BeforeC++20 (see C++20)
// https://stackoverflow.com/questions/2342162/stdstring-formatting-like-sprintf

namespace {
template <typename... Args>
std::string string_format(const std::string& format, Args... args)
{
  int size_s = std::snprintf(nullptr, 0, format.c_str(), args...) +
               1; // Extra space for '\0'
  if (size_s <= 0)
  {
    throw std::runtime_error("Error during formatting.");
  }
  auto size = static_cast<size_t>(size_s);
  std::unique_ptr<char[]> buf(new char[size]);
  std::snprintf(buf.get(), size, format.c_str(), args...);
  return std::string(buf.get(),
                     buf.get() + size - 1); // We don't want the '\0' inside
}
} // namespace

//-------------------------------------------------------------------------------------------------
vtkStandardNewMacro(vtkHyperTreeGridFragmentation)

    //--------------------------------------------------------------------------------------------------
    // Destructeur
    vtkHyperTreeGridFragmentation::~vtkHyperTreeGridFragmentation()
{
  delete[] this->MassName;
  this->MassName = nullptr;
  delete[] this->DensityName;
  this->DensityName = nullptr;
  delete[] this->VelocityName;
  this->VelocityName = nullptr;

  this->Internal = nullptr;
}

//--------------------------------------------------------------------------------------------------
namespace {
/**
 * Comme son nom l'indique, la classe vtkVectorConstantImplicitBackend définit
 * la partie backend, c'est-à-dire le fonctionnement spécifique et caché, d'un
 * tableau implicite, d'où le vtkImplicitArray, spécifique à la définition d'un
 * tableau implicit de vecteurs qui ont tous la même valeur, d'où le constant.
 * L'intérêt de passer par ce tableau implicit est de limiter l'allocation
 * mémoire au juste nécessaire tout en répondant comme un tableau classique VTK.
 * A ce jour, il n'a pas été abordé la possibilité de sérialiser/désérialiser ce
 * type d'objet à des fins de sauvegardes que ce soit au format VTK XML ou
 * autre. La sauvegarde d'un tel tableau produit explicitement le tableau
 * équivalent en mémoire avant son écriture et comme lors de sa lecture future.
 */
template <typename ValueType> struct vtkVectorConstantImplicitBackend final {
  /**
   * A non-trivially contructible constructor
   *
   * @param _values: the constant vector to return for all indexes
   */
  vtkVectorConstantImplicitBackend(const std::array<ValueType, 3>& _values)
      : Values(_values)
  {
  }

  /**
   * The main call method for the backend
   *
   * @param _index: the offset in array
   * \return one of the components of the constant vector
   */
  ValueType operator()(const int _index) const { return Values[_index % 3]; }

  /**
   * The constant vector stored in the backend
   */
  const std::array<ValueType, 3> Values;
};

template <typename T>
using vtkVectorConstantArray =
    vtkImplicitArray<vtkVectorConstantImplicitBackend<T>>;

//------------------------------------------------------------------------------------------------
template <class VtkArrayTypeT>
void SetConstantScalar(const std::string& _name, vtkPolyData* _polydata,
                       const unsigned int _nbPts,
                       const typename VtkArrayTypeT::ValueType _value)
{
  /* La version du code en stockant sur FieldData, coût mémoire 1 :
      vtkNew<VtkArrayTypeT> data;
      data->SetName(_name.c_str());
      data->SetNumberOfComponents(1);
      data->InsertNextValue(_value);
      _polydata->GetFieldData()->AddArray(data);
    }
  */

  /* La version du code avec un tableau explicit, coût mémoire _nbPts :
      vtkNew<VtkArrayTypeT> data;
      data->SetName(_name.c_str());
      data->SetNumberOfComponents(1);
      for(unsigned int iPt = 0; iPt < _nbPts; ++iPt)
      {
        data->InsertNextValue(_value);
      }
      _polydata->GetPointData()->AddArray(data);
  */

  /* La version du code avec un tableau implicit, coût mémoire 1, coût stockage
   * _nbPts :
   */
  using SourceT = vtk::GetAPIType<VtkArrayTypeT>;
  vtkNew<vtkConstantArray<SourceT>> data;
  data->ConstructBackend(_value);
  data->SetName(_name.c_str());
  data->SetNumberOfComponents(1);
  data->SetNumberOfTuples(_nbPts);
  _polydata->GetPointData()->AddArray(data);
}

//------------------------------------------------------------------------------------------------
template <class VtkArrayTypeT>
void SetConstantVector(
    const std::string& _name, vtkPolyData* _polydata, const unsigned int _nbPts,
    const std::array<typename VtkArrayTypeT::ValueType, 3>& _values)
{
  /* La version du code en stockant sur FieldData, coût mémoire 1 :
      vtkNew<VtkArrayTypeT> data;
      data->SetName(_name.c_str());
      data->SetNumberOfComponents(3);
      data->InsertNextTuple(_values.data());
      _polydata->GetFieldData()->AddArray(data);
    }
  */

  /* La version du code avec un tableau explicit, coût mémoire _nbPts :
      vtkNew<VtkArrayTypeT> data;
      data->SetName(_name.c_str());
      data->SetNumberOfComponents(3);
      for(unsigned int iPt = 0; iPt < _nbPts; ++iPt)
      {
        data->InsertNextTuple(_values.data());
      }
      _polydata->GetPointData()->AddArray(data);
  */

  /* La version du code avec un tableau implicit, coût mémoire 1, coût stockage
   * _nbPts :
   */
  using SourceT = vtk::GetAPIType<VtkArrayTypeT>;
  vtkNew<vtkVectorConstantArray<SourceT>> data;
  data->ConstructBackend(_values);
  data->SetName(_name.c_str());
  data->SetNumberOfComponents(3);
  data->SetNumberOfTuples(_nbPts);
  _polydata->GetPointData()->AddArray(data);
}

//------------------------------------------------------------------------------------------------
// Sauvegarde un champ scalaire unsigned int, unsigned long ou double global a
// la simulation
template <class VtkArrayTypeT>
void SetScalarFieldData(const std::string& _name, vtkPolyData* _polydata,
                        typename VtkArrayTypeT::ValueType _value)
{
  vtkNew<VtkArrayTypeT> data;
  data->SetName(_name.c_str());
  data->SetNumberOfComponents(1);
  data->InsertNextValue(_value);
  _polydata->GetFieldData()->AddArray(data);
}

//------------------------------------------------------------------------------------------------
// Sauvegarde un champ scalaire unsigned int
// pour chaque point representant un fragment/region de la simulation
void SetUnsignedIntKeyPointData(
    const std::map<unsigned int, bool>& _local, const std::string& _name,
    vtkPolyData* _polydata,
    const std::map<unsigned int, unsigned long>& _values // _pounds
)
{
  vtkNew<vtkUnsignedIntArray> data;
  data->SetName(_name.c_str());
  data->SetNumberOfComponents(1);
  for (const auto& entry : _values)
  {
    if (!_local.at(entry.first))
    {
      continue;
    }
    data->InsertNextValue(entry.first);
  }
  _polydata->GetPointData()->AddArray(data);
}

//------------------------------------------------------------------------------------------------
// Sauvegarde un champ scalaire unsigned long ou double
// pour chaque point representant un fragment/region de la simulation
template <class VtkArrayTypeT>
void SetScalarPointData(
    const std::map<unsigned int, bool>& _local, const std::string& _name,
    vtkPolyData* _polydata,
    const std::map<unsigned int, typename VtkArrayTypeT::ValueType>& _values)
{
  vtkNew<VtkArrayTypeT> data;
  data->SetName(_name.c_str());
  data->SetNumberOfComponents(1);
  for (const auto& entry : _values)
  {
    if (!_local.at(entry.first))
    {
      continue;
    }
    data->InsertNextValue(entry.second);
  }
  _polydata->GetPointData()->AddArray(data);
}

//------------------------------------------------------------------------------------------------
// Sauvegarde un champ vectoriel double pour chaque point representant un
// fragment/region de la simulation, par indexation sur un champ disponible au
// niveau de la simulation
void SetDoubleVectorPointData(
    const std::map<unsigned int, bool>& _local, const std::string& _name,
    vtkPolyData* _polydata,
    const std::map<unsigned int, std::array<double, 3>>& _values)
{
  vtkNew<vtkDoubleArray> data;
  data->SetName(_name.c_str());
  data->SetNumberOfComponents(3);
  for (const auto& entry : _values)
  {
    if (!_local.at(entry.first))
    {
      continue;
    }
    data->InsertNextTuple(entry.second.data());
  }
  _polydata->GetPointData()->AddArray(data);
}
} // namespace

//--------------------------------------------------------------------------------------------------
int vtkHyperTreeGridFragmentation::RequestUpdateExtent(
    vtkInformation* vtkNotUsed(rqst), vtkInformationVector** inputVector,
    vtkInformationVector* vtkNotUsed(outputVector))
{
  if (this->Internal == nullptr)
  {
    this->Internal = std::make_unique<vtkInternal>(
        vtkMultiProcessController::GetGlobalController());
    if (this->Internal->HasController())
    {
      // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
      vtkInformation* info = inputVector[0]->GetInformationObject(0);
      info->Set(vtkStreamingDemandDrivenPipeline::UPDATE_PIECE_NUMBER(),
                this->Internal->GetMyRank());
      info->Set(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_PIECES(),
                this->Internal->GetNumberOfProcesses());
    }
  }

  return 1;
}

//--------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::ComputeBlockCenters(
    vtkMultiBlockDataSet* _output)
{
  auto IsLocalFragment = [](const auto& _number_of_processes,
                            const auto& _local, const auto& _entry) {
    if (_number_of_processes > 1)
    {
      assert(_local.find(_entry.first) != _local.end());
    } else
    {
      assert(_local.at(_entry.first));
    }
    if (!_local.at(_entry.first))
    {
      return false;
    }
    return true;
  };

  const int myrank = this->Internal->GetMyRank();
  const int number_of_processes = this->Internal->GetNumberOfProcesses();
  const std::map<unsigned int, bool>& local = this->Internal->GetOutputLocal();
  // Determine le nombre de fragments qui sont geres par le server local
  unsigned int nbLocalFrags = 0;
  for (const auto& entry : this->Internal->GetOutputPounds())
  {
    if (!IsLocalFragment(number_of_processes, local, entry))
    {
      continue;
    }
    ++nbLocalFrags;
  }
  assert(number_of_processes > 1 ||
         nbLocalFrags == this->Internal->GetOutputPounds().size());

  vtkNew<vtkPoints> pts_ctr;
  pts_ctr->SetNumberOfPoints(static_cast<vtkIdType>(nbLocalFrags));

  unsigned int iFrag = 0;
  if (this->Internal->HasInputMass())
  {
    for (const auto& entry : this->Internal->GetOutputBarycenter())
    {
      if (!IsLocalFragment(number_of_processes, local, entry))
      {
        continue;
      }
      pts_ctr->SetPoint(iFrag, (double*)(entry.second.data()));
      ++iFrag;
    }
  } else
  {
    for (const auto& entry : this->Internal->GetOutputAvgCenter())
    {
      if (!IsLocalFragment(number_of_processes, local, entry))
      {
        continue;
      }
      pts_ctr->SetPoint(iFrag, entry.second.data());
      ++iFrag;
    }
  }
  assert(nbLocalFrags == iFrag);

  vtkNew<vtkPolyData> centers;
  centers->SetPoints(pts_ctr);

  if (number_of_processes > 1)
  {
    vtkNew<vtkMultiPieceDataSet> mpds;
    mpds->SetNumberOfPieces(number_of_processes);
    mpds->SetPiece(myrank, centers);
    std::string name = "Centers_" + std::to_string(myrank);
    mpds->GetMetaData(myrank)->Set(vtkCompositeDataSet::NAME(), name.c_str());

    int iBlock{0};
    _output->SetBlock(iBlock, mpds);
  } else
  {
    int iBlock{0};
    _output->SetBlock(iBlock, centers);
  }

  vtkNew<vtkCellArray> vertices_ctr;
  vertices_ctr->AllocateEstimate(nbLocalFrags, 1);
  for (unsigned int iFrag = 0; iFrag < nbLocalFrags; ++iFrag)
  {
    vtkIdType index{iFrag};
    vertices_ctr->InsertNextCell(1, &index);
  }
  centers->SetVerts(vertices_ctr);

  ::SetUnsignedIntKeyPointData(local, "FragmentId", centers,
                               this->Internal->GetOutputPounds());
  ::SetScalarPointData<vtkUnsignedLongArray>(local, "Pounds", centers,
                                             this->Internal->GetOutputPounds());
  ::SetScalarPointData<vtkDoubleArray>(local, "Volume", centers,
                                       this->Internal->GetOutputVolume());

  std::map<unsigned int, double> radius;
  this->Internal->ComputeFormFactorRadius(radius);
  ::SetScalarPointData<vtkDoubleArray>(local, "FormFactorRadius", centers,
                                       radius);

  ::SetDoubleVectorPointData(local, "AvgCenter", centers,
                             this->Internal->GetOutputAvgCenter());
  if (this->Internal->HasInputMass())
  {
    ::SetDoubleVectorPointData(local, "Barycenter", centers,
                               this->Internal->GetOutputBarycenter());
    ::SetScalarPointData<vtkDoubleArray>(local, "Mass", centers,
                                         this->Internal->GetOutputMass());
    std::map<unsigned int, double> density;
    this->Internal->ComputeDensity(density);
    ::SetScalarPointData<vtkDoubleArray>(local, "Density", centers, density);
  }

  if (this->Internal->HasInputVelocity())
  {
    ::SetDoubleVectorPointData(local, "AvgVelocity", centers,
                               this->Internal->GetOutputAvgVelocity());
    if (this->Internal->HasInputMass())
    {
      ::SetDoubleVectorPointData(local, "Velocity", centers,
                                 this->Internal->GetOutputVelocity());
    }
  }
}

//--------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::ComputeBlocksFragments(
    const unsigned int _nbRealGlobalFrags, vtkMultiBlockDataSet* _output)
{
  const int myrank = this->Internal->GetMyRank();
  const int number_of_processes = this->Internal->GetNumberOfProcesses();

  const std::map<unsigned int, unsigned long>& pounds =
      this->Internal->GetOutputPounds();
  if (number_of_processes > 1)
  {
    for (unsigned int iFrag = 0; iFrag < _nbRealGlobalFrags; ++iFrag)
    {
      if (pounds.find(iFrag) == pounds.end())
      {
        vtkNew<vtkMultiPieceDataSet> mpds;
        mpds->SetNumberOfPieces(number_of_processes);
        // std::string name = std::format("Fragment_{}", iFrag); C++20 (see
        // BeforeC++20)
        std::string name_fragment =
            string_format("Fragment_%d", iFrag); // BeforeC++20 (see C++20)
        std::string name = name_fragment + "_" + std::to_string(myrank);
        mpds->GetMetaData(myrank)->Set(vtkCompositeDataSet::NAME(),
                                       name.c_str());
        _output->SetBlock(iFrag + 1, mpds);
      }
    }
  }

  // On se base sur Pounds car il est toujours defini
  for (const auto& entry : pounds)
  {
    unsigned int iFrag = entry.first;
    assert(iFrag < _nbRealGlobalFrags);

    vtkNew<vtkPolyData> polydata;
    vtkPoints* points = this->Internal->GetOutputPoints(iFrag);
    polydata->SetPoints(points);
    vtkIdType nbpts{0};
    if (points != nullptr)
    {
      nbpts = points->GetNumberOfPoints();
    }
    vtkNew<vtkCellArray> vertices;
    vertices->AllocateEstimate(nbpts, 1);
    for (vtkIdType ipt = 0; ipt < nbpts; ++ipt)
    {
      vertices->InsertNextCell(1, &ipt);
    } // ipt
    polydata->SetVerts(vertices);

    if (number_of_processes > 1)
    {
      vtkNew<vtkMultiPieceDataSet> mpds;
      mpds->SetNumberOfPieces(number_of_processes);
      mpds->SetPiece(myrank, polydata);
      // std::string name = std::format("Fragment_{}", iFrag); C++20 (see
      // BeforeC++20)
      std::string name_fragment =
          string_format("Fragment_%d", iFrag); // BeforeC++20 (see C++20)
      std::string name = name_fragment + "_" + std::to_string(myrank);
      mpds->GetMetaData(myrank)->Set(vtkCompositeDataSet::NAME(), name.c_str());
      _output->SetBlock(iFrag + 1, mpds);
    } else
    {
      _output->SetBlock(iFrag + 1, polydata);
    }

    SetConstantScalar<vtkUnsignedIntArray>("FragmentId", polydata, nbpts,
                                           iFrag);

    if (this->EnableOuputGlobalFields)
    {
      SetConstantScalar<vtkUnsignedLongArray>(
          "Pounds", polydata, nbpts,
          this->Internal->GetOutputPounds().at(iFrag));
      SetConstantScalar<vtkDoubleArray>(
          "Volume", polydata, nbpts,
          this->Internal->GetOutputVolume().at(iFrag));
      SetConstantScalar<vtkDoubleArray>(
          "FormFactorRadius", polydata, nbpts,
          this->Internal->ComputeOneFormFactorRadius(iFrag));
      SetConstantVector<vtkDoubleArray>(
          "AvgCenter", polydata, nbpts,
          this->Internal->GetOutputAvgCenter().at(iFrag));
      if (this->Internal->HasInputMass())
      {
        SetConstantScalar<vtkDoubleArray>(
            "Mass", polydata, nbpts, this->Internal->GetOutputMass().at(iFrag));
        SetConstantScalar<vtkDoubleArray>(
            "Density", polydata, nbpts,
            this->Internal->ComputeOneDensity(iFrag));

        SetConstantVector<vtkDoubleArray>(
            "Barycenter", polydata, nbpts,
            this->Internal->GetOutputBarycenter().at(iFrag));
      }
      if (this->Internal->HasInputVelocity())
      {
        SetConstantVector<vtkDoubleArray>(
            "AvgVelocity", polydata, nbpts,
            this->Internal->GetOutputAvgVelocity().at(iFrag));
        if (this->Internal->HasInputMass())
        {
          SetConstantVector<vtkDoubleArray>(
              "Velocity", polydata, nbpts,
              this->Internal->GetOutputVelocity().at(iFrag));
        }
      }
    }
  }
  this->Internal->FinalizeOutputPoints();
}

//--------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::RecursivelyProcessGhostTree(
    const vtkBitArray* _masked, const vtkUnsignedCharArray* _ghosted,
    const vtkUnsignedIntArray* _fragIdGlobal,
    vtkHyperTreeGridNonOrientedVonNeumannSuperCursor* _supercursor,
    std::map<unsigned int, unsigned int>& _sameFrag)
{
  // Index central
  vtkIdType idC = _supercursor->GetGlobalNodeIndex();
  // Case is ghosted
  if (_ghosted->GetValue(idC) == 0)
  {
    return;
  }
  // Case is masked
  if (_masked != nullptr && _masked->GetValue(idC) == 1)
  {
    return;
  }
  // Case is coarse
  if (!_supercursor->IsLeaf())
  {
    // Mere : on parcourt les filles
    for (unsigned int child = 0; child < this->Internal->GetNumberOfChildren();
         ++child)
    {
      _supercursor->ToChild(child);
      this->RecursivelyProcessGhostTree(_masked, _ghosted, _fragIdGlobal,
                                        _supercursor, _sameFrag);
      _supercursor->ToParent();
    }
    return;
  }

  // Case is leaf, on recupere l'indice du fragment auquel la cellule est
  // associee
  unsigned int fragId = _fragIdGlobal->GetValue(idC);

  assert(fragId != std::numeric_limits<unsigned int>::max());

  for (unsigned int idim = 0; idim < this->Internal->GetNumberOfDimension();
       ++idim) // dimension
  {
    for (unsigned int orientation = 0; orientation < 2;
         ++orientation) // gauche, centre, droite
    {
      unsigned int neighborIdx = this->Internal->GetNumberOfDimension() +
                                 (2 * orientation - 1) * (idim + 1);
      bool isValidN = _supercursor->HasTree(neighborIdx);
      // index d'une cellule voisine
      vtkIdType idN = 0;
      if (isValidN)
      {
        idN = _supercursor->GetGlobalNodeIndex(neighborIdx);
      }
      if (isValidN && _masked != nullptr && _masked->GetValue(idN) == 0)
      {
        vtkSmartPointer<vtkHyperTreeGridOrientedGeometryCursor> voisin =
            _supercursor->GetOrientedGeometryCursor(neighborIdx);
        if (voisin->IsLeaf())
        {
          // La voisine est presente
          unsigned int fragIdN = _fragIdGlobal->GetValue(idN);
          assert(fragIdN != std::numeric_limits<unsigned int>::max());
          if (fragId != fragIdN)
          {
            _sameFrag[std::max(fragId, fragIdN)] = std::min(fragId, fragIdN);
          }
        }
      }
    }
  }
}

//--------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::ProcessAllGhostTree(
    vtkHyperTreeGrid* _input, std::map<unsigned int, unsigned int>& _sameFrag)
{
  vtkBitArray* masked = _input->HasMask() ? _input->GetMask() : nullptr;

  vtkUnsignedCharArray* ghosted = _input->GetGhostCells();
  // Par implementation, cela ne peut pas arriver
  assert(ghosted != nullptr);

  // Existe-t-il une cellule ghost ?
  bool noGhostCells = true;
  for (vtkIdType iCell = 0; iCell < _input->GetNumberOfCells(); ++iCell)
  {
    if (ghosted->GetValue(iCell) != 0)
    {
      noGhostCells = false;
      break;
    }
  }
  if (noGhostCells)
  {
    return;
  }

  int iArray = -1; // Cette initialisation n'a pas de sens mais elle est imposee
                   // par clang-tidy
  vtkUnsignedIntArray* fragIdGlobal = vtkUnsignedIntArray::SafeDownCast(
      _input->GetCellData()->GetArray("FragIdGlobal", iArray));
  // Par implementation, cela ne peut pas arriver
  assert(fragIdGlobal != nullptr);

  // Parcours de tous les HyperTree afin d'identifier les fragments et de
  // construire les champs associes. Concernant les champs globaux, seule la
  // premiere phase est realisée sous l'aspect d'une accumulation.
  vtkIdType index = 0;
  vtkHyperTreeGrid::vtkHyperTreeGridIterator hypertree_iterator;
  _input->InitializeTreeIterator(hypertree_iterator);
  vtkNew<vtkHyperTreeGridNonOrientedVonNeumannSuperCursor> supercursor;
  while (hypertree_iterator.GetNextTree(index) != nullptr)
  {
    _input->InitializeNonOrientedVonNeumannSuperCursor(supercursor, index);
    this->RecursivelyProcessGhostTree(masked, ghosted, fragIdGlobal,
                                      supercursor, _sameFrag);
  }
  // Ce traitement permet de construire sur chacune des cellules le champ
  // FragIds indiquant à quel fragment/région la cellule est rattachée ainsi que
  // le champ Bords qui indiquent si cette cellule a au moins un voisin
  // manquant.
}

//--------------------------------------------------------------------------------------------------
namespace {
//------------------------------------------------------------------------------------------------
// Localement a un serveur, permet de regrouper trois fragments A - B - C
// sous le meme numero de fragment
unsigned int
recursiveFrag(std::map<unsigned int, unsigned int>& _sameFrag, // [max] = min
              unsigned int _max, unsigned int _min)
{
  assert(_min <= _max);
  if (_sameFrag.find(_min) != _sameFrag.end())
  {
    unsigned int nmin = recursiveFrag(_sameFrag, _min, _sameFrag[_min]);
    _sameFrag[_max] = nmin;
    return nmin;
  }
  return _min;
}

//------------------------------------------------------------------------------------------------
// Information regroupee, equivalent a la methode precedente
unsigned int recursiveFrag(std::vector<unsigned int>& _sameFrag, // [max] = min
                           unsigned int _max, unsigned int _min)
{
  assert(_min <= _max);
  assert(_min < _sameFrag.size());
  assert(_max < _sameFrag.size());
  if (_sameFrag[_min] != _min)
  {
    unsigned int nmin = recursiveFrag(_sameFrag, _min, _sameFrag[_min]);
    _sameFrag[_max] = nmin;
    return nmin;
  }
  return _min;
}

#ifndef NDEBUG
//------------------------------------------------------------------------------------------------
// Cette methode valide que toutes les cellules feuilles non masquees et
// non ghost ont bien été attribuées à un fragment depuis un curseur
bool validRecursivelyProcess(vtkHyperTreeGrid* _input,
                             vtkHyperTreeGridNonOrientedCursor* _cursor)
{
  vtkIdType idC{_cursor->GetGlobalNodeIndex()};
  if (_input->GetMask()->GetValue(idC) != 0 ||
      (_input->GetGhostCells() != nullptr &&
       _input->GetGhostCells()->GetValue(idC) != 0))
  {
    return true;
  }
  if (_cursor->IsLeaf())
  {
    if (dynamic_cast<vtkUnsignedIntArray*>(
            _input->GetCellData()->GetArray("FragIdGlobal"))
            ->GetValue(idC) == std::numeric_limits<unsigned int>::max())
    {
      assert(
          !(dynamic_cast<vtkUnsignedIntArray*>(
                _input->GetCellData()->GetArray("FragIdGlobal"))
                ->GetValue(idC) == std::numeric_limits<unsigned int>::max()));
    }
    return true;
  }
  for (unsigned int iChild = 0; iChild < _cursor->GetNumberOfChildren();
       ++iChild)
  {
    _cursor->ToChild(iChild);
    if (!validRecursivelyProcess(_input, _cursor))
    {
      return false;
    }
    _cursor->ToParent();
  }
  return true;
}

//------------------------------------------------------------------------------------------------
// Cette methode valide que toutes les cellules feuilles non masquees et
// non ghost ont bien été attribuées à un fragment en parcourant tous les
// HyperTrees
bool validProcess(vtkHyperTreeGrid* _input)
{
  vtkIdType index{0};
  vtkHyperTreeGrid::vtkHyperTreeGridIterator hypertree_iterator;
  _input->InitializeTreeIterator(hypertree_iterator);
  vtkNew<vtkHyperTreeGridNonOrientedCursor> cursor;
  while (hypertree_iterator.GetNextTree(index) != nullptr)
  {
    _input->InitializeNonOrientedCursor(cursor, index);
    if (!validRecursivelyProcess(_input, cursor))
    {
      return false;
    }
  }
  return true;
}
#endif
} // namespace

//-------------------------------------------------------------------------------------------
// Fonction principale qui va parcourir tous les arbres de decomposition de
// l'HTG
unsigned int vtkHyperTreeGridFragmentation::DistributedProcessTrees(
    vtkHyperTreeGrid* _input, unsigned int nbLocalFrags)
{
  // Detection du mode distributed
  if (this->Internal->GetNumberOfProcesses() == 1)
  {
    return this->Internal->GetOutputPounds().size();
  }

  // Determination du shift a appliquer sur le numero des fragments pour le
  // serveur courant ainsi que du nombre total de fragments
  std::vector<unsigned int> recvNbFragments(
      this->Internal->GetNumberOfProcesses(), 0);
  this->Internal->GetController()->AllGather(&nbLocalFrags,
                                             recvNbFragments.data(), 1);
  unsigned int shift = 0;
  for (int crtRank = 0; crtRank < this->Internal->GetMyRank(); ++crtRank)
  {
    shift += recvNbFragments[crtRank];
  }
  unsigned int nbAllFrags = shift;
  for (int crtRank = this->Internal->GetMyRank();
       crtRank < this->Internal->GetNumberOfProcesses(); ++crtRank)
  {
    nbAllFrags += recvNbFragments[crtRank];
  }

  // construit un champ de valeurs FragIdGlobal en attribuant le numero du
  // fragment augmente du shift
  vtkNew<vtkUnsignedIntArray> FragIdGlobal;
  FragIdGlobal->SetName("FragIdGlobal");
  FragIdGlobal->SetNumberOfComponents(1);
  FragIdGlobal->SetNumberOfTuples(_input->GetNumberOfCells());
  for (vtkIdType iCell = 0; iCell < _input->GetNumberOfCells(); ++iCell)
  {
    unsigned int iFrag = this->Internal->GetOutputFragByCells()[iCell];
    if (iFrag != std::numeric_limits<unsigned int>::max())
    {
      iFrag += shift;
    }
    FragIdGlobal->SetTuple1(iCell, iFrag);
  }

  // Copy du maillage de l'input qu'avec le champ CellData
  vtkNew<vtkHyperTreeGrid> htgIntermediate;
  htgIntermediate->ShallowCopy(_input);
  // TODO Une optimisation est de ne pas copier tous les tableaux
  htgIntermediate->GetCellData()->AddArray(FragIdGlobal);

  // Par l'implementation, cela ne devrait pas arriver
  assert(validProcess(htgIntermediate));

  vtkSmartPointer<vtkCEAHyperTreeGridGhostCellsGenerator> htgGcg =
      vtkCEAHyperTreeGridGhostCellsGenerator::New();
  htgGcg->SetInputData(htgIntermediate);
  htgGcg->Update();
  vtkHyperTreeGrid* htg = vtkHyperTreeGrid::SafeDownCast(htgGcg->GetOutput());

  // sameFrag[key] = value
  // key alias max > value alias min par construction
  std::map<unsigned int, unsigned int> sameFrag;
  this->ProcessAllGhostTree(htg, sameFrag);

  for (auto& entry : sameFrag)
  {
    entry.second = ::recursiveFrag(sameFrag, entry.first, entry.second);
  }

  this->Internal->ShiftFragId(shift);
  this->Internal->ComputeLocalFragId(shift, nbLocalFrags, sameFrag);

  std::vector<unsigned int> recvfrag2frag(
      nbAllFrags, std::numeric_limits<unsigned int>::max());
  std::vector<unsigned int> frag2frag(nbAllFrags,
                                      std::numeric_limits<unsigned int>::max());
  for (const auto& entry : sameFrag)
  {
    frag2frag[entry.first] = entry.second;
  }
  this->Internal->GetController()->AllReduce(frag2frag.data(),
                                             recvfrag2frag.data(), nbAllFrags,
                                             vtkCommunicator::MIN_OP);

  unsigned int nRealAllFrag = 0;
  for (unsigned int iFrag = 0; iFrag < nbAllFrags; ++iFrag)
  {
    if (recvfrag2frag[iFrag] == iFrag)
    {
      recvfrag2frag[iFrag] = nRealAllFrag++;
    } else
    {
      recvfrag2frag[iFrag] =
          ::recursiveFrag(recvfrag2frag, iFrag, recvfrag2frag[iFrag]);
    }
  }

  this->Internal->ShiftFragId(recvfrag2frag);
  this->Internal->AllReduceAllGlobalField(nbAllFrags);

  return nRealAllFrag;
}

//-------------------------------------------------------------------------------------------
// Fonction principale qui va parcourir tous les arbres de decomposition de
// l'HTG
int vtkHyperTreeGridFragmentation::ProcessTrees(vtkHyperTreeGrid* input,
                                                vtkDataObject* outputDO)
{
  vtkMultiBlockDataSet* output = vtkMultiBlockDataSet::SafeDownCast(outputDO);
  if (output == nullptr)
  {
    vtkErrorMacro("Incorrect type of output: " << outputDO->GetClassName());
    return 0;
  }

  this->Internal->Initialize(input);
  this->Internal->RetrieveMassInput(input, this->EnableMassName,
                                    this->MassName);
  this->Internal->RetrieveDensityInput(input, this->EnableDensityName,
                                       this->DensityName);
  this->Internal->RetrieveVelocityInput(input, this->EnableVelocityName,
                                        this->VelocityName);

  // Parcours de tous les HyperTree afin d'identifier les fragments et de
  // construire les champs associes. Concernant les champs globaux, seule la
  // premiere phase est realisée sous l'aspect d'une accumulation.
  this->ProcessAllTree(input);
  // Ce traitement permet de construire sur chacune des cellules le champ
  // FragIds indiquant à quel fragment/région la cellule est rattachée ainsi que
  // le champ m_is_skin_celldata qui indiquent si cette cellule a au moins un
  // voisin manquant.

  // Compaction de la numerotation discontinue des fragments pour la rendre
  // continue
  unsigned int nbLocalFrags = this->Internal->CompactFragId();

  // Detection du mode distribue
  unsigned int nbRealGlobalFrags =
      this->DistributedProcessTrees(input, nbLocalFrags);

  this->Internal->ComputeAverageField();

  // Parcours recursif pour recuperer les centres des cellules participant pour
  // chacun des fragments/regions
  this->GeneratePoints(input);

  // 0 : Centers
  // # : Fragment_{#+1}
  output->SetNumberOfBlocks(nbRealGlobalFrags + 1);
  int iBlock{0};
  output->GetMetaData(iBlock)->Set(vtkCompositeDataSet::NAME(), "Centers");
  for (unsigned int iFrag = 0; iFrag < nbRealGlobalFrags; ++iFrag)
  {
    // std::string name = std::format("Fragment_{}", iFrag); C++20 (see
    // BeforeC++20)
    std::string name_fragment =
        string_format("Fragment_%d", iFrag); // BeforeC++20 (see C++20)
    output->GetMetaData(iFrag + 1)->Set(vtkCompositeDataSet::NAME(),
                                        name_fragment.c_str());
  }
  this->ComputeBlockCenters(output);
  this->ComputeBlocksFragments(nbRealGlobalFrags, output);
  this->Internal->Finalize();
  return 1;
}

//--------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::ProcessAllTree(vtkHyperTreeGrid* _input)
{
  // Parcours de tous les HyperTree afin d'identifier les fragments et de
  // construire les champs associes. Concernant les champs globaux, seule la
  // premiere phase est realisée sous l'aspect d'une accumulation.
  vtkIdType index{0};
  vtkHyperTreeGrid::vtkHyperTreeGridIterator hypertree_iterator;
  _input->InitializeTreeIterator(hypertree_iterator);
  vtkNew<vtkHyperTreeGridNonOrientedVonNeumannSuperCursor> supercursor;
  while (hypertree_iterator.GetNextTree(index) != nullptr)
  {
    _input->InitializeNonOrientedVonNeumannSuperCursor(supercursor, index);
    this->RecursivelyProcessTree(supercursor);
  }
  // Ce traitement permet de construire sur chacune des cellules le champ
  // FragIds indiquant à quel fragment/région la cellule est rattachée ainsi que
  // le champ m_is_skin_celldata qui indiquent si cette cellule a au moins un
  // voisin manquant.
}

//--------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::RecursivelyProcessTree(
    vtkHyperTreeGridNonOrientedVonNeumannSuperCursor* _supercursor)
{
  // Index central
  vtkIdType idC{_supercursor->GetGlobalNodeIndex()};
  // Case is masked or ghosted
  if (this->Internal->IsMasked(idC) || this->Internal->IsGhosted(idC))
  {
    return;
  }

  // Case is coarse
  if (!_supercursor->IsLeaf())
  {
    // Mere : on parcourt les filles
    for (unsigned int child = 0; child < this->Internal->GetNumberOfChildren();
         ++child)
    {
      _supercursor->ToChild(child);
      this->RecursivelyProcessTree(_supercursor);
      _supercursor->ToParent();
    }
    return;
  }

  // Case is leaf
  this->Internal->ComputeField(_supercursor);
}

//--------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::GeneratePoints(vtkHyperTreeGrid* _input)
{
  // Parcours recursif pour recuperer les centres des cellules participant pour
  // chacun des fragments/regions
  if (this->ExtractType !=
      static_cast<int>(EnumClassExtractType::NO_EXTRACT_FRAGMENT))
  {
    vtkIdType index{0};
    vtkHyperTreeGrid::vtkHyperTreeGridIterator hypertree_iterator;
    _input->InitializeTreeIterator(hypertree_iterator);
    vtkNew<vtkHyperTreeGridNonOrientedGeometryCursor> cursor;
    while (hypertree_iterator.GetNextTree(index) != nullptr)
    {
      _input->InitializeNonOrientedGeometryCursor(cursor, index);
      this->RecursivelyProcessGeneratePoints(cursor);
    }
  }
}

//--------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::RecursivelyProcessGeneratePoints(
    vtkHyperTreeGridNonOrientedGeometryCursor* _cursor)
{
  // Index central
  const vtkIdType idC{_cursor->GetGlobalNodeIndex()};
  // Case is masked or ghosted
  if (this->Internal->IsMasked(idC) || this->Internal->IsGhosted(idC))
  {
    return;
  }

  // Case is coarse
  if (!_cursor->IsLeaf())
  {
    // Mere : on parcourt les filles
    for (unsigned int child = 0; child < this->Internal->GetNumberOfChildren();
         ++child)
    {
      _cursor->ToChild(child);
      this->RecursivelyProcessGeneratePoints(_cursor);
      _cursor->ToParent();
    }
    return;
  }

  if (this->ExtractType ==
          static_cast<int>(EnumClassExtractType::EXTRACT_EDGE_FRAGMENT) &&
      !this->Internal->HasSkinCell(idC))
  {
    return;
  }

  // Case is leaf
  this->Internal->AddOutputPoints(_cursor);
}

//----------------------------------------------------------------------------
int vtkHyperTreeGridFragmentation::FillOutputPortInformation(
    int vtkNotUsed(port), vtkInformation* info)
{
  assert("pre: information object is nullptr!" && (info != nullptr));
  info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkMultiBlockDataSet");
  return 1;
}
//----------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::PrintSelf(ostream& ost, vtkIndent indent)
{
  this->Superclass::PrintSelf(ost, indent);
}
//--------------------------------------------------------------------------------------------------
