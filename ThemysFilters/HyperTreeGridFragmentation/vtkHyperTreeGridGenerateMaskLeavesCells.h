/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkHyperTreeGridGenerateMaskLeavesCells.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class   vtkHyperTreeGridGenerateMaskLeavesCells
 * @brief   Generates a mask that evaluates to True for each leaf cell.
 *
 * A cell in Hyper Tree Grid can be:
 *  - coarse or leaf;
 *  - masked;
 *  - ghosted.
 *
 * vtkHyperTreeGridGenerateMaskLeavesCells is a filter that takes as
 * input an hypertree grid and produces a new boolean field that evaluates
 * to true when :
 *  - leaf, so not coarse;
 *  - not masked;
 *  - not ghosted.
 *
 * WARNING
 * The notion of cell sheet relates to the application or not of the
 * vtkDepthLimiter filter, as well the change of the parameter of this filter.
 *
 * This method compute as `vtkVolume`.
 *
 * The compute of a global field must be done taking this boolean into
 * account in order to only take the leaf cells into account.
 *
 * In fact, ParaView don't accept a vtkBitArray, vtkValidCell is a
 * implicit array (storage is std::vector<bool> but ParaView/VTK see
 * as vtkDoubleArray).
 *
 * For compute of a global field, you use `Python Calculator` and, instead
 * of to write `sum(rho)`, you write expression `sum(rho*vtkValidCell)`.
 * You can verify the first expression produces a value greater than or
 * equal to the second expression.
 *
 * @sa
 * vtkHyperTreeGrid vtkHyperTreeGridAlgorithm
 *
 * @par Thanks:
 * This class was written by Jacques-Bernard Lekien, 2023
 * This work was supported by Commissariat a l'Energie Atomique
 * CEA, DAM, DIF, F-91297 Arpajon, France.
 */

#ifndef vtkHyperTreeGridGenerateMaskLeavesCells_h
#define vtkHyperTreeGridGenerateMaskLeavesCells_h

#include <memory>
#include <ostream> // for ostream

#include <vtkABINamespace.h> // for VTK_ABI_NAMESPACE_BEGIN, VTK_...
#include <vtkHyperTreeGridAlgorithm.h>
#include <vtkIOStream.h> // for ostream
#include <vtkSetGet.h>   // for vtkTypeMacro

#include "vtkFiltersHyperTreeModule.h" // For export macro

VTK_ABI_NAMESPACE_BEGIN
class vtkDataObject;
class vtkIndent;
class vtkInformation;
class vtkHyperTreeGrid;
class vtkHyperTreeGridNonOrientedGeometryCursor;

class VTKFILTERSHYPERTREE_EXPORT vtkHyperTreeGridGenerateMaskLeavesCells
    : public vtkHyperTreeGridAlgorithm
{
public:
  static vtkHyperTreeGridGenerateMaskLeavesCells* New();
  vtkTypeMacro(
      vtkHyperTreeGridGenerateMaskLeavesCells,
      vtkHyperTreeGridAlgorithm) void PrintSelf(ostream& ost,
                                                vtkIndent indent) override;

protected:
  vtkHyperTreeGridGenerateMaskLeavesCells();
  ~vtkHyperTreeGridGenerateMaskLeavesCells() override;

  /**
   * Main routine to extract hyper tree grid levels
   */
  int ProcessTrees(vtkHyperTreeGrid*, vtkDataObject*) override;

  ///@{
  /**
   * Define default input and output port types
   */
  int FillOutputPortInformation(int, vtkInformation*) override;
  ///@}

private:
  vtkHyperTreeGridGenerateMaskLeavesCells(
      const vtkHyperTreeGridGenerateMaskLeavesCells&) = delete;
  void operator=(const vtkHyperTreeGridGenerateMaskLeavesCells&) = delete;

  /**
   * Recursively descend into tree down to leaves
   */
  void ProcessNode(vtkHyperTreeGridNonOrientedGeometryCursor*);

  class vtkInternal;
  std::unique_ptr<vtkInternal> Internal;
};

VTK_ABI_NAMESPACE_END
#endif // vtkHyperTreeGridGenerateMaskLeavesCells_h
