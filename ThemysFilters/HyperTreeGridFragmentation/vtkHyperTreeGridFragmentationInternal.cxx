/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkHyperTreeGridFragmentation.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkHyperTreeGridFragmentationInternal.h"
// IWYU pragma: no_include <bits/std_abs.h>
// IWYU pragma: no_include <ext/alloc_traits.h>
// IWYU pragma: no_include <stdlib.h>
#include <algorithm> // for min
#include <cassert>   // for assert
#include <cmath>     // for pow, sqrt
#include <iostream>  // for operat...
#include <iterator>  // for operat...
#include <limits>    // for numeri...
#include <string>    // for allocator
#include <utility>   // for pair

#include <vtkBitArray.h>                                      // for vtkBit...
#include <vtkCellData.h>                                      // for vtkCel...
#include <vtkCommunicator.h>                                  // for vtkCom...
#include <vtkDataArray.h>                                     // for vtkDat...
#include <vtkDoubleArray.h>                                   // for vtkDou...
#include <vtkHyperTreeGrid.h>                                 // for vtkHyp...
#include <vtkHyperTreeGridNonOrientedGeometryCursor.h>        // for vtkHyp...
#include <vtkHyperTreeGridNonOrientedVonNeumannSuperCursor.h> // for vtkHyp...
#include <vtkIOStream.h>                                      // for cout
#include <vtkLogger.h>                                        // for vtkLogger
#include <vtkMultiProcessController.h>                        // for vtkMul...
#include <vtkPoints.h>                                        // for vtkPoints
#include <vtkSystemIncludes.h>                                // for vtkOSt...
#include <vtkUnsignedCharArray.h>                             // for vtkUns...

//-------------------------------------------------------------------------------------------------
constexpr double MY_EPSILON_ABSOLU = 1.e-7;

// BeforeC++20
constexpr double MY_PI = M_PI;
// AfterC++20
// constexpr double MY_PI = std::numbers::pi;

//-------------------------------------------------------------------------------------------------
// Enrichissement autour du std::array

namespace {
// Operateur permettant d'accumuler par addition un std:array par un autre
// std::array de même dimension
template <typename T, unsigned long N>
std::array<T, N>& operator+=(std::array<T, N>& _accumulator,
                             const std::array<T, N>& _other)
{
  for (unsigned long i = 0; i < N; ++i)
  {
    _accumulator.at(i) += _other.at(i);
  }
  return _accumulator;
}

// Operateur permettant d'accumuler par addition un std:array par une valeur
// constante
template <typename T, unsigned long N>
std::array<T, N>& operator+=(std::array<T, N>& _accumulator, const T _other)
{
  for (unsigned long i = 0; i < N; ++i)
  {
    _accumulator.at(i) += _other;
  }
  return _accumulator;
}

// Operateur permettant d'accumuler par multiplication un std:array par une
// valeur constante
template <typename T, unsigned long N>
std::array<T, N>& operator*=(std::array<T, N>& _accumulator, const T _other)
{
  for (unsigned long i = 0; i < N; ++i)
  {
    _accumulator.at(i) *= _other;
  }
  return _accumulator;
}

// Operateur permettant d'accumuler par division un std:array par une valeur
// constante
template <typename T, unsigned long N>
std::array<T, N>& operator/=(std::array<T, N>& _accumulator, const T _other)
{
  for (unsigned long i = 0; i < N; ++i)
  {
    _accumulator.at(i) /= _other;
  }
  return _accumulator;
}
} // namespace

//--------------------------------------------------------------------------------------------------
namespace {
//------------------------------------------------------------------------------------------------
template <class P>
void OneComputeAverageField(
    std::map<unsigned int, std::array<double, 3>>& _values,
    const std::map<unsigned int, P> _pounds)
{
  for (const auto& entry : _values)
  {
    auto iFrag = entry.first;

    assert(_pounds.find(iFrag) != _pounds.end());

    if (_pounds.at(iFrag) == 0)
    {
      // Si le poids (que ce soit en nombre que ce soit en masse est nulle,
      // on s'attend à ce que le champ cumule pondere le soit aussi
      assert(_values[iFrag][0] == 0 && _values[iFrag][1] == 0 &&
             _values[iFrag][2] == 0);
    } else
    {
      // #ifndef NDEBUG
      //   constexpr unsigned long DBL_MANT_MAX = std::pow(2, DBL_MANT_DIG);
      //   if (_pounds[i] > DBL_MANT_MAX)
      //   {
      //     vtkVLog(vtkLogger::VERBOSITY_WARNING, "Pounds " << _pounds[i]
      //       << " has oversize limit (" << DBL_MANT_MAX
      //       << ") on fragment #" << i << ". Compute average with rounded
      //       calculation.");
      //   }
      // #endif
      //
      // Calcul de la moyenne ponderee
      _values[iFrag] /= (double)_pounds.at(iFrag);
      // Si _pounds[i] est <= DBL_MANT_MAX alors la conversion est exacte
      // ce qui n'est pas le cas sinon. Il faut etre conscient que l'on parle
      // ici de tres grand nombre representant un nombre de cellules.
      // C'est bien gentil de vouloir passer par double weight{_pounds[i]}
      // mais alors le compilateur râle statiquement sans savoir si on
      // atteindra dynamiquement un jour la limite.
    }
  }
}
} // namespace

//------------------------------------------------------------------------------------------------
vtkHyperTreeGridFragmentation::vtkInternal::vtkInternal(
    vtkMultiProcessController* _controller)
    : m_controller(_controller)
{
  if (this->HasController())
  {
    this->m_number_of_processes = this->m_controller->GetNumberOfProcesses();
    this->m_my_rank = this->m_controller->GetLocalProcessId();
  }
}

//------------------------------------------------------------------------------------------------
vtkHyperTreeGridFragmentation::vtkInternal::~vtkInternal() = default;

//------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::vtkInternal::PrintSelf()
{
  std::cout << "##Children " << this->m_number_of_children << std::endl;
  std::cout << "##Dimension " << this->m_number_of_dimension << std::endl;

  std::cout << "Mask " << this->m_in_mask << std::endl;
  std::cout << "Ghost " << this->m_in_ghost << std::endl;

  std::cout << "Mass " << this->m_input_mass << std::endl;
  std::cout << "Density " << this->m_input_density << std::endl;
  std::cout << "Velocity " << this->m_input_velocity << std::endl;

  std::cout << "m_crt_uncontinuous_frag " << this->m_crt_uncontinuous_frag
            << std::endl;

  std::cout << "##m_frag_id_celldata " << this->m_frag_id_celldata.size()
            << std::endl;
  std::cout << "##m_is_skin_celldata " << this->m_is_skin_celldata.size()
            << std::endl;

  std::cout << "##m_output_pounds " << this->m_output_pounds.size()
            << std::endl;
  std::cout << "##m_output_volume " << this->m_output_volume.size()
            << std::endl;
  std::cout << "##m_output_mass " << this->m_output_mass.size() << std::endl;
  std::cout << "##m_output_density " << this->m_output_density.size()
            << std::endl;

  std::cout << "##m_output_avg_center " << this->m_output_avg_center.size()
            << std::endl;
  std::cout << "##m_output_barycenter " << this->m_output_barycenter.size()
            << std::endl;
  std::cout << "##m_output_avg_velocity " << this->m_output_avg_velocity.size()
            << std::endl;
  std::cout << "##m_output_velocity " << this->m_output_velocity.size()
            << std::endl;

  std::cout << "##m_output_local " << this->m_output_local.size() << std::endl;

  std::cout << "m_output_points " << this->m_output_points.size() << std::endl;
}

//------------------------------------------------------------------------------------------------
bool vtkHyperTreeGridFragmentation::vtkInternal::HasController() const
{
  return this->m_controller != nullptr;
}

vtkMultiProcessController*
vtkHyperTreeGridFragmentation::vtkInternal::GetController() // non const cause
                                                            // VTK
{
  return this->m_controller;
}

int vtkHyperTreeGridFragmentation::vtkInternal::GetNumberOfProcesses() const
{
  return this->m_number_of_processes;
}

int vtkHyperTreeGridFragmentation::vtkInternal::GetMyRank() const
{
  return this->m_my_rank;
}

//------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::vtkInternal::Initialize(
    vtkHyperTreeGrid* _input)
{
  this->Cleaner();

  this->m_number_of_children = _input->GetNumberOfChildren();
  this->m_number_of_dimension = _input->GetDimension();

  this->m_in_mask = _input->HasMask() ? _input->GetMask() : nullptr;
  this->m_in_ghost = _input->GetGhostCells();

  this->m_frag_id_celldata.resize(_input->GetNumberOfCells(),
                                  std::numeric_limits<unsigned int>::max());
  this->m_is_skin_celldata.resize(_input->GetNumberOfCells(), false);
}

//------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::vtkInternal::RetrieveMassInput(
    vtkHyperTreeGrid* _input, const bool _enable, const char* _name)
{
  this->m_input_mass = this->RetrieveScalarInput(_input, _enable, _name);
}

//------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::vtkInternal::RetrieveDensityInput(
    vtkHyperTreeGrid* _input, const bool _enable, const char* _name)
{
  this->m_input_density = this->RetrieveScalarInput(_input, _enable, _name);
}

//------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::vtkInternal::RetrieveVelocityInput(
    vtkHyperTreeGrid* _input, const bool _enable, const char* _name)
{
  this->m_input_velocity = this->RetrieveVectorInput(_input, _enable, _name);
}

//------------------------------------------------------------------------------------------------
bool vtkHyperTreeGridFragmentation::vtkInternal::IsMasked(const vtkIdType _id)
{
  return (this->m_in_mask != nullptr && this->m_in_mask->GetValue(_id) != 0);
}

//------------------------------------------------------------------------------------------------
bool vtkHyperTreeGridFragmentation::vtkInternal::IsGhosted(const vtkIdType _id)
{
  return (this->m_in_ghost != nullptr && this->m_in_ghost->GetValue(_id) != 0);
}

//------------------------------------------------------------------------------------------------
unsigned int
vtkHyperTreeGridFragmentation::vtkInternal::GetNumberOfChildren() const
{
  return this->m_number_of_children;
}

//------------------------------------------------------------------------------------------------
unsigned int
vtkHyperTreeGridFragmentation::vtkInternal::GetNumberOfDimension() const
{
  return this->m_number_of_dimension;
}

//------------------------------------------------------------------------------------------------
bool vtkHyperTreeGridFragmentation::vtkInternal::HasInputMass() const
{
  return this->m_input_mass != nullptr || this->m_input_density != nullptr;
}

//------------------------------------------------------------------------------------------------
bool vtkHyperTreeGridFragmentation::vtkInternal::HasInputDensity() const
{
  return this->m_input_density != nullptr;
}

//------------------------------------------------------------------------------------------------
bool vtkHyperTreeGridFragmentation::vtkInternal::HasInputVelocity() const
{
  return this->m_input_velocity != nullptr;
}

//------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::vtkInternal::ComputeField(
    vtkHyperTreeGridNonOrientedVonNeumannSuperCursor* _supercursor)
{
  vtkIdType idC{_supercursor->GetGlobalNodeIndex()};

  unsigned int fragId{this->m_frag_id_celldata[idC]};
  if (fragId == std::numeric_limits<unsigned int>::max())
  {
    // Creation d'un nouveau fragment
    fragId = this->m_crt_uncontinuous_frag++;
    this->NewFragment(idC, fragId);
  }

  this->m_output_pounds[fragId] += 1;
  double cellVolume = this->ComputeOutputVolume(_supercursor, fragId);
  double cellMass = this->ComputeOutputMass(cellVolume, idC, fragId);
  this->ComputeOutputCenters(_supercursor, cellMass, fragId);
  this->ComputeOutputVelocity(cellMass, idC, fragId);

  for (unsigned int idim = 0; idim < this->m_number_of_dimension;
       ++idim) // dimension
  {
    for (unsigned int orientation = 0; orientation < 2;
         ++orientation) // gauche, centre, droite
    {
      const unsigned int neighborIdx{this->m_number_of_dimension +
                                     (2 * orientation - 1) * (idim + 1)};
      const bool isValidN{_supercursor->HasTree(neighborIdx)};
      if (isValidN)
      {
        // Index d'une cellule voisine
        const vtkIdType idN{_supercursor->GetGlobalNodeIndex(neighborIdx)};
        if (this->IsMasked(idN))
        {
          // Si une cellule voisine est masquee alors la cellule courante est de
          // bord/peau
          this->m_is_skin_celldata[idC] = true;
        } else
        {
          if (_supercursor->IsLeaf(neighborIdx))
          {
            this->ComputeNeighboringCell(idN, fragId);
          }
        }
      } else
      {
        // Si une cellule voisine n'existe pas alors la cellule courante est de
        // bord/peau
        this->m_is_skin_celldata[idC] = true;
      }
    }
  }
}

//------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::vtkInternal::ComputeAverageField()
{
  // Finalisation des calculs des champs globaux :
  // - le calcul du centre moyenne : somme des centres divisés par le nombre
  // d'élements, le poids
  OneComputeAverageField<unsigned long>(this->m_output_avg_center,
                                        this->m_output_pounds);
  // - le calcul du centre moyen pondere par la masse : somme des produits du
  // centre par la masse
  //   le tout divise par la masse totale
  OneComputeAverageField<double>(this->m_output_barycenter,
                                 this->m_output_mass);
  // - le calcul de la vitesse moyenne : somme des vitesses divisées par le
  // nombre d'élements, le poids
  OneComputeAverageField<unsigned long>(this->m_output_avg_velocity,
                                        this->m_output_pounds);
  // - le calcul de la vitesse moyenne ponderee par la masse : somme des
  // produits de la vitesse par la masse
  //   le tout divise par la masse totale
  OneComputeAverageField<double>(this->m_output_velocity, this->m_output_mass);
}

//------------------------------------------------------------------------------------------------
const std::map<unsigned int, unsigned long>&
vtkHyperTreeGridFragmentation::vtkInternal::GetOutputPounds() const
{
  return this->m_output_pounds;
}

//------------------------------------------------------------------------------------------------
const std::map<unsigned int, double>&
vtkHyperTreeGridFragmentation::vtkInternal::GetOutputVolume() const
{
  return this->m_output_volume;
}

//------------------------------------------------------------------------------------------------
const std::map<unsigned int, double>&
vtkHyperTreeGridFragmentation::vtkInternal::GetOutputMass() const
{
  return this->m_output_mass;
}

//------------------------------------------------------------------------------------------------
const std::map<unsigned int, std::array<double, 3>>&
vtkHyperTreeGridFragmentation::vtkInternal::GetOutputAvgCenter() const
{
  return this->m_output_avg_center;
}

//------------------------------------------------------------------------------------------------
const std::map<unsigned int, std::array<double, 3>>&
vtkHyperTreeGridFragmentation::vtkInternal::GetOutputBarycenter() const
{
  return this->m_output_barycenter;
}

//------------------------------------------------------------------------------------------------
const std::map<unsigned int, std::array<double, 3>>&
vtkHyperTreeGridFragmentation::vtkInternal::GetOutputVelocity() const
{
  return this->m_output_velocity;
}

//------------------------------------------------------------------------------------------------
const std::map<unsigned int, std::array<double, 3>>&
vtkHyperTreeGridFragmentation::vtkInternal::GetOutputAvgVelocity() const
{
  return this->m_output_avg_velocity;
}

//------------------------------------------------------------------------------------------------
const std::map<unsigned int, bool>&
vtkHyperTreeGridFragmentation::vtkInternal::GetOutputLocal() const
{
  return this->m_output_local;
}

//------------------------------------------------------------------------------------------------
double vtkHyperTreeGridFragmentation::vtkInternal::ComputeOneFormFactorRadius(
    const unsigned int _iFrag) const
{
  switch (this->m_number_of_dimension)
  {
  case 1: {
    constexpr double half = 1.0 / 2.0;
    return this->m_output_volume.at(_iFrag) * half;
  }
  case 2: {
    return std::sqrt(this->m_output_volume.at(_iFrag) / MY_PI);
  }
  case 3: {
    constexpr double coef = 3.0 / (4.0 * MY_PI);
    constexpr double tiers = 1.0 / 3.0;
    return std::pow(coef * this->m_output_volume.at(_iFrag), tiers);
  }
  }
  return 0.;
}

//------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::vtkInternal::ComputeFormFactorRadius(
    std::map<unsigned int, double>& _radius) const
{
  switch (this->m_number_of_dimension)
  {
  case 1: {
    constexpr double half = 1.0 / 2.0;
    for (const auto& entry : this->m_output_volume)
    {
      auto iFrag = entry.first;
      _radius[iFrag] = this->m_output_volume.at(iFrag) * half;
    }
    break;
  }
  case 2: {
    for (const auto& entry : this->m_output_volume)
    {
      auto iFrag = entry.first;
      _radius[iFrag] = std::sqrt(this->m_output_volume.at(iFrag) / MY_PI);
    }
    break;
  }
  case 3: {
    constexpr double coef = 3.0 / (4.0 * MY_PI);
    constexpr double tiers = 1.0 / 3.0;
    for (const auto& entry : this->m_output_volume)
    {
      auto iFrag = entry.first;
      _radius[iFrag] = std::pow(coef * this->m_output_volume.at(iFrag), tiers);
    }
    break;
  }
  default: {
    for (const auto& entry : this->m_output_volume)
    {
      auto iFrag = entry.first;
      _radius[iFrag] = 0.;
    }
  }
  }
}

//------------------------------------------------------------------------------------------------
double vtkHyperTreeGridFragmentation::vtkInternal::ComputeOneDensity(
    const unsigned int _iFrag) const
{
  return this->m_output_mass.at(_iFrag) / this->m_output_volume.at(_iFrag);
}

//------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::vtkInternal::ComputeDensity(
    std::map<unsigned int, double>& _density) const
{
  for (const auto& entry : this->m_output_volume)
  {
    auto iFrag = entry.first;
    _density[iFrag] = this->ComputeOneDensity(iFrag);
  }
}

//--------------------------------------------------------------------------------------------------
unsigned int vtkHyperTreeGridFragmentation::vtkInternal::CompactFragId()
{
  std::map<unsigned int, unsigned int>
      indexation_frag_continuous_to_uncontinuous;

  // FragIds decrit un champ sur les cellules du maillage en input
  // On parcourt toutes les cellules pour lister les numeros de
  // fragments/régions restant a la fin du ProcessAllTree
  unsigned int numberContinuousIndexFrag{0};
  for (auto& iFrag : this->m_frag_id_celldata)
  {
    // std::numeric_limits<unsigned int>::max() peut rester positionner pour les
    // cellules masquees
    if (iFrag != std::numeric_limits<unsigned int>::max() &&
        indexation_frag_continuous_to_uncontinuous.find(iFrag) ==
            indexation_frag_continuous_to_uncontinuous.end())
    {
      if (iFrag != numberContinuousIndexFrag)
      {
        assert(m_output_pounds.find(numberContinuousIndexFrag) ==
               m_output_pounds.end());
        m_output_pounds[numberContinuousIndexFrag] = m_output_pounds[iFrag];
        m_output_pounds.erase(iFrag);
        assert(m_output_pounds.find(iFrag) == m_output_pounds.end());

        assert(m_output_volume.find(numberContinuousIndexFrag) ==
               m_output_volume.end());
        m_output_volume[numberContinuousIndexFrag] = m_output_volume[iFrag];
        m_output_volume.erase(iFrag);
        assert(m_output_volume.find(iFrag) == m_output_volume.end());

        assert(m_output_mass.find(numberContinuousIndexFrag) ==
               m_output_mass.end());
        m_output_mass[numberContinuousIndexFrag] = m_output_mass[iFrag];
        m_output_mass.erase(iFrag);
        assert(m_output_mass.find(iFrag) == m_output_mass.end());

        assert(m_output_density.find(numberContinuousIndexFrag) ==
               m_output_density.end());
        m_output_density[numberContinuousIndexFrag] = m_output_density[iFrag];
        m_output_density.erase(iFrag);
        assert(m_output_density.find(iFrag) == m_output_density.end());

        assert(m_output_avg_center.find(numberContinuousIndexFrag) ==
               m_output_avg_center.end());
        m_output_avg_center[numberContinuousIndexFrag] =
            m_output_avg_center[iFrag];
        m_output_avg_center.erase(iFrag);
        assert(m_output_avg_center.find(iFrag) == m_output_avg_center.end());

        assert(m_output_barycenter.find(numberContinuousIndexFrag) ==
               m_output_barycenter.end());
        m_output_barycenter[numberContinuousIndexFrag] =
            m_output_barycenter[iFrag];
        m_output_barycenter.erase(iFrag);
        assert(m_output_barycenter.find(iFrag) == m_output_barycenter.end());

        assert(m_output_avg_velocity.find(numberContinuousIndexFrag) ==
               m_output_avg_velocity.end());
        m_output_avg_velocity[numberContinuousIndexFrag] =
            m_output_avg_velocity[iFrag];
        m_output_avg_velocity.erase(iFrag);
        assert(m_output_avg_velocity.find(iFrag) ==
               m_output_avg_velocity.end());

        assert(m_output_velocity.find(numberContinuousIndexFrag) ==
               m_output_velocity.end());
        m_output_velocity[numberContinuousIndexFrag] = m_output_velocity[iFrag];
        m_output_velocity.erase(iFrag);
        assert(m_output_velocity.find(iFrag) == m_output_velocity.end());

        assert(m_output_local.find(numberContinuousIndexFrag) ==
               m_output_local.end());
        m_output_local[numberContinuousIndexFrag] = m_output_local[iFrag];
        m_output_local.erase(iFrag);
        assert(m_output_local.find(iFrag) == m_output_local.end());
        assert(m_output_local.at(
            numberContinuousIndexFrag)); // doit valoir true par construction
      }

      indexation_frag_continuous_to_uncontinuous[iFrag] =
          numberContinuousIndexFrag++;
    }

    iFrag = indexation_frag_continuous_to_uncontinuous[iFrag];
  }

  return numberContinuousIndexFrag;
}

//--------------------------------------------------------------------------------------------------
template <class T> class backwards
{
  T& _obj;

public:
  backwards(T& obj) : _obj(obj) {}
  auto begin() { return _obj.rbegin(); }
  auto end() { return _obj.rend(); }
};

//--------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::vtkInternal::ShiftFragId(
    const unsigned int _shift)
{
  if (_shift == 0)
  {
    return;
  }

  for (auto& iFrag : this->m_frag_id_celldata)
  {
    iFrag += _shift;
  }

  std::vector<unsigned int> keys(this->m_output_pounds.size());
  unsigned int iKey = 0;
  for (const auto& entry : backwards(this->m_output_pounds))
  {
    keys[iKey++] = entry.first;
  }

  for (const auto& key : keys)
  {
    unsigned int iFrag = key;
    unsigned int nFrag = iFrag + _shift;

    assert(m_output_pounds.find(nFrag) == m_output_pounds.end());
    m_output_pounds[nFrag] = m_output_pounds[iFrag];
    m_output_pounds.erase(iFrag);
    assert(m_output_pounds.find(iFrag) == m_output_pounds.end());

    assert(m_output_volume.find(nFrag) == m_output_volume.end());
    m_output_volume[nFrag] = m_output_volume[iFrag];
    m_output_volume.erase(iFrag);
    assert(m_output_volume.find(iFrag) == m_output_volume.end());

    assert(m_output_mass.find(nFrag) == m_output_mass.end());
    m_output_mass[nFrag] = m_output_mass[iFrag];
    m_output_mass.erase(iFrag);
    assert(m_output_mass.find(iFrag) == m_output_mass.end());

    assert(m_output_density.find(nFrag) == m_output_density.end());
    m_output_density[nFrag] = m_output_density[iFrag];
    m_output_density.erase(iFrag);
    assert(m_output_density.find(iFrag) == m_output_density.end());

    assert(m_output_avg_center.find(nFrag) == m_output_avg_center.end());
    m_output_avg_center[nFrag] = m_output_avg_center[iFrag];
    m_output_avg_center.erase(iFrag);
    assert(m_output_avg_center.find(iFrag) == m_output_avg_center.end());

    assert(m_output_barycenter.find(nFrag) == m_output_barycenter.end());
    m_output_barycenter[nFrag] = m_output_barycenter[iFrag];
    m_output_barycenter.erase(iFrag);
    assert(m_output_barycenter.find(iFrag) == m_output_barycenter.end());

    assert(m_output_avg_velocity.find(nFrag) == m_output_avg_velocity.end());
    m_output_avg_velocity[nFrag] = m_output_avg_velocity[iFrag];
    m_output_avg_velocity.erase(iFrag);
    assert(m_output_avg_velocity.find(iFrag) == m_output_avg_velocity.end());

    assert(m_output_velocity.find(nFrag) == m_output_velocity.end());
    m_output_velocity[nFrag] = m_output_velocity[iFrag];
    m_output_velocity.erase(iFrag);
    assert(m_output_velocity.find(iFrag) == m_output_velocity.end());

    assert(m_output_local.find(nFrag) == m_output_local.end());
    m_output_local[nFrag] = m_output_local[iFrag];
    m_output_local.erase(iFrag);
    assert(m_output_local.find(iFrag) == m_output_local.end());
    assert(m_output_local.at(nFrag)); // doit valoir true par construction
  }
}

//--------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::vtkInternal::ComputeLocalFragId(
    const unsigned int _shift, const unsigned int _nbLocalFrags,
    std::map<unsigned int, unsigned int>& _sameFrag) const
{
  for (unsigned int iFrag = _shift; iFrag < _shift + _nbLocalFrags; ++iFrag)
  {
    if (_sameFrag.find(iFrag) == _sameFrag.end())
    {
      _sameFrag[iFrag] = iFrag;
    } else
    {
      assert(_sameFrag[iFrag] < _shift);
    }
  }
}

//--------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::vtkInternal::ShiftFragId(
    const std::vector<unsigned int>& _frag2frag)
{
  for (auto& iFrag : this->m_frag_id_celldata)
  {
    iFrag = _frag2frag.at(iFrag);
  }

  std::vector<unsigned int> keys(this->m_output_pounds.size());
  unsigned int iKey = 0;
  unsigned int minLocalKey = std::numeric_limits<unsigned int>::max();
  for (const auto& entry : this->m_output_pounds)
  {
    keys[iKey++] = entry.first;
    minLocalKey = std::min(minLocalKey, entry.first);
  }

  for (const auto& key : keys)
  {
    unsigned int iFrag = key;
    unsigned int nFrag = _frag2frag.at(iFrag);
    std::cerr << "iFrag " << iFrag << " nFrag " << nFrag << std::endl;

    if (iFrag == nFrag)
    {
      continue;
    }

    assert(m_output_pounds.find(nFrag) == m_output_pounds.end());
    m_output_pounds[nFrag] = m_output_pounds[iFrag];
    m_output_pounds.erase(iFrag);
    assert(m_output_pounds.find(iFrag) == m_output_pounds.end());

    assert(m_output_volume.find(nFrag) == m_output_volume.end());
    m_output_volume[nFrag] = m_output_volume[iFrag];
    m_output_volume.erase(iFrag);
    assert(m_output_volume.find(iFrag) == m_output_volume.end());

    assert(m_output_mass.find(nFrag) == m_output_mass.end());
    m_output_mass[nFrag] = m_output_mass[iFrag];
    m_output_mass.erase(iFrag);
    assert(m_output_mass.find(iFrag) == m_output_mass.end());

    assert(m_output_density.find(nFrag) == m_output_density.end());
    m_output_density[nFrag] = m_output_density[iFrag];
    m_output_density.erase(iFrag);
    assert(m_output_density.find(iFrag) == m_output_density.end());

    assert(m_output_avg_center.find(nFrag) == m_output_avg_center.end());
    m_output_avg_center[nFrag] = m_output_avg_center[iFrag];
    m_output_avg_center.erase(iFrag);
    assert(m_output_avg_center.find(iFrag) == m_output_avg_center.end());

    assert(m_output_barycenter.find(nFrag) == m_output_barycenter.end());
    m_output_barycenter[nFrag] = m_output_barycenter[iFrag];
    m_output_barycenter.erase(iFrag);
    assert(m_output_barycenter.find(iFrag) == m_output_barycenter.end());

    assert(m_output_avg_velocity.find(nFrag) == m_output_avg_velocity.end());
    m_output_avg_velocity[nFrag] = m_output_avg_velocity[iFrag];
    m_output_avg_velocity.erase(iFrag);
    assert(m_output_avg_velocity.find(iFrag) == m_output_avg_velocity.end());

    assert(m_output_velocity.find(nFrag) == m_output_velocity.end());
    m_output_velocity[nFrag] = m_output_velocity[iFrag];
    m_output_velocity.erase(iFrag);
    assert(m_output_velocity.find(iFrag) == m_output_velocity.end());

    if (nFrag < minLocalKey)
    {
      assert(m_output_local.find(nFrag) == m_output_local.end());
      m_output_local[nFrag] = false;
      m_output_local.erase(iFrag);
      assert(m_output_local.find(iFrag) == m_output_local.end());
    } else
    {
      assert(m_output_local.find(nFrag) == m_output_local.end());
      m_output_local[nFrag] = m_output_local[iFrag];
      m_output_local.erase(iFrag);
      assert(m_output_local.find(iFrag) == m_output_local.end());
      assert(m_output_local[nFrag]);
    }
  }
}

//--------------------------------------------------------------------------------------------------
template <class T>
void vtkHyperTreeGridFragmentation::vtkInternal::AllReduceOneGlobalField(
    const unsigned int _nbAllFrags, std::map<unsigned int, T>& _values)
{
  std::vector<T> values(_nbAllFrags, 0);
  std::vector<T> recvValues(_nbAllFrags, 0);
  for (const auto& entry : _values)
  {
    values[entry.first] = entry.second;
  }
  this->GetController()->AllReduce(values.data(), recvValues.data(),
                                   _nbAllFrags, vtkCommunicator::SUM_OP);
  for (auto& entry : _values)
  {
    entry.second = recvValues[entry.first];
  }
}

//--------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::vtkInternal::AllReduceOneGlobalFieldVector(
    const unsigned int _nbAllFrags,
    std::map<unsigned int, std::array<double, 3>>& _values)
{
  std::vector<double> values(3 * _nbAllFrags, 0);
  std::vector<double> recvValues(3 * _nbAllFrags, 0);
  for (const auto& entry : _values)
  {
    unsigned int offset = 3 * entry.first;
    values[offset++] = entry.second[0];
    values[offset++] = entry.second[1];
    values[offset] = entry.second[2];
  }
  this->GetController()->AllReduce(values.data(), recvValues.data(),
                                   3 * _nbAllFrags, vtkCommunicator::SUM_OP);
  for (auto& entry : _values)
  {
    unsigned int offset = 3 * entry.first;
    entry.second[0] = recvValues[offset++];
    entry.second[1] = recvValues[offset++];
    entry.second[2] = recvValues[offset++];
  }
}

//--------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::vtkInternal::AllReduceAllGlobalField(
    const unsigned int _nbAllFrags)
{
  this->AllReduceOneGlobalField<unsigned long>(_nbAllFrags,
                                               this->m_output_pounds);
  this->AllReduceOneGlobalField<double>(_nbAllFrags, this->m_output_volume);
  this->AllReduceOneGlobalField<double>(_nbAllFrags, this->m_output_mass);
  this->AllReduceOneGlobalFieldVector(_nbAllFrags, this->m_output_avg_center);
  this->AllReduceOneGlobalFieldVector(_nbAllFrags, this->m_output_barycenter);
  this->AllReduceOneGlobalFieldVector(_nbAllFrags, this->m_output_avg_velocity);
  this->AllReduceOneGlobalFieldVector(_nbAllFrags, this->m_output_velocity);
  // this->m_output_local n'est pas concerné puisque c'est une information
  // locale au serveur
}

//--------------------------------------------------------------------------------------------------
bool vtkHyperTreeGridFragmentation::vtkInternal::HasSkinCell(
    const vtkIdType _id) const
{
  return this->m_is_skin_celldata[_id];
}

//--------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::vtkInternal::AddOutputPoints(
    vtkHyperTreeGridNonOrientedGeometryCursor* _cursor)
{
  const vtkIdType idC{_cursor->GetGlobalNodeIndex()};
  std::array<double, 3> xyz{0, 0, 0};
  _cursor->GetPoint(xyz.data());
  this->GetOutputPoints(this->m_frag_id_celldata.at(idC))
      ->InsertNextPoint(xyz.data());
}

//--------------------------------------------------------------------------------------------------
vtkPoints* vtkHyperTreeGridFragmentation::vtkInternal::GetOutputPoints(
    const unsigned int _iFrag)
{
  if (this->m_output_points.find(_iFrag) == this->m_output_points.end())
  {
    this->m_output_points[_iFrag] = vtkPoints::New();
  }
  return this->m_output_points[_iFrag];
}

//--------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::vtkInternal::FinalizeOutputPoints()
{
  for (auto& entry : this->m_output_points)
  {
    entry.second->Delete();
  }
  this->m_output_points.clear();
}

//--------------------------------------------------------------------------------------------------
const std::vector<unsigned int>&
vtkHyperTreeGridFragmentation::vtkInternal::GetOutputFragByCells() const
{
  return this->m_frag_id_celldata;
}

//------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::vtkInternal::Finalize() { this->Cleaner(); }

//------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::vtkInternal::Cleaner()
{
  this->m_number_of_children = 0;
  this->m_number_of_dimension = 0;

  this->m_in_mask = nullptr;
  this->m_in_ghost = nullptr;

  this->m_input_mass = nullptr;
  this->m_input_density = nullptr;
  this->m_input_velocity = nullptr;

  this->m_crt_uncontinuous_frag = 0;

  this->m_frag_id_celldata.clear();
  this->m_is_skin_celldata.clear();

  this->m_output_pounds.clear();
  this->m_output_volume.clear();
  this->m_output_mass.clear();
  this->m_output_density.clear();
  this->m_output_avg_center.clear();
  this->m_output_barycenter.clear();
  this->m_output_avg_velocity.clear();
  this->m_output_velocity.clear();
  this->m_output_local.clear();

  this->m_output_points.clear();
}

//------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::vtkInternal::NewFragment(
    const vtkIdType _id, const unsigned int _fragId)
{
  this->m_frag_id_celldata[_id] = _fragId;
  this->m_output_pounds[_fragId] = 0;
  this->m_output_volume[_fragId] = 0;
  this->m_output_mass[_fragId] = 0;
  std::array<double, 3> vectornul{0, 0, 0};
  this->m_output_avg_center[_fragId] = vectornul;
  this->m_output_barycenter[_fragId] = vectornul;
  this->m_output_avg_velocity[_fragId] = vectornul;
  this->m_output_velocity[_fragId] = vectornul;
  this->m_output_local[_fragId] = true;
}

//------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::vtkInternal::AccFragment(
    const unsigned int _fragIdAcc, const unsigned int _fragId)
{
  this->m_output_pounds[_fragIdAcc] += this->m_output_pounds[_fragId];
  this->m_output_volume[_fragIdAcc] += this->m_output_volume[_fragId];
  this->m_output_mass[_fragIdAcc] += this->m_output_mass[_fragId];
  this->m_output_avg_center[_fragIdAcc] += this->m_output_avg_center[_fragId];
  this->m_output_barycenter[_fragIdAcc] += this->m_output_barycenter[_fragId];
  this->m_output_avg_velocity[_fragIdAcc] +=
      this->m_output_avg_velocity[_fragId];
  this->m_output_velocity[_fragIdAcc] += this->m_output_velocity[_fragId];
  assert(this->m_output_local[_fragIdAcc] == this->m_output_local[_fragId]);
}

//------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::vtkInternal::CleanFragment(
    const unsigned int _fragId)
{
  this->m_output_pounds.erase(_fragId);
  this->m_output_volume.erase(_fragId);
  this->m_output_mass.erase(_fragId);
  this->m_output_avg_center.erase(_fragId);
  this->m_output_barycenter.erase(_fragId);
  this->m_output_avg_velocity.erase(_fragId);
  this->m_output_velocity.erase(_fragId);
  this->m_output_local.erase(_fragId);
}

//------------------------------------------------------------------------------------------------
vtkDoubleArray* vtkHyperTreeGridFragmentation::vtkInternal::RetrieveScalarInput(
    vtkHyperTreeGrid* _input, const bool _enable, const char* _name) const
{
  if (!_enable || std::string(_name).empty())
  {
    return nullptr;
  }
  vtkDoubleArray* arr =
      vtkDoubleArray::SafeDownCast(_input->GetCellData()->GetArray(_name));
  if (arr == nullptr)
  {
    vtkVLog(vtkLogger::VERBOSITY_WARNING,
            "vtkDoubleArray " << _name << " (as scalar) not found !");
  }
  return arr;
}

//------------------------------------------------------------------------------------------------
vtkDoubleArray* vtkHyperTreeGridFragmentation::vtkInternal::RetrieveVectorInput(
    vtkHyperTreeGrid* _input, const bool _enable, const char* _name) const
{
  if (!_enable || std::string(_name).empty())
  {
    return nullptr;
  }
  vtkDoubleArray* arr =
      vtkDoubleArray::SafeDownCast(_input->GetCellData()->GetArray(_name));
  if (arr == nullptr)
  {
    vtkVLog(vtkLogger::VERBOSITY_WARNING,
            "vtkDoubleArray " << _name << " (as vector) not found !");
  } else if (arr->GetNumberOfComponents() != 3)
  {
    vtkVLog(vtkLogger::VERBOSITY_WARNING,
            "vtkDoubleArray " << _name << " (as vector with 3 components, here "
                              << arr->GetNumberOfComponents()
                              << ") not found !");
  }
  return arr;
}

//--------------------------------------------------------------------------------------------------
double vtkHyperTreeGridFragmentation::vtkInternal::ComputeOutputVolume(
    vtkHyperTreeGridNonOrientedVonNeumannSuperCursor* _supercursor,
    const unsigned int _fragId)
{
  bool checkVolume = false;
  double cellVolume{1};
  double* ptr{_supercursor->GetSize()};
  for (unsigned int iDim = 0; iDim < 3; ++iDim, ++ptr)
  {
    if (*ptr != 0)
    {
      cellVolume *= *ptr;
      checkVolume = true;
    }
  }
  if (!checkVolume)
  {
    cellVolume = 0;
  }
  this->m_output_volume[_fragId] += cellVolume;
  return cellVolume;
}

//------------------------------------------------------------------------------------------------
double vtkHyperTreeGridFragmentation::vtkInternal::ComputeOutputMass(
    const double _cellVolume, const vtkIdType _id, const unsigned int _fragId)
{
  double cellMass{0};
  if (this->m_input_mass != nullptr)
  {
    cellMass = this->m_input_mass->GetValue(_id);
    if (this->m_input_density != nullptr)
    {
      double cellDensity{this->m_input_density->GetValue(_id)};
      if (std::abs(cellMass - _cellVolume * cellDensity) > MY_EPSILON_ABSOLU)
      {
        vtkVLog(vtkLogger::VERBOSITY_WARNING,
                "Inconsistence mass:" << cellMass << " volume:" << _cellVolume
                                      << " density:" << cellDensity);
      }
    }
  } else if (this->m_input_density != nullptr)
  {
    cellMass = _cellVolume * this->m_input_density->GetValue(_id);
  }
  this->m_output_mass[_fragId] += cellMass;
  return cellMass;
}

//--------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::vtkInternal::ComputeOutputCenters(
    vtkHyperTreeGridNonOrientedVonNeumannSuperCursor* _supercursor,
    const double _cellMass, const unsigned int _fragId)
{
  std::array<double, 3> center{0, 0, 0};
  _supercursor->GetPoint(center.data());
  this->m_output_avg_center[_fragId] += center;
  if (_cellMass != 0)
  {
    center *= _cellMass;
    this->m_output_barycenter[_fragId] += center;
  }
}

//--------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::vtkInternal::ComputeOutputVelocity(
    const double _cellMass, const vtkIdType _id, const unsigned int _fragId)
{
  if (this->m_input_velocity != nullptr)
  {
    double* ptr{this->m_input_velocity->GetTuple(_id)};
    std::array<double, 3> cellVelocity{*ptr, *(ptr + 1), *(ptr + 2)};
    this->m_output_avg_velocity[_fragId] += cellVelocity;
    if (_cellMass != 0)
    {
      cellVelocity *= _cellMass;
      this->m_output_velocity[_fragId] += cellVelocity;
    }
  }
}

//--------------------------------------------------------------------------------------------------
void vtkHyperTreeGridFragmentation::vtkInternal::ComputeNeighboringCell(
    const vtkIdType _idN, const unsigned int _fragId)
{
  // La voisine est presente
  const unsigned int fragIdN{this->m_frag_id_celldata[_idN]};
  if (fragIdN == std::numeric_limits<unsigned int>::max())
  {
    // La cellule voisine n'a pas encore ete affectee a un fragment
    // On l'affecte au fragment en cours
    this->m_frag_id_celldata[_idN] = _fragId;
  } else if (_fragId != fragIdN)
  {
    // La cellule voisine a deja affecte a un autre fragment
    // On fusionne ces deux fragments dans le nouveau
    // en affectant toutes les cellules du fragment voisin
    // a celui qui est en cours
    for (auto& fragIdCurrent : this->m_frag_id_celldata)
    {
      if (fragIdCurrent == fragIdN)
      {
        fragIdCurrent = _fragId;
      }
    }
    // On fusionne le contenu du fragment voisin (fragIdN)
    // dans le courant (_fragId)
    this->AccFragment(_fragId, fragIdN);
    // Libere le contenu du fragment voisin (fragIdN)
    this->CleanFragment(fragIdN);
  }
}

//--------------------------------------------------------------------------------------------------
