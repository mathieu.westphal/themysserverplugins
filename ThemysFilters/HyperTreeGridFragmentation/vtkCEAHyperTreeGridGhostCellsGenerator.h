// SPDX-FileCopyrightText: Copyright (c) Ken Martin, Will Schroeder, Bill
// Lorensen SPDX-License-Identifier: BSD-3-Clause
/**
 * @class   vtkCEAHyperTreeGridGhostCellsGenerator
 * @brief   Generated ghost cells (HyperTree's distributed).
 *
 * This filter generates ghost cells for vtkHyperTreeGrid type data. The input
 * vtkHyperTreeGrid should have hyper trees distributed to a single process.
 * This filter produces ghost hyper trees at the interfaces between different
 * processes, only composed of the nodes and leafs at this interface to avoid
 * data waste.
 *
 * This filter should be used in a multi-processes environment, and is only
 * required if wanting to filter a vtkHyperTreeGrid with algorithms using Von
 * Neumann or Moore supercursors afterwards.
 *
 * @par Thanks:
 * This class was written by Jacques-Bernard Lekien, 2019
 * This work was supported by Commissariat a l'Energie Atomique
 * CEA, DAM, DIF, F-91297 Arpajon, France.
 */

// TODO ATTENTION AU CAS OU UN SERVEUR AURAIT DEFINI UN MAILLAGE VIDE
// IL FAUT QUE LES CHAMPS EXISTENT NBTUPLES VIDE MAIS AVEC LE BON NBCOMPONENT

#ifndef vtkCEAHyperTreeGridGhostCellsGenerator_h
#define vtkCEAHyperTreeGridGhostCellsGenerator_h

#include <memory>  // for allocator, unique_ptr
#include <ostream> // for ostream

// IWYU pragma: no_include <vtkIOStream.h>
#include <vtkABINamespace.h> // for VTK_ABI_NAMESPACE_BEGIN, VTK_...
#include <vtkObject.h>       // for vtkObject
#include <vtkSetGet.h>       // for vtkGetMacro, vtkSetMacro, vtk...

#include "vtkFiltersParallelModule.h"  // for VTKFILTERSPARALLEL_EXPORT
#include "vtkHyperTreeGridAlgorithm.h" // for vtkHyperTreeGridAlgorithm

VTK_ABI_NAMESPACE_BEGIN
class vtkBitArray; // lines 34-34
class vtkCellData; // lines 35-35
class vtkDataObject;
class vtkHyperTreeGrid;                  // lines 36-36
class vtkHyperTreeGridNonOrientedCursor; // lines 37-37
class vtkIndent;
class vtkInformation;
class vtkInformationVector;

class VTKFILTERSPARALLEL_EXPORT vtkCEAHyperTreeGridGhostCellsGenerator
    : public vtkHyperTreeGridAlgorithm
{
public:
  static vtkCEAHyperTreeGridGhostCellsGenerator* New();
  vtkTypeMacro(
      vtkCEAHyperTreeGridGhostCellsGenerator,
      vtkHyperTreeGridAlgorithm) void PrintSelf(ostream& ost,
                                                vtkIndent indent) override;

  vtkSetMacro(EnableVisibilityGhostCells, bool);
  vtkGetMacro(EnableVisibilityGhostCells, bool);

  /**
   * @brief Set MPI MultiProcess Controller.
   */
  int RequestUpdateExtent(vtkInformation*, vtkInformationVector** inputVector,
                          vtkInformationVector*) override;

  /**
   * For this algorithm the output is a vtkHyperTreeGrid instance
   */
  int FillOutputPortInformation(int, vtkInformation*) override;

  /**
   * Main routine to extract cells based on thresholded value
   */
  int ProcessTrees(vtkHyperTreeGrid*, vtkDataObject*) override;

protected:
  vtkCEAHyperTreeGridGhostCellsGenerator();
  ~vtkCEAHyperTreeGridGhostCellsGenerator() override;

  /**
   * Copies the input to the output, filling memory gaps if present.
   */
  void CopyInputTreeToOutput(vtkHyperTreeGridNonOrientedCursor* inCursor,
                             vtkHyperTreeGridNonOrientedCursor* outCursor,
                             vtkCellData* inCellData, vtkCellData* outCellData,
                             vtkBitArray* inMask, vtkBitArray* outMask);

private:
  vtkCEAHyperTreeGridGhostCellsGenerator(
      const vtkCEAHyperTreeGridGhostCellsGenerator&) = delete;
  void operator=(const vtkCEAHyperTreeGridGhostCellsGenerator&) = delete;

  bool EnableVisibilityGhostCells = false;

  //------------------------------------------------------------------------------------------------------------------
  // Class internal

  class vtkInternal;
  std::unique_ptr<vtkInternal> Internal;
};

VTK_ABI_NAMESPACE_END
#endif /* vtkCEAHyperTreeGridGhostCellsGenerator */
