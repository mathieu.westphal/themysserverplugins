set(module_name "HyperTreeGridFragmentation")

vtk_module_add_module(
  ${module_name} CLASSES vtkHyperTreeGridFragmentation vtkHyperTreeGridFragmentationInternal
  vtkCEAHyperTreeGridGhostCellsGenerator vtkHyperTreeGridGenerateMaskLeavesCells
)

vtk_module_compile_options(${module_name} PRIVATE -Wall -Wextra -pedantic -Werror)

vtk_module_set_properties(${module_name} CXX_STANDARD 17 CXX_STANDARD_REQUIRED ON)

paraview_add_server_manager_xmls(XMLS HyperTreeGridFragmentation.xml)

# Adds support for include-what-you-use
find_program(iwyu_path NAMES include-what-you-use iwyu)
if(${iwyu_path} STREQUAL "iwyu_path-NOTFOUND")
  message(STATUS "include-what-you-use not found!")
else()
  message(STATUS "Found iwyu: ${iwyu_path}")
  vtk_module_set_properties(
    ${module_name} CXX_INCLUDE_WHAT_YOU_USE "${iwyu_path};-Xiwyu;--mapping_file=${CMAKE_SOURCE_DIR}/libcxx.imp"
  )
endif()
