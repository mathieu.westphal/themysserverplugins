/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkHyperTreeGridFragmentation.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class   vtkHyperTreeGridFragmentationInternal
 * @brief   Internal method of vtkHyperTreeGridFragmentation
 *
 * @sa
 * vtkHyperTreeGridFragmentationInternal vtkHyperTreeGrid
 * vtkHyperTreeGridAlgorithm
 *
 * @par Thanks:
 * This work was realized by Jacques-Bernard Lekien in 2023 for Commissariat a
 * l'Energie Atomique CEA, DAM, DIF, F-91297 Arpajon, France.
 */

#ifndef VTK_HYPER_TREE_GRID_FRAGMENTATION_INTERNAL_H
#define VTK_HYPER_TREE_GRID_FRAGMENTATION_INTERNAL_H

#include <array>  // for array
#include <map>    // for map
#include <vector> // for vector

#include <vtkType.h> // for vtkIdType

#include "vtkHyperTreeGridFragmentation.h" // for vtkHyperTreeGridFragmenta...

class vtkBitArray;    // lines 32-32
class vtkDoubleArray; // lines 33-33
class vtkHyperTreeGrid;
class vtkHyperTreeGridNonOrientedGeometryCursor;
class vtkHyperTreeGridNonOrientedVonNeumannSuperCursor;
class vtkMultiProcessController;
class vtkPoints;            // lines 34-34
class vtkUnsignedCharArray; // lines 35-35
//-------------------------------------------------------------------------------------------------
class vtkHyperTreeGridFragmentation::vtkInternal
{
public:
  //------------------------------------------------------------------------------------------------
  /**
   * @brief Constructeur.
   */
  vtkInternal(vtkMultiProcessController* _controller);

  //------------------------------------------------------------------------------------------------
  /**
   * @brief Destructeur.
   */
  virtual ~vtkInternal();

  //------------------------------------------------------------------------------------------------
  /**
   * @brief Print.
   */
  void PrintSelf();

  //------------------------------------------------------------------------------------------------
  /**
   * @brief HasController.
   *
   * Say if has MPI MultiProcess controller.
   *
   * @return true: if exist MPI MultiProcess controller
   * @return false: if not exist MPI MultiProcess controller
   */
  bool HasController() const;

  //------------------------------------------------------------------------------------------------
  /**
   * @brief GetController.
   *
   * Get Current Multi Process Controller.
   *
   * @return: current MPI multi process controller
   */
  vtkMultiProcessController* GetController(); // non const cause VTK

  //------------------------------------------------------------------------------------------------
  /**
   * @brief GetNumberOfProcesses.
   *
   * Get number of processes/servers MPI.
   *
   * @return: number of processes/servers MPI
   */
  int GetNumberOfProcesses() const;

  //------------------------------------------------------------------------------------------------
  /**
   * @brief GetMyRank.
   *
   * Get MPI rank of current processes/servers.
   *
   * @return: current rank MPI
   */
  int GetMyRank() const;

  //------------------------------------------------------------------------------------------------
  /**
   * @brief Initialisation.
   *
   * Nettoie et exploite les informations globales du maillage.
   *
   * @param _input: le maillage en entrée du filtre
   */
  void Initialize(vtkHyperTreeGrid* _input);

  //------------------------------------------------------------------------------------------------
  /**
   * @brief Récupère le champ de masse.
   *
   * En fonction des options de l'utilisateur, activation du champ de masse et
   * la définition du nom d'un champ, on récupère le champ de valeurs
   * correspondant à la sémantique d'un champ de masse.
   *
   * @param _input: le maillage en entrée du filtre
   * @param _enable: l'activation ou non de l'emploi de ce nom de champ
   * @param _name: le nom d'un champ fourni par l'utilisateur ayant une
   * sémantique de masse
   */
  void RetrieveMassInput(vtkHyperTreeGrid* _input, const bool _enable,
                         const char* _name);

  //------------------------------------------------------------------------------------------------
  /**
   * @brief Récupère le champ de densité.
   *
   * En fonction des options de l'utilisateur, activation du champ de densité et
   * la définition du nom d'un champ, on récupère le champ de valeurs
   * correspondant à la sémantique d'un champ de densité.
   *
   * @param _input: le maillage en entrée du filtre
   * @param _enable: l'activation ou non de l'emploi de ce nom de champ
   * @param _name: le nom d'un champ fourni par l'utilisateur ayant une
   * sémantique de densité
   */
  void RetrieveDensityInput(vtkHyperTreeGrid* _input, const bool _enable,
                            const char* _name);

  //------------------------------------------------------------------------------------------------
  /**
   * @brief Récupère le champ de vitesse.
   *
   * En fonction des options de l'utilisateur, activation du champ de vitesse et
   * la définition du nom d'un champ, on récupère le champ de valeurs
   * correspondant à la sémantique d'un champ de vitesse.
   *
   * @param _input: le maillage en entrée du filtre
   * @param _enable: l'activation ou non de l'emploi de ce nom de champ
   * @param _name: le nom d'un champ fourni par l'utilisateur ayant une
   * sémantique de vitesse
   */
  void RetrieveVelocityInput(vtkHyperTreeGrid* _input, const bool _enable,
                             const char* _name);

  //------------------------------------------------------------------------------------------------
  /**
   * @brief IsMasked.
   *
   * Indique si la cellule du maillage identifée par _id est masquée.
   * Cela dépend déjà de l'existant d'un masque associé à ce maillage puis de la
   * valeur de ce masque.
   *
   * @param _id: identifiant de la cellule dans l'HyperTreeGrid
   * @return true: si la cellule est masquée
   * @return false: si la cellule n'est pas masquée
   */
  bool IsMasked(const vtkIdType _id);

  //------------------------------------------------------------------------------------------------
  /**
   * @brief IsGhosted.
   *
   * Indique si la cellule du maillage identifée par _id est une cellule
   * fantôme, c'est à dire une cellule rajoutée par le filtre
   * vtkHyperTreeGridGhosCellsGenerator afin de pouvoir exploiter sur les
   * cellules locales les cellules voisines. Cela dépend déjà de l'existant d'un
   * tel masque associé à ce maillage puis de la valeur de ce masque.
   *
   * @param _id: identifiant de la cellule dans l'HyperTreeGrid
   * @return true: si la cellule est fantôme
   * @return false: si la cellule n'est pas fantôme
   */
  bool IsGhosted(const vtkIdType _id);

  //------------------------------------------------------------------------------------------------
  /**
   * @brief GetNumberOfChildren.
   *
   * Retourne le nombre de cellules filles d'une cellule mére.
   * Ce choix dépend du facteur de raffinement (f) associé globalement au
   * mailage d'input HyperTreeGrid ainsi que de son nombre de dimension spatial
   * (d) : f^d. Cette valeur est la même pour toutes les cellules mères /
   * grossières du maillage. L'utilisation d'une templaitisation dépend de f et
   * d permettrait d'éviter de nombreux tests récurrents à un maillage donné
   * mais engendrerait une multiplication des versions du source compilées
   * impactant la taille du logicel qui est déjà imposant. C'est pourquoi ce
   * choix n'a pas été retenu.
   *
   * @return: le nombre de cellules filles produite par une cellule mère
   */
  unsigned int GetNumberOfChildren() const;

  //------------------------------------------------------------------------------------------------
  /**
   * @brief GetNumberOfDimension.
   *
   * Retourne le nombre de dimension spatial du mailage en input 1, 2 ou 3.
   *
   * @return: le nombre de dimension spatial
   */
  unsigned int GetNumberOfDimension() const;

  //------------------------------------------------------------------------------------------------
  /**
   * @brief HasInputMass.
   *
   * Informe qu'il existe un champ de masse à partir du moment que l'utilisateur
   * a fourni un nom de champ de masse ou de densité (dont on obtient la masse
   * en multipliant la valeur par le volume).
   *
   * @return true: si existe
   * @return false: si il n'existe pas
   */
  bool HasInputMass() const;

  //------------------------------------------------------------------------------------------------
  /**
   * @brief HasInputDensity.
   *
   * Informe qu'il existe un champ de densité seulement si l'utilisateur en a
   * fourni le nom.
   *
   * @return true: si existe
   * @return false: si il n'existe pas
   */
  bool HasInputDensity() const;

  //------------------------------------------------------------------------------------------------
  /**
   * @brief HasInputVelocity.
   *
   * Informe qu'il existe un champ de vitesse seulement si l'utilisateur en a
   * fourni le nom.
   *
   * @return true: si existe
   * @return false: si il n'existe pas
   */
  bool HasInputVelocity() const;

  //------------------------------------------------------------------------------------------------
  /**
   * @brief ComputeField.
   *
   * ComputeField calcule la contribution par la cellule courante pointée par le
   * supercursor aux différents champs globaux du fragment auquel est associée
   * cette cellule.
   *
   * Cette méthode associe la cellule courante à un fragment puis appelle
   * ComputeOutputVolume, ComputeOutputMass, ComputeOutputCenters,
   * ComputeOutputVelocity puis ComputeNeighboringCell afin d'identifier les
   * fragments qui sont voisins et qui ne font qu'un. Pour finir, cette méthode
   * renseigne m_is_skin_celldata qui permet de savoir si une cellule est de
   * bord c'est à dire qu'une de ses cellules voisines n'existe pas ou est
   * masquée.
   *
   * @param _supercursor:
   */
  void
  ComputeField(vtkHyperTreeGridNonOrientedVonNeumannSuperCursor* _supercursor);

  //------------------------------------------------------------------------------------------------
  /**
   * @brief ComputeAverageField.
   *
   * ComputeAverageField finalise le calcul des différents champs globaux du
   * fragment à travers des appels à la méthode OneComputeAverageField pour
   * calculer la vitesse et le centre, suivant le calcul de la moyenne pondérée
   * par la masse ou non .
   */
  void ComputeAverageField();

  //------------------------------------------------------------------------------------------------
  /**
   * @brief GetOutputPounds.
   *
   * @return: retourne le tableau de valeurs associé au poids attribué à chacun
   * des fragments
   */
  const std::map<unsigned int, unsigned long>& GetOutputPounds() const;

  //------------------------------------------------------------------------------------------------
  /**
   * @brief GetOutputVolume.
   *
   * @return: retourne le tableau de valeurs associé au volume attribué à chacun
   * des fragments
   */
  const std::map<unsigned int, double>& GetOutputVolume() const;

  //------------------------------------------------------------------------------------------------
  /**
   * @brief GetOutputMass.
   *
   * @return: retourne le tableau de valeurs associé à la masse attribué à
   * chacun des fragments
   */
  const std::map<unsigned int, double>& GetOutputMass() const;

  //------------------------------------------------------------------------------------------------
  /**
   * @brief GetOutputAvgCenter.
   *
   * @return: retourne le tableau de valeurs associé à la vitesse moyenne
   * attribué à chacun des fragments
   */
  const std::map<unsigned int, std::array<double, 3>>&
  GetOutputAvgCenter() const;

  //------------------------------------------------------------------------------------------------
  /**
   * @brief GetOutputBarycenter.
   *
   * @return: retourne le tableau de valeurs associé au barycentre attribué à
   * chacun des fragments
   */
  const std::map<unsigned int, std::array<double, 3>>&
  GetOutputBarycenter() const;

  //------------------------------------------------------------------------------------------------
  /**
   * @brief GetOutputVelocity.
   *
   * @return: retourne le tableau de valeurs associé à la vitesse attribué à
   * chacun des fragments
   */
  const std::map<unsigned int, std::array<double, 3>>&
  GetOutputVelocity() const;

  //------------------------------------------------------------------------------------------------
  /**
   * @brief GetOutputAvgVelocity.
   *
   * @return: retourne le tableau de valeurs associé à la vitesse moyenne
   * attribué à chacun des fragments
   */
  const std::map<unsigned int, std::array<double, 3>>&
  GetOutputAvgVelocity() const;

  //------------------------------------------------------------------------------------------------
  /**
   * @brief GetOutputLocal.
   *
   * @return: retourne le tableau de valeurs associé à l'affectation local d'un
   * fragment
   */
  const std::map<unsigned int, bool>& GetOutputLocal() const;

  //------------------------------------------------------------------------------------------------
  /**
   * @brief ComputeOneFormFactorRadius.
   *
   * Calcul le rayon du facteur de forme sphérique pour un fragment à partir du
   * volume de ce dernier.
   *
   * @param _iFrag: l'index du fragment
   * @return: le résultat du calcul du rayon
   */
  double ComputeOneFormFactorRadius(unsigned int _iFrag) const;

  //------------------------------------------------------------------------------------------------
  /**
   * @brief ComputeFormFactorRadius.
   *
   * Calcul le rayon du facteur de forme sphérique pour tous les fragments en
   * faisant appel à ComputeOneFormFactorRadius.
   *
   * @param _indexation_frag_continuous_to_uncontinuous: le tableau permettant
   * de passer de l'indexation continue finale à l'indexation non continue
   * produite par le process de calcul.
   * @param _radius: le tableau de valeurs représentant le rayon du facteur de
   * forme sphérique associé à chacun des fragments
   */
  void ComputeFormFactorRadius(std::map<unsigned int, double>& _radius) const;

  //------------------------------------------------------------------------------------------------
  /**
   * @brief ComputeOneDensity.
   *
   * Calcul la densité pour un fragment à partir du calcul de la masse et du
   * volume de ce dernier.
   *
   * @param _iFrag: l'index du fragment
   * @return: le résultat du calcul de la densité
   */
  double ComputeOneDensity(const unsigned int _iFrag) const;

  //------------------------------------------------------------------------------------------------
  /**
   * @brief ComputeDensity.
   *
   * Calcul la densité pour tous les fragments en faisant appel à
   * ComputeOneDensity.
   *
   * @param _density: le tableau de valeurs représentant la densité associée à
   * chacun des fragments
   */
  void ComputeDensity(std::map<unsigned int, double>& _density) const;

  //--------------------------------------------------------------------------------------------------
  /**
   * @brief CompactFragId.
   *
   * Cette methode realise une compaction des fragments locaux afin de les
   * regrouper de 0 à N-1
   */
  unsigned int CompactFragId();

  //--------------------------------------------------------------------------------------------------
  /**
   * @brief ShiftFragId.
   *
   * Cette méthode décalle de la valeur de _shift les indices des fragments
   *
   * @param _shift: la valeur du shift a appliquer sur les indices des fragments
   */
  void ShiftFragId(const unsigned int _shift);

  //--------------------------------------------------------------------------------------------------
  /**
   * @brief ComputeLocalFragId.
   *
   * Cette méthode compacte les fragments à la façon de CompactFragId mais après
   * qu'une décallage des indices a été appliqué (ShiftFragId) et que l'on ait
   * identifié localement des fragments voisins à travers l'application d'un
   * GhostCellGenerator.
   *
   * @param _shift: la valeur du shift a appliquer sur les indices des fragments
   * @param _nbLocalFrags: le nombre de fragments locaux avant application du
   * _sameFrag
   * @param _sameFrag: la tableau d'équivalence qui amène à reverser le contenu
   * d'un fragment iFrag dans un fragment d'indice inférieur _sameFrag[iFrag].
   */
  void
  ComputeLocalFragId(const unsigned int _shift,
                     const unsigned int _nbLocalFrags,
                     std::map<unsigned int, unsigned int>& _sameFrag) const;

  //--------------------------------------------------------------------------------------------------
  /**
   * @brief ShiftFragId.
   *
   * Cette méthode réalise un décallage des indices de fragments suivant la
   * table d'indexation passée en paramètre.
   *
   * @param _frag2frag: la tableau d'indexation qui fait passer d'un indice de
   * fragment à un autre Cette table a été construite par tous les processus
   * afin d'avoir une indexation unique tenant compte du fait qu'il faille
   * obtenir une indexation continue globalement.
   */
  void ShiftFragId(const std::vector<unsigned int>& _frag2frag);

  //--------------------------------------------------------------------------------------------------
  /**
   * @brief AllReduceAllGlobalField.
   *
   * Cette méthode a pour objet d'accumuler les valeurs globales en cours de
   * sommation pouvant venir intialement d'un meme fragment déclaré sur des
   * serveurs différents.
   *
   * @param _nbAllFrags: le nombre total de fragments décrits sur tous les
   * processus
   */
  void AllReduceAllGlobalField(const unsigned int _nbAllFrags);

  //--------------------------------------------------------------------------------------------------
  /**
   * @brief HasSkinCell.
   *
   * Retourne si une cellule est de bord / peau c'est à dire à au moins une
   * cellule voisine de non définie ou de masquée.
   *
   * @param _id: identifiant de la cellule dans l'HyperTreeGrid
   * @return true: si la cellule est fantôme
   * @return false: si la cellule n'est pas fantôme
   */
  bool HasSkinCell(const vtkIdType _id) const;

  //--------------------------------------------------------------------------------------------------
  /**
   * @brief AddOutputPoints.
   *
   * Ajoute la cellule pointée par le curseur à la description fine du fragment.
   * L'appel dépendra de l'option d'extraction choisi par l'utilisateur.
   * On utilise ici un curseur et un non supercurseur car nous n'avons pas
   * besoin de l'information de voisinage, nous ne faisons qu'exploiter un champ
   * sur chaque cellule. On aurait pu construire un champ pour dire si une
   * cellule était feuille ou non mais le parcours non dirigé des cellules
   * pourraient ne pas être performant dans le cas où l'utilisateur aurait
   * appliqué un filtre qui produit un masque (comme un threshold) avant
   * l'application du filtre vtkHyperTreeGridFragmentation.
   *
   * @param _cursor:
   * @param _indexation_frag_uncontinuous_to_continuous: le tableau d'indexation
   * permettant de passer de la numérotation discontinue d'un fragment à
   * l'indexation continue finale.
   */
  void AddOutputPoints(vtkHyperTreeGridNonOrientedGeometryCursor* _cursor);

  //------------------------------------------------------------------------------------------------
  /**
   * @brief GetOutputPoints.
   *
   * Retourne le nuage de points correspondant à l'extraction demandé par
   * l'utilisateu, ici pour un fragment donné.
   *
   * @param _iContinuousFrag: l'index continue du fragment à retourner
   * @return: le nuage de points correspond à l'extraction d'un fragment
   */
  vtkPoints* GetOutputPoints(const unsigned int _iContinuousFrag);

  //--------------------------------------------------------------------------------------------------
  /**
   * @brief GetOutputFragByCells.
   *
   * @return: retourne le tableau de valeurs associé décrivant l'identifiant du
   * fragment attribué à chacune des cellules
   */
  const std::vector<unsigned int>& GetOutputFragByCells() const;

  //------------------------------------------------------------------------------------------------
  /**
   * @brief FinalizeOutputPoints.
   *
   * Finalise la construction des vtkPoints par fragment en libérant la mémoire.
   */
  void FinalizeOutputPoints();

  //------------------------------------------------------------------------------------------------
  /**
   * Finalize.
   */
  void Finalize();

protected:
  //------------------------------------------------------------------------------------------------
  /**
   * Cleaner.
   *
   * Méthode de nettoyage qui est appliquée en debut et en fin de traitement
   * afin de s'assurer de bien repartir dans un contexte propre.
   */
  void Cleaner();

  //------------------------------------------------------------------------------------------------
  /**
   * NewFragment.
   *
   * Méthode de construction d'un nouveau fragment à partir de l'identifiant de
   * la cellule dans l'HyperTreeGrid et de l'index non continue de ce nouveau
   * fragment.
   *
   * @param _id: identifiant de la cellule
   * @param _fragId: identifiant de ce nouveau fragment
   */
  void NewFragment(const vtkIdType _id, const unsigned int _fragId);

  //------------------------------------------------------------------------------------------------
  /**
   * AccFragment.
   *
   * Méthode d'accumultation d'un fragment vers un autre. Elle est appelée
   * lorsqu'on identifie que deux fragments sont voisins alors on accumule les
   * calculs partielles des champs globaux d'un fragment vers l'autre.
   *
   * @param _fragIdAcc: identifiant du fragment sur lequel on accumule
   * @param _fragId: identifiant du fragment voisin qu'on va accumuler
   */
  void AccFragment(const unsigned int _fragIdAcc, const unsigned int _fragId);

  //------------------------------------------------------------------------------------------------
  /**
   * CleanFragment.
   *
   * Une fois que le contenu d'un fragment a été accumulé vers un autre, on le
   * vide de ses valeurs.
   *
   * @param _fragId: identifiant du fragment voisin qu'on va accumuler
   */
  void CleanFragment(const unsigned int _fragId);

  //------------------------------------------------------------------------------------------------
  /**
   * @brief Récupère un champ scalaire d'entrée.
   *
   * En fonction des options de l'utilisateur, activation d'un champ scalaire et
   * la définition du nom d'un champ, on récupère le champ scalaire de valeurs
   * correspondant à une sémantique particulière.
   *
   * @param _input: le maillage en entrée du filtre
   * @param _enable: l'activation ou non de l'emploi de ce nom de champ scalaire
   * @param _name: le nom d'un champ fourni par l'utilisateur ayant la
   * sémantique attendue
   */
  vtkDoubleArray* RetrieveScalarInput(vtkHyperTreeGrid* _input,
                                      const bool _enable,
                                      const char* _name) const;

  //------------------------------------------------------------------------------------------------
  /**
   * @brief Récupère un champ vectoriel d'entrée.
   *
   * En fonction des options de l'utilisateur, activation d'un champ vectoriel
   * et la définition du nom d'un champ, on récupère le champ vectoriel de
   * valeurs correspondant à une sémantique particulière.
   *
   * @param _input: le maillage en entrée du filtre
   * @param _enable: l'activation ou non de l'emploi de ce nom de champ
   * vectoriel
   * @param _name: le nom d'un champ fourni par l'utilisateur ayant la
   * sémantique attendue
   */
  vtkDoubleArray* RetrieveVectorInput(vtkHyperTreeGrid* _input,
                                      const bool _enable,
                                      const char* _name) const;

  //--------------------------------------------------------------------------------------------------
  /**
   * @brief AllReduceOneGlobalField.
   *
   * ... _frag2frag
   *
   */
  template <class T>
  void AllReduceOneGlobalField(const unsigned int _nbAllFrags,
                               std::map<unsigned int, T>& _values);

  //--------------------------------------------------------------------------------------------------
  /**
   * @brief AllReduceOneGlobalFieldVector.
   *
   * ... _frag2frag
   *
   */
  void AllReduceOneGlobalFieldVector(
      const unsigned int _nbAllFrags,
      std::map<unsigned int, std::array<double, 3>>& _values);

  //------------------------------------------------------------------------------------------------
  /**
   * @brief Calcul le volume.
   *
   * Le calcul du volume de la cellule se fait en exploitant l'information
   * retournée par GetSize() au niveau de cette cellule ciblée par le
   * supercurseur. On accumule cette valeur à la valeur globale définie pour le
   * fragment concerné dont fait partie cette cellule.
   *
   * @param _supercursor:
   * @param _fragId: index non continue pointant un fragment valide dont fait
   *   partie cette cellule
   * @return: retourne la valeur du volume pour cette cellule
   */
  double ComputeOutputVolume(
      vtkHyperTreeGridNonOrientedVonNeumannSuperCursor* _supercursor,
      const unsigned int _fragId);

  //------------------------------------------------------------------------------------------------
  /**
   * @brief Calcul la masse.
   *
   * On accumule la valeur de la masse correspondant à la cellule ciblée par le
   * supercurseur à la valeur globale définie pour le fragment concerné dont
   * fait partie cette cellule.
   *
   * Dans le cas où le champ de masse n'a pas été positionné par l'utilisateur,
   * on peut exploiter le champ de densité (si celui ci a été défini) pour
   * calculer la valeur de la masse de la cellule afin de l'accumuler.
   *
   * @param _cellVolume: valeur du volume pour cette cellule
   * @param _id: index de la cellule dans l'input
   * @param _fragId: index non continue pointant un fragment valide dont fait
   *   partie cette cellule
   * @return: retourne la valeur de la masse pour cette cellule
   */
  double ComputeOutputMass(const double _cellVolume, const vtkIdType _id,
                           const unsigned int _fragId);

  //------------------------------------------------------------------------------------------------
  /**
   * @brief Calcul des centres.
   *
   * Il existe deux calculs du centre :
   * - le centre moyen qui est l'accumulation des positions des centres dans
   * chacun des champs globaux à chacun des fragments que l'on divisera par la
   * suite par le nombre de valeurs additionné ;
   * - le barycentre qui est l'accumulation des positions multipliées par la
   * masse dans chacun des champs globaux à chacun des fragments que l'on
   * divisiera par la suite par la masse propre à chaque fragment.
   *
   * Ce dernier n'est pas calculé si l'utilisateur a passé une masse nulle.
   *
   * @param _supercursor:
   * @param _cellMass: valeur de la masse pour cette cellule
   * @param _fragId: index non continue pointant un fragment valide dont fait
   *   partie cette cellule
   */
  void ComputeOutputCenters(
      vtkHyperTreeGridNonOrientedVonNeumannSuperCursor* _supercursor,
      const double _cellMass, const unsigned int _fragId);

  //------------------------------------------------------------------------------------------------
  /**
   * @brief Calcul la vitesse.
   *
   * Il existe deux calculs de la vitesse :
   * - la vitesse moyenne qui est l'accumulation des vitesses dans chacun des
   *   champs globaux à chacun des fragments que l'on divisera par la suite par
   * le nombre de valeurs additionné ;
   * - la vitesse qui est l'accumulation des vitesses multipliées par la masse
   * dans chacun des champs globaux à chacun des fragments que l'on divisiera
   * par la suite par la masse propre à chaque fragment.
   *
   * Ce dernier n'est pas calculé si l'utilisateur a passé une masse nulle.
   *
   * @param _cellMass: valeur de la masse pour cette cellule
   * @param _id: index de la cellule dans l'input
   * @param _fragId: index non continue pointant un fragment valide dont fait
   *   partie cette cellule
   */
  void ComputeOutputVelocity(const double _cellMass, const vtkIdType _id,
                             const unsigned int _fragId);

  //------------------------------------------------------------------------------------------------
  /**
   * @brief Calcul relatif aux cellules voisines.
   *
   * Si la cellule voisine ne fait pas partie d'un fragment alors on l'attribue
   * à celui-ci en reportant à plus part la prise en compte de sa contribution
   * dans ce fragment.
   *
   * Si la cellule voisine fait partie du même fragment on ne fait rien de plus.
   *
   * Si la cellule voisine fait partie d'un autre fragment alors on attribue
   * toutes les cellules de cet autre fragment à celui en cours puis on accumule
   * dans le fragment en cours la valeur de cet autre fragment avant de le
   * réinitialiser (sans plus jamais l'exploiter par la suite).
   *
   * @param _idN : index de la cellule voisine dans l'input
   * @param _fragId: index non continue pointant un fragment valide dont fait
   *   partie cette cellule
   */
  void ComputeNeighboringCell(const vtkIdType _idN, const unsigned int _fragId);

private:
  vtkMultiProcessController* m_controller = nullptr;

  int m_number_of_processes{0};
  int m_my_rank{0};

  unsigned int m_number_of_children{0};
  unsigned int m_number_of_dimension{0};

  vtkBitArray* m_in_mask{nullptr};
  vtkUnsignedCharArray* m_in_ghost{nullptr};

  // Input
  vtkDoubleArray* m_input_mass{nullptr};
  vtkDoubleArray* m_input_density{nullptr};
  vtkDoubleArray* m_input_velocity{nullptr};

  // Fragments :
  // L'index du prochain fragment, on parle de non continu car au cours
  // du traitement des numéros ne seront plus utilisés
  unsigned int m_crt_uncontinuous_frag{0};

  // Association par cellule du maillage :
  // Le numéro du fragment auquel est associé la cellule, cela ne concerne
  // que les cellules filles non masquées
  std::vector<unsigned int> m_frag_id_celldata;
  // Indique si une cellule est de bord/peau, c'est a dire a au moins une
  // cellule voisine non définie ou masquée
  std::vector<bool> m_is_skin_celldata;

  // Information globale par fragment :
  // Dimensionne au nombre de fragments suivant une numeration discontinue
  // Cette dimension vaut m_crt_uncontinuous_frag
  std::map<unsigned int, unsigned long> m_output_pounds;
  std::map<unsigned int, double> m_output_volume;
  std::map<unsigned int, double> m_output_mass;
  std::map<unsigned int, double> m_output_density;
  std::map<unsigned int, std::array<double, 3>> m_output_avg_center;
  std::map<unsigned int, std::array<double, 3>> m_output_barycenter;
  std::map<unsigned int, std::array<double, 3>> m_output_avg_velocity;
  std::map<unsigned int, std::array<double, 3>> m_output_velocity;
  // Indique si le fragment doit etre gere par le server local
  std::map<unsigned int, bool> m_output_local;

  // Output
  // Dimensionne au nombre de fragments suivant une numerotation continue
  std::map<unsigned int, vtkPoints*> m_output_points;
};

#endif // VTK_HYPER_TREE_GRID_FRAGMENTATION_INTERNAL_H
