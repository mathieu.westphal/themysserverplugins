/*=========================================================================

  Program:   Themys
  Module:    vtkContourWriter.h

  Copyright (c) G. Peillex
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// IWYU pragma: no_include "vtkSystemIncludes.h"
#include "vtkContourWriter.h"

#include <array>       // for array
#include <fstream>     // IWYU pragma: keep // for basic_ofstream
#include <iomanip>     // for operator<<, setprecision
#include <ostream>     // for operator<<, basic_ostream
#include <sstream>     // IWYU pragma: keep
#include <string>      // for char_traits, basic_string
#include <string_view> // for basic_string_view

#include <vtkAlgorithm.h>              // for vtkAlgorithm
#include <vtkAppendPolyData.h>         // for vtkAppendPolyData
#include <vtkCell.h>                   // for vtkCell
#include <vtkCleanPolyData.h>          // for vtkCleanPolyData
#include <vtkCommunicator.h>           // for vtkCommunicator
#include <vtkCompositeDataIterator.h>  // for vtkCompositeDataIterator
#include <vtkCompositeDataSet.h>       // for vtkCompositeDataSet
#include <vtkDataObject.h>             // for vtkDataObject
#include <vtkDataObjectTreeIterator.h> // for vtkDataObjectTreeIterator
#include <vtkDataSet.h>                // for vtkDataSet
#include <vtkDataSetSurfaceFilter.h>   // for vtkDataSetSurfaceFilter
#include <vtkFeatureEdges.h>           // for vtkFeatureEdges
#include <vtkIndent.h>                 // for operator<<, vtkIndent
#include <vtkInformation.h>            // for vtkInformation
#include <vtkInformationVector.h>      // for vtkInformationVector
#include <vtkLogger.h>                 // for vtkLogger, vtkLogger::...
#include <vtkMultiProcessController.h> // for vtkMultiProcessController
#include <vtkNew.h>                    // for vtkNew
#include <vtkObjectFactory.h>          // for vtkStandardNewMacro
#include <vtkPVLogger.h>               // for PARAVIEW_LOG_PLUGIN_VERBOSITY
#include <vtkPolyData.h>               // for vtkPolyData
#include <vtkReductionFilter.h>        // for vtkReductionFilter
#include <vtkSmartPointerBase.h>       // for operator==
#include <vtkStreamingDemandDrivenPipeline.h> // for vtkStreamingDemandDriv...
#include <vtkStripper.h>                      // for vtkStripper

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// clang-format off
vtkStandardNewMacro(vtkContourWriter)

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
vtkContourWriter::vtkContourWriter()
    : FileName{nullptr}, GhostLevel{0}, Controller{nullptr},
      NumberOfProcesses{0}, MyRank{0}
{
}
// clang-format on

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void vtkContourWriter::PrintSelf(std::ostream& ostr, vtkIndent indent)
{
  this->Superclass::PrintSelf(ostr, indent);
  ostr << indent << "FileName ";
  if (this->FileName == nullptr)
  {
    ostr << "(none)";
  } else
  {
    ostr << this->FileName;
  }
  ostr << std::endl;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
vtkTypeBool vtkContourWriter::ProcessRequest(vtkInformation* request,
                                             vtkInformationVector** inputVector,
                                             vtkInformationVector* outputVector)
{
  if (request->Has(vtkStreamingDemandDrivenPipeline::REQUEST_UPDATE_EXTENT()) !=
      0)
  {
    return this->RequestUpdateExtent(request, inputVector, outputVector);
  }

  return this->Superclass::ProcessRequest(request, inputVector, outputVector);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
int vtkContourWriter::RequestUpdateExtent(vtkInformation*,
                                          vtkInformationVector** inputVector,
                                          vtkInformationVector*)
{
  auto* global_controller = vtkMultiProcessController::GetGlobalController();
  if (global_controller != nullptr)
  {
    this->Controller = global_controller;
    this->NumberOfProcesses = global_controller->GetNumberOfProcesses();
    this->MyRank = global_controller->GetLocalProcessId();

    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    vtkInformation* info = inputVector[0]->GetInformationObject(0);
    info->Set(vtkStreamingDemandDrivenPipeline::UPDATE_PIECE_NUMBER(),
              this->MyRank);
    info->Set(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_PIECES(),
              this->NumberOfProcesses);
  }

  return 1;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
int vtkContourWriter::FillInputPortInformation(int, vtkInformation* info)
{
  info->Remove(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE());
  info->Append(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkDataSet");
  info->Append(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkCompositeDataSet");
  return 1;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void vtkContourWriter::WriteData()
{
  if (!this->openFileForWriting())
  {
    return;
  }

  auto* composite_ds{vtkCompositeDataSet::SafeDownCast(this->GetInput())};

  if (composite_ds != nullptr)
  {
    if (!this->allProcessesSeeTheSameNumberOfLeaves(composite_ds))
    {
      return;
    }

    this->walkThroughCompositeDataSetTree(composite_ds);
  } else
  {
    auto* single_ds{vtkDataSet::SafeDownCast(this->GetInput())};
    this->processDataSet(single_ds);
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void vtkContourWriter::walkThroughCompositeDataSetTree(
    vtkCompositeDataSet* comp_ds)
{
  vtkVLogScopeF(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
                "walkThroughCompositeDataSetTree");

  // This lambda is used to get the name of the dataset the iterator is pointing
  // to if it has one.
  const auto get_dataset_name =
      [](vtkDataObjectTreeIterator* iter) -> std::string {
    if (iter->HasCurrentMetaData() == 1 &&
        iter->GetCurrentMetaData()->Has(vtkCompositeDataSet::NAME()) == 1)
    {
      return iter->GetCurrentMetaData()->Get(vtkCompositeDataSet::NAME());
    }
    return {};
  };

  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
          "Walking through composite data set " << comp_ds);

  // vtkDataObjectTreeIterator is necessary to have access to VisitOnlyLeavesOff
  // method to visit all the nodes of the tree.
  // It is not an absolute necessity to visit all nodes but it is so much easier
  // to follow the logic of the code when debugging.
  const auto comp_ds_iter{vtk::TakeSmartPointer(
      vtkDataObjectTreeIterator::SafeDownCast(comp_ds->NewIterator()))};

  if (comp_ds_iter == nullptr)
  {
    vtkErrorMacro(<< "Unable to build a vtkDataObjectTreeIterator!");
  }

  // Visit all nodes, not only leaves
  comp_ds_iter->VisitOnlyLeavesOff();

  for (comp_ds_iter->InitTraversal(); comp_ds_iter->IsDoneWithTraversal() == 0;
       comp_ds_iter->GoToNextItem())
  {
    const auto& current_do_name{get_dataset_name(comp_ds_iter)};
    auto* current_do{comp_ds->GetDataSet(comp_ds_iter)};

    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "The current data object "
                                                 << current_do << " is named "
                                                 << current_do_name);

    auto* current_cds = vtkCompositeDataSet::SafeDownCast(current_do);
    if (current_cds != nullptr)
    {
      vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "It is a CompositeDataSet");
      continue;
    }
    auto* current_ds = vtkDataSet::SafeDownCast(current_do);
    if (current_ds != nullptr)
    {
      vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "It is a DataSet");
      this->processDataSet(current_ds);
    } else
    {
      vtkVLog(vtkLogger::Verbosity::VERBOSITY_WARNING,
              "Unable to write contour for an object different from DataSet "
              "or CompositeDataSet");
    }
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void vtkContourWriter::processDataSet(vtkDataSet* dataset)
{
  vtkVLogScopeF(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "processDataSet");

  auto local_contour = extract_contour(dataset);
  auto global_contour = this->reduceContour(local_contour);
  auto clean_block_contour = clean_up_contour(global_contour);

  if (this->NumberOfProcesses != 1 && this->MyRank != 0)
  {
    return;
  }

  append_polydata_to_contour_file(this->File, clean_block_contour);
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool vtkContourWriter::allProcessesSeeTheSameNumberOfLeaves(
    vtkCompositeDataSet* comp_ds)
{
  vtkVLogScopeF(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
                "allProcessesSeeTheSameNumberOfLeaves");

  // This lambda function is used to get the number of leaves of a composite
  // data set
  const auto get_number_of_leaves = [](vtkCompositeDataSet* cds) {
    auto leaves_iter = vtk::TakeSmartPointer(
        vtkCompositeDataIterator::SafeDownCast(cds->NewIterator()));
    auto nb_leaves{0};
    for (leaves_iter->InitTraversal(); leaves_iter->IsDoneWithTraversal() == 0;
         leaves_iter->GoToNextItem())
    {
      nb_leaves++;
    }
    return nb_leaves;
  };

  if (this->NumberOfProcesses > 1)
  {
    const auto nb_leaves{get_number_of_leaves(comp_ds)};
    auto max_nb_leaves{0};
    this->Controller->AllReduce(&nb_leaves, &max_nb_leaves, 1,
                                vtkCommunicator::MAX_OP);
    auto min_nb_leaves{0};
    this->Controller->AllReduce(&nb_leaves, &min_nb_leaves, 1,
                                vtkCommunicator::MIN_OP);
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "The number of leaves in the composite data set is "
                << max_nb_leaves << " on the largest process and "
                << min_nb_leaves << " on the smallest process and " << nb_leaves
                << " on the current process.");
    if (max_nb_leaves != min_nb_leaves)
    {
      vtkErrorMacro(
          "The number of leaves in the composite data set is not the "
          "same on all processes. The writing of the contour cannot be made "
          "in parallel.");
      return false;
    }
  }
  return true;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
vtkSmartPointer<vtkPolyData> vtkContourWriter::reduceContour(
    const vtkSmartPointer<vtkPolyData>& local_contour)
{
  vtkVLogScopeF(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "reduceContour");

  auto reduced_contour = vtkSmartPointer<vtkPolyData>::New();
  if (this->NumberOfProcesses > 1)
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "Parallel case, reducing the contour");

    auto reduce_filter = vtkNew<vtkReductionFilter>();
    auto reduce_operation = vtkNew<vtkAppendPolyData>();

    reduce_filter->SetController(this->Controller);
    reduce_filter->SetInputDataObject(local_contour);
    reduce_filter->SetPostGatherHelper(reduce_operation);
    reduce_filter->Update();
    reduced_contour = vtkPolyData::SafeDownCast(reduce_filter->GetOutput());
  } else
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "Sequential case, transmitting the contour (no op)");
    reduced_contour = local_contour;
  }

  return reduced_contour;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
bool vtkContourWriter::openFileForWriting()
{
  vtkVLogScopeF(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "openFileForWriting");

  // If in parallel, only the root process can open the file
  if (this->NumberOfProcesses != 1 && this->MyRank != 0)
  {
    vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
            "This process is not the root process, so it will not open the "
            "file.");
    return true;
  }

  if (this->FileName == nullptr)
  {
    vtkErrorMacro(<< "The filename is not set");
    return false;
  }

  if (this->File.is_open())
  {
    this->File.close();
  }

  vtkVLog(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
          "Opening the file " << this->FileName << " for writing the contour.");
  this->File = std::ofstream{this->FileName, std::ios::out | std::ios::trunc};
  return this->File.is_open();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
vtkSmartPointer<vtkPolyData> extract_contour(vtkDataSet* dataset)
{
  vtkVLogScopeF(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "extract_contour");

  vtkNew<vtkDataSetSurfaceFilter> surface_filter_algo;
  vtkNew<vtkFeatureEdges> feature_edge_algo;

  surface_filter_algo->SetInputData(dataset);
  feature_edge_algo->SetInputConnection(surface_filter_algo->GetOutputPort());
  feature_edge_algo->Update();

  auto res = vtk::MakeSmartPointer(
      vtkPolyData::SafeDownCast(feature_edge_algo->GetOutput()));
  return res;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
vtkSmartPointer<vtkPolyData>
clean_up_contour(const vtkSmartPointer<vtkPolyData>& global_contour)
{
  vtkVLogScopeF(PARAVIEW_LOG_PLUGIN_VERBOSITY(), "clean_up_contour");

  vtkNew<vtkCleanPolyData> clean_filter;
  vtkNew<vtkStripper> stripper_filter;
  clean_filter->SetInputDataObject(global_contour);
  stripper_filter->SetInputConnection(clean_filter->GetOutputPort());
  stripper_filter->Update();

  return stripper_filter->GetOutput();
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void append_polydata_to_contour_file(std::ofstream& file_stream,
                                     vtkPolyData* vtkpd,
                                     std::string_view polydata_sep)
{
  vtkVLogScopeF(PARAVIEW_LOG_PLUGIN_VERBOSITY(),
                "append_polydata_to_contour_file");

  constexpr auto precision{10};
  constexpr auto width{12};

  for (const auto& cycle : get_poly_contours(vtkpd))
  {
    for (const auto& point : cycle)
    {
      std::ostringstream res;
      res << std::fixed;
      res << std::showpoint;
      res << std::setprecision(precision);
      res << std::setw(width);
      res << point[0] << " " << point[1] << " " << point[2] << "\n";
      file_stream << res.str();
    }
    file_stream << polydata_sep;
  }
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
Coords get_point_coords(const int node_index, vtkCell* cell, vtkPolyData* vtkpd)
{
  const auto pt_id{cell->GetPointId(node_index)};
  Coords pt_coords{};
  vtkpd->GetPoint(pt_id, pt_coords.data());
  return pt_coords;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
ContourCoords get_cell_contour(const int cell_index, vtkPolyData* vtkpd)
{
  auto* cell{vtkpd->GetCell(cell_index)};
  const auto nb_nodes{cell->GetNumberOfPoints()};
  if (nb_nodes <= 1)
  {
    return {};
  }

  ContourCoords res{};
  for (auto index{0}; index < nb_nodes; ++index)
  {
    const auto& new_pt = get_point_coords(index, cell, vtkpd);
    res.push_back(new_pt);
  }
  return res;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
std::vector<ContourCoords> get_poly_contours(vtkPolyData* vtkpd)
{
  std::vector<ContourCoords> res{};
  const auto cell_nb{vtkpd->GetNumberOfCells()};
  for (auto index{0}; index < cell_nb; ++index)
  {
    const auto& new_pts = get_cell_contour(index, vtkpd);
    res.push_back(new_pts);
  }
  return res;
}
