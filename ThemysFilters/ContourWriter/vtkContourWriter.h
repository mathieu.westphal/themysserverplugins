/*=========================================================================

  Program:   Themys
  Module:    vtkContourWriter.h

  Copyright (c) G. Peillex
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef VTK_CONTOUR_WRITER_H
#define VTK_CONTOUR_WRITER_H

// IWYU pragma: no_include <vtkIOStream.h>
// IWYU pragma: no_include <vtkObject.h>
// IWYU pragma: no_include <fwd>

#include <array>       // for array
#include <ostream>     // IWYU pragma: keep
#include <string>      // IWYU pragma: keep
#include <string_view> // for basic_string_view
#include <vector>      // for vector

#include <vtkABI.h>          // for vtkTypeBool
#include <vtkSetGet.h>       // for vtkGetFilePathMacro, vtkGetMacro
#include <vtkSmartPointer.h> // for vtkSmartPointer
#include <vtkWriter.h>       // for vtkWriter

#include "ContourWriterModule.h" // for CONTOURWRITER_EXPORT

class vtkCell;
class vtkCompositeDataSet;
class vtkDataSet;
class vtkIndent;
class vtkInformation;
class vtkInformationVector;
class vtkMultiProcessController;
class vtkPolyData;

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
class CONTOURWRITER_EXPORT vtkContourWriter : public vtkWriter
{
public:
  static vtkContourWriter* New();

  // clang-format off
  vtkTypeMacro(vtkContourWriter, vtkWriter)

  void PrintSelf(std::ostream &os, vtkIndent indent) override;
  // clang-format on

  vtkSetFilePathMacro(FileName);
  vtkGetFilePathMacro(FileName);

  vtkSetMacro(GhostLevel, int);
  vtkGetMacro(GhostLevel, int);

protected:
  vtkContourWriter();
  virtual ~vtkContourWriter() = default;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Depending on the information stored in the request, calls the
   * RequestUpdateExtent method or delegate to same method in the superclass.
   *
   * @param request : information about the request
   * @param inputVector :  input data
   * @param outputVector : output data
   * @return vtkTypeBool
   */
  /*----------------------------------------------------------------------------*/
  vtkTypeBool ProcessRequest(vtkInformation* request,
                             vtkInformationVector** inputVector,
                             vtkInformationVector* outputVector) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Update the input vector with parallelism informations such as the
   * MPI rank and number of processes.
   *
   * @note: this is a upstream request
   * @param inputVector : input vector to be updated
   * @return int
   */
  /*----------------------------------------------------------------------------*/
  int RequestUpdateExtent(vtkInformation*, vtkInformationVector** inputVector,
                          vtkInformationVector*);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Updates the information with the type of vtkDataObject that are
   * accepted
   *
   * @param info : information to be updated
   * @return int
   */
  /*----------------------------------------------------------------------------*/
  int FillInputPortInformation(int, vtkInformation* info) override;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Writes the data to the file.
   *
   */
  /*----------------------------------------------------------------------------*/
  void WriteData() override;

  /// Name for the output file.
  char* FileName;

  /// @brief Output file stream.
  std::ofstream File;

  /*----------------------------------------------------------------------------*/
  /**
   * @note We never write out ghost cells.  This variable is here to satisfy the
   * behavior of ParaView on invoking a parallel writer.
   */
  /*----------------------------------------------------------------------------*/
  int GhostLevel;

  /// @brief Controller used to communicate between processes.
  vtkMultiProcessController* Controller;

  /// @brief Number of processes.
  int NumberOfProcesses;

  /// @brief Rank of the current process.
  int MyRank;

private:
  vtkContourWriter(const vtkContourWriter&) = delete;
  void operator=(const vtkContourWriter&) = delete;

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Aggregate the polydata from all leafs of the composite dataset tree
   * into a single polydata.
   *
   * @param comp_ds : the composite dataset to be walked through
   * @return vtkSmartPointer<vtkPolyData>
   */
  /*----------------------------------------------------------------------------*/
  void walkThroughCompositeDataSetTree(vtkCompositeDataSet* comp_ds);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Applies different filters on the dataset to extract on ordered
   * contour (vtkPolyData) and writes it down to the output file.
   *
   * @param dataset: dataset from which the contour should be extracted
   */
  /*----------------------------------------------------------------------------*/
  void processDataSet(vtkDataSet* dataset);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return True if all processes see the same number of leaves in the
   * composite dataset tree.
   *
   * @param comp_ds : composite data set tree
   * @return true
   * @return false
   */
  /*----------------------------------------------------------------------------*/
  bool allProcessesSeeTheSameNumberOfLeaves(vtkCompositeDataSet* comp_ds);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Return a polydata that is the union of all the polydata on all
   * processes
   *
   * @note: in sequential, just return what is passed in
   * @param local_contour: the polydata to be aggregated
   * @return vtkSmartPointer<vtkPolyData>
   */
  /*----------------------------------------------------------------------------*/
  vtkSmartPointer<vtkPolyData>
  reduceContour(const vtkSmartPointer<vtkPolyData>& local_contour);

  /*----------------------------------------------------------------------------*/
  /**
   * @brief Open the output file for writing
   *
   * @return true : on success
   * @return false : otherwise
   */
  /*----------------------------------------------------------------------------*/
  bool openFileForWriting();
};

/*----------------------------------------------------------------------------*/
/**
 * @brief Given a vtkDataSet, extract the contour and return it as a vtkPolyData
 *
 * @param dataset : the dataset to be processed
 * @return vtkSmartPointer<vtkPolyData>
 */
/*----------------------------------------------------------------------------*/
vtkSmartPointer<vtkPolyData> extract_contour(vtkDataSet* dataset);

/*----------------------------------------------------------------------------*/
/**
 * @brief Return a polydata that has been cleaned up and stripped
 *
 * Necessary to have ordered points in the polydata
 *
 * @param global_contour: contour to be cleaned up
 * @return vtkSmartPointer<vtkPolyData>
 */
/*----------------------------------------------------------------------------*/
vtkSmartPointer<vtkPolyData>
clean_up_contour(const vtkSmartPointer<vtkPolyData>& global_contour);

/*----------------------------------------------------------------------------*/
/**
 * @brief Given a vtkPolyData, write it to a file
 *
 * @param filename : name of the file to be written
 * @param vtkpd : the polydata to be written
 * @param polydata_sep : separator between polydata
 * @return true : if the writing is successful
 * @return false : otherwise
 */
/*----------------------------------------------------------------------------*/
void append_polydata_to_contour_file(std::ofstream& file_stream,
                                     vtkPolyData* vtkpd,
                                     std::string_view polydata_sep = "&\n");

using Coords = std::array<double, 3>;
using ContourCoords = std::vector<Coords>;

/*----------------------------------------------------------------------------*/
/**
 * @brief Get the coordinates of the point
 *
 * @param node_index : index of the node in the cell
 * @param cell : cell the node belongs to
 * @param vtkpd : polydata the cell belongs to
 * @return Coords
 */
/*----------------------------------------------------------------------------*/
Coords get_point_coords(const int node_index, vtkCell* cell,
                        vtkPolyData* vtkpd);

/*----------------------------------------------------------------------------*/
/**
 * @brief Get the coordinates of all the points that define the cell
 *        (i.e the contour of the cell)
 *
 * @param cell_index : index of the cell
 * @param vtkpd : polydata the cell belongs to
 * @return ContourCoords
 */
/*----------------------------------------------------------------------------*/
ContourCoords get_cell_contour(const int cell_index, vtkPolyData* vtkpd);

/*----------------------------------------------------------------------------*/
/**
 * @brief Get the contour of each of the cells that define the polydata
 *
 * @param vtkpd : polydata
 * @return std::vector<ContourCoords>
 */
/*----------------------------------------------------------------------------*/
std::vector<ContourCoords> get_poly_contours(vtkPolyData* vtkpd);
#endif // VTK_CONTOUR_WRITER_H
