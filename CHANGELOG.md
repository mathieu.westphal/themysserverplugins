# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

Guiding Principles:

- Changelogs are for humans, not machines.
- There should be an entry for every single version.
- The same types of changes should be grouped.
- Versions and sections should be linkable.
- The latest version comes first.
- The release date of each version is displayed.
- Mention whether you follow Semantic Versioning.

Types of changes:

- **Added** for new features.
- **Changed** for changes in existing functionality.
- **Deprecated** for soon-to-be removed features.
- **Removed** for now removed features.
- **Fixed** for any bug fixes.
- **Security** in case of vulnerabilities.

## Unreleased

### Fixed

- Fixed a very rare issue where the FaseDecimateSurfaceRepresentation plugin would crash ParaView

### Added

### Changed

- Changes the repository of the image used for CI

## Version 1.0.7 - 2024-04-02 -- Paraview needed at commit 8f4e48bc or newer

### Fixed

- Fix style
- Fix an issue with FastDecimateRepresentation crashing in C/S mode
- Fix the GUI toolbars bugs
- Fix an issue with FastDecimateRepresentation causing random segfaults

### Added

### Changed

- Use common repo for storing and mutualizing CMake scripts

- Use common repo for storing and mutualizing CI across Themys repositories

- Uniformize the formatting tools across the whole project.
    - .pre-commit-config.yaml file is identical to the one used in readers project
    - .clang-format file is identical to the one used in readers project (except some specificities of readers project)

- Factorizes CI jobs

## Version 1.0.6 - 2023-12-06

### Fixed

- Use run-clang-tidy-15 in accordance with the tools available in the Docker
  image used for the CI

### Added

- Add test for CEAEducational Filter

### Changed

- Do not install tools for code quality checks in the CI because
  they are already present in the Docker image

- Replace advanced mode by interface mode setting

## Version 1.0.5 - 2023-10-02

### Fixed

- Compilation without Qt

### Added

## Version 1.0.4 - 2023-09-25

### Changed

- The legacy build job in the CI uses gcc-8.

- Revert the launch of MPI tests in the CI
  (random crash due to X server connection problem).

- Fixes compilation failure due to the use of new VTK version
  where the Implicit Arrays are not a module anymore but are
  part of the CommonCore module.

- The CI now runs the MPI tests. Use saas-linux-large-amd64 machine type which
  offers 8 CPUs and 32 GB of RAM.

- The fragmentation filter is now part of this project.
  It is renamed `HyperTreeGridFragmentation`.

  Closes #44

- Apart from a maintenance effort (appropriation, comments,
  optimization and better consideration of ghost cells in the
  parallel case), the fragmentation filter has been enhanced
  with the ExtractType option allowing you to choose the type
  of fragment extraction:

  - none,
  - than the centers of the side cells;
  - centers of all cells.

  It should be noted that the extraction by the centers of the side
  cells of the fragments requires to have applied before the call
  of this filter a GhostCellsGenetaor filter. Otherwise, sides
  will be identified at the borders following the distribution on the servers.

  It is now possible to optionally output the global fields on the
  centers of the extraction cells via EnableOuputGlobalFields option.
  Using implicit arrays, the memory overhead of this activation is almost
  nil... except when a Save Data is triggered.

  A FormFactorRadius is now available allowing to define a
  spherical form factor for each fragment using the average center
  (the average of the position of the centers of the cells that make
  up each fragment) or the barycenter (the centers weighted by the mass,
  the all divided by the global mass). The barycenter is avalaible
  only if define a MassName ou DensityName.
  The choice for the centers definition prefer barycenter on average center.

  Closes #52

- The tests and baseline directories have been reorganized with a subdirectory
  for each filter tested.

- The default value of the "ZEpsilon" property is changed to 0.0001.

  Closes #41

### Fixed

- Fix major `cppcheck` warnings

- Avoid holes in the final mesh (Emmental aspect) when using the  `vtkMaterialInterface` filter,
  if the current PV server have a normal array with all components that are zero and
  other arrays (order and distance) have default values (order = -1 and distance = 0)

  Closes #37

### Added

- Iwyu is used for the `vtkHyperTreeGridFragmentationFilter`.

- Bug fixes and improvements of the `vtkHyperTreeGridGeometry` filter have been
  transferred into `VTK` sources.
  Adds tests on the `vtkHyperTreeGridGeometry` filter to check this transfer is ok.

  Closes #42

- Each test is labeled with the name of the directory it lay in.

- Adds a `CMake` target named `RunClangTidy` that launches `clang-tidy` analysis
  on the whole project. Also adds a job in the CI that uses this target.

- Adds a `CMake` target named `RunCppCheck` that launches `cppcheck` analysis
  on the whole project. Also adds a job in the CI that uses this target.

- Adds the `CEACellDataToPointData` filter. The goal of this filter is to compute
  a projection on an unstructured mesh that is in fact a partially defined structured
  mesh made of quadrangles (2D) or hexaedrons (3D). Missing part of the mesh
  are considered filled with a null volumic fraction.

  ┌───┬───┬───┬───┬───┐
  │   │   │   │   │   │
  ├───┼───┼───┼───┼───┤
  │   │   │   │   │   │
  ├───┼───┼───┼───┼───┤
  │   │   │0.5│0.2│   │
  └───┴───┴───┼───┼───┼───┬───┐
            X │0.4│   │   │   │
              ├───┼───┼───┼───┤
              │   │   │   │   │
              ├───┼───┼───┼───┤
              │   │   │   │   │
              └───┴───┴───┴───┘

  For example, the quantity computed on the node X will be
  (0.5 + 0.2 + 0.4 + 0) / 4 and not (0.5 + 0.2 + 0.4) / 3 as it would be with
  the standard CellDataToPointData filter.

  Closes #38
  Closes #39

- Adds HyperTree Grid Fragmentation filter parallel capabilities

  Closes #51

- Fix HyperTree Grid Ghost Cells Generator filter

  A option allows you to visualize the mesh with the ghost cells.

- Adds HyperTree Grid Generate Mask Leaves Cells

  Adds vtkValidCell field on the cells set to 1 if the cell is valid
  (leafed, unmasked and unghosted) otherwise 0 (coarse, masked or ghosted)

  Adds vtkVolume field to the cells

  These new fields are essential to calculate global fields on HTG.
  Thus the true calculation of global mass with Python Calculator filter becomes :
    sum(Mass * vtkValidCell).
  Thus the true calculation of average density with Python Calculator filter becomes :
    sum(Density * vtkVolume * vtkValidCell) / sum(vtkVolum * vtkValidCell)

## Version 1.0.3 - 2023-05-03

### Added

- Introduces the `vtkContourWriter` module that extract contours and write them
  down into a `.dat` file. Each block has its contour extracted and it forms a line
  described as a collection of points in the `.dat` file. Each collection of points
  is separated by a `&` character followed by a new line.
  This module works in parallel (C/S) mode.

  Closes #30

- Adds an educational plugin (`CEAEducationalFilter`) for custom filter.

### Fixed

- Fixed the CXX standard in MaterialFilters and adds build test with `gcc10` to
  ensure the CXX standard is correctly taken into account.

- The Themys's specific property `PipelineName` is replaced with the ParaView's
  propery `RegistrationName`.

- Fixes compilation error when using `clang-tidy`.

  Closes #34

- Avoid holes in the final mesh (Emmental aspect) if the current PV server
  does not have the interface distance array and other arrays (normal and order)
  have default values (normal = [0., 0., 0.] and order = -1).

  Closes #31

- Fixes the bug that make Themys crash when applying the `vtkMaterialInterface` filter
  on a data object that holds points arrays.

  Closes #33

### Removed

- The old filter that was used to extract contour (only in sequential mode) is
  removed because the `vtkContourWriter` replaces it.

## Version 1.0.2 - 2023-02-20

### Added

- Introduces storage settings for uses in the assistant

- In case the FillMaterial option is on, the output meshes have the same type
  as the input meshes

  Closes #27

### Fixed

- Fixes the way the types of the cells produced by the clip method are attributed

  Closes #25
  Closes #26

- Fixes the wrong values of the cell fields after applying the vtkMaterialInterface filter.

  Closes #28

- Reduces the amount of log messages.

  Closes #24

- Adds property settings to set the regex used for renaming a source in the pipeline

- Avoid holes in the final mesh (Emmental aspect) if the current PV server
  does not have any interface arrays.

  Closes #22

- Avoid SIGSEGV crashes when interface data are ill formed. Also checks the
  validity of the order array.

  Closes #23

## Version 1.0.1 - 2022-12-19

### Fixed

- Fixes the writer to dat files. This writer allows contour extraction on

  - MultiBlockDataSet
  - PolyData
  - UnstructuredGrid

  and writes the obtained contour into a file with `.dat` extension.

  To use it, just click on `File` -> `Save Data...` then select the field
  `CEA Writer DAT (Python)(*.dat)` in the `Files of type:` selection box.

  Closes #17

## Version 1.0.0 - 2022-11-25

### Fixed

- Fixes the transfer of the Field data through the vtkMaterialInterface filter.
  Closes #14

- Fixes the missing interface for some materials problem.
  The topology construction of the last material of the mixed cell was failing.
  Closes #12

## Version 0.3.3 - 2022-07-18
